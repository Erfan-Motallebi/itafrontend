/// <reference types="cypress" />
import { faker } from '@faker-js/faker';
import isps from "../fixtures/isps.json";

describe(`Login process`, () => {
    it('login to the admin dashboard', async () => {

        //! Login process using admin details
        cy.visit('/auth/login')
        cy.get("[name='v-field-1']").type('ita')
        cy.get("[name='v-field-2']").type('@Razhman@123')
        cy.get("button").should('have.class', 'button').click({ timeout: 5000 })
        console.log({ isps })
        //! Getting to admin setting to add more users
        cy.get('#open-settings').click()
        cy.get('.profile-body').contains('New Member').click()

        //! Making 1000 users for test
        //! Faking user details
        let username: string, firstname: string, lastname: string, position: string, password: string, userModuleAccess: string, userISPLimit: string = ''
        //*Creating a user profile 
        cy.readFile('cypress/fixtures/isps.json').then((isps) => {

            for (const { key } of isps) {
                username = faker.person.firstName("male")
                firstname = username
                lastname = faker.person.lastName("male")
                position = faker.person.jobTitle()
                password = "tester"
                userISPLimit = 'NetworkVisibility',
                userModuleAccess = key

                // @ts-ignore
                const [usernameKey, ...rest] = (userModuleAccess as string | undefined).split(" ")
                
                cy.get('.fieldset').find('input[id="username"]').type(`${username}${usernameKey}`)
                cy.get('.fieldset').find('input[id="firstname"]').type(firstname)
                cy.get('.fieldset').find('input[id="lastname"]').type(lastname)
                cy.get('.fieldset').find('input[id="position"]').type(position)
                cy.get('.fieldset').find('input[id="password"]').type(password)


                //* User access network limit / ISP choice

                //* User module access
                cy.get('[aria-placeholder="Select Access"]', { timeout: 10000 }).type(`${userISPLimit}{enter}`)
                //* User isp access
                cy.get('[aria-placeholder="Add ISP Access"]', { timeout: 10000 }).type(`${key}{enter}`)
                cy.get('.buttons').find('#submit').click()
                cy.url().should('include', '/app/settings/users/list')

                //* Assertions of created user
                cy.get('[data-hint="New User"]').click()
            }

        })






    })
})
