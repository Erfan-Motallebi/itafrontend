import { Socket, io } from "socket.io-client";

import { useUserSession } from "./src/stores/userSession";
import { type App } from "vue";


declare module '@vue/runtime-core' {
    interface ComponentCustomProperties {
        $socket: Socket;
    }
}
export default {
    install: function (app: App, pinia: any) {
        app.config.globalProperties.$socket = this.socket()
        // app.config.globalProperties.$socket.reconnect = () => {
        //     this.install(app, pinia)
        // }
    },
    socket(): Socket {
        console.log('socket made with', useUserSession().token)
        const userSession = useUserSession()
        if (!userSession)
            this.socket()
        let socket = io(import.meta.env.VITE_API_SOCKET_URL, { auth: this.newUserToken(), transports: ['websocket'], path: '/ita' })
        return socket
    },
    newUserToken() {
        return { token: useUserSession().token }
    }
}