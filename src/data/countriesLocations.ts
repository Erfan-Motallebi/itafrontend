export type CountryCoords = { lng: number, lat: number, fullName?: string };
export let countryLocations: { [key: string]: CountryCoords } = {
    "AF": {
        "lat": 33,
        "lng": 65,
        "fullName": "Afghanistan"
    },
    "AL": {
        "lat": 41,
        "lng": 20,
        "fullName": "Albania"
    },
    "DZ": {
        "lat": 28,
        "lng": 3,
        "fullName": "Algeria"
    },
    "AS": {
        "lat": -14.3333,
        "lng": -170,
        "fullName": "American Samoa"
    },
    "AD": {
        "lat": 42.5,
        "lng": 1.6,
        "fullName": "Andorra"
    },
    "AO": {
        "lat": -12.5,
        "lng": 18.5,
        "fullName": "Angola"
    },
    "AI": {
        "lat": 18.25,
        "lng": -63.1667,
        "fullName": "Anguilla"
    },
    "AQ": {
        "lat": -90,
        "lng": 0,
        "fullName": "Antarctica"
    },
    "AG": {
        "lat": 17.05,
        "lng": -61.8,
        "fullName": "Antigua and Barbuda"
    },
    "AR": {
        "lat": -34,
        "lng": -64,
        "fullName": "Argentina"
    },
    "AM": {
        "lat": 40,
        "lng": 45,
        "fullName": "Armenia"
    },
    "AW": {
        "lat": 12.5,
        "lng": -69.9667,
        "fullName": "Aruba"
    },
    "AU": {
        "lat": -27,
        "lng": 133,
        "fullName": "Australia"
    },
    "AT": {
        "lat": 47.3333,
        "lng": 13.3333,
        "fullName": "Austria"
    },
    "AZ": {
        "lat": 40.5,
        "lng": 47.5,
        "fullName": "Azerbaijan"
    },
    "BS": {
        "lat": 24.25,
        "lng": -76,
        "fullName": "Bahamas"
    },
    "BH": {
        "lat": 26,
        "lng": 50.55,
        "fullName": "Bahrain"
    },
    "BD": {
        "lat": 24,
        "lng": 90,
        "fullName": "Bangladesh"
    },
    "BB": {
        "lat": 13.1667,
        "lng": -59.5333,
        "fullName": "Barbados"
    },
    "BY": {
        "lat": 53,
        "lng": 28,
        "fullName": "Belarus"
    },
    "BE": {
        "lat": 50.8333,
        "lng": 4,
        "fullName": "Belgium"
    },
    "BZ": {
        "lat": 17.25,
        "lng": -88.75,
        "fullName": "Belize"
    },
    "BJ": {
        "lat": 9.5,
        "lng": 2.25,
        "fullName": "Benin"
    },
    "BM": {
        "lat": 32.3333,
        "lng": -64.75,
        "fullName": "Bermuda"
    },
    "BT": {
        "lat": 27.5,
        "lng": 90.5,
        "fullName": "Bhutan"
    },
    "BO": {
        "lat": -17,
        "lng": -65,
        "fullName": "Bolivia"
    },
    "BA": {
        "lat": 44,
        "lng": 18,
        "fullName": "Bosnia and Herzegovina"
    },
    "BW": {
        "lat": -22,
        "lng": 24,
        "fullName": "Botswana"
    },
    "BV": {
        "lat": -54.4333,
        "lng": 3.4,
        "fullName": "Bouvet Island"
    },
    "BR": {
        "lat": -10,
        "lng": -55,
        "fullName": "Brazil"
    },
    "IO": {
        "lat": -6,
        "lng": 71.5,
        "fullName": "British Indian Ocean Territory"
    },
    "BN": {
        "lat": 4.5,
        "lng": 114.6667,
        "fullName": "Brunei"
    },
    "BG": {
        "lat": 43,
        "lng": 25,
        "fullName": "Bulgaria"
    },
    "BF": {
        "lat": 13,
        "lng": -2,
        "fullName": "Burkina Faso"
    },
    "BI": {
        "lat": -3.5,
        "lng": 30,
        "fullName": "Burundi"
    },
    "KH": {
        "lat": 13,
        "lng": 105,
        "fullName": "Cambodia"
    },
    "CM": {
        "lat": 6,
        "lng": 12,
        "fullName": "Cameroon"
    },
    "CA": {
        "lat": 60,
        "lng": -95,
        "fullName": "Canada"
    },
    "CV": {
        "lat": 16,
        "lng": -24,
        "fullName": "Cape Verde"
    },
    "KY": {
        "lat": 19.5,
        "lng": -80.5,
        "fullName": "Cayman Islands"
    },
    "CF": {
        "lat": 7,
        "lng": 21,
        "fullName": "Central African Republic"
    },
    "TD": {
        "lat": 15,
        "lng": 19,
        "fullName": "Chad"
    },
    "CL": {
        "lat": -30,
        "lng": -71,
        "fullName": "Chile"
    },
    "CN": {
        "lat": 35,
        "lng": 105,
        "fullName": "China"
    },
    "CX": {
        "lat": -10.5,
        "lng": 105.6667,
        "fullName": "Christmas Island"
    },
    "CC": {
        "lat": -12.5,
        "lng": 96.8333,
        "fullName": "Cocos (Keeling) Islands"
    },
    "CO": {
        "lat": 4,
        "lng": -72,
        "fullName": "Colombia"
    },
    "KM": {
        "lat": -12.1667,
        "lng": 44.25,
        "fullName": "Comoros"
    },
    "CG": {
        "lat": -1,
        "lng": 15,
        "fullName": "Congo"
    },
    "CD": {
        "lat": 0,
        "lng": 25,
        "fullName": "Congo, the Democratic Republic of the"
    },
    "CK": {
        "lat": -21.2333,
        "lng": -159.7667,
        "fullName": "Cook Islands"
    },
    "CR": {
        "lat": 10,
        "lng": -84,
        "fullName": "Costa Rica"
    },
    "CI": {
        "lat": 8,
        "lng": -5,
        "fullName": "Ivory Coast"
    },
    "HR": {
        "lat": 45.1667,
        "lng": 15.5,
        "fullName": "Croatia"
    },
    "CU": {
        "lat": 21.5,
        "lng": -80,
        "fullName": "Cuba"
    },
    "CY": {
        "lat": 35,
        "lng": 33,
        "fullName": "Cyprus"
    },
    "CZ": {
        "lat": 49.75,
        "lng": 15.5,
        "fullName": "Czech Republic"
    },
    "DK": {
        "lat": 56,
        "lng": 10,
        "fullName": "Denmark"
    },
    "DJ": {
        "lat": 11.5,
        "lng": 43,
        "fullName": "Djibouti"
    },
    "DM": {
        "lat": 15.4167,
        "lng": -61.3333,
        "fullName": "Dominica"
    },
    "DO": {
        "lat": 19,
        "lng": -70.6667,
        "fullName": "Dominican Republic"
    },
    "EC": {
        "lat": -2,
        "lng": -77.5,
        "fullName": "Ecuador"
    },
    "EG": {
        "lat": 27,
        "lng": 30,
        "fullName": "Egypt"
    },
    "SV": {
        "lat": 13.8333,
        "lng": -88.9167,
        "fullName": "El Salvador"
    },
    "GQ": {
        "lat": 2,
        "lng": 10,
        "fullName": "Equatorial Guinea"
    },
    "ER": {
        "lat": 15,
        "lng": 39,
        "fullName": "Eritrea"
    },
    "EE": {
        "lat": 59,
        "lng": 26,
        "fullName": "Estonia"
    },
    "ET": {
        "lat": 8,
        "lng": 38,
        "fullName": "Ethiopia"
    },
    "FK": {
        "lat": -51.75,
        "lng": -59,
        "fullName": "Falkland Islands (Malvinas)"
    },
    "FO": {
        "lat": 62,
        "lng": -7,
        "fullName": "Faroe Islands"
    },
    "FJ": {
        "lat": -18,
        "lng": 175,
        "fullName": "Fiji"
    },
    "FI": {
        "lat": 64,
        "lng": 26,
        "fullName": "Finland"
    },
    "FR": {
        "lat": 46,
        "lng": 2,
        "fullName": "France"
    },
    "GF": {
        "lat": 4,
        "lng": -53,
        "fullName": "French Guiana"
    },
    "PF": {
        "lat": -15,
        "lng": -140,
        "fullName": "French Polynesia"
    },
    "TF": {
        "lat": -43,
        "lng": 67,
        "fullName": "French Southern Territories"
    },
    "GA": {
        "lat": -1,
        "lng": 11.75,
        "fullName": "Gabon"
    },
    "GM": {
        "lat": 13.4667,
        "lng": -16.5667,
        "fullName": "Gambia"
    },
    "GE": {
        "lat": 42,
        "lng": 43.5,
        "fullName": "Georgia"
    },
    "DE": {
        "lat": 51,
        "lng": 9,
        "fullName": "Germany"
    },
    "GH": {
        "lat": 8,
        "lng": -2,
        "fullName": "Ghana"
    },
    "GI": {
        "lat": 36.1833,
        "lng": -5.3667,
        "fullName": "Gibraltar"
    },
    "GR": {
        "lat": 39,
        "lng": 22,
        "fullName": "Greece"
    },
    "GL": {
        "lat": 72,
        "lng": -40,
        "fullName": "Greenland"
    },
    "GD": {
        "lat": 12.1167,
        "lng": -61.6667,
        "fullName": "Grenada"
    },
    "GP": {
        "lat": 16.25,
        "lng": -61.5833,
        "fullName": "Guadeloupe"
    },
    "GU": {
        "lat": 13.4667,
        "lng": 144.7833,
        "fullName": "Guam"
    },
    "GT": {
        "lat": 15.5,
        "lng": -90.25,
        "fullName": "Guatemala"
    },
    "GG": {
        "lat": 49.5,
        "lng": -2.56,
        "fullName": "Guernsey"
    },
    "GN": {
        "lat": 11,
        "lng": -10,
        "fullName": "Guinea"
    },
    "GW": {
        "lat": 12,
        "lng": -15,
        "fullName": "Guinea-Bissau"
    },
    "GY": {
        "lat": 5,
        "lng": -59,
        "fullName": "Guyana"
    },
    "HT": {
        "lat": 19,
        "lng": -72.4167,
        "fullName": "Haiti"
    },
    "HM": {
        "lat": -53.1,
        "lng": 72.5167,
        "fullName": "Heard Island and McDonald Islands"
    },
    "VA": {
        "lat": 41.9,
        "lng": 12.45,
        "fullName": "Holy See (Vatican City State)"
    },
    "HN": {
        "lat": 15,
        "lng": -86.5,
        "fullName": "Honduras"
    },
    "HK": {
        "lat": 22.25,
        "lng": 114.1667,
        "fullName": "Hong Kong"
    },
    "HU": {
        "lat": 47,
        "lng": 20,
        "fullName": "Hungary"
    },
    "IS": {
        "lat": 65,
        "lng": -18,
        "fullName": "Iceland"
    },
    "IN": {
        "lat": 20,
        "lng": 77,
        "fullName": "India"
    },
    "ID": {
        "lat": -5,
        "lng": 120,
        "fullName": "Indonesia"
    },
    "IR": {
        "lat": 32,
        "lng": 53,
        "fullName": "Iran, Islamic Republic of"
    },
    "IQ": {
        "lat": 33,
        "lng": 44,
        "fullName": "Iraq"
    },
    "IE": {
        "lat": 53,
        "lng": -8,
        "fullName": "Ireland"
    },
    "IM": {
        "lat": 54.23,
        "lng": -4.55,
        "fullName": "Isle of Man"
    },
    "IL": {
        "lat": 31.5,
        "lng": 34.75,
        "fullName": "Israel"
    },
    "IT": {
        "lat": 42.8333,
        "lng": 12.8333,
        "fullName": "Italy"
    },
    "JM": {
        "lat": 18.25,
        "lng": -77.5,
        "fullName": "Jamaica"
    },
    "JP": {
        "lat": 36,
        "lng": 138,
        "fullName": "Japan"
    },
    "JE": {
        "lat": 49.21,
        "lng": -2.13,
        "fullName": "Jersey"
    },
    "JO": {
        "lat": 31,
        "lng": 36,
        "fullName": "Jordan"
    },
    "KZ": {
        "lat": 48,
        "lng": 68,
        "fullName": "Kazakhstan"
    },
    "KE": {
        "lat": 1,
        "lng": 38,
        "fullName": "Kenya"
    },
    "KI": {
        "lat": 1.4167,
        "lng": 173,
        "fullName": "Kiribati"
    },
    "KP": {
        "lat": 40,
        "lng": 127,
        "fullName": "Korea, Democratic People's Republic of"
    },
    "KR": {
        "lat": 37,
        "lng": 127.5,
        "fullName": "South Korea"
    },
    "KW": {
        "lat": 29.3375,
        "lng": 47.6581,
        "fullName": "Kuwait"
    },
    "KG": {
        "lat": 41,
        "lng": 75,
        "fullName": "Kyrgyzstan"
    },
    "LA": {
        "lat": 18,
        "lng": 105,
        "fullName": "Lao People's Democratic Republic"
    },
    "LV": {
        "lat": 57,
        "lng": 25,
        "fullName": "Latvia"
    },
    "LB": {
        "lat": 33.8333,
        "lng": 35.8333,
        "fullName": "Lebanon"
    },
    "LS": {
        "lat": -29.5,
        "lng": 28.5,
        "fullName": "Lesotho"
    },
    "LR": {
        "lat": 6.5,
        "lng": -9.5,
        "fullName": "Liberia"
    },
    "LY": {
        "lat": 25,
        "lng": 17,
        "fullName": "Libya"
    },
    "LI": {
        "lat": 47.1667,
        "lng": 9.5333,
        "fullName": "Liechtenstein"
    },
    "LT": {
        "lat": 56,
        "lng": 24,
        "fullName": "Lithuania"
    },
    "LU": {
        "lat": 49.75,
        "lng": 6.1667,
        "fullName": "Luxembourg"
    },
    "MO": {
        "lat": 22.1667,
        "lng": 113.55,
        "fullName": "Macao"
    },
    "MK": {
        "lat": 41.8333,
        "lng": 22,
        "fullName": "Macedonia, the former Yugoslav Republic of"
    },
    "MG": {
        "lat": -20,
        "lng": 47,
        "fullName": "Madagascar"
    },
    "MW": {
        "lat": -13.5,
        "lng": 34,
        "fullName": "Malawi"
    },
    "MY": {
        "lat": 2.5,
        "lng": 112.5,
        "fullName": "Malaysia"
    },
    "MV": {
        "lat": 3.25,
        "lng": 73,
        "fullName": "Maldives"
    },
    "ML": {
        "lat": 17,
        "lng": -4,
        "fullName": "Mali"
    },
    "MT": {
        "lat": 35.8333,
        "lng": 14.5833,
        "fullName": "Malta"
    },
    "MH": {
        "lat": 9,
        "lng": 168,
        "fullName": "Marshall Islands"
    },
    "MQ": {
        "lat": 14.6667,
        "lng": -61,
        "fullName": "Martinique"
    },
    "MR": {
        "lat": 20,
        "lng": -12,
        "fullName": "Mauritania"
    },
    "MU": {
        "lat": -20.2833,
        "lng": 57.55,
        "fullName": "Mauritius"
    },
    "YT": {
        "lat": -12.8333,
        "lng": 45.1667,
        "fullName": "Mayotte"
    },
    "MX": {
        "lat": 23,
        "lng": -102,
        "fullName": "Mexico"
    },
    "FM": {
        "lat": 6.9167,
        "lng": 158.25,
        "fullName": "Micronesia, Federated States of"
    },
    "MD": {
        "lat": 47,
        "lng": 29,
        "fullName": "Moldova, Republic of"
    },
    "MC": {
        "lat": 43.7333,
        "lng": 7.4,
        "fullName": "Monaco"
    },
    "MN": {
        "lat": 46,
        "lng": 105,
        "fullName": "Mongolia"
    },
    "ME": {
        "lat": 42,
        "lng": 19,
        "fullName": "Montenegro"
    },
    "MS": {
        "lat": 16.75,
        "lng": -62.2,
        "fullName": "Montserrat"
    },
    "MA": {
        "lat": 32,
        "lng": -5,
        "fullName": "Morocco"
    },
    "MZ": {
        "lat": -18.25,
        "lng": 35,
        "fullName": "Mozambique"
    },
    "MM": {
        "lat": 22,
        "lng": 98,
        "fullName": "Burma"
    },
    "NA": {
        "lat": -22,
        "lng": 17,
        "fullName": "Namibia"
    },
    "NR": {
        "lat": -0.5333,
        "lng": 166.9167,
        "fullName": "Nauru"
    },
    "NP": {
        "lat": 28,
        "lng": 84,
        "fullName": "Nepal"
    },
    "NL": {
        "lat": 52.5,
        "lng": 5.75,
        "fullName": "Netherlands"
    },
    "AN": {
        "lat": 12.25,
        "lng": -68.75,
        "fullName": "Netherlands Antilles"
    },
    "NC": {
        "lat": -21.5,
        "lng": 165.5,
        "fullName": "New Caledonia"
    },
    "NZ": {
        "lat": -41,
        "lng": 174,
        "fullName": "New Zealand"
    },
    "NI": {
        "lat": 13,
        "lng": -85,
        "fullName": "Nicaragua"
    },
    "NE": {
        "lat": 16,
        "lng": 8,
        "fullName": "Niger"
    },
    "NG": {
        "lat": 10,
        "lng": 8,
        "fullName": "Nigeria"
    },
    "NU": {
        "lat": -19.0333,
        "lng": -169.8667,
        "fullName": "Niue"
    },
    "NF": {
        "lat": -29.0333,
        "lng": 167.95,
        "fullName": "Norfolk Island"
    },
    "MP": {
        "lat": 15.2,
        "lng": 145.75,
        "fullName": "Northern Mariana Islands"
    },
    "NO": {
        "lat": 62,
        "lng": 10,
        "fullName": "Norway"
    },
    "OM": {
        "lat": 21,
        "lng": 57,
        "fullName": "Oman"
    },
    "PK": {
        "lat": 30,
        "lng": 70,
        "fullName": "Pakistan"
    },
    "PW": {
        "lat": 7.5,
        "lng": 134.5,
        "fullName": "Palau"
    },
    "PS": {
        "lat": 32,
        "lng": 35.25,
        "fullName": "Palestinian Territory, Occupied"
    },
    "PA": {
        "lat": 9,
        "lng": -80,
        "fullName": "Panama"
    },
    "PG": {
        "lat": -6,
        "lng": 147,
        "fullName": "Papua New Guinea"
    },
    "PY": {
        "lat": -23,
        "lng": -58,
        "fullName": "Paraguay"
    },
    "PE": {
        "lat": -10,
        "lng": -76,
        "fullName": "Peru"
    },
    "PH": {
        "lat": 13,
        "lng": 122,
        "fullName": "Philippines"
    },
    "PN": {
        "lat": -24.7,
        "lng": -127.4,
        "fullName": "Pitcairn"
    },
    "PL": {
        "lat": 52,
        "lng": 20,
        "fullName": "Poland"
    },
    "PT": {
        "lat": 39.5,
        "lng": -8,
        "fullName": "Portugal"
    },
    "PR": {
        "lat": 18.25,
        "lng": -66.5,
        "fullName": "Puerto Rico"
    },
    "QA": {
        "lat": 25.5,
        "lng": 51.25,
        "fullName": "Qatar"
    },
    "RE": {
        "lat": -21.1,
        "lng": 55.6,
        "fullName": "Réunion"
    },
    "RO": {
        "lat": 46,
        "lng": 25,
        "fullName": "Romania"
    },
    "RU": {
        "lat": 60,
        "lng": 100,
        "fullName": "Russia"
    },
    "RW": {
        "lat": -2,
        "lng": 30,
        "fullName": "Rwanda"
    },
    "SH": {
        "lat": -15.9333,
        "lng": -5.7,
        "fullName": "Saint Helena, Ascension and Tristan da Cunha"
    },
    "KN": {
        "lat": 17.3333,
        "lng": -62.75,
        "fullName": "Saint Kitts and Nevis"
    },
    "LC": {
        "lat": 13.8833,
        "lng": -61.1333,
        "fullName": "Saint Lucia"
    },
    "PM": {
        "lat": 46.8333,
        "lng": -56.3333,
        "fullName": "Saint Pierre and Miquelon"
    },
    "VC": {
        "lat": 13.25,
        "lng": -61.2,
        "fullName": "St. Vincent and the Grenadines"
    },
    "WS": {
        "lat": -13.5833,
        "lng": -172.3333,
        "fullName": "Samoa"
    },
    "SM": {
        "lat": 43.7667,
        "lng": 12.4167,
        "fullName": "San Marino"
    },
    "ST": {
        "lat": 1,
        "lng": 7,
        "fullName": "Sao Tome and Principe"
    },
    "SA": {
        "lat": 25,
        "lng": 45,
        "fullName": "Saudi Arabia"
    },
    "SN": {
        "lat": 14,
        "lng": -14,
        "fullName": "Senegal"
    },
    "RS": {
        "lat": 44,
        "lng": 21,
        "fullName": "Serbia"
    },
    "SC": {
        "lat": -4.5833,
        "lng": 55.6667,
        "fullName": "Seychelles"
    },
    "SL": {
        "lat": 8.5,
        "lng": -11.5,
        "fullName": "Sierra Leone"
    },
    "SG": {
        "lat": 1.3667,
        "lng": 103.8,
        "fullName": "Singapore"
    },
    "SK": {
        "lat": 48.6667,
        "lng": 19.5,
        "fullName": "Slovakia"
    },
    "SI": {
        "lat": 46,
        "lng": 15,
        "fullName": "Slovenia"
    },
    "SB": {
        "lat": -8,
        "lng": 159,
        "fullName": "Solomon Islands"
    },
    "SO": {
        "lat": 10,
        "lng": 49,
        "fullName": "Somalia"
    },
    "ZA": {
        "lat": -29,
        "lng": 24,
        "fullName": "South Africa"
    },
    "GS": {
        "lat": -54.5,
        "lng": -37,
        "fullName": "South Georgia and the South Sandwich Islands"
    },
    "SS": {
        "lat": 8,
        "lng": 30,
        "fullName": "South Sudan"
    },
    "ES": {
        "lat": 40,
        "lng": -4,
        "fullName": "Spain"
    },
    "LK": {
        "lat": 7,
        "lng": 81,
        "fullName": "Sri Lanka"
    },
    "SD": {
        "lat": 15,
        "lng": 30,
        "fullName": "Sudan"
    },
    "SR": {
        "lat": 4,
        "lng": -56,
        "fullName": "Suriname"
    },
    "SJ": {
        "lat": 78,
        "lng": 20,
        "fullName": "Svalbard and Jan Mayen"
    },
    "SZ": {
        "lat": -26.5,
        "lng": 31.5,
        "fullName": "Swaziland"
    },
    "SE": {
        "lat": 62,
        "lng": 15,
        "fullName": "Sweden"
    },
    "CH": {
        "lat": 47,
        "lng": 8,
        "fullName": "Switzerland"
    },
    "SY": {
        "lat": 35,
        "lng": 38,
        "fullName": "Syrian Arab Republic"
    },
    "TW": {
        "lat": 23.5,
        "lng": 121,
        "fullName": "Taiwan"
    },
    "TJ": {
        "lat": 39,
        "lng": 71,
        "fullName": "Tajikistan"
    },
    "TZ": {
        "lat": -6,
        "lng": 35,
        "fullName": "Tanzania, United Republic of"
    },
    "TH": {
        "lat": 15,
        "lng": 100,
        "fullName": "Thailand"
    },
    "TL": {
        "lat": -8.55,
        "lng": 125.5167,
        "fullName": "Timor-Leste"
    },
    "TG": {
        "lat": 8,
        "lng": 1.1667,
        "fullName": "Togo"
    },
    "TK": {
        "lat": -9,
        "lng": -172,
        "fullName": "Tokelau"
    },
    "TO": {
        "lat": -20,
        "lng": -175,
        "fullName": "Tonga"
    },
    "TT": {
        "lat": 11,
        "lng": -61,
        "fullName": "Trinidad and Tobago"
    },
    "TN": {
        "lat": 34,
        "lng": 9,
        "fullName": "Tunisia"
    },
    "TR": {
        "lat": 39,
        "lng": 35,
        "fullName": "Turkey"
    },
    "TM": {
        "lat": 40,
        "lng": 60,
        "fullName": "Turkmenistan"
    },
    "TC": {
        "lat": 21.75,
        "lng": -71.5833,
        "fullName": "Turks and Caicos Islands"
    },
    "TV": {
        "lat": -8,
        "lng": 178,
        "fullName": "Tuvalu"
    },
    "UG": {
        "lat": 1,
        "lng": 32,
        "fullName": "Uganda"
    },
    "UA": {
        "lat": 49,
        "lng": 32,
        "fullName": "Ukraine"
    },
    "AE": {
        "lat": 24,
        "lng": 54,
        "fullName": "United Arab Emirates"
    },
    "GB": {
        "lat": 54,
        "lng": -2,
        "fullName": "United Kingdom"
    },
    "US": {
        "lat": 38,
        "lng": -97,
        "fullName": "United States"
    },
    "UM": {
        "lat": 19.2833,
        "lng": 166.6,
        "fullName": "United States Minor Outlying Islands"
    },
    "UY": {
        "lat": -33,
        "lng": -56,
        "fullName": "Uruguay"
    },
    "UZ": {
        "lat": 41,
        "lng": 64,
        "fullName": "Uzbekistan"
    },
    "VU": {
        "lat": -16,
        "lng": 167,
        "fullName": "Vanuatu"
    },
    "VE": {
        "lat": 8,
        "lng": -66,
        "fullName": "Venezuela"
    },
    "VN": {
        "lat": 16,
        "lng": 106,
        "fullName": "Vietnam"
    },
    "VG": {
        "lat": 18.5,
        "lng": -64.5,
        "fullName": "Virgin Islands, British"
    },
    "VI": {
        "lat": 18.3333,
        "lng": -64.8333,
        "fullName": "Virgin Islands, U.S."
    },
    "WF": {
        "lat": -13.3,
        "lng": -176.2,
        "fullName": "Wallis and Futuna"
    },
    "XK": {
        "lat": 42.5773,
        "lng": 20.8905,
        "fullName": "Kosovo"
    },
    "EH": {
        "lat": 24.5,
        "lng": -13,
        "fullName": "Western Sahara"
    },
    "YE": {
        "lat": 15,
        "lng": 48,
        "fullName": "Yemen"
    },
    "ZM": {
        "lat": -15,
        "lng": 30,
        "fullName": "Zambia"
    },
    "ZW": {
        "lat": -20,
        "lng": 30,
        "fullName": "Zimbabwe"
    }
}