export type Coordinate = [number, number]
export type Statistic = { key: string; count: number; percentage: number; color?: string }
export type APIStatisticsResponse = { total: number; data: Statistic[] }
export type ProtocolStatistics = {
    srcPorts: APIStatisticsResponse
    destPorts: APIStatisticsResponse
    srcIPs: APIStatisticsResponse
    destIPs: APIStatisticsResponse
    flags?: APIStatisticsResponse
    packetSize: {
        total: number
        '60t128': number
        '128t256': number
        '256t512': number
        '512t1024': number
        gt1024: number
    }
}
export type ProtocolStatsType = { [key: string]: ProtocolStatistics }