export let barChartOptions = {
    series: [{ name: '', data: [] }],
    dataLabels: {
        enabled: false
    },
    chart: {
        type: 'bar',
        toolbar: {
            show: false,
        },
        zoom: {
            enabled: true,
        },
    },
    responsive: [
        {
            breakpoint: 480,
            options: {
                legend: {
                    position: 'top',
                },
            },
        },
    ],
    plotOptions: {
        bar: {
            borderRadius: 7,
            borderRadiusApplication: 'auto',
            columnWidth: '50px',
        },
    },
    grid: {
        yaxis: {
            lines: {
                show: false
            },
        },
    },
    xaxis: {
        labels: {
            rotate: 90,
            top: -400,
        },
        // tickPlacement: 'on',
        categories: [],
    },
    legend: {
        position: 'top',
        horizontalAlign: 'center',
    },
    fill: {
        type: 'gradient',
        gradient: {
            shade: 'light',
            type: "horizontal",
            shadeIntensity: 0.25,
            gradientToColors: undefined,
            inverseColors: true,
            opacityFrom: 0.85,
            opacityTo: 0.85,
            stops: [50, 0, 100]
        },
    }
}
export let radialChartOptions = {
    series: [],
    chart: {
        height: 295,
        type: 'radialBar',
        toolbar: {
            show: false,
        },
    },
    colors: [
        'var(--danger)',
        'var(--info)',
        'var(--warning)',
    ],

    legend: {
        show: true,
        floating: true,
        fontSize: '16px',
        position: 'top',
        offsetY: -12,
        labels: {
            useSeriesColors: true,
        },
        markers: {
            size: 0
        },
        itemMargin: {
            vertical: 3
        }
    },
    plotOptions: {
        radialBar: {
            dataLabels: {
                name: {
                    show: true,
                    fontSize: '22px',
                },
                value: {
                    show: true,
                    fontSize: '16px',
                    formatter: (r: number) => r + ' عدد'
                },
                total: {
                    show: true,
                    color: 'var(--text-color)',
                    label: 'جمع کل',
                    formatter: (r: number) => r + ' عدد'
                },
            },
            startAngle: -135,
            endAngle: 140,
            hollow: {
                margin: 5,
                size: '30%',
                position: 'front',
                dropShadow: {
                    enabled: true,
                    top: 3,
                    left: 0,
                    blur: 4,
                    opacity: 0.24
                }
            },
            track: {
                strokeWidth: '20%',
                margin: 15, // margin is in pixels
                // dropShadow: {
                //   enabled: true,
                //   top: -3,
                //   left: 0,
                //   blur: 4,
                //   opacity: 0.35,
                //   inset: true,
                // }
            },
        },
    },
    // fill: {
    //   type: 'gradient',
    //   gradient: {
    //     type: "vertical",
    //     // inverseColors: true,
    //     shadeIntensity: 1,
    //     opacityFrom: 1,
    //     opacityTo: 0,
    //     stops: [0, 300, 800]
    //   },
    // },
    stroke: {
        lineCap: 'round'
    },
    labels: [],
}