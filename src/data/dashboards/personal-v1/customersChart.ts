import { useThemeColors } from '/@src/composable/useThemeColors'

export function useCustomersCharts() {
  const themeColors = useThemeColors()

  const customersOptions = ref({
    series: [
      {
        name: 'UDP',
        data: [31, 40, 28, 51, 42, 109, 100],
      },
      {
        name: 'TCP',
        data: [11, 32, 45, 32, 34, 52, 41],
      },
    ],
    chart: {
      height: 295,
      type: 'area',
      toolbar: {
        show: false,
      },
    },
    colors: [themeColors.info, themeColors.purple],
    stroke: {
      width: 5,
    },
    grid: {
      clipMarkers: false,
      yaxis: {
        lines: {
          show: false
        },
      },
      xaxis: {
        labels: {
          style: {
            colors: '#cd0000'
          }
        }
      },
      padding: {
        right: -5,
        left: -5,
      },
    },
    title: {
      text: '',
      align: 'left',
    },
    legend: {
      position: 'top',
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      width: [4, 4],
      curve: 'smooth',
    },
    fill: {
      gradient: {
        enabled: true,
        opacityFrom: 0.5,
        opacityTo: 0
      }
    },
    yaxis: {
      show: false,
    },
    xaxis: {
      type: 'datetime',
      categories: [
        '2020-09-19T00:00:00.000Z',
        '2020-09-20T01:30:00.000Z',
        '2020-09-21T02:30:00.000Z',
        '2020-09-22T03:30:00.000Z',
        '2020-09-23T04:30:00.000Z',
        '2020-09-24T05:30:00.000Z',
        '2020-09-25T06:30:00.000Z',
      ],
      labels: {
        style: {
          color: '#cd0000'
        }
      }
    },
    tooltip: {
      x: {
        format: 'dd/MM/yy HH:mm',
      },
    },
  })

  return {
    customersOptions,
  }
}
