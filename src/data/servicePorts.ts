export type ServicePort = { name: string, port: number, protocol: string };
export let servicePorts: ServicePort[] = [
    {
        "protocol": "UDP",
        "name": "tcpmux",
        "port": 1
    },
    {
        "protocol": "UDP",
        "name": "compressnet",
        "port": 2
    },
    {
        "protocol": "UDP",
        "name": "compressnet",
        "port": 3
    },
    {
        "protocol": "UDP",
        "name": "rje",
        "port": 5
    },
    {
        "protocol": "UDP",
        "name": "echo",
        "port": 7
    },
    {
        "protocol": "UDP",
        "name": "discard",
        "port": 9
    },
    {
        "protocol": "UDP",
        "name": "systat",
        "port": 11
    },
    {
        "protocol": "UDP",
        "name": "daytime",
        "port": 13
    },
    {
        "protocol": "UDP",
        "name": "qotd",
        "port": 17
    },
    {
        "protocol": "UDP",
        "name": "msp",
        "port": 18
    },
    {
        "protocol": "UDP",
        "name": "chargen",
        "port": 19
    },
    {
        "protocol": "UDP",
        "name": "ftp-data",
        "port": 20
    },
    {
        "protocol": "UDP",
        "name": "ftp",
        "port": 21
    },
    {
        "protocol": "UDP",
        "name": "ssh",
        "port": 22
    },
    {
        "protocol": "UDP",
        "name": "telnet",
        "port": 23
    },
    {
        "protocol": "UDP",
        "name": "smtp",
        "port": 25
    },
    {
        "protocol": "UDP",
        "name": "nsw-fe",
        "port": 27
    },
    {
        "protocol": "UDP",
        "name": "msg-icp",
        "port": 29
    },
    {
        "protocol": "UDP",
        "name": "msg-auth",
        "port": 31
    },
    {
        "protocol": "UDP",
        "name": "dsp",
        "port": 33
    },
    {
        "protocol": "UDP",
        "name": "time",
        "port": 37
    },
    {
        "protocol": "UDP",
        "name": "rap",
        "port": 38
    },
    {
        "protocol": "UDP",
        "name": "rlp",
        "port": 39
    },
    {
        "protocol": "UDP",
        "name": "graphics",
        "port": 41
    },
    {
        "protocol": "UDP",
        "name": "nameserver",
        "port": 42
    },
    {
        "protocol": "UDP",
        "name": "nicname",
        "port": 43
    },
    {
        "protocol": "UDP",
        "name": "mpm-flags",
        "port": 44
    },
    {
        "protocol": "UDP",
        "name": "mpm",
        "port": 45
    },
    {
        "protocol": "UDP",
        "name": "mpm-snd",
        "port": 46
    },
    {
        "protocol": "UDP",
        "name": "ni-ftp",
        "port": 47
    },
    {
        "protocol": "UDP",
        "name": "auditd",
        "port": 48
    },
    {
        "protocol": "UDP",
        "name": "tacacs",
        "port": 49
    },
    {
        "protocol": "UDP",
        "name": "re-mail-ck",
        "port": 50
    },
    {
        "protocol": "UDP",
        "name": "la-maint",
        "port": 51
    },
    {
        "protocol": "UDP",
        "name": "xns-time",
        "port": 52
    },
    {
        "protocol": "UDP",
        "name": "domain",
        "port": 53
    },
    {
        "protocol": "UDP",
        "name": "xns-ch",
        "port": 54
    },
    {
        "protocol": "UDP",
        "name": "isi-gl",
        "port": 55
    },
    {
        "protocol": "UDP",
        "name": "xns-auth",
        "port": 56
    },
    {
        "protocol": "UDP",
        "name": "xns-mail",
        "port": 58
    },
    {
        "protocol": "UDP",
        "name": "ni-mail",
        "port": 61
    },
    {
        "protocol": "UDP",
        "name": "acas",
        "port": 62
    },
    {
        "protocol": "UDP",
        "name": "whois++",
        "port": 63
    },
    {
        "protocol": "UDP",
        "name": "covia",
        "port": 64
    },
    {
        "protocol": "UDP",
        "name": "tacacs-ds",
        "port": 65
    },
    {
        "protocol": "UDP",
        "name": "sql*net",
        "port": 66
    },
    {
        "protocol": "UDP",
        "name": "bootps",
        "port": 67
    },
    {
        "protocol": "UDP",
        "name": "bootpc",
        "port": 68
    },
    {
        "protocol": "UDP",
        "name": "tftp",
        "port": 69
    },
    {
        "protocol": "UDP",
        "name": "gopher",
        "port": 70
    },
    {
        "protocol": "UDP",
        "name": "netrjs-1",
        "port": 71
    },
    {
        "protocol": "UDP",
        "name": "netrjs-2",
        "port": 72
    },
    {
        "protocol": "UDP",
        "name": "netrjs-3",
        "port": 73
    },
    {
        "protocol": "UDP",
        "name": "netrjs-4",
        "port": 74
    },
    {
        "protocol": "UDP",
        "name": "deos",
        "port": 76
    },
    {
        "protocol": "UDP",
        "name": "vettcp",
        "port": 78
    },
    {
        "protocol": "UDP",
        "name": "finger",
        "port": 79
    },
    {
        "protocol": "UDP",
        "name": "http",
        "port": 80
    },
    {
        "protocol": "UDP",
        "name": "hosts2-ns",
        "port": 81
    },
    {
        "protocol": "UDP",
        "name": "xfer",
        "port": 82
    },
    {
        "protocol": "UDP",
        "name": "mit-ml-dev",
        "port": 83
    },
    {
        "protocol": "UDP",
        "name": "ctf",
        "port": 84
    },
    {
        "protocol": "UDP",
        "name": "mit-ml-dev",
        "port": 85
    },
    {
        "protocol": "UDP",
        "name": "mfcobol",
        "port": 86
    },
    {
        "protocol": "UDP",
        "name": "kerberos",
        "port": 88
    },
    {
        "protocol": "UDP",
        "name": "su-mit-tg",
        "port": 89
    },
    {
        "protocol": "UDP",
        "name": "dnsix",
        "port": 90
    },
    {
        "protocol": "UDP",
        "name": "mit-dov",
        "port": 91
    },
    {
        "protocol": "UDP",
        "name": "npp",
        "port": 92
    },
    {
        "protocol": "UDP",
        "name": "dcp",
        "port": 93
    },
    {
        "protocol": "UDP",
        "name": "objcall",
        "port": 94
    },
    {
        "protocol": "UDP",
        "name": "supdup",
        "port": 95
    },
    {
        "protocol": "UDP",
        "name": "dixie",
        "port": 96
    },
    {
        "protocol": "UDP",
        "name": "swift-rvf",
        "port": 97
    },
    {
        "protocol": "UDP",
        "name": "tacnews",
        "port": 98
    },
    {
        "protocol": "UDP",
        "name": "metagram",
        "port": 99
    },
    {
        "protocol": "UDP",
        "name": "hostname",
        "port": 101
    },
    {
        "protocol": "UDP",
        "name": "iso-tsap",
        "port": 102
    },
    {
        "protocol": "UDP",
        "name": "gppitnp",
        "port": 103
    },
    {
        "protocol": "UDP",
        "name": "acr-nema",
        "port": 104
    },
    {
        "protocol": "UDP",
        "name": "csnet-ns",
        "port": 105
    },
    {
        "protocol": "UDP",
        "name": "3com-tsmux",
        "port": 106
    },
    {
        "protocol": "UDP",
        "name": "rtelnet",
        "port": 107
    },
    {
        "protocol": "UDP",
        "name": "snagas",
        "port": 108
    },
    {
        "protocol": "UDP",
        "name": "pop2",
        "port": 109
    },
    {
        "protocol": "UDP",
        "name": "pop3",
        "port": 110
    },
    {
        "protocol": "UDP",
        "name": "sunrpc",
        "port": 111
    },
    {
        "protocol": "UDP",
        "name": "mcidas",
        "port": 112
    },
    {
        "protocol": "UDP",
        "name": "auth",
        "port": 113
    },
    {
        "protocol": "UDP",
        "name": "sftp",
        "port": 115
    },
    {
        "protocol": "UDP",
        "name": "ansanotify",
        "port": 116
    },
    {
        "protocol": "UDP",
        "name": "uucp-path",
        "port": 117
    },
    {
        "protocol": "UDP",
        "name": "sqlserv",
        "port": 118
    },
    {
        "protocol": "UDP",
        "name": "nntp",
        "port": 119
    },
    {
        "protocol": "UDP",
        "name": "cfdptkt",
        "port": 120
    },
    {
        "protocol": "UDP",
        "name": "erpc",
        "port": 121
    },
    {
        "protocol": "UDP",
        "name": "smakynet",
        "port": 122
    },
    {
        "protocol": "UDP",
        "name": "ntp",
        "port": 123
    },
    {
        "protocol": "UDP",
        "name": "ansatrader",
        "port": 124
    },
    {
        "protocol": "UDP",
        "name": "locus-map",
        "port": 125
    },
    {
        "protocol": "UDP",
        "name": "nxedit",
        "port": 126
    },
    {
        "protocol": "UDP",
        "name": "locus-con",
        "port": 127
    },
    {
        "protocol": "UDP",
        "name": "gss-xlicen",
        "port": 128
    },
    {
        "protocol": "UDP",
        "name": "pwdgen",
        "port": 129
    },
    {
        "protocol": "UDP",
        "name": "cisco-fna",
        "port": 130
    },
    {
        "protocol": "UDP",
        "name": "cisco-tna",
        "port": 131
    },
    {
        "protocol": "UDP",
        "name": "cisco-sys",
        "port": 132
    },
    {
        "protocol": "UDP",
        "name": "statsrv",
        "port": 133
    },
    {
        "protocol": "UDP",
        "name": "ingres-net",
        "port": 134
    },
    {
        "protocol": "UDP",
        "name": "epmap",
        "port": 135
    },
    {
        "protocol": "UDP",
        "name": "profile",
        "port": 136
    },
    {
        "protocol": "UDP",
        "name": "netbios-ns",
        "port": 137
    },
    {
        "protocol": "UDP",
        "name": "netbios-dgm",
        "port": 138
    },
    {
        "protocol": "UDP",
        "name": "netbios-ssn",
        "port": 139
    },
    {
        "protocol": "UDP",
        "name": "emfis-data",
        "port": 140
    },
    {
        "protocol": "UDP",
        "name": "emfis-cntl",
        "port": 141
    },
    {
        "protocol": "UDP",
        "name": "bl-idm",
        "port": 142
    },
    {
        "protocol": "UDP",
        "name": "imap",
        "port": 143
    },
    {
        "protocol": "UDP",
        "name": "uma",
        "port": 144
    },
    {
        "protocol": "UDP",
        "name": "uaac",
        "port": 145
    },
    {
        "protocol": "UDP",
        "name": "iso-tp0",
        "port": 146
    },
    {
        "protocol": "UDP",
        "name": "iso-ip",
        "port": 147
    },
    {
        "protocol": "UDP",
        "name": "jargon",
        "port": 148
    },
    {
        "protocol": "UDP",
        "name": "aed-512",
        "port": 149
    },
    {
        "protocol": "UDP",
        "name": "sql-net",
        "port": 150
    },
    {
        "protocol": "UDP",
        "name": "hems",
        "port": 151
    },
    {
        "protocol": "UDP",
        "name": "bftp",
        "port": 152
    },
    {
        "protocol": "UDP",
        "name": "sgmp",
        "port": 153
    },
    {
        "protocol": "UDP",
        "name": "netsc-prod",
        "port": 154
    },
    {
        "protocol": "UDP",
        "name": "netsc-dev",
        "port": 155
    },
    {
        "protocol": "UDP",
        "name": "sqlsrv",
        "port": 156
    },
    {
        "protocol": "UDP",
        "name": "knet-cmp",
        "port": 157
    },
    {
        "protocol": "UDP",
        "name": "pcmail-srv",
        "port": 158
    },
    {
        "protocol": "UDP",
        "name": "nss-routing",
        "port": 159
    },
    {
        "protocol": "UDP",
        "name": "sgmp-traps",
        "port": 160
    },
    {
        "protocol": "UDP",
        "name": "snmp",
        "port": 161
    },
    {
        "protocol": "UDP",
        "name": "snmptrap",
        "port": 162
    },
    {
        "protocol": "UDP",
        "name": "cmip-man",
        "port": 163
    },
    {
        "protocol": "UDP",
        "name": "cmip-agent",
        "port": 164
    },
    {
        "protocol": "UDP",
        "name": "xns-courier",
        "port": 165
    },
    {
        "protocol": "UDP",
        "name": "s-net",
        "port": 166
    },
    {
        "protocol": "UDP",
        "name": "namp",
        "port": 167
    },
    {
        "protocol": "UDP",
        "name": "rsvd",
        "port": 168
    },
    {
        "protocol": "UDP",
        "name": "send",
        "port": 169
    },
    {
        "protocol": "UDP",
        "name": "print-srv",
        "port": 170
    },
    {
        "protocol": "UDP",
        "name": "multiplex",
        "port": 171
    },
    {
        "protocol": "UDP",
        "name": "cl/1",
        "port": 172
    },
    {
        "protocol": "UDP",
        "name": "xyplex-mux",
        "port": 173
    },
    {
        "protocol": "UDP",
        "name": "mailq",
        "port": 174
    },
    {
        "protocol": "UDP",
        "name": "vmnet",
        "port": 175
    },
    {
        "protocol": "UDP",
        "name": "genrad-mux",
        "port": 176
    },
    {
        "protocol": "UDP",
        "name": "xdmcp",
        "port": 177
    },
    {
        "protocol": "UDP",
        "name": "nextstep",
        "port": 178
    },
    {
        "protocol": "UDP",
        "name": "bgp",
        "port": 179
    },
    {
        "protocol": "UDP",
        "name": "ris",
        "port": 180
    },
    {
        "protocol": "UDP",
        "name": "unify",
        "port": 181
    },
    {
        "protocol": "UDP",
        "name": "audit",
        "port": 182
    },
    {
        "protocol": "UDP",
        "name": "ocbinder",
        "port": 183
    },
    {
        "protocol": "UDP",
        "name": "ocserver",
        "port": 184
    },
    {
        "protocol": "UDP",
        "name": "remote-kis",
        "port": 185
    },
    {
        "protocol": "UDP",
        "name": "kis",
        "port": 186
    },
    {
        "protocol": "UDP",
        "name": "aci",
        "port": 187
    },
    {
        "protocol": "UDP",
        "name": "mumps",
        "port": 188
    },
    {
        "protocol": "UDP",
        "name": "qft",
        "port": 189
    },
    {
        "protocol": "UDP",
        "name": "gacp",
        "port": 190
    },
    {
        "protocol": "UDP",
        "name": "prospero",
        "port": 191
    },
    {
        "protocol": "UDP",
        "name": "osu-nms",
        "port": 192
    },
    {
        "protocol": "UDP",
        "name": "srmp",
        "port": 193
    },
    {
        "protocol": "UDP",
        "name": "irc",
        "port": 194
    },
    {
        "protocol": "UDP",
        "name": "dn6-nlm-aud",
        "port": 195
    },
    {
        "protocol": "UDP",
        "name": "dn6-smm-red",
        "port": 196
    },
    {
        "protocol": "UDP",
        "name": "dls",
        "port": 197
    },
    {
        "protocol": "UDP",
        "name": "dls-mon",
        "port": 198
    },
    {
        "protocol": "UDP",
        "name": "smux",
        "port": 199
    },
    {
        "protocol": "UDP",
        "name": "novastorbakcup",
        "port": 308
    },
    {
        "protocol": "UDP",
        "name": "entrusttime",
        "port": 309
    },
    {
        "protocol": "UDP",
        "name": "bhmds",
        "port": 310
    },
    {
        "protocol": "UDP",
        "name": "asip-webadmin",
        "port": 311
    },
    {
        "protocol": "UDP",
        "name": "vslmp",
        "port": 312
    },
    {
        "protocol": "UDP",
        "name": "magenta-logic",
        "port": 313
    },
    {
        "protocol": "UDP",
        "name": "opalis-robot",
        "port": 314
    },
    {
        "protocol": "UDP",
        "name": "dpsi",
        "port": 315
    },
    {
        "protocol": "UDP",
        "name": "decauth",
        "port": 316
    },
    {
        "protocol": "UDP",
        "name": "zannet",
        "port": 317
    },
    {
        "protocol": "UDP",
        "name": "pkix-timestamp",
        "port": 318
    },
    {
        "protocol": "UDP",
        "name": "ptp-event",
        "port": 319
    },
    {
        "protocol": "UDP",
        "name": "ptp-general",
        "port": 320
    },
    {
        "protocol": "UDP",
        "name": "pip",
        "port": 321
    },
    {
        "protocol": "UDP",
        "name": "rtsps",
        "port": 322
    },
    {
        "protocol": "UDP",
        "name": "texar",
        "port": 333
    },
    {
        "protocol": "UDP",
        "name": "pdap",
        "port": 344
    },
    {
        "protocol": "UDP",
        "name": "pawserv",
        "port": 345
    },
    {
        "protocol": "UDP",
        "name": "zserv",
        "port": 346
    },
    {
        "protocol": "UDP",
        "name": "fatserv",
        "port": 347
    },
    {
        "protocol": "UDP",
        "name": "csi-sgwp",
        "port": 348
    },
    {
        "protocol": "UDP",
        "name": "mftp",
        "port": 349
    },
    {
        "protocol": "UDP",
        "name": "matip-type-a",
        "port": 350
    },
    {
        "protocol": "UDP",
        "name": "matip-type-b",
        "port": 351
    },
    {
        "protocol": "UDP",
        "name": "dtag-ste-sb",
        "port": 352
    },
    {
        "protocol": "UDP",
        "name": "ndsauth",
        "port": 353
    },
    {
        "protocol": "UDP",
        "name": "bh611",
        "port": 354
    },
    {
        "protocol": "UDP",
        "name": "datex-asn",
        "port": 355
    },
    {
        "protocol": "UDP",
        "name": "cloanto-net-1",
        "port": 356
    },
    {
        "protocol": "UDP",
        "name": "bhevent",
        "port": 357
    },
    {
        "protocol": "UDP",
        "name": "shrinkwrap",
        "port": 358
    },
    {
        "protocol": "UDP",
        "name": "nsrmp",
        "port": 359
    },
    {
        "protocol": "UDP",
        "name": "scoi2odialog",
        "port": 360
    },
    {
        "protocol": "UDP",
        "name": "semantix",
        "port": 361
    },
    {
        "protocol": "UDP",
        "name": "srssend",
        "port": 362
    },
    {
        "protocol": "UDP",
        "name": "rsvp_tunnel",
        "port": 363
    },
    {
        "protocol": "UDP",
        "name": "aurora-cmgr",
        "port": 364
    },
    {
        "protocol": "UDP",
        "name": "dtk",
        "port": 365
    },
    {
        "protocol": "UDP",
        "name": "odmr",
        "port": 366
    },
    {
        "protocol": "UDP",
        "name": "mortgageware",
        "port": 367
    },
    {
        "protocol": "UDP",
        "name": "qbikgdp",
        "port": 368
    },
    {
        "protocol": "UDP",
        "name": "rpc2portmap",
        "port": 369
    },
    {
        "protocol": "UDP",
        "name": "codaauth2",
        "port": 370
    },
    {
        "protocol": "UDP",
        "name": "clearcase",
        "port": 371
    },
    {
        "protocol": "UDP",
        "name": "ulistproc",
        "port": 372
    },
    {
        "protocol": "UDP",
        "name": "legent-1",
        "port": 373
    },
    {
        "protocol": "UDP",
        "name": "legent-2",
        "port": 374
    },
    {
        "protocol": "UDP",
        "name": "hassle",
        "port": 375
    },
    {
        "protocol": "UDP",
        "name": "nip",
        "port": 376
    },
    {
        "protocol": "UDP",
        "name": "tnETOS",
        "port": 377
    },
    {
        "protocol": "UDP",
        "name": "dsETOS",
        "port": 378
    },
    {
        "protocol": "UDP",
        "name": "is99c",
        "port": 379
    },
    {
        "protocol": "UDP",
        "name": "is99s",
        "port": 380
    },
    {
        "protocol": "UDP",
        "name": "hp-collector",
        "port": 381
    },
    {
        "protocol": "UDP",
        "name": "hp-managed-node",
        "port": 382
    },
    {
        "protocol": "UDP",
        "name": "hp-alarm-mgr",
        "port": 383
    },
    {
        "protocol": "UDP",
        "name": "arns",
        "port": 384
    },
    {
        "protocol": "UDP",
        "name": "ibm-app",
        "port": 385
    },
    {
        "protocol": "UDP",
        "name": "asa",
        "port": 386
    },
    {
        "protocol": "UDP",
        "name": "aurp",
        "port": 387
    },
    {
        "protocol": "UDP",
        "name": "unidata-ldm",
        "port": 388
    },
    {
        "protocol": "UDP",
        "name": "ldap",
        "port": 389
    },
    {
        "protocol": "UDP",
        "name": "uis",
        "port": 390
    },
    {
        "protocol": "UDP",
        "name": "synotics-relay",
        "port": 391
    },
    {
        "protocol": "UDP",
        "name": "UDP",
        "port": 392
    },
    {
        "protocol": "UDP",
        "name": "meta5",
        "port": 393
    },
    {
        "protocol": "UDP",
        "name": "embl-ndt",
        "port": 394
    },
    {
        "protocol": "UDP",
        "name": "netcp",
        "port": 395
    },
    {
        "protocol": "UDP",
        "name": "netware-ip",
        "port": 396
    },
    {
        "protocol": "UDP",
        "name": "mptn",
        "port": 397
    },
    {
        "protocol": "UDP",
        "name": "kryptolan",
        "port": 398
    },
    {
        "protocol": "UDP",
        "name": "iso-tsap-c2",
        "port": 399
    },
    {
        "protocol": "UDP",
        "name": "work-sol",
        "port": 400
    },
    {
        "protocol": "UDP",
        "name": "ups",
        "port": 401
    },
    {
        "protocol": "UDP",
        "name": "genie",
        "port": 402
    },
    {
        "protocol": "UDP",
        "name": "decap",
        "port": 403
    },
    {
        "protocol": "UDP",
        "name": "nced",
        "port": 404
    },
    {
        "protocol": "UDP",
        "name": "ncld",
        "port": 405
    },
    {
        "protocol": "UDP",
        "name": "imsp",
        "port": 406
    },
    {
        "protocol": "UDP",
        "name": "timbuktu",
        "port": 407
    },
    {
        "protocol": "UDP",
        "name": "prm-sm",
        "port": 408
    },
    {
        "protocol": "UDP",
        "name": "prm-nm",
        "port": 409
    },
    {
        "protocol": "UDP",
        "name": "decladebug",
        "port": 410
    },
    {
        "protocol": "UDP",
        "name": "rmt",
        "port": 411
    },
    {
        "protocol": "UDP",
        "name": "synoptics-trap",
        "port": 412
    },
    {
        "protocol": "UDP",
        "name": "smsp",
        "port": 413
    },
    {
        "protocol": "UDP",
        "name": "infoseek",
        "port": 414
    },
    {
        "protocol": "UDP",
        "name": "bnet",
        "port": 415
    },
    {
        "protocol": "UDP",
        "name": "silverplatter",
        "port": 416
    },
    {
        "protocol": "UDP",
        "name": "onmux",
        "port": 417
    },
    {
        "protocol": "UDP",
        "name": "hyper-g",
        "port": 418
    },
    {
        "protocol": "UDP",
        "name": "ariel1",
        "port": 419
    },
    {
        "protocol": "UDP",
        "name": "smpte",
        "port": 420
    },
    {
        "protocol": "UDP",
        "name": "ariel2",
        "port": 421
    },
    {
        "protocol": "UDP",
        "name": "ariel3",
        "port": 422
    },
    {
        "protocol": "UDP",
        "name": "opc-job-start",
        "port": 423
    },
    {
        "protocol": "UDP",
        "name": "opc-job-track",
        "port": 424
    },
    {
        "protocol": "UDP",
        "name": "icad-el",
        "port": 425
    },
    {
        "protocol": "UDP",
        "name": "smartsdp",
        "port": 426
    },
    {
        "protocol": "UDP",
        "name": "svrloc",
        "port": 427
    },
    {
        "protocol": "UDP",
        "name": "ocs_cmu",
        "port": 428
    },
    {
        "protocol": "UDP",
        "name": "ocs_amu",
        "port": 429
    },
    {
        "protocol": "UDP",
        "name": "utmpsd",
        "port": 430
    },
    {
        "protocol": "UDP",
        "name": "utmpcd",
        "port": 431
    },
    {
        "protocol": "UDP",
        "name": "iasd",
        "port": 432
    },
    {
        "protocol": "UDP",
        "name": "nnsp",
        "port": 433
    },
    {
        "protocol": "UDP",
        "name": "mobileip-agent",
        "port": 434
    },
    {
        "protocol": "UDP",
        "name": "mobilip-mn",
        "port": 435
    },
    {
        "protocol": "UDP",
        "name": "dna-cml",
        "port": 436
    },
    {
        "protocol": "UDP",
        "name": "comscm",
        "port": 437
    },
    {
        "protocol": "UDP",
        "name": "dsfgw",
        "port": 438
    },
    {
        "protocol": "UDP",
        "name": "sgcp",
        "port": 440
    },
    {
        "protocol": "UDP",
        "name": "decvms-sysmgt",
        "port": 441
    },
    {
        "protocol": "UDP",
        "name": "cvc_hostd",
        "port": 442
    },
    {
        "protocol": "UDP",
        "name": "https",
        "port": 443
    },
    {
        "protocol": "UDP",
        "name": "snpp",
        "port": 444
    },
    {
        "protocol": "UDP",
        "name": "microsoft-ds",
        "port": 445
    },
    {
        "protocol": "UDP",
        "name": "ddm-rdb",
        "port": 446
    },
    {
        "protocol": "UDP",
        "name": "ddm-dfm",
        "port": 447
    },
    {
        "protocol": "UDP",
        "name": "ddm-ssl",
        "port": 448
    },
    {
        "protocol": "UDP",
        "name": "as-servermap",
        "port": 449
    },
    {
        "protocol": "UDP",
        "name": "tserver",
        "port": 450
    },
    {
        "protocol": "UDP",
        "name": "sfs-smp-net",
        "port": 451
    },
    {
        "protocol": "UDP",
        "name": "sfs-config",
        "port": 452
    },
    {
        "protocol": "UDP",
        "name": "creativeserver",
        "port": 453
    },
    {
        "protocol": "UDP",
        "name": "contentserver",
        "port": 454
    },
    {
        "protocol": "UDP",
        "name": "creativepartnr",
        "port": 455
    },
    {
        "protocol": "UDP",
        "name": "macon-udp",
        "port": 456
    },
    {
        "protocol": "UDP",
        "name": "scohelp",
        "port": 457
    },
    {
        "protocol": "UDP",
        "name": "appleqtc",
        "port": 458
    },
    {
        "protocol": "UDP",
        "name": "ampr-rcmd",
        "port": 459
    },
    {
        "protocol": "UDP",
        "name": "skronk",
        "port": 460
    },
    {
        "protocol": "UDP",
        "name": "datasurfsrv",
        "port": 461
    },
    {
        "protocol": "UDP",
        "name": "datasurfsrvsec",
        "port": 462
    },
    {
        "protocol": "UDP",
        "name": "alpes",
        "port": 463
    },
    {
        "protocol": "UDP",
        "name": "kpasswd",
        "port": 464
    },
    {
        "protocol": "UDP",
        "name": "igmpv3lite",
        "port": 465
    },
    {
        "protocol": "UDP",
        "name": "digital-vrc",
        "port": 466
    },
    {
        "protocol": "UDP",
        "name": "mylex-mapd",
        "port": 467
    },
    {
        "protocol": "UDP",
        "name": "photuris",
        "port": 468
    },
    {
        "protocol": "UDP",
        "name": "rcp",
        "port": 469
    },
    {
        "protocol": "UDP",
        "name": "scx-proxy",
        "port": 470
    },
    {
        "protocol": "UDP",
        "name": "mondex",
        "port": 471
    },
    {
        "protocol": "UDP",
        "name": "ljk-login",
        "port": 472
    },
    {
        "protocol": "UDP",
        "name": "hybrid-pop",
        "port": 473
    },
    {
        "protocol": "UDP",
        "name": "tn-tl-w2",
        "port": 474
    },
    {
        "protocol": "UDP",
        "name": "tcpnethaspsrv",
        "port": 475
    },
    {
        "protocol": "UDP",
        "name": "tn-tl-fd1",
        "port": 476
    },
    {
        "protocol": "UDP",
        "name": "ss7ns",
        "port": 477
    },
    {
        "protocol": "UDP",
        "name": "spsc",
        "port": 478
    },
    {
        "protocol": "UDP",
        "name": "iafserver",
        "port": 479
    },
    {
        "protocol": "UDP",
        "name": "iafdbase",
        "port": 480
    },
    {
        "protocol": "UDP",
        "name": "ph",
        "port": 481
    },
    {
        "protocol": "UDP",
        "name": "bgs-nsi",
        "port": 482
    },
    {
        "protocol": "UDP",
        "name": "ulpnet",
        "port": 483
    },
    {
        "protocol": "UDP",
        "name": "integra-sme",
        "port": 484
    },
    {
        "protocol": "UDP",
        "name": "powerburst",
        "port": 485
    },
    {
        "protocol": "UDP",
        "name": "avian",
        "port": 486
    },
    {
        "protocol": "UDP",
        "name": "saft",
        "port": 487
    },
    {
        "protocol": "UDP",
        "name": "gss-http",
        "port": 488
    },
    {
        "protocol": "UDP",
        "name": "nest-protocol",
        "port": 489
    },
    {
        "protocol": "UDP",
        "name": "micom-pfs",
        "port": 490
    },
    {
        "protocol": "UDP",
        "name": "go-login",
        "port": 491
    },
    {
        "protocol": "UDP",
        "name": "ticf-1",
        "port": 492
    },
    {
        "protocol": "UDP",
        "name": "ticf-2",
        "port": 493
    },
    {
        "protocol": "UDP",
        "name": "pov-ray",
        "port": 494
    },
    {
        "protocol": "UDP",
        "name": "intecourier",
        "port": 495
    },
    {
        "protocol": "UDP",
        "name": "pim-rp-disc",
        "port": 496
    },
    {
        "protocol": "UDP",
        "name": "dantz",
        "port": 497
    },
    {
        "protocol": "UDP",
        "name": "siam",
        "port": 498
    },
    {
        "protocol": "UDP",
        "name": "iso-ill",
        "port": 499
    },
    {
        "protocol": "UDP",
        "name": "isakmp",
        "port": 500
    },
    {
        "protocol": "UDP",
        "name": "stmf",
        "port": 501
    },
    {
        "protocol": "UDP",
        "name": "asa-appl-proto",
        "port": 502
    },
    {
        "protocol": "UDP",
        "name": "intrinsa",
        "port": 503
    },
    {
        "protocol": "UDP",
        "name": "citadel",
        "port": 504
    },
    {
        "protocol": "UDP",
        "name": "mailbox-lm",
        "port": 505
    },
    {
        "protocol": "UDP",
        "name": "ohimsrv",
        "port": 506
    },
    {
        "protocol": "UDP",
        "name": "crs",
        "port": 507
    },
    {
        "protocol": "UDP",
        "name": "xvttp",
        "port": 508
    },
    {
        "protocol": "UDP",
        "name": "snare",
        "port": 509
    },
    {
        "protocol": "UDP",
        "name": "fcp",
        "port": 510
    },
    {
        "protocol": "UDP",
        "name": "passgo",
        "port": 511
    },
    {
        "protocol": "UDP",
        "name": "comsat",
        "port": 512
    },
    {
        "protocol": "UDP",
        "name": "who",
        "port": 513
    },
    {
        "protocol": "UDP",
        "name": "syslog",
        "port": 514
    },
    {
        "protocol": "UDP",
        "name": "printer",
        "port": 515
    },
    {
        "protocol": "UDP",
        "name": "videotex",
        "port": 516
    },
    {
        "protocol": "UDP",
        "name": "talk",
        "port": 517
    },
    {
        "protocol": "UDP",
        "name": "ntalk",
        "port": 518
    },
    {
        "protocol": "UDP",
        "name": "utime",
        "port": 519
    },
    {
        "protocol": "UDP",
        "name": "router",
        "port": 520
    },
    {
        "protocol": "UDP",
        "name": "ripng",
        "port": 521
    },
    {
        "protocol": "UDP",
        "name": "ulp",
        "port": 522
    },
    {
        "protocol": "UDP",
        "name": "ibm-db2",
        "port": 523
    },
    {
        "protocol": "UDP",
        "name": "ncp",
        "port": 524
    },
    {
        "protocol": "UDP",
        "name": "timed",
        "port": 525
    },
    {
        "protocol": "UDP",
        "name": "tempo",
        "port": 526
    },
    {
        "protocol": "UDP",
        "name": "stx",
        "port": 527
    },
    {
        "protocol": "UDP",
        "name": "custix",
        "port": 528
    },
    {
        "protocol": "UDP",
        "name": "irc-serv",
        "port": 529
    },
    {
        "protocol": "UDP",
        "name": "courier",
        "port": 530
    },
    {
        "protocol": "UDP",
        "name": "conference",
        "port": 531
    },
    {
        "protocol": "UDP",
        "name": "netnews",
        "port": 532
    },
    {
        "protocol": "UDP",
        "name": "netwall",
        "port": 533
    },
    {
        "protocol": "UDP",
        "name": "mm-admin",
        "port": 534
    },
    {
        "protocol": "UDP",
        "name": "iiop",
        "port": 535
    },
    {
        "protocol": "UDP",
        "name": "opalis-rdv",
        "port": 536
    },
    {
        "protocol": "UDP",
        "name": "nmsp",
        "port": 537
    },
    {
        "protocol": "UDP",
        "name": "gdomap",
        "port": 538
    },
    {
        "protocol": "UDP",
        "name": "apertus-ldp",
        "port": 539
    },
    {
        "protocol": "UDP",
        "name": "uucp",
        "port": 540
    },
    {
        "protocol": "UDP",
        "name": "uucp-rlogin",
        "port": 541
    },
    {
        "protocol": "UDP",
        "name": "commerce",
        "port": 542
    },
    {
        "protocol": "UDP",
        "name": "klogin",
        "port": 543
    },
    {
        "protocol": "UDP",
        "name": "kshell",
        "port": 544
    },
    {
        "protocol": "UDP",
        "name": "appleqtcsrvr",
        "port": 545
    },
    {
        "protocol": "UDP",
        "name": "dhcpv6-client",
        "port": 546
    },
    {
        "protocol": "UDP",
        "name": "dhcpv6-server",
        "port": 547
    },
    {
        "protocol": "UDP",
        "name": "afpovertcp",
        "port": 548
    },
    {
        "protocol": "UDP",
        "name": "idfp",
        "port": 549
    },
    {
        "protocol": "UDP",
        "name": "new-rwho",
        "port": 550
    },
    {
        "protocol": "UDP",
        "name": "cybercash",
        "port": 551
    },
    {
        "protocol": "UDP",
        "name": "devshr-nts",
        "port": 552
    },
    {
        "protocol": "UDP",
        "name": "pirp",
        "port": 553
    },
    {
        "protocol": "UDP",
        "name": "rtsp",
        "port": 554
    },
    {
        "protocol": "UDP",
        "name": "dsf",
        "port": 555
    },
    {
        "protocol": "UDP",
        "name": "remotefs",
        "port": 556
    },
    {
        "protocol": "UDP",
        "name": "openvms-sysipc",
        "port": 557
    },
    {
        "protocol": "UDP",
        "name": "sdnskmp",
        "port": 558
    },
    {
        "protocol": "UDP",
        "name": "teedtap",
        "port": 559
    },
    {
        "protocol": "UDP",
        "name": "rmonitor",
        "port": 560
    },
    {
        "protocol": "UDP",
        "name": "monitor",
        "port": 561
    },
    {
        "protocol": "UDP",
        "name": "chshell",
        "port": 562
    },
    {
        "protocol": "UDP",
        "name": "nntps",
        "port": 563
    },
    {
        "protocol": "UDP",
        "name": "9pfs",
        "port": 564
    },
    {
        "protocol": "UDP",
        "name": "whoami",
        "port": 565
    },
    {
        "protocol": "UDP",
        "name": "streettalk",
        "port": 566
    },
    {
        "protocol": "UDP",
        "name": "banyan-rpc",
        "port": 567
    },
    {
        "protocol": "UDP",
        "name": "ms-shuttle",
        "port": 568
    },
    {
        "protocol": "UDP",
        "name": "ms-rome",
        "port": 569
    },
    {
        "protocol": "UDP",
        "name": "meter",
        "port": 570
    },
    {
        "protocol": "UDP",
        "name": "meter",
        "port": 571
    },
    {
        "protocol": "UDP",
        "name": "sonar",
        "port": 572
    },
    {
        "protocol": "UDP",
        "name": "banyan-vip",
        "port": 573
    },
    {
        "protocol": "UDP",
        "name": "ftp-agent",
        "port": 574
    },
    {
        "protocol": "UDP",
        "name": "vemmi",
        "port": 575
    },
    {
        "protocol": "UDP",
        "name": "ipcd",
        "port": 576
    },
    {
        "protocol": "UDP",
        "name": "vnas",
        "port": 577
    },
    {
        "protocol": "UDP",
        "name": "ipdd",
        "port": 578
    },
    {
        "protocol": "UDP",
        "name": "decbsrv",
        "port": 579
    },
    {
        "protocol": "UDP",
        "name": "sntp-heartbeat",
        "port": 580
    },
    {
        "protocol": "UDP",
        "name": "bdp",
        "port": 581
    },
    {
        "protocol": "UDP",
        "name": "scc-security",
        "port": 582
    },
    {
        "protocol": "UDP",
        "name": "philips-vc",
        "port": 583
    },
    {
        "protocol": "UDP",
        "name": "keyserver",
        "port": 584
    },
    {
        "protocol": "UDP",
        "name": "imap4-ssl",
        "port": 585
    },
    {
        "protocol": "UDP",
        "name": "password-chg",
        "port": 586
    },
    {
        "protocol": "UDP",
        "name": "submission",
        "port": 587
    },
    {
        "protocol": "UDP",
        "name": "cal",
        "port": 588
    },
    {
        "protocol": "UDP",
        "name": "eyelink",
        "port": 589
    },
    {
        "protocol": "UDP",
        "name": "tns-cml",
        "port": 590
    },
    {
        "protocol": "UDP",
        "name": "http-alt",
        "port": 591
    },
    {
        "protocol": "UDP",
        "name": "eudora-set",
        "port": 592
    },
    {
        "protocol": "UDP",
        "name": "http-rpc-epmap",
        "port": 593
    },
    {
        "protocol": "UDP",
        "name": "tpip",
        "port": 594
    },
    {
        "protocol": "UDP",
        "name": "cab-protocol",
        "port": 595
    },
    {
        "protocol": "UDP",
        "name": "smsd",
        "port": 596
    },
    {
        "protocol": "UDP",
        "name": "ptcnameservice",
        "port": 597
    },
    {
        "protocol": "UDP",
        "name": "sco-websrvrmg3",
        "port": 598
    },
    {
        "protocol": "UDP",
        "name": "acp",
        "port": 599
    },
    {
        "protocol": "UDP",
        "name": "ipcserver",
        "port": 600
    },
    {
        "protocol": "UDP",
        "name": "syslog-conn",
        "port": 601
    },
    {
        "protocol": "UDP",
        "name": "xmlrpc-beep",
        "port": 602
    },
    {
        "protocol": "UDP",
        "name": "idxp",
        "port": 603
    },
    {
        "protocol": "UDP",
        "name": "tunnel",
        "port": 604
    },
    {
        "protocol": "UDP",
        "name": "soap-beep",
        "port": 605
    },
    {
        "protocol": "UDP",
        "name": "urm",
        "port": 606
    },
    {
        "protocol": "UDP",
        "name": "nqs",
        "port": 607
    },
    {
        "protocol": "UDP",
        "name": "sift-uft",
        "port": 608
    },
    {
        "protocol": "UDP",
        "name": "npmp-trap",
        "port": 609
    },
    {
        "protocol": "UDP",
        "name": "npmp-local",
        "port": 610
    },
    {
        "protocol": "UDP",
        "name": "npmp-gui",
        "port": 611
    },
    {
        "protocol": "UDP",
        "name": "hmmp-ind",
        "port": 612
    },
    {
        "protocol": "UDP",
        "name": "hmmp-op",
        "port": 613
    },
    {
        "protocol": "UDP",
        "name": "sshell",
        "port": 614
    },
    {
        "protocol": "UDP",
        "name": "sco-inetmgr",
        "port": 615
    },
    {
        "protocol": "UDP",
        "name": "sco-sysmgr",
        "port": 616
    },
    {
        "protocol": "UDP",
        "name": "sco-dtmgr",
        "port": 617
    },
    {
        "protocol": "UDP",
        "name": "dei-icda",
        "port": 618
    },
    {
        "protocol": "UDP",
        "name": "compaq-evm",
        "port": 619
    },
    {
        "protocol": "UDP",
        "name": "sco-websrvrmgr",
        "port": 620
    },
    {
        "protocol": "UDP",
        "name": "escp-ip",
        "port": 621
    },
    {
        "protocol": "UDP",
        "name": "collaborator",
        "port": 622
    },
    {
        "protocol": "UDP",
        "name": "asf-rmcp",
        "port": 623
    },
    {
        "protocol": "UDP",
        "name": "cryptoadmin",
        "port": 624
    },
    {
        "protocol": "UDP",
        "name": "dec_dlm",
        "port": 625
    },
    {
        "protocol": "UDP",
        "name": "asia",
        "port": 626
    },
    {
        "protocol": "UDP",
        "name": "passgo-tivoli",
        "port": 627
    },
    {
        "protocol": "UDP",
        "name": "qmqp",
        "port": 628
    },
    {
        "protocol": "UDP",
        "name": "3com-amp3",
        "port": 629
    },
    {
        "protocol": "UDP",
        "name": "rda",
        "port": 630
    },
    {
        "protocol": "UDP",
        "name": "ipp",
        "port": 631
    },
    {
        "protocol": "UDP",
        "name": "bmpp",
        "port": 632
    },
    {
        "protocol": "UDP",
        "name": "servstat",
        "port": 633
    },
    {
        "protocol": "UDP",
        "name": "ginad",
        "port": 634
    },
    {
        "protocol": "UDP",
        "name": "rlzdbase",
        "port": 635
    },
    {
        "protocol": "UDP",
        "name": "ldaps",
        "port": 636
    },
    {
        "protocol": "UDP",
        "name": "lanserver",
        "port": 637
    },
    {
        "protocol": "UDP",
        "name": "mcns-sec",
        "port": 638
    },
    {
        "protocol": "UDP",
        "name": "msdp",
        "port": 639
    },
    {
        "protocol": "UDP",
        "name": "entrust-sps",
        "port": 640
    },
    {
        "protocol": "UDP",
        "name": "repcmd",
        "port": 641
    },
    {
        "protocol": "UDP",
        "name": "esro-emsdp",
        "port": 642
    },
    {
        "protocol": "UDP",
        "name": "sanity",
        "port": 643
    },
    {
        "protocol": "UDP",
        "name": "dwr",
        "port": 644
    },
    {
        "protocol": "UDP",
        "name": "pssc",
        "port": 645
    },
    {
        "protocol": "UDP",
        "name": "ldp",
        "port": 646
    },
    {
        "protocol": "UDP",
        "name": "dhcp-failover",
        "port": 647
    },
    {
        "protocol": "UDP",
        "name": "rrp",
        "port": 648
    },
    {
        "protocol": "UDP",
        "name": "cadview-3d",
        "port": 649
    },
    {
        "protocol": "UDP",
        "name": "obex",
        "port": 650
    },
    {
        "protocol": "UDP",
        "name": "ieee-mms",
        "port": 651
    },
    {
        "protocol": "UDP",
        "name": "hello-port",
        "port": 652
    },
    {
        "protocol": "UDP",
        "name": "repscmd",
        "port": 653
    },
    {
        "protocol": "UDP",
        "name": "aodv",
        "port": 654
    },
    {
        "protocol": "UDP",
        "name": "tinc",
        "port": 655
    },
    {
        "protocol": "UDP",
        "name": "spmp",
        "port": 656
    },
    {
        "protocol": "UDP",
        "name": "rmc",
        "port": 657
    },
    {
        "protocol": "UDP",
        "name": "tenfold",
        "port": 658
    },
    {
        "protocol": "UDP",
        "name": "mac-srvr-admin",
        "port": 660
    },
    {
        "protocol": "UDP",
        "name": "hap",
        "port": 661
    },
    {
        "protocol": "UDP",
        "name": "pftp",
        "port": 662
    },
    {
        "protocol": "UDP",
        "name": "purenoise",
        "port": 663
    },
    {
        "protocol": "UDP",
        "name": "asf-secure-rmcp",
        "port": 664
    },
    {
        "protocol": "UDP",
        "name": "sun-dr",
        "port": 665
    },
    {
        "protocol": "UDP",
        "name": "mdqs",
        "port": 666
    },
    {
        "protocol": "UDP",
        "name": "disclose",
        "port": 667
    },
    {
        "protocol": "UDP",
        "name": "mecomm",
        "port": 668
    },
    {
        "protocol": "UDP",
        "name": "meregister",
        "port": 669
    },
    {
        "protocol": "UDP",
        "name": "vacdsm-sws",
        "port": 670
    },
    {
        "protocol": "UDP",
        "name": "vacdsm-app",
        "port": 671
    },
    {
        "protocol": "UDP",
        "name": "vpps-qua",
        "port": 672
    },
    {
        "protocol": "UDP",
        "name": "cimplex",
        "port": 673
    },
    {
        "protocol": "UDP",
        "name": "acap",
        "port": 674
    },
    {
        "protocol": "UDP",
        "name": "dctp",
        "port": 675
    },
    {
        "protocol": "UDP",
        "name": "vpps-via",
        "port": 676
    },
    {
        "protocol": "UDP",
        "name": "vpp",
        "port": 677
    },
    {
        "protocol": "UDP",
        "name": "ggf-ncp",
        "port": 678
    },
    {
        "protocol": "UDP",
        "name": "mrm",
        "port": 679
    },
    {
        "protocol": "UDP",
        "name": "entrust-aaas",
        "port": 680
    },
    {
        "protocol": "UDP",
        "name": "entrust-aams",
        "port": 681
    },
    {
        "protocol": "UDP",
        "name": "xfr",
        "port": 682
    },
    {
        "protocol": "UDP",
        "name": "corba-iiop",
        "port": 683
    },
    {
        "protocol": "UDP",
        "name": "corba-iiop-ssl",
        "port": 684
    },
    {
        "protocol": "UDP",
        "name": "mdc-portmapper",
        "port": 685
    },
    {
        "protocol": "UDP",
        "name": "hcp-wismar",
        "port": 686
    },
    {
        "protocol": "UDP",
        "name": "asipregistry",
        "port": 687
    },
    {
        "protocol": "UDP",
        "name": "realm-rusd",
        "port": 688
    },
    {
        "protocol": "UDP",
        "name": "nmap",
        "port": 689
    },
    {
        "protocol": "UDP",
        "name": "vatp",
        "port": 690
    },
    {
        "protocol": "UDP",
        "name": "msexch-routing",
        "port": 691
    },
    {
        "protocol": "UDP",
        "name": "hyperwave-isp",
        "port": 692
    },
    {
        "protocol": "UDP",
        "name": "connendp",
        "port": 693
    },
    {
        "protocol": "UDP",
        "name": "ha-cluster",
        "port": 694
    },
    {
        "protocol": "UDP",
        "name": "ieee-mms-ssl",
        "port": 695
    },
    {
        "protocol": "UDP",
        "name": "rushd",
        "port": 696
    },
    {
        "protocol": "UDP",
        "name": "uuidgen",
        "port": 697
    },
    {
        "protocol": "UDP",
        "name": "olsr",
        "port": 698
    },
    {
        "protocol": "UDP",
        "name": "accessnetwork",
        "port": 699
    },
    {
        "protocol": "UDP",
        "name": "epp",
        "port": 700
    },
    {
        "protocol": "UDP",
        "name": "lmp",
        "port": 701
    },
    {
        "protocol": "UDP",
        "name": "iris-beep",
        "port": 702
    },
    {
        "protocol": "UDP",
        "name": "elcsd",
        "port": 704
    },
    {
        "protocol": "UDP",
        "name": "agentx",
        "port": 705
    },
    {
        "protocol": "UDP",
        "name": "silc",
        "port": 706
    },
    {
        "protocol": "UDP",
        "name": "borland-dsj",
        "port": 707
    },
    {
        "protocol": "UDP",
        "name": "entrust-kmsh",
        "port": 709
    },
    {
        "protocol": "UDP",
        "name": "entrust-ash",
        "port": 710
    },
    {
        "protocol": "UDP",
        "name": "cisco-tdp",
        "port": 711
    },
    {
        "protocol": "UDP",
        "name": "tbrpf",
        "port": 712
    },
    {
        "protocol": "UDP",
        "name": "netviewdm1",
        "port": 729
    },
    {
        "protocol": "UDP",
        "name": "netviewdm2",
        "port": 730
    },
    {
        "protocol": "UDP",
        "name": "netviewdm3",
        "port": 731
    },
    {
        "protocol": "UDP",
        "name": "netgw",
        "port": 741
    },
    {
        "protocol": "UDP",
        "name": "netrcs",
        "port": 742
    },
    {
        "protocol": "UDP",
        "name": "flexlm",
        "port": 744
    },
    {
        "protocol": "UDP",
        "name": "fujitsu-dev",
        "port": 747
    },
    {
        "protocol": "UDP",
        "name": "ris-cm",
        "port": 748
    },
    {
        "protocol": "UDP",
        "name": "kerberos-adm",
        "port": 749
    },
    {
        "protocol": "UDP",
        "name": "kerberos-iv",
        "port": 750
    },
    {
        "protocol": "UDP",
        "name": "pump",
        "port": 751
    },
    {
        "protocol": "UDP",
        "name": "qrh",
        "port": 752
    },
    {
        "protocol": "UDP",
        "name": "rrh",
        "port": 753
    },
    {
        "protocol": "UDP",
        "name": "tell",
        "port": 754
    },
    {
        "protocol": "UDP",
        "name": "nlogin",
        "port": 758
    },
    {
        "protocol": "UDP",
        "name": "con",
        "port": 759
    },
    {
        "protocol": "UDP",
        "name": "ns",
        "port": 760
    },
    {
        "protocol": "UDP",
        "name": "rxe",
        "port": 761
    },
    {
        "protocol": "UDP",
        "name": "quotad",
        "port": 762
    },
    {
        "protocol": "UDP",
        "name": "cycleserv",
        "port": 763
    },
    {
        "protocol": "UDP",
        "name": "omserv",
        "port": 764
    },
    {
        "protocol": "UDP",
        "name": "webster",
        "port": 765
    },
    {
        "protocol": "UDP",
        "name": "phonebook",
        "port": 767
    },
    {
        "protocol": "UDP",
        "name": "vid",
        "port": 769
    },
    {
        "protocol": "UDP",
        "name": "cadlock",
        "port": 770
    },
    {
        "protocol": "UDP",
        "name": "rtip",
        "port": 771
    },
    {
        "protocol": "UDP",
        "name": "cycleserv2",
        "port": 772
    },
    {
        "protocol": "UDP",
        "name": "notify",
        "port": 773
    },
    {
        "protocol": "UDP",
        "name": "acmaint_dbd",
        "port": 774
    },
    {
        "protocol": "UDP",
        "name": "acmaint_transd",
        "port": 775
    },
    {
        "protocol": "UDP",
        "name": "wpages",
        "port": 776
    },
    {
        "protocol": "UDP",
        "name": "multiling-http",
        "port": 777
    },
    {
        "protocol": "UDP",
        "name": "wpgs",
        "port": 780
    },
    {
        "protocol": "UDP",
        "name": "mdbs_daemon",
        "port": 800
    },
    {
        "protocol": "UDP",
        "name": "device",
        "port": 801
    },
    {
        "protocol": "UDP",
        "name": "fcp-udp",
        "port": 810
    },
    {
        "protocol": "UDP",
        "name": "itm-mcell-s",
        "port": 828
    },
    {
        "protocol": "UDP",
        "name": "pkix-3-ca-ra",
        "port": 829
    },
    {
        "protocol": "UDP",
        "name": "dhcp-failover2",
        "port": 847
    },
    {
        "protocol": "UDP",
        "name": "gdoi",
        "port": 848
    },
    {
        "protocol": "UDP",
        "name": "iscsi",
        "port": 860
    },
    {
        "protocol": "UDP",
        "name": "rsync",
        "port": 873
    },
    {
        "protocol": "UDP",
        "name": "iclcnet-locate",
        "port": 886
    },
    {
        "protocol": "UDP",
        "name": "iclcnet_svinfo",
        "port": 887
    },
    {
        "protocol": "UDP",
        "name": "accessbuilder",
        "port": 888
    },
    {
        "protocol": "UDP",
        "name": "omginitialrefs",
        "port": 900
    },
    {
        "protocol": "UDP",
        "name": "smpnameres",
        "port": 901
    },
    {
        "protocol": "UDP",
        "name": "ideafarm-chat",
        "port": 902
    },
    {
        "protocol": "UDP",
        "name": "ideafarm-catch",
        "port": 903
    },
    {
        "protocol": "UDP",
        "name": "kink",
        "port": 910
    },
    {
        "protocol": "UDP",
        "name": "xact-backup",
        "port": 911
    },
    {
        "protocol": "UDP",
        "name": "apex-mesh",
        "port": 912
    },
    {
        "protocol": "UDP",
        "name": "apex-edge",
        "port": 913
    },
    {
        "protocol": "UDP",
        "name": "ftps-data",
        "port": 989
    },
    {
        "protocol": "UDP",
        "name": "ftps",
        "port": 990
    },
    {
        "protocol": "UDP",
        "name": "nas",
        "port": 991
    },
    {
        "protocol": "UDP",
        "name": "telnets",
        "port": 992
    },
    {
        "protocol": "UDP",
        "name": "imaps",
        "port": 993
    },
    {
        "protocol": "UDP",
        "name": "ircs",
        "port": 994
    },
    {
        "protocol": "UDP",
        "name": "pop3s",
        "port": 995
    },
    {
        "protocol": "UDP",
        "name": "vsinet",
        "port": 996
    },
    {
        "protocol": "UDP",
        "name": "maitrd",
        "port": 997
    },
    {
        "protocol": "UDP",
        "name": "puparp",
        "port": 998
    },
    {
        "protocol": "UDP",
        "name": "applix",
        "port": 999
    },
    {
        "protocol": "UDP",
        "name": "cadlock2",
        "port": 1000
    },
    {
        "protocol": "UDP",
        "name": "surf",
        "port": 1010
    },
    {
        "protocol": "UDP",
        "name": "blackjack",
        "port": 1025
    },
    {
        "protocol": "UDP",
        "name": "cap",
        "port": 1026
    },
    {
        "protocol": "UDP",
        "name": "solid-mux",
        "port": 1029
    },
    {
        "protocol": "UDP",
        "name": "iad1",
        "port": 1030
    },
    {
        "protocol": "UDP",
        "name": "iad2",
        "port": 1031
    },
    {
        "protocol": "UDP",
        "name": "iad3",
        "port": 1032
    },
    {
        "protocol": "UDP",
        "name": "netinfo-local",
        "port": 1033
    },
    {
        "protocol": "UDP",
        "name": "activesync",
        "port": 1034
    },
    {
        "protocol": "UDP",
        "name": "mxxrlogin",
        "port": 1035
    },
    {
        "protocol": "UDP",
        "name": "nsstp",
        "port": 1036
    },
    {
        "protocol": "UDP",
        "name": "ams",
        "port": 1037
    },
    {
        "protocol": "UDP",
        "name": "mtqp",
        "port": 1038
    },
    {
        "protocol": "UDP",
        "name": "sbl",
        "port": 1039
    },
    {
        "protocol": "UDP",
        "name": "netarx",
        "port": 1040
    },
    {
        "protocol": "UDP",
        "name": "danf-ak2",
        "port": 1041
    },
    {
        "protocol": "UDP",
        "name": "afrog",
        "port": 1042
    },
    {
        "protocol": "UDP",
        "name": "boinc-client",
        "port": 1043
    },
    {
        "protocol": "UDP",
        "name": "dcutility",
        "port": 1044
    },
    {
        "protocol": "UDP",
        "name": "fpitp",
        "port": 1045
    },
    {
        "protocol": "UDP",
        "name": "wfremotertm",
        "port": 1046
    },
    {
        "protocol": "UDP",
        "name": "neod1",
        "port": 1047
    },
    {
        "protocol": "UDP",
        "name": "neod2",
        "port": 1048
    },
    {
        "protocol": "UDP",
        "name": "td-postman",
        "port": 1049
    },
    {
        "protocol": "UDP",
        "name": "cma",
        "port": 1050
    },
    {
        "protocol": "UDP",
        "name": "optima-vnet",
        "port": 1051
    },
    {
        "protocol": "UDP",
        "name": "ddt",
        "port": 1052
    },
    {
        "protocol": "UDP",
        "name": "remote-as",
        "port": 1053
    },
    {
        "protocol": "UDP",
        "name": "brvread",
        "port": 1054
    },
    {
        "protocol": "UDP",
        "name": "ansyslmd",
        "port": 1055
    },
    {
        "protocol": "UDP",
        "name": "vfo",
        "port": 1056
    },
    {
        "protocol": "UDP",
        "name": "startron",
        "port": 1057
    },
    {
        "protocol": "UDP",
        "name": "nim",
        "port": 1058
    },
    {
        "protocol": "UDP",
        "name": "nimreg",
        "port": 1059
    },
    {
        "protocol": "UDP",
        "name": "polestar",
        "port": 1060
    },
    {
        "protocol": "UDP",
        "name": "kiosk",
        "port": 1061
    },
    {
        "protocol": "UDP",
        "name": "veracity",
        "port": 1062
    },
    {
        "protocol": "UDP",
        "name": "kyoceranetdev",
        "port": 1063
    },
    {
        "protocol": "UDP",
        "name": "jstel",
        "port": 1064
    },
    {
        "protocol": "UDP",
        "name": "syscomlan",
        "port": 1065
    },
    {
        "protocol": "UDP",
        "name": "fpo-fns",
        "port": 1066
    },
    {
        "protocol": "UDP",
        "name": "instl_boots",
        "port": 1067
    },
    {
        "protocol": "UDP",
        "name": "instl_bootc",
        "port": 1068
    },
    {
        "protocol": "UDP",
        "name": "cognex-insight",
        "port": 1069
    },
    {
        "protocol": "UDP",
        "name": "gmrupdateserv",
        "port": 1070
    },
    {
        "protocol": "UDP",
        "name": "bsquare-voip",
        "port": 1071
    },
    {
        "protocol": "UDP",
        "name": "cardax",
        "port": 1072
    },
    {
        "protocol": "UDP",
        "name": "bridgecontrol",
        "port": 1073
    },
    {
        "protocol": "UDP",
        "name": "fastechnologlm",
        "port": 1074
    },
    {
        "protocol": "UDP",
        "name": "rdrmshc",
        "port": 1075
    },
    {
        "protocol": "UDP",
        "name": "dab-sti-c",
        "port": 1076
    },
    {
        "protocol": "UDP",
        "name": "imgames",
        "port": 1077
    },
    {
        "protocol": "UDP",
        "name": "avocent-proxy",
        "port": 1078
    },
    {
        "protocol": "UDP",
        "name": "asprovatalk",
        "port": 1079
    },
    {
        "protocol": "UDP",
        "name": "socks",
        "port": 1080
    },
    {
        "protocol": "UDP",
        "name": "pvuniwien",
        "port": 1081
    },
    {
        "protocol": "UDP",
        "name": "amt-esd-prot",
        "port": 1082
    },
    {
        "protocol": "UDP",
        "name": "ansoft-lm-1",
        "port": 1083
    },
    {
        "protocol": "UDP",
        "name": "ansoft-lm-2",
        "port": 1084
    },
    {
        "protocol": "UDP",
        "name": "webobjects",
        "port": 1085
    },
    {
        "protocol": "UDP",
        "name": "cplscrambler-lg",
        "port": 1086
    },
    {
        "protocol": "UDP",
        "name": "cplscrambler-in",
        "port": 1087
    },
    {
        "protocol": "UDP",
        "name": "cplscrambler-al",
        "port": 1088
    },
    {
        "protocol": "UDP",
        "name": "ff-annunc",
        "port": 1089
    },
    {
        "protocol": "UDP",
        "name": "ff-fms",
        "port": 1090
    },
    {
        "protocol": "UDP",
        "name": "ff-sm",
        "port": 1091
    },
    {
        "protocol": "UDP",
        "name": "obrpd",
        "port": 1092
    },
    {
        "protocol": "UDP",
        "name": "proofd",
        "port": 1093
    },
    {
        "protocol": "UDP",
        "name": "rootd",
        "port": 1094
    },
    {
        "protocol": "UDP",
        "name": "nicelink",
        "port": 1095
    },
    {
        "protocol": "UDP",
        "name": "cnrprotocol",
        "port": 1096
    },
    {
        "protocol": "UDP",
        "name": "sunclustermgr",
        "port": 1097
    },
    {
        "protocol": "UDP",
        "name": "rmiactivation",
        "port": 1098
    },
    {
        "protocol": "UDP",
        "name": "rmiregistry",
        "port": 1099
    },
    {
        "protocol": "UDP",
        "name": "mctp",
        "port": 1100
    },
    {
        "protocol": "UDP",
        "name": "pt2-discover",
        "port": 1101
    },
    {
        "protocol": "UDP",
        "name": "adobeserver-1",
        "port": 1102
    },
    {
        "protocol": "UDP",
        "name": "adobeserver-2",
        "port": 1103
    },
    {
        "protocol": "UDP",
        "name": "xrl",
        "port": 1104
    },
    {
        "protocol": "UDP",
        "name": "ftranhc",
        "port": 1105
    },
    {
        "protocol": "UDP",
        "name": "isoipsigport-1",
        "port": 1106
    },
    {
        "protocol": "UDP",
        "name": "isoipsigport-2",
        "port": 1107
    },
    {
        "protocol": "UDP",
        "name": "ratio-adp",
        "port": 1108
    },
    {
        "protocol": "UDP",
        "name": "nfsd-keepalive",
        "port": 1110
    },
    {
        "protocol": "UDP",
        "name": "lmsocialserver",
        "port": 1111
    },
    {
        "protocol": "UDP",
        "name": "icp",
        "port": 1112
    },
    {
        "protocol": "UDP",
        "name": "ltp-deepspace",
        "port": 1113
    },
    {
        "protocol": "UDP",
        "name": "mini-sql",
        "port": 1114
    },
    {
        "protocol": "UDP",
        "name": "ardus-trns",
        "port": 1115
    },
    {
        "protocol": "UDP",
        "name": "ardus-cntl",
        "port": 1116
    },
    {
        "protocol": "UDP",
        "name": "ardus-mtrns",
        "port": 1117
    },
    {
        "protocol": "UDP",
        "name": "sacred",
        "port": 1118
    },
    {
        "protocol": "UDP",
        "name": "bnetgame",
        "port": 1119
    },
    {
        "protocol": "UDP",
        "name": "bnetfile",
        "port": 1120
    },
    {
        "protocol": "UDP",
        "name": "rmpp",
        "port": 1121
    },
    {
        "protocol": "UDP",
        "name": "availant-mgr",
        "port": 1122
    },
    {
        "protocol": "UDP",
        "name": "murray",
        "port": 1123
    },
    {
        "protocol": "UDP",
        "name": "hpvmmcontrol",
        "port": 1124
    },
    {
        "protocol": "UDP",
        "name": "hpvmmagent",
        "port": 1125
    },
    {
        "protocol": "UDP",
        "name": "hpvmmdata",
        "port": 1126
    },
    {
        "protocol": "UDP",
        "name": "kwdb-commn",
        "port": 1127
    },
    {
        "protocol": "UDP",
        "name": "casp",
        "port": 1130
    },
    {
        "protocol": "UDP",
        "name": "caspssl",
        "port": 1131
    },
    {
        "protocol": "UDP",
        "name": "kvm-via-ip",
        "port": 1132
    },
    {
        "protocol": "UDP",
        "name": "dfn",
        "port": 1133
    },
    {
        "protocol": "UDP",
        "name": "aplx",
        "port": 1134
    },
    {
        "protocol": "UDP",
        "name": "omnivision",
        "port": 1135
    },
    {
        "protocol": "UDP",
        "name": "hhb-gateway",
        "port": 1136
    },
    {
        "protocol": "UDP",
        "name": "trim",
        "port": 1137
    },
    {
        "protocol": "UDP",
        "name": "autonoc",
        "port": 1140
    },
    {
        "protocol": "UDP",
        "name": "mxomss",
        "port": 1141
    },
    {
        "protocol": "UDP",
        "name": "edtools",
        "port": 1142
    },
    {
        "protocol": "UDP",
        "name": "fuscript",
        "port": 1144
    },
    {
        "protocol": "UDP",
        "name": "x9-icue",
        "port": 1145
    },
    {
        "protocol": "UDP",
        "name": "audit-transfer",
        "port": 1146
    },
    {
        "protocol": "UDP",
        "name": "capioverlan",
        "port": 1147
    },
    {
        "protocol": "UDP",
        "name": "elfiq-repl",
        "port": 1148
    },
    {
        "protocol": "UDP",
        "name": "bvtsonar",
        "port": 1149
    },
    {
        "protocol": "UDP",
        "name": "blaze",
        "port": 1150
    },
    {
        "protocol": "UDP",
        "name": "unizensus",
        "port": 1151
    },
    {
        "protocol": "UDP",
        "name": "winpoplanmess",
        "port": 1152
    },
    {
        "protocol": "UDP",
        "name": "c1222-acse",
        "port": 1153
    },
    {
        "protocol": "UDP",
        "name": "resacommunity",
        "port": 1154
    },
    {
        "protocol": "UDP",
        "name": "nfa",
        "port": 1155
    },
    {
        "protocol": "UDP",
        "name": "iascontrol-oms",
        "port": 1156
    },
    {
        "protocol": "UDP",
        "name": "iascontrol",
        "port": 1157
    },
    {
        "protocol": "UDP",
        "name": "dbcontrol-oms",
        "port": 1158
    },
    {
        "protocol": "UDP",
        "name": "oracle-oms",
        "port": 1159
    },
    {
        "protocol": "UDP",
        "name": "olsv",
        "port": 1160
    },
    {
        "protocol": "UDP",
        "name": "health-polling",
        "port": 1161
    },
    {
        "protocol": "UDP",
        "name": "health-trap",
        "port": 1162
    },
    {
        "protocol": "UDP",
        "name": "sddp",
        "port": 1163
    },
    {
        "protocol": "UDP",
        "name": "qsm-proxy",
        "port": 1164
    },
    {
        "protocol": "UDP",
        "name": "qsm-gui",
        "port": 1165
    },
    {
        "protocol": "UDP",
        "name": "qsm-remote",
        "port": 1166
    },
    {
        "protocol": "UDP",
        "name": "vchat",
        "port": 1168
    },
    {
        "protocol": "UDP",
        "name": "tripwire",
        "port": 1169
    },
    {
        "protocol": "UDP",
        "name": "atc-lm",
        "port": 1170
    },
    {
        "protocol": "UDP",
        "name": "atc-appserver",
        "port": 1171
    },
    {
        "protocol": "UDP",
        "name": "dnap",
        "port": 1172
    },
    {
        "protocol": "UDP",
        "name": "d-cinema-rrp",
        "port": 1173
    },
    {
        "protocol": "UDP",
        "name": "fnet-remote-ui",
        "port": 1174
    },
    {
        "protocol": "UDP",
        "name": "dossier",
        "port": 1175
    },
    {
        "protocol": "UDP",
        "name": "indigo-server",
        "port": 1176
    },
    {
        "protocol": "UDP",
        "name": "dkmessenger",
        "port": 1177
    },
    {
        "protocol": "UDP",
        "name": "sgi-storman",
        "port": 1178
    },
    {
        "protocol": "UDP",
        "name": "b2n",
        "port": 1179
    },
    {
        "protocol": "UDP",
        "name": "mc-client",
        "port": 1180
    },
    {
        "protocol": "UDP",
        "name": "3comnetman",
        "port": 1181
    },
    {
        "protocol": "UDP",
        "name": "accelenet",
        "port": 1182
    },
    {
        "protocol": "UDP",
        "name": "llsurfup-http",
        "port": 1183
    },
    {
        "protocol": "UDP",
        "name": "llsurfup-https",
        "port": 1184
    },
    {
        "protocol": "UDP",
        "name": "catchpole",
        "port": 1185
    },
    {
        "protocol": "UDP",
        "name": "mysql-cluster",
        "port": 1186
    },
    {
        "protocol": "UDP",
        "name": "alias",
        "port": 1187
    },
    {
        "protocol": "UDP",
        "name": "hp-webadmin",
        "port": 1188
    },
    {
        "protocol": "UDP",
        "name": "unet",
        "port": 1189
    },
    {
        "protocol": "UDP",
        "name": "commlinx-avl",
        "port": 1190
    },
    {
        "protocol": "UDP",
        "name": "gpfs",
        "port": 1191
    },
    {
        "protocol": "UDP",
        "name": "caids-sensor",
        "port": 1192
    },
    {
        "protocol": "UDP",
        "name": "fiveacross",
        "port": 1193
    },
    {
        "protocol": "UDP",
        "name": "openvpn",
        "port": 1194
    },
    {
        "protocol": "UDP",
        "name": "rsf-1",
        "port": 1195
    },
    {
        "protocol": "UDP",
        "name": "netmagic",
        "port": 1196
    },
    {
        "protocol": "UDP",
        "name": "carrius-rshell",
        "port": 1197
    },
    {
        "protocol": "UDP",
        "name": "cajo-discovery",
        "port": 1198
    },
    {
        "protocol": "UDP",
        "name": "dmidi",
        "port": 1199
    },
    {
        "protocol": "UDP",
        "name": "scol",
        "port": 1200
    },
    {
        "protocol": "UDP",
        "name": "nucleus-sand",
        "port": 1201
    },
    {
        "protocol": "UDP",
        "name": "caiccipc",
        "port": 1202
    },
    {
        "protocol": "UDP",
        "name": "ssslic-mgr",
        "port": 1203
    },
    {
        "protocol": "UDP",
        "name": "ssslog-mgr",
        "port": 1204
    },
    {
        "protocol": "UDP",
        "name": "accord-mgc",
        "port": 1205
    },
    {
        "protocol": "UDP",
        "name": "anthony-data",
        "port": 1206
    },
    {
        "protocol": "UDP",
        "name": "metasage",
        "port": 1207
    },
    {
        "protocol": "UDP",
        "name": "seagull-ais",
        "port": 1208
    },
    {
        "protocol": "UDP",
        "name": "ipcd3",
        "port": 1209
    },
    {
        "protocol": "UDP",
        "name": "eoss",
        "port": 1210
    },
    {
        "protocol": "UDP",
        "name": "groove-dpp",
        "port": 1211
    },
    {
        "protocol": "UDP",
        "name": "lupa",
        "port": 1212
    },
    {
        "protocol": "UDP",
        "name": "mpc-lifenet",
        "port": 1213
    },
    {
        "protocol": "UDP",
        "name": "kazaa",
        "port": 1214
    },
    {
        "protocol": "UDP",
        "name": "scanstat-1",
        "port": 1215
    },
    {
        "protocol": "UDP",
        "name": "etebac5",
        "port": 1216
    },
    {
        "protocol": "UDP",
        "name": "hpss-ndapi",
        "port": 1217
    },
    {
        "protocol": "UDP",
        "name": "aeroflight-ads",
        "port": 1218
    },
    {
        "protocol": "UDP",
        "name": "aeroflight-ret",
        "port": 1219
    },
    {
        "protocol": "UDP",
        "name": "qt-serveradmin",
        "port": 1220
    },
    {
        "protocol": "UDP",
        "name": "sweetware-apps",
        "port": 1221
    },
    {
        "protocol": "UDP",
        "name": "nerv",
        "port": 1222
    },
    {
        "protocol": "UDP",
        "name": "tgp",
        "port": 1223
    },
    {
        "protocol": "UDP",
        "name": "vpnz",
        "port": 1224
    },
    {
        "protocol": "UDP",
        "name": "slinkysearch",
        "port": 1225
    },
    {
        "protocol": "UDP",
        "name": "stgxfws",
        "port": 1226
    },
    {
        "protocol": "UDP",
        "name": "dns2go",
        "port": 1227
    },
    {
        "protocol": "UDP",
        "name": "florence",
        "port": 1228
    },
    {
        "protocol": "UDP",
        "name": "novell-zfs",
        "port": 1229
    },
    {
        "protocol": "UDP",
        "name": "periscope",
        "port": 1230
    },
    {
        "protocol": "UDP",
        "name": "menandmice-lpm",
        "port": 1231
    },
    {
        "protocol": "UDP",
        "name": "univ-appserver",
        "port": 1233
    },
    {
        "protocol": "UDP",
        "name": "search-agent",
        "port": 1234
    },
    {
        "protocol": "UDP",
        "name": "mosaicsyssvc1",
        "port": 1235
    },
    {
        "protocol": "UDP",
        "name": "bvcontrol",
        "port": 1236
    },
    {
        "protocol": "UDP",
        "name": "tsdos390",
        "port": 1237
    },
    {
        "protocol": "UDP",
        "name": "hacl-qs",
        "port": 1238
    },
    {
        "protocol": "UDP",
        "name": "nmsd",
        "port": 1239
    },
    {
        "protocol": "UDP",
        "name": "instantia",
        "port": 1240
    },
    {
        "protocol": "UDP",
        "name": "nessus",
        "port": 1241
    },
    {
        "protocol": "UDP",
        "name": "nmasoverip",
        "port": 1242
    },
    {
        "protocol": "UDP",
        "name": "serialgateway",
        "port": 1243
    },
    {
        "protocol": "UDP",
        "name": "isbconference1",
        "port": 1244
    },
    {
        "protocol": "UDP",
        "name": "isbconference2",
        "port": 1245
    },
    {
        "protocol": "UDP",
        "name": "payrouter",
        "port": 1246
    },
    {
        "protocol": "UDP",
        "name": "visionpyramid",
        "port": 1247
    },
    {
        "protocol": "UDP",
        "name": "hermes",
        "port": 1248
    },
    {
        "protocol": "UDP",
        "name": "mesavistaco",
        "port": 1249
    },
    {
        "protocol": "UDP",
        "name": "swldy-sias",
        "port": 1250
    },
    {
        "protocol": "UDP",
        "name": "servergraph",
        "port": 1251
    },
    {
        "protocol": "UDP",
        "name": "bspne-pcc",
        "port": 1252
    },
    {
        "protocol": "UDP",
        "name": "q55-pcc",
        "port": 1253
    },
    {
        "protocol": "UDP",
        "name": "de-noc",
        "port": 1254
    },
    {
        "protocol": "UDP",
        "name": "de-cache-query",
        "port": 1255
    },
    {
        "protocol": "UDP",
        "name": "de-server",
        "port": 1256
    },
    {
        "protocol": "UDP",
        "name": "shockwave2",
        "port": 1257
    },
    {
        "protocol": "UDP",
        "name": "opennl",
        "port": 1258
    },
    {
        "protocol": "UDP",
        "name": "opennl-voice",
        "port": 1259
    },
    {
        "protocol": "UDP",
        "name": "ibm-ssd",
        "port": 1260
    },
    {
        "protocol": "UDP",
        "name": "mpshrsv",
        "port": 1261
    },
    {
        "protocol": "UDP",
        "name": "qnts-orb",
        "port": 1262
    },
    {
        "protocol": "UDP",
        "name": "dka",
        "port": 1263
    },
    {
        "protocol": "UDP",
        "name": "prat",
        "port": 1264
    },
    {
        "protocol": "UDP",
        "name": "dssiapi",
        "port": 1265
    },
    {
        "protocol": "UDP",
        "name": "dellpwrappks",
        "port": 1266
    },
    {
        "protocol": "UDP",
        "name": "epc",
        "port": 1267
    },
    {
        "protocol": "UDP",
        "name": "propel-msgsys",
        "port": 1268
    },
    {
        "protocol": "UDP",
        "name": "watilapp",
        "port": 1269
    },
    {
        "protocol": "UDP",
        "name": "opsmgr",
        "port": 1270
    },
    {
        "protocol": "UDP",
        "name": "excw",
        "port": 1271
    },
    {
        "protocol": "UDP",
        "name": "cspmlockmgr",
        "port": 1272
    },
    {
        "protocol": "UDP",
        "name": "emc-gateway",
        "port": 1273
    },
    {
        "protocol": "UDP",
        "name": "t1distproc",
        "port": 1274
    },
    {
        "protocol": "UDP",
        "name": "ivcollector",
        "port": 1275
    },
    {
        "protocol": "UDP",
        "name": "ivmanager",
        "port": 1276
    },
    {
        "protocol": "UDP",
        "name": "miva-mqs",
        "port": 1277
    },
    {
        "protocol": "UDP",
        "name": "dellwebadmin-1",
        "port": 1278
    },
    {
        "protocol": "UDP",
        "name": "dellwebadmin-2",
        "port": 1279
    },
    {
        "protocol": "UDP",
        "name": "pictrography",
        "port": 1280
    },
    {
        "protocol": "UDP",
        "name": "healthd",
        "port": 1281
    },
    {
        "protocol": "UDP",
        "name": "emperion",
        "port": 1282
    },
    {
        "protocol": "UDP",
        "name": "productinfo",
        "port": 1283
    },
    {
        "protocol": "UDP",
        "name": "iee-qfx",
        "port": 1284
    },
    {
        "protocol": "UDP",
        "name": "neoiface",
        "port": 1285
    },
    {
        "protocol": "UDP",
        "name": "netuitive",
        "port": 1286
    },
    {
        "protocol": "UDP",
        "name": "routematch",
        "port": 1287
    },
    {
        "protocol": "UDP",
        "name": "navbuddy",
        "port": 1288
    },
    {
        "protocol": "UDP",
        "name": "jwalkserver",
        "port": 1289
    },
    {
        "protocol": "UDP",
        "name": "winjaserver",
        "port": 1290
    },
    {
        "protocol": "UDP",
        "name": "seagulllms",
        "port": 1291
    },
    {
        "protocol": "UDP",
        "name": "dsdn",
        "port": 1292
    },
    {
        "protocol": "UDP",
        "name": "pkt-krb-ipsec",
        "port": 1293
    },
    {
        "protocol": "UDP",
        "name": "cmmdriver",
        "port": 1294
    },
    {
        "protocol": "UDP",
        "name": "ehtp",
        "port": 1295
    },
    {
        "protocol": "UDP",
        "name": "dproxy",
        "port": 1296
    },
    {
        "protocol": "UDP",
        "name": "sdproxy",
        "port": 1297
    },
    {
        "protocol": "UDP",
        "name": "lpcp",
        "port": 1298
    },
    {
        "protocol": "UDP",
        "name": "hp-sci",
        "port": 1299
    },
    {
        "protocol": "UDP",
        "name": "h323hostcallsc",
        "port": 1300
    },
    {
        "protocol": "UDP",
        "name": "ci3-software-1",
        "port": 1301
    },
    {
        "protocol": "UDP",
        "name": "ci3-software-2",
        "port": 1302
    },
    {
        "protocol": "UDP",
        "name": "sftsrv",
        "port": 1303
    },
    {
        "protocol": "UDP",
        "name": "boomerang",
        "port": 1304
    },
    {
        "protocol": "UDP",
        "name": "pe-mike",
        "port": 1305
    },
    {
        "protocol": "UDP",
        "name": "re-conn-proto",
        "port": 1306
    },
    {
        "protocol": "UDP",
        "name": "pacmand",
        "port": 1307
    },
    {
        "protocol": "UDP",
        "name": "odsi",
        "port": 1308
    },
    {
        "protocol": "UDP",
        "name": "jtag-server",
        "port": 1309
    },
    {
        "protocol": "UDP",
        "name": "husky",
        "port": 1310
    },
    {
        "protocol": "UDP",
        "name": "rxmon",
        "port": 1311
    },
    {
        "protocol": "UDP",
        "name": "sti-envision",
        "port": 1312
    },
    {
        "protocol": "UDP",
        "name": "bmc_patroldb",
        "port": 1313
    },
    {
        "protocol": "UDP",
        "name": "pdps",
        "port": 1314
    },
    {
        "protocol": "UDP",
        "name": "els",
        "port": 1315
    },
    {
        "protocol": "UDP",
        "name": "exbit-escp",
        "port": 1316
    },
    {
        "protocol": "UDP",
        "name": "vrts-ipcserver",
        "port": 1317
    },
    {
        "protocol": "UDP",
        "name": "krb5gatekeeper",
        "port": 1318
    },
    {
        "protocol": "UDP",
        "name": "panja-icsp",
        "port": 1319
    },
    {
        "protocol": "UDP",
        "name": "panja-axbnet",
        "port": 1320
    },
    {
        "protocol": "UDP",
        "name": "pip",
        "port": 1321
    },
    {
        "protocol": "UDP",
        "name": "novation",
        "port": 1322
    },
    {
        "protocol": "UDP",
        "name": "brcd",
        "port": 1323
    },
    {
        "protocol": "UDP",
        "name": "delta-mcp",
        "port": 1324
    },
    {
        "protocol": "UDP",
        "name": "dx-instrument",
        "port": 1325
    },
    {
        "protocol": "UDP",
        "name": "wimsic",
        "port": 1326
    },
    {
        "protocol": "UDP",
        "name": "ultrex",
        "port": 1327
    },
    {
        "protocol": "UDP",
        "name": "ewall",
        "port": 1328
    },
    {
        "protocol": "UDP",
        "name": "netdb-export",
        "port": 1329
    },
    {
        "protocol": "UDP",
        "name": "streetperfect",
        "port": 1330
    },
    {
        "protocol": "UDP",
        "name": "intersan",
        "port": 1331
    },
    {
        "protocol": "UDP",
        "name": "pcia-rxp-b",
        "port": 1332
    },
    {
        "protocol": "UDP",
        "name": "passwrd-policy",
        "port": 1333
    },
    {
        "protocol": "UDP",
        "name": "writesrv",
        "port": 1334
    },
    {
        "protocol": "UDP",
        "name": "digital-notary",
        "port": 1335
    },
    {
        "protocol": "UDP",
        "name": "ischat",
        "port": 1336
    },
    {
        "protocol": "UDP",
        "name": "menandmice-dns",
        "port": 1337
    },
    {
        "protocol": "UDP",
        "name": "wmc-log-svc",
        "port": 1338
    },
    {
        "protocol": "UDP",
        "name": "kjtsiteserver",
        "port": 1339
    },
    {
        "protocol": "UDP",
        "name": "naap",
        "port": 1340
    },
    {
        "protocol": "UDP",
        "name": "qubes",
        "port": 1341
    },
    {
        "protocol": "UDP",
        "name": "esbroker",
        "port": 1342
    },
    {
        "protocol": "UDP",
        "name": "re101",
        "port": 1343
    },
    {
        "protocol": "UDP",
        "name": "icap",
        "port": 1344
    },
    {
        "protocol": "UDP",
        "name": "vpjp",
        "port": 1345
    },
    {
        "protocol": "UDP",
        "name": "alta-ana-lm",
        "port": 1346
    },
    {
        "protocol": "UDP",
        "name": "bbn-mmc",
        "port": 1347
    },
    {
        "protocol": "UDP",
        "name": "bbn-mmx",
        "port": 1348
    },
    {
        "protocol": "UDP",
        "name": "sbook",
        "port": 1349
    },
    {
        "protocol": "UDP",
        "name": "editbench",
        "port": 1350
    },
    {
        "protocol": "UDP",
        "name": "equationbuilder",
        "port": 1351
    },
    {
        "protocol": "UDP",
        "name": "lotusnote",
        "port": 1352
    },
    {
        "protocol": "UDP",
        "name": "relief",
        "port": 1353
    },
    {
        "protocol": "UDP",
        "name": "XSIP-network",
        "port": 1354
    },
    {
        "protocol": "UDP",
        "name": "intuitive-edge",
        "port": 1355
    },
    {
        "protocol": "UDP",
        "name": "pegboard",
        "port": 1357
    },
    {
        "protocol": "UDP",
        "name": "connlcli",
        "port": 1358
    },
    {
        "protocol": "UDP",
        "name": "ftsrv",
        "port": 1359
    },
    {
        "protocol": "UDP",
        "name": "mimer",
        "port": 1360
    },
    {
        "protocol": "UDP",
        "name": "linx",
        "port": 1361
    },
    {
        "protocol": "UDP",
        "name": "timeflies",
        "port": 1362
    },
    {
        "protocol": "UDP",
        "name": "ndm-requester",
        "port": 1363
    },
    {
        "protocol": "UDP",
        "name": "ndm-server",
        "port": 1364
    },
    {
        "protocol": "UDP",
        "name": "adapt-sna",
        "port": 1365
    },
    {
        "protocol": "UDP",
        "name": "netware-csp",
        "port": 1366
    },
    {
        "protocol": "UDP",
        "name": "dcs",
        "port": 1367
    },
    {
        "protocol": "UDP",
        "name": "screencast",
        "port": 1368
    },
    {
        "protocol": "UDP",
        "name": "gv-us",
        "port": 1369
    },
    {
        "protocol": "UDP",
        "name": "us-gv",
        "port": 1370
    },
    {
        "protocol": "UDP",
        "name": "fc-cli",
        "port": 1371
    },
    {
        "protocol": "UDP",
        "name": "fc-ser",
        "port": 1372
    },
    {
        "protocol": "UDP",
        "name": "chromagrafx",
        "port": 1373
    },
    {
        "protocol": "UDP",
        "name": "molly",
        "port": 1374
    },
    {
        "protocol": "UDP",
        "name": "bytex",
        "port": 1375
    },
    {
        "protocol": "UDP",
        "name": "ibm-pps",
        "port": 1376
    },
    {
        "protocol": "UDP",
        "name": "cichlid",
        "port": 1377
    },
    {
        "protocol": "UDP",
        "name": "elan",
        "port": 1378
    },
    {
        "protocol": "UDP",
        "name": "dbreporter",
        "port": 1379
    },
    {
        "protocol": "UDP",
        "name": "telesis-licman",
        "port": 1380
    },
    {
        "protocol": "UDP",
        "name": "apple-licman",
        "port": 1381
    },
    {
        "protocol": "UDP",
        "name": "udt_os",
        "port": 1382
    },
    {
        "protocol": "UDP",
        "name": "gwha",
        "port": 1383
    },
    {
        "protocol": "UDP",
        "name": "os-licman",
        "port": 1384
    },
    {
        "protocol": "UDP",
        "name": "atex_elmd",
        "port": 1385
    },
    {
        "protocol": "UDP",
        "name": "checksum",
        "port": 1386
    },
    {
        "protocol": "UDP",
        "name": "cadsi-lm",
        "port": 1387
    },
    {
        "protocol": "UDP",
        "name": "objective-dbc",
        "port": 1388
    },
    {
        "protocol": "UDP",
        "name": "iclpv-dm",
        "port": 1389
    },
    {
        "protocol": "UDP",
        "name": "iclpv-sc",
        "port": 1390
    },
    {
        "protocol": "UDP",
        "name": "iclpv-sas",
        "port": 1391
    },
    {
        "protocol": "UDP",
        "name": "iclpv-pm",
        "port": 1392
    },
    {
        "protocol": "UDP",
        "name": "iclpv-nls",
        "port": 1393
    },
    {
        "protocol": "UDP",
        "name": "iclpv-nlc",
        "port": 1394
    },
    {
        "protocol": "UDP",
        "name": "iclpv-wsm",
        "port": 1395
    },
    {
        "protocol": "UDP",
        "name": "dvl-activemail",
        "port": 1396
    },
    {
        "protocol": "UDP",
        "name": "audio-activmail",
        "port": 1397
    },
    {
        "protocol": "UDP",
        "name": "video-activmail",
        "port": 1398
    },
    {
        "protocol": "UDP",
        "name": "cadkey-licman",
        "port": 1399
    },
    {
        "protocol": "UDP",
        "name": "cadkey-tablet",
        "port": 1400
    },
    {
        "protocol": "UDP",
        "name": "goldleaf-licman",
        "port": 1401
    },
    {
        "protocol": "UDP",
        "name": "prm-sm-np",
        "port": 1402
    },
    {
        "protocol": "UDP",
        "name": "prm-nm-np",
        "port": 1403
    },
    {
        "protocol": "UDP",
        "name": "igi-lm",
        "port": 1404
    },
    {
        "protocol": "UDP",
        "name": "ibm-res",
        "port": 1405
    },
    {
        "protocol": "UDP",
        "name": "netlabs-lm",
        "port": 1406
    },
    {
        "protocol": "UDP",
        "name": "dbsa-lm",
        "port": 1407
    },
    {
        "protocol": "UDP",
        "name": "sophia-lm",
        "port": 1408
    },
    {
        "protocol": "UDP",
        "name": "here-lm",
        "port": 1409
    },
    {
        "protocol": "UDP",
        "name": "hiq",
        "port": 1410
    },
    {
        "protocol": "UDP",
        "name": "af",
        "port": 1411
    },
    {
        "protocol": "UDP",
        "name": "innosys",
        "port": 1412
    },
    {
        "protocol": "UDP",
        "name": "innosys-acl",
        "port": 1413
    },
    {
        "protocol": "UDP",
        "name": "ibm-mqseries",
        "port": 1414
    },
    {
        "protocol": "UDP",
        "name": "dbstar",
        "port": 1415
    },
    {
        "protocol": "UDP",
        "name": "novell-lu6.2",
        "port": 1416
    },
    {
        "protocol": "UDP",
        "name": "timbuktu-srv1",
        "port": 1417
    },
    {
        "protocol": "UDP",
        "name": "timbuktu-srv2",
        "port": 1418
    },
    {
        "protocol": "UDP",
        "name": "timbuktu-srv3",
        "port": 1419
    },
    {
        "protocol": "UDP",
        "name": "timbuktu-srv4",
        "port": 1420
    },
    {
        "protocol": "UDP",
        "name": "gandalf-lm",
        "port": 1421
    },
    {
        "protocol": "UDP",
        "name": "autodesk-lm",
        "port": 1422
    },
    {
        "protocol": "UDP",
        "name": "essbase",
        "port": 1423
    },
    {
        "protocol": "UDP",
        "name": "hybrid",
        "port": 1424
    },
    {
        "protocol": "UDP",
        "name": "zion-lm",
        "port": 1425
    },
    {
        "protocol": "UDP",
        "name": "sais",
        "port": 1426
    },
    {
        "protocol": "UDP",
        "name": "mloadd",
        "port": 1427
    },
    {
        "protocol": "UDP",
        "name": "informatik-lm",
        "port": 1428
    },
    {
        "protocol": "UDP",
        "name": "nms",
        "port": 1429
    },
    {
        "protocol": "UDP",
        "name": "tpdu",
        "port": 1430
    },
    {
        "protocol": "UDP",
        "name": "rgtp",
        "port": 1431
    },
    {
        "protocol": "UDP",
        "name": "blueberry-lm",
        "port": 1432
    },
    {
        "protocol": "UDP",
        "name": "ms-sql-s",
        "port": 1433
    },
    {
        "protocol": "UDP",
        "name": "ms-sql-m",
        "port": 1434
    },
    {
        "protocol": "UDP",
        "name": "ibm-cics",
        "port": 1435
    },
    {
        "protocol": "UDP",
        "name": "saism",
        "port": 1436
    },
    {
        "protocol": "UDP",
        "name": "tabula",
        "port": 1437
    },
    {
        "protocol": "UDP",
        "name": "eicon-server",
        "port": 1438
    },
    {
        "protocol": "UDP",
        "name": "eicon-x25",
        "port": 1439
    },
    {
        "protocol": "UDP",
        "name": "eicon-slp",
        "port": 1440
    },
    {
        "protocol": "UDP",
        "name": "cadis-1",
        "port": 1441
    },
    {
        "protocol": "UDP",
        "name": "cadis-2",
        "port": 1442
    },
    {
        "protocol": "UDP",
        "name": "ies-lm",
        "port": 1443
    },
    {
        "protocol": "UDP",
        "name": "marcam-lm",
        "port": 1444
    },
    {
        "protocol": "UDP",
        "name": "proxima-lm",
        "port": 1445
    },
    {
        "protocol": "UDP",
        "name": "ora-lm",
        "port": 1446
    },
    {
        "protocol": "UDP",
        "name": "apri-lm",
        "port": 1447
    },
    {
        "protocol": "UDP",
        "name": "oc-lm",
        "port": 1448
    },
    {
        "protocol": "UDP",
        "name": "peport",
        "port": 1449
    },
    {
        "protocol": "UDP",
        "name": "dwf",
        "port": 1450
    },
    {
        "protocol": "UDP",
        "name": "infoman",
        "port": 1451
    },
    {
        "protocol": "UDP",
        "name": "gtegsc-lm",
        "port": 1452
    },
    {
        "protocol": "UDP",
        "name": "genie-lm",
        "port": 1453
    },
    {
        "protocol": "UDP",
        "name": "interhdl_elmd",
        "port": 1454
    },
    {
        "protocol": "UDP",
        "name": "esl-lm",
        "port": 1455
    },
    {
        "protocol": "UDP",
        "name": "dca",
        "port": 1456
    },
    {
        "protocol": "UDP",
        "name": "valisys-lm",
        "port": 1457
    },
    {
        "protocol": "UDP",
        "name": "nrcabq-lm",
        "port": 1458
    },
    {
        "protocol": "UDP",
        "name": "proshare1",
        "port": 1459
    },
    {
        "protocol": "UDP",
        "name": "proshare2",
        "port": 1460
    },
    {
        "protocol": "UDP",
        "name": "ibm_wrless_lan",
        "port": 1461
    },
    {
        "protocol": "UDP",
        "name": "world-lm",
        "port": 1462
    },
    {
        "protocol": "UDP",
        "name": "nucleus",
        "port": 1463
    },
    {
        "protocol": "UDP",
        "name": "msl_lmd",
        "port": 1464
    },
    {
        "protocol": "UDP",
        "name": "pipes",
        "port": 1465
    },
    {
        "protocol": "UDP",
        "name": "oceansoft-lm",
        "port": 1466
    },
    {
        "protocol": "UDP",
        "name": "csdmbase",
        "port": 1467
    },
    {
        "protocol": "UDP",
        "name": "csdm",
        "port": 1468
    },
    {
        "protocol": "UDP",
        "name": "aal-lm",
        "port": 1469
    },
    {
        "protocol": "UDP",
        "name": "uaiact",
        "port": 1470
    },
    {
        "protocol": "UDP",
        "name": "csdmbase",
        "port": 1471
    },
    {
        "protocol": "UDP",
        "name": "csdm",
        "port": 1472
    },
    {
        "protocol": "UDP",
        "name": "openmath",
        "port": 1473
    },
    {
        "protocol": "UDP",
        "name": "telefinder",
        "port": 1474
    },
    {
        "protocol": "UDP",
        "name": "taligent-lm",
        "port": 1475
    },
    {
        "protocol": "UDP",
        "name": "clvm-cfg",
        "port": 1476
    },
    {
        "protocol": "UDP",
        "name": "ms-sna-server",
        "port": 1477
    },
    {
        "protocol": "UDP",
        "name": "ms-sna-base",
        "port": 1478
    },
    {
        "protocol": "UDP",
        "name": "dberegister",
        "port": 1479
    },
    {
        "protocol": "UDP",
        "name": "pacerforum",
        "port": 1480
    },
    {
        "protocol": "UDP",
        "name": "airs",
        "port": 1481
    },
    {
        "protocol": "UDP",
        "name": "miteksys-lm",
        "port": 1482
    },
    {
        "protocol": "UDP",
        "name": "afs",
        "port": 1483
    },
    {
        "protocol": "UDP",
        "name": "confluent",
        "port": 1484
    },
    {
        "protocol": "UDP",
        "name": "lansource",
        "port": 1485
    },
    {
        "protocol": "UDP",
        "name": "nms_topo_serv",
        "port": 1486
    },
    {
        "protocol": "UDP",
        "name": "localinfosrvr",
        "port": 1487
    },
    {
        "protocol": "UDP",
        "name": "docstor",
        "port": 1488
    },
    {
        "protocol": "UDP",
        "name": "dmdocbroker",
        "port": 1489
    },
    {
        "protocol": "UDP",
        "name": "insitu-conf",
        "port": 1490
    },
    {
        "protocol": "UDP",
        "name": "anynetgateway",
        "port": 1491
    },
    {
        "protocol": "UDP",
        "name": "stone-design-1",
        "port": 1492
    },
    {
        "protocol": "UDP",
        "name": "netmap_lm",
        "port": 1493
    },
    {
        "protocol": "UDP",
        "name": "ica",
        "port": 1494
    },
    {
        "protocol": "UDP",
        "name": "cvc",
        "port": 1495
    },
    {
        "protocol": "UDP",
        "name": "liberty-lm",
        "port": 1496
    },
    {
        "protocol": "UDP",
        "name": "rfx-lm",
        "port": 1497
    },
    {
        "protocol": "UDP",
        "name": "sybase-sqlany",
        "port": 1498
    },
    {
        "protocol": "UDP",
        "name": "fhc",
        "port": 1499
    },
    {
        "protocol": "UDP",
        "name": "vlsi-lm",
        "port": 1500
    },
    {
        "protocol": "UDP",
        "name": "saiscm",
        "port": 1501
    },
    {
        "protocol": "UDP",
        "name": "shivadiscovery",
        "port": 1502
    },
    {
        "protocol": "UDP",
        "name": "imtc-mcs",
        "port": 1503
    },
    {
        "protocol": "UDP",
        "name": "evb-elm",
        "port": 1504
    },
    {
        "protocol": "UDP",
        "name": "funkproxy",
        "port": 1505
    },
    {
        "protocol": "UDP",
        "name": "utcd",
        "port": 1506
    },
    {
        "protocol": "UDP",
        "name": "symplex",
        "port": 1507
    },
    {
        "protocol": "UDP",
        "name": "diagmond",
        "port": 1508
    },
    {
        "protocol": "UDP",
        "name": "robcad-lm",
        "port": 1509
    },
    {
        "protocol": "UDP",
        "name": "mvx-lm",
        "port": 1510
    },
    {
        "protocol": "UDP",
        "name": "3l-l1",
        "port": 1511
    },
    {
        "protocol": "UDP",
        "name": "wins",
        "port": 1512
    },
    {
        "protocol": "UDP",
        "name": "fujitsu-dtc",
        "port": 1513
    },
    {
        "protocol": "UDP",
        "name": "fujitsu-dtcns",
        "port": 1514
    },
    {
        "protocol": "UDP",
        "name": "ifor-protocol",
        "port": 1515
    },
    {
        "protocol": "UDP",
        "name": "vpad",
        "port": 1516
    },
    {
        "protocol": "UDP",
        "name": "vpac",
        "port": 1517
    },
    {
        "protocol": "UDP",
        "name": "vpvd",
        "port": 1518
    },
    {
        "protocol": "UDP",
        "name": "vpvc",
        "port": 1519
    },
    {
        "protocol": "UDP",
        "name": "atm-zip-office",
        "port": 1520
    },
    {
        "protocol": "UDP",
        "name": "ncube-lm",
        "port": 1521
    },
    {
        "protocol": "UDP",
        "name": "ricardo-lm",
        "port": 1522
    },
    {
        "protocol": "UDP",
        "name": "cichild-lm",
        "port": 1523
    },
    {
        "protocol": "UDP",
        "name": "ingreslock",
        "port": 1524
    },
    {
        "protocol": "UDP",
        "name": "prospero-np",
        "port": 1525
    },
    {
        "protocol": "UDP",
        "name": "pdap-np",
        "port": 1526
    },
    {
        "protocol": "UDP",
        "name": "tlisrv",
        "port": 1527
    },
    {
        "protocol": "UDP",
        "name": "mciautoreg",
        "port": 1528
    },
    {
        "protocol": "UDP",
        "name": "coauthor",
        "port": 1529
    },
    {
        "protocol": "UDP",
        "name": "rap-service",
        "port": 1530
    },
    {
        "protocol": "UDP",
        "name": "rap-listen",
        "port": 1531
    },
    {
        "protocol": "UDP",
        "name": "miroconnect",
        "port": 1532
    },
    {
        "protocol": "UDP",
        "name": "virtual-places",
        "port": 1533
    },
    {
        "protocol": "UDP",
        "name": "micromuse-lm",
        "port": 1534
    },
    {
        "protocol": "UDP",
        "name": "ampr-info",
        "port": 1535
    },
    {
        "protocol": "UDP",
        "name": "ampr-inter",
        "port": 1536
    },
    {
        "protocol": "UDP",
        "name": "sdsc-lm",
        "port": 1537
    },
    {
        "protocol": "UDP",
        "name": "3ds-lm",
        "port": 1538
    },
    {
        "protocol": "UDP",
        "name": "intellistor-lm",
        "port": 1539
    },
    {
        "protocol": "UDP",
        "name": "rds",
        "port": 1540
    },
    {
        "protocol": "UDP",
        "name": "rds2",
        "port": 1541
    },
    {
        "protocol": "UDP",
        "name": "gridgen-elmd",
        "port": 1542
    },
    {
        "protocol": "UDP",
        "name": "simba-cs",
        "port": 1543
    },
    {
        "protocol": "UDP",
        "name": "aspeclmd",
        "port": 1544
    },
    {
        "protocol": "UDP",
        "name": "vistium-share",
        "port": 1545
    },
    {
        "protocol": "UDP",
        "name": "abbaccuray",
        "port": 1546
    },
    {
        "protocol": "UDP",
        "name": "laplink",
        "port": 1547
    },
    {
        "protocol": "UDP",
        "name": "axon-lm",
        "port": 1548
    },
    {
        "protocol": "UDP",
        "name": "shivasound",
        "port": 1549
    },
    {
        "protocol": "UDP",
        "name": "3m-image-lm",
        "port": 1550
    },
    {
        "protocol": "UDP",
        "name": "hecmtl-db",
        "port": 1551
    },
    {
        "protocol": "UDP",
        "name": "pciarray",
        "port": 1552
    },
    {
        "protocol": "UDP",
        "name": "sna-cs",
        "port": 1553
    },
    {
        "protocol": "UDP",
        "name": "caci-lm",
        "port": 1554
    },
    {
        "protocol": "UDP",
        "name": "livelan",
        "port": 1555
    },
    {
        "protocol": "UDP",
        "name": "veritas_pbx",
        "port": 1556
    },
    {
        "protocol": "UDP",
        "name": "arbortext-lm",
        "port": 1557
    },
    {
        "protocol": "UDP",
        "name": "xingmpeg",
        "port": 1558
    },
    {
        "protocol": "UDP",
        "name": "web2host",
        "port": 1559
    },
    {
        "protocol": "UDP",
        "name": "asci-val",
        "port": 1560
    },
    {
        "protocol": "UDP",
        "name": "facilityview",
        "port": 1561
    },
    {
        "protocol": "UDP",
        "name": "pconnectmgr",
        "port": 1562
    },
    {
        "protocol": "UDP",
        "name": "cadabra-lm",
        "port": 1563
    },
    {
        "protocol": "UDP",
        "name": "pay-per-view",
        "port": 1564
    },
    {
        "protocol": "UDP",
        "name": "winddlb",
        "port": 1565
    },
    {
        "protocol": "UDP",
        "name": "corelvideo",
        "port": 1566
    },
    {
        "protocol": "UDP",
        "name": "jlicelmd",
        "port": 1567
    },
    {
        "protocol": "UDP",
        "name": "tsspmap",
        "port": 1568
    },
    {
        "protocol": "UDP",
        "name": "ets",
        "port": 1569
    },
    {
        "protocol": "UDP",
        "name": "orbixd",
        "port": 1570
    },
    {
        "protocol": "UDP",
        "name": "rdb-dbs-disp",
        "port": 1571
    },
    {
        "protocol": "UDP",
        "name": "chip-lm",
        "port": 1572
    },
    {
        "protocol": "UDP",
        "name": "itscomm-ns",
        "port": 1573
    },
    {
        "protocol": "UDP",
        "name": "mvel-lm",
        "port": 1574
    },
    {
        "protocol": "UDP",
        "name": "oraclenames",
        "port": 1575
    },
    {
        "protocol": "UDP",
        "name": "moldflow-lm",
        "port": 1576
    },
    {
        "protocol": "UDP",
        "name": "hypercube-lm",
        "port": 1577
    },
    {
        "protocol": "UDP",
        "name": "jacobus-lm",
        "port": 1578
    },
    {
        "protocol": "UDP",
        "name": "ioc-sea-lm",
        "port": 1579
    },
    {
        "protocol": "UDP",
        "name": "tn-tl-r2",
        "port": 1580
    },
    {
        "protocol": "UDP",
        "name": "mil-2045-47001",
        "port": 1581
    },
    {
        "protocol": "UDP",
        "name": "msims",
        "port": 1582
    },
    {
        "protocol": "UDP",
        "name": "simbaexpress",
        "port": 1583
    },
    {
        "protocol": "UDP",
        "name": "tn-tl-fd2",
        "port": 1584
    },
    {
        "protocol": "UDP",
        "name": "intv",
        "port": 1585
    },
    {
        "protocol": "UDP",
        "name": "ibm-abtact",
        "port": 1586
    },
    {
        "protocol": "UDP",
        "name": "pra_elmd",
        "port": 1587
    },
    {
        "protocol": "UDP",
        "name": "triquest-lm",
        "port": 1588
    },
    {
        "protocol": "UDP",
        "name": "vqp",
        "port": 1589
    },
    {
        "protocol": "UDP",
        "name": "gemini-lm",
        "port": 1590
    },
    {
        "protocol": "UDP",
        "name": "ncpm-pm",
        "port": 1591
    },
    {
        "protocol": "UDP",
        "name": "commonspace",
        "port": 1592
    },
    {
        "protocol": "UDP",
        "name": "mainsoft-lm",
        "port": 1593
    },
    {
        "protocol": "UDP",
        "name": "sixtrak",
        "port": 1594
    },
    {
        "protocol": "UDP",
        "name": "radio",
        "port": 1595
    },
    {
        "protocol": "UDP",
        "name": "radio-bc",
        "port": 1596
    },
    {
        "protocol": "UDP",
        "name": "orbplus-iiop",
        "port": 1597
    },
    {
        "protocol": "UDP",
        "name": "picknfs",
        "port": 1598
    },
    {
        "protocol": "UDP",
        "name": "simbaservices",
        "port": 1599
    },
    {
        "protocol": "UDP",
        "name": "issd",
        "port": 1600
    },
    {
        "protocol": "UDP",
        "name": "aas",
        "port": 1601
    },
    {
        "protocol": "UDP",
        "name": "inspect",
        "port": 1602
    },
    {
        "protocol": "UDP",
        "name": "picodbc",
        "port": 1603
    },
    {
        "protocol": "UDP",
        "name": "icabrowser",
        "port": 1604
    },
    {
        "protocol": "UDP",
        "name": "slp",
        "port": 1605
    },
    {
        "protocol": "UDP",
        "name": "slm-api",
        "port": 1606
    },
    {
        "protocol": "UDP",
        "name": "stt",
        "port": 1607
    },
    {
        "protocol": "UDP",
        "name": "smart-lm",
        "port": 1608
    },
    {
        "protocol": "UDP",
        "name": "isysg-lm",
        "port": 1609
    },
    {
        "protocol": "UDP",
        "name": "taurus-wh",
        "port": 1610
    },
    {
        "protocol": "UDP",
        "name": "ill",
        "port": 1611
    },
    {
        "protocol": "UDP",
        "name": "netbill-trans",
        "port": 1612
    },
    {
        "protocol": "UDP",
        "name": "netbill-keyrep",
        "port": 1613
    },
    {
        "protocol": "UDP",
        "name": "netbill-cred",
        "port": 1614
    },
    {
        "protocol": "UDP",
        "name": "netbill-auth",
        "port": 1615
    },
    {
        "protocol": "UDP",
        "name": "netbill-prod",
        "port": 1616
    },
    {
        "protocol": "UDP",
        "name": "nimrod-agent",
        "port": 1617
    },
    {
        "protocol": "UDP",
        "name": "skytelnet",
        "port": 1618
    },
    {
        "protocol": "UDP",
        "name": "xs-openstorage",
        "port": 1619
    },
    {
        "protocol": "UDP",
        "name": "faxportwinport",
        "port": 1620
    },
    {
        "protocol": "UDP",
        "name": "softdataphone",
        "port": 1621
    },
    {
        "protocol": "UDP",
        "name": "ontime",
        "port": 1622
    },
    {
        "protocol": "UDP",
        "name": "jaleosnd",
        "port": 1623
    },
    {
        "protocol": "UDP",
        "name": "udp-sr-port",
        "port": 1624
    },
    {
        "protocol": "UDP",
        "name": "svs-omagent",
        "port": 1625
    },
    {
        "protocol": "UDP",
        "name": "shockwave",
        "port": 1626
    },
    {
        "protocol": "UDP",
        "name": "t128-gateway",
        "port": 1627
    },
    {
        "protocol": "UDP",
        "name": "lontalk-norm",
        "port": 1628
    },
    {
        "protocol": "UDP",
        "name": "lontalk-urgnt",
        "port": 1629
    },
    {
        "protocol": "UDP",
        "name": "oraclenet8cman",
        "port": 1630
    },
    {
        "protocol": "UDP",
        "name": "visitview",
        "port": 1631
    },
    {
        "protocol": "UDP",
        "name": "pammratc",
        "port": 1632
    },
    {
        "protocol": "UDP",
        "name": "pammrpc",
        "port": 1633
    },
    {
        "protocol": "UDP",
        "name": "loaprobe",
        "port": 1634
    },
    {
        "protocol": "UDP",
        "name": "edb-server1",
        "port": 1635
    },
    {
        "protocol": "UDP",
        "name": "cncp",
        "port": 1636
    },
    {
        "protocol": "UDP",
        "name": "cnap",
        "port": 1637
    },
    {
        "protocol": "UDP",
        "name": "cnip",
        "port": 1638
    },
    {
        "protocol": "UDP",
        "name": "cert-initiator",
        "port": 1639
    },
    {
        "protocol": "UDP",
        "name": "cert-responder",
        "port": 1640
    },
    {
        "protocol": "UDP",
        "name": "invision",
        "port": 1641
    },
    {
        "protocol": "UDP",
        "name": "isis-am",
        "port": 1642
    },
    {
        "protocol": "UDP",
        "name": "isis-ambc",
        "port": 1643
    },
    {
        "protocol": "UDP",
        "name": "sightline",
        "port": 1645
    },
    {
        "protocol": "UDP",
        "name": "sa-msg-port",
        "port": 1646
    },
    {
        "protocol": "UDP",
        "name": "rsap",
        "port": 1647
    },
    {
        "protocol": "UDP",
        "name": "concurrent-lm",
        "port": 1648
    },
    {
        "protocol": "UDP",
        "name": "kermit",
        "port": 1649
    },
    {
        "protocol": "UDP",
        "name": "nkd",
        "port": 1650
    },
    {
        "protocol": "UDP",
        "name": "shiva_confsrvr",
        "port": 1651
    },
    {
        "protocol": "UDP",
        "name": "xnmp",
        "port": 1652
    },
    {
        "protocol": "UDP",
        "name": "alphatech-lm",
        "port": 1653
    },
    {
        "protocol": "UDP",
        "name": "stargatealerts",
        "port": 1654
    },
    {
        "protocol": "UDP",
        "name": "dec-mbadmin",
        "port": 1655
    },
    {
        "protocol": "UDP",
        "name": "dec-mbadmin-h",
        "port": 1656
    },
    {
        "protocol": "UDP",
        "name": "fujitsu-mmpdc",
        "port": 1657
    },
    {
        "protocol": "UDP",
        "name": "sixnetudr",
        "port": 1658
    },
    {
        "protocol": "UDP",
        "name": "sg-lm",
        "port": 1659
    },
    {
        "protocol": "UDP",
        "name": "skip-mc-gikreq",
        "port": 1660
    },
    {
        "protocol": "UDP",
        "name": "netview-aix-1",
        "port": 1661
    },
    {
        "protocol": "UDP",
        "name": "netview-aix-2",
        "port": 1662
    },
    {
        "protocol": "UDP",
        "name": "netview-aix-3",
        "port": 1663
    },
    {
        "protocol": "UDP",
        "name": "netview-aix-4",
        "port": 1664
    },
    {
        "protocol": "UDP",
        "name": "netview-aix-5",
        "port": 1665
    },
    {
        "protocol": "UDP",
        "name": "netview-aix-6",
        "port": 1666
    },
    {
        "protocol": "UDP",
        "name": "netview-aix-7",
        "port": 1667
    },
    {
        "protocol": "UDP",
        "name": "netview-aix-8",
        "port": 1668
    },
    {
        "protocol": "UDP",
        "name": "netview-aix-9",
        "port": 1669
    },
    {
        "protocol": "UDP",
        "name": "netview-aix-10",
        "port": 1670
    },
    {
        "protocol": "UDP",
        "name": "netview-aix-11",
        "port": 1671
    },
    {
        "protocol": "UDP",
        "name": "netview-aix-12",
        "port": 1672
    },
    {
        "protocol": "UDP",
        "name": "proshare-mc-1",
        "port": 1673
    },
    {
        "protocol": "UDP",
        "name": "proshare-mc-2",
        "port": 1674
    },
    {
        "protocol": "UDP",
        "name": "pdp",
        "port": 1675
    },
    {
        "protocol": "UDP",
        "name": "netcomm2",
        "port": 1676
    },
    {
        "protocol": "UDP",
        "name": "groupwise",
        "port": 1677
    },
    {
        "protocol": "UDP",
        "name": "prolink",
        "port": 1678
    },
    {
        "protocol": "UDP",
        "name": "darcorp-lm",
        "port": 1679
    },
    {
        "protocol": "UDP",
        "name": "microcom-sbp",
        "port": 1680
    },
    {
        "protocol": "UDP",
        "name": "sd-elmd",
        "port": 1681
    },
    {
        "protocol": "UDP",
        "name": "lanyon-lantern",
        "port": 1682
    },
    {
        "protocol": "UDP",
        "name": "ncpm-hip",
        "port": 1683
    },
    {
        "protocol": "UDP",
        "name": "snaresecure",
        "port": 1684
    },
    {
        "protocol": "UDP",
        "name": "n2nremote",
        "port": 1685
    },
    {
        "protocol": "UDP",
        "name": "cvmon",
        "port": 1686
    },
    {
        "protocol": "UDP",
        "name": "nsjtp-ctrl",
        "port": 1687
    },
    {
        "protocol": "UDP",
        "name": "nsjtp-data",
        "port": 1688
    },
    {
        "protocol": "UDP",
        "name": "firefox",
        "port": 1689
    },
    {
        "protocol": "UDP",
        "name": "ng-umds",
        "port": 1690
    },
    {
        "protocol": "UDP",
        "name": "empire-empuma",
        "port": 1691
    },
    {
        "protocol": "UDP",
        "name": "sstsys-lm",
        "port": 1692
    },
    {
        "protocol": "UDP",
        "name": "rrirtr",
        "port": 1693
    },
    {
        "protocol": "UDP",
        "name": "rrimwm",
        "port": 1694
    },
    {
        "protocol": "UDP",
        "name": "rrilwm",
        "port": 1695
    },
    {
        "protocol": "UDP",
        "name": "rrifmm",
        "port": 1696
    },
    {
        "protocol": "UDP",
        "name": "rrisat",
        "port": 1697
    },
    {
        "protocol": "UDP",
        "name": "rsvp-encap-1",
        "port": 1698
    },
    {
        "protocol": "UDP",
        "name": "rsvp-encap-2",
        "port": 1699
    },
    {
        "protocol": "UDP",
        "name": "mps-raft",
        "port": 1700
    },
    {
        "protocol": "UDP",
        "name": "L2TP",
        "port": 1701
    },
    {
        "protocol": "UDP",
        "name": "deskshare",
        "port": 1702
    },
    {
        "protocol": "UDP",
        "name": "hb-engine",
        "port": 1703
    },
    {
        "protocol": "UDP",
        "name": "bcs-broker",
        "port": 1704
    },
    {
        "protocol": "UDP",
        "name": "slingshot",
        "port": 1705
    },
    {
        "protocol": "UDP",
        "name": "jetform",
        "port": 1706
    },
    {
        "protocol": "UDP",
        "name": "vdmplay",
        "port": 1707
    },
    {
        "protocol": "UDP",
        "name": "gat-lmd",
        "port": 1708
    },
    {
        "protocol": "UDP",
        "name": "centra",
        "port": 1709
    },
    {
        "protocol": "UDP",
        "name": "impera",
        "port": 1710
    },
    {
        "protocol": "UDP",
        "name": "pptconference",
        "port": 1711
    },
    {
        "protocol": "UDP",
        "name": "registrar",
        "port": 1712
    },
    {
        "protocol": "UDP",
        "name": "conferencetalk",
        "port": 1713
    },
    {
        "protocol": "UDP",
        "name": "sesi-lm",
        "port": 1714
    },
    {
        "protocol": "UDP",
        "name": "houdini-lm",
        "port": 1715
    },
    {
        "protocol": "UDP",
        "name": "xmsg",
        "port": 1716
    },
    {
        "protocol": "UDP",
        "name": "fj-hdnet",
        "port": 1717
    },
    {
        "protocol": "UDP",
        "name": "h323gatedisc",
        "port": 1718
    },
    {
        "protocol": "UDP",
        "name": "h323gatestat",
        "port": 1719
    },
    {
        "protocol": "UDP",
        "name": "h323hostcall",
        "port": 1720
    },
    {
        "protocol": "UDP",
        "name": "caicci",
        "port": 1721
    },
    {
        "protocol": "UDP",
        "name": "hks-lm",
        "port": 1722
    },
    {
        "protocol": "UDP",
        "name": "pptp",
        "port": 1723
    },
    {
        "protocol": "UDP",
        "name": "csbphonemaster",
        "port": 1724
    },
    {
        "protocol": "UDP",
        "name": "iden-ralp",
        "port": 1725
    },
    {
        "protocol": "UDP",
        "name": "iberiagames",
        "port": 1726
    },
    {
        "protocol": "UDP",
        "name": "winddx",
        "port": 1727
    },
    {
        "protocol": "UDP",
        "name": "telindus",
        "port": 1728
    },
    {
        "protocol": "UDP",
        "name": "citynl",
        "port": 1729
    },
    {
        "protocol": "UDP",
        "name": "roketz",
        "port": 1730
    },
    {
        "protocol": "UDP",
        "name": "msiccp",
        "port": 1731
    },
    {
        "protocol": "UDP",
        "name": "proxim",
        "port": 1732
    },
    {
        "protocol": "UDP",
        "name": "siipat",
        "port": 1733
    },
    {
        "protocol": "UDP",
        "name": "cambertx-lm",
        "port": 1734
    },
    {
        "protocol": "UDP",
        "name": "privatechat",
        "port": 1735
    },
    {
        "protocol": "UDP",
        "name": "street-stream",
        "port": 1736
    },
    {
        "protocol": "UDP",
        "name": "ultimad",
        "port": 1737
    },
    {
        "protocol": "UDP",
        "name": "gamegen1",
        "port": 1738
    },
    {
        "protocol": "UDP",
        "name": "webaccess",
        "port": 1739
    },
    {
        "protocol": "UDP",
        "name": "encore",
        "port": 1740
    },
    {
        "protocol": "UDP",
        "name": "cisco-net-mgmt",
        "port": 1741
    },
    {
        "protocol": "UDP",
        "name": "3Com-nsd",
        "port": 1742
    },
    {
        "protocol": "UDP",
        "name": "cinegrfx-lm",
        "port": 1743
    },
    {
        "protocol": "UDP",
        "name": "ncpm-ft",
        "port": 1744
    },
    {
        "protocol": "UDP",
        "name": "remote-winsock",
        "port": 1745
    },
    {
        "protocol": "UDP",
        "name": "ftrapid-1",
        "port": 1746
    },
    {
        "protocol": "UDP",
        "name": "ftrapid-2",
        "port": 1747
    },
    {
        "protocol": "UDP",
        "name": "oracle-em1",
        "port": 1748
    },
    {
        "protocol": "UDP",
        "name": "aspen-services",
        "port": 1749
    },
    {
        "protocol": "UDP",
        "name": "sslp",
        "port": 1750
    },
    {
        "protocol": "UDP",
        "name": "swiftnet",
        "port": 1751
    },
    {
        "protocol": "UDP",
        "name": "lofr-lm",
        "port": 1752
    },
    {
        "protocol": "UDP",
        "name": "oracle-em2",
        "port": 1754
    },
    {
        "protocol": "UDP",
        "name": "ms-streaming",
        "port": 1755
    },
    {
        "protocol": "UDP",
        "name": "capfast-lmd",
        "port": 1756
    },
    {
        "protocol": "UDP",
        "name": "cnhrp",
        "port": 1757
    },
    {
        "protocol": "UDP",
        "name": "tftp-mcast",
        "port": 1758
    },
    {
        "protocol": "UDP",
        "name": "spss-lm",
        "port": 1759
    },
    {
        "protocol": "UDP",
        "name": "www-ldap-gw",
        "port": 1760
    },
    {
        "protocol": "UDP",
        "name": "cft-0",
        "port": 1761
    },
    {
        "protocol": "UDP",
        "name": "cft-1",
        "port": 1762
    },
    {
        "protocol": "UDP",
        "name": "cft-2",
        "port": 1763
    },
    {
        "protocol": "UDP",
        "name": "cft-3",
        "port": 1764
    },
    {
        "protocol": "UDP",
        "name": "cft-4",
        "port": 1765
    },
    {
        "protocol": "UDP",
        "name": "cft-5",
        "port": 1766
    },
    {
        "protocol": "UDP",
        "name": "cft-6",
        "port": 1767
    },
    {
        "protocol": "UDP",
        "name": "cft-7",
        "port": 1768
    },
    {
        "protocol": "UDP",
        "name": "bmc-net-adm",
        "port": 1769
    },
    {
        "protocol": "UDP",
        "name": "bmc-net-svc",
        "port": 1770
    },
    {
        "protocol": "UDP",
        "name": "vaultbase",
        "port": 1771
    },
    {
        "protocol": "UDP",
        "name": "essweb-gw",
        "port": 1772
    },
    {
        "protocol": "UDP",
        "name": "kmscontrol",
        "port": 1773
    },
    {
        "protocol": "UDP",
        "name": "global-dtserv",
        "port": 1774
    },
    {
        "protocol": "UDP",
        "name": "femis",
        "port": 1776
    },
    {
        "protocol": "UDP",
        "name": "powerguardian",
        "port": 1777
    },
    {
        "protocol": "UDP",
        "name": "prodigy-intrnet",
        "port": 1778
    },
    {
        "protocol": "UDP",
        "name": "pharmasoft",
        "port": 1779
    },
    {
        "protocol": "UDP",
        "name": "dpkeyserv",
        "port": 1780
    },
    {
        "protocol": "UDP",
        "name": "answersoft-lm",
        "port": 1781
    },
    {
        "protocol": "UDP",
        "name": "hp-hcip",
        "port": 1782
    },
    {
        "protocol": "UDP",
        "name": "finle-lm",
        "port": 1784
    },
    {
        "protocol": "UDP",
        "name": "windlm",
        "port": 1785
    },
    {
        "protocol": "UDP",
        "name": "funk-logger",
        "port": 1786
    },
    {
        "protocol": "UDP",
        "name": "funk-license",
        "port": 1787
    },
    {
        "protocol": "UDP",
        "name": "psmond",
        "port": 1788
    },
    {
        "protocol": "UDP",
        "name": "hello",
        "port": 1789
    },
    {
        "protocol": "UDP",
        "name": "nmsp",
        "port": 1790
    },
    {
        "protocol": "UDP",
        "name": "ea1",
        "port": 1791
    },
    {
        "protocol": "UDP",
        "name": "ibm-dt-2",
        "port": 1792
    },
    {
        "protocol": "UDP",
        "name": "rsc-robot",
        "port": 1793
    },
    {
        "protocol": "UDP",
        "name": "cera-bcm",
        "port": 1794
    },
    {
        "protocol": "UDP",
        "name": "dpi-proxy",
        "port": 1795
    },
    {
        "protocol": "UDP",
        "name": "vocaltec-admin",
        "port": 1796
    },
    {
        "protocol": "UDP",
        "name": "uma",
        "port": 1797
    },
    {
        "protocol": "UDP",
        "name": "etp",
        "port": 1798
    },
    {
        "protocol": "UDP",
        "name": "netrisk",
        "port": 1799
    },
    {
        "protocol": "UDP",
        "name": "ansys-lm",
        "port": 1800
    },
    {
        "protocol": "UDP",
        "name": "msmq",
        "port": 1801
    },
    {
        "protocol": "UDP",
        "name": "concomp1",
        "port": 1802
    },
    {
        "protocol": "UDP",
        "name": "hp-hcip-gwy",
        "port": 1803
    },
    {
        "protocol": "UDP",
        "name": "enl",
        "port": 1804
    },
    {
        "protocol": "UDP",
        "name": "enl-name",
        "port": 1805
    },
    {
        "protocol": "UDP",
        "name": "musiconline",
        "port": 1806
    },
    {
        "protocol": "UDP",
        "name": "fhsp",
        "port": 1807
    },
    {
        "protocol": "UDP",
        "name": "oracle-vp2",
        "port": 1808
    },
    {
        "protocol": "UDP",
        "name": "oracle-vp1",
        "port": 1809
    },
    {
        "protocol": "UDP",
        "name": "jerand-lm",
        "port": 1810
    },
    {
        "protocol": "UDP",
        "name": "scientia-sdb",
        "port": 1811
    },
    {
        "protocol": "UDP",
        "name": "radius",
        "port": 1812
    },
    {
        "protocol": "UDP",
        "name": "radius-acct",
        "port": 1813
    },
    {
        "protocol": "UDP",
        "name": "tdp-suite",
        "port": 1814
    },
    {
        "protocol": "UDP",
        "name": "mmpft",
        "port": 1815
    },
    {
        "protocol": "UDP",
        "name": "harp",
        "port": 1816
    },
    {
        "protocol": "UDP",
        "name": "rkb-oscs",
        "port": 1817
    },
    {
        "protocol": "UDP",
        "name": "etftp",
        "port": 1818
    },
    {
        "protocol": "UDP",
        "name": "plato-lm",
        "port": 1819
    },
    {
        "protocol": "UDP",
        "name": "mcagent",
        "port": 1820
    },
    {
        "protocol": "UDP",
        "name": "donnyworld",
        "port": 1821
    },
    {
        "protocol": "UDP",
        "name": "es-elmd",
        "port": 1822
    },
    {
        "protocol": "UDP",
        "name": "unisys-lm",
        "port": 1823
    },
    {
        "protocol": "UDP",
        "name": "metrics-pas",
        "port": 1824
    },
    {
        "protocol": "UDP",
        "name": "direcpc-video",
        "port": 1825
    },
    {
        "protocol": "UDP",
        "name": "ardt",
        "port": 1826
    },
    {
        "protocol": "UDP",
        "name": "asi",
        "port": 1827
    },
    {
        "protocol": "UDP",
        "name": "itm-mcell-u",
        "port": 1828
    },
    {
        "protocol": "UDP",
        "name": "optika-emedia",
        "port": 1829
    },
    {
        "protocol": "UDP",
        "name": "net8-cman",
        "port": 1830
    },
    {
        "protocol": "UDP",
        "name": "myrtle",
        "port": 1831
    },
    {
        "protocol": "UDP",
        "name": "tht-treasure",
        "port": 1832
    },
    {
        "protocol": "UDP",
        "name": "udpradio",
        "port": 1833
    },
    {
        "protocol": "UDP",
        "name": "ardusuni",
        "port": 1834
    },
    {
        "protocol": "UDP",
        "name": "ardusmul",
        "port": 1835
    },
    {
        "protocol": "UDP",
        "name": "ste-smsc",
        "port": 1836
    },
    {
        "protocol": "UDP",
        "name": "csoft1",
        "port": 1837
    },
    {
        "protocol": "UDP",
        "name": "talnet",
        "port": 1838
    },
    {
        "protocol": "UDP",
        "name": "netopia-vo1",
        "port": 1839
    },
    {
        "protocol": "UDP",
        "name": "netopia-vo2",
        "port": 1840
    },
    {
        "protocol": "UDP",
        "name": "netopia-vo3",
        "port": 1841
    },
    {
        "protocol": "UDP",
        "name": "netopia-vo4",
        "port": 1842
    },
    {
        "protocol": "UDP",
        "name": "netopia-vo5",
        "port": 1843
    },
    {
        "protocol": "UDP",
        "name": "direcpc-dll",
        "port": 1844
    },
    {
        "protocol": "UDP",
        "name": "altalink",
        "port": 1845
    },
    {
        "protocol": "UDP",
        "name": "tunstall-pnc",
        "port": 1846
    },
    {
        "protocol": "UDP",
        "name": "slp-notify",
        "port": 1847
    },
    {
        "protocol": "UDP",
        "name": "fjdocdist",
        "port": 1848
    },
    {
        "protocol": "UDP",
        "name": "alpha-sms",
        "port": 1849
    },
    {
        "protocol": "UDP",
        "name": "gsi",
        "port": 1850
    },
    {
        "protocol": "UDP",
        "name": "ctcd",
        "port": 1851
    },
    {
        "protocol": "UDP",
        "name": "virtual-time",
        "port": 1852
    },
    {
        "protocol": "UDP",
        "name": "vids-avtp",
        "port": 1853
    },
    {
        "protocol": "UDP",
        "name": "buddy-draw",
        "port": 1854
    },
    {
        "protocol": "UDP",
        "name": "fiorano-rtrsvc",
        "port": 1855
    },
    {
        "protocol": "UDP",
        "name": "fiorano-msgsvc",
        "port": 1856
    },
    {
        "protocol": "UDP",
        "name": "datacaptor",
        "port": 1857
    },
    {
        "protocol": "UDP",
        "name": "privateark",
        "port": 1858
    },
    {
        "protocol": "UDP",
        "name": "gammafetchsvr",
        "port": 1859
    },
    {
        "protocol": "UDP",
        "name": "sunscalar-svc",
        "port": 1860
    },
    {
        "protocol": "UDP",
        "name": "lecroy-vicp",
        "port": 1861
    },
    {
        "protocol": "UDP",
        "name": "techra-server",
        "port": 1862
    },
    {
        "protocol": "UDP",
        "name": "msnp",
        "port": 1863
    },
    {
        "protocol": "UDP",
        "name": "paradym-31port",
        "port": 1864
    },
    {
        "protocol": "UDP",
        "name": "entp",
        "port": 1865
    },
    {
        "protocol": "UDP",
        "name": "swrmi",
        "port": 1866
    },
    {
        "protocol": "UDP",
        "name": "udrive",
        "port": 1867
    },
    {
        "protocol": "UDP",
        "name": "viziblebrowser",
        "port": 1868
    },
    {
        "protocol": "UDP",
        "name": "yestrader",
        "port": 1869
    },
    {
        "protocol": "UDP",
        "name": "sunscalar-dns",
        "port": 1870
    },
    {
        "protocol": "UDP",
        "name": "canocentral0",
        "port": 1871
    },
    {
        "protocol": "UDP",
        "name": "canocentral1",
        "port": 1872
    },
    {
        "protocol": "UDP",
        "name": "fjmpjps",
        "port": 1873
    },
    {
        "protocol": "UDP",
        "name": "fjswapsnp",
        "port": 1874
    },
    {
        "protocol": "UDP",
        "name": "westell-stats",
        "port": 1875
    },
    {
        "protocol": "UDP",
        "name": "ewcappsrv",
        "port": 1876
    },
    {
        "protocol": "UDP",
        "name": "hp-webqosdb",
        "port": 1877
    },
    {
        "protocol": "UDP",
        "name": "drmsmc",
        "port": 1878
    },
    {
        "protocol": "UDP",
        "name": "nettgain-nms",
        "port": 1879
    },
    {
        "protocol": "UDP",
        "name": "vsat-control",
        "port": 1880
    },
    {
        "protocol": "UDP",
        "name": "ibm-mqseries2",
        "port": 1881
    },
    {
        "protocol": "UDP",
        "name": "ecsqdmn",
        "port": 1882
    },
    {
        "protocol": "UDP",
        "name": "ibm-mqisdp",
        "port": 1883
    },
    {
        "protocol": "UDP",
        "name": "idmaps",
        "port": 1884
    },
    {
        "protocol": "UDP",
        "name": "vrtstrapserver",
        "port": 1885
    },
    {
        "protocol": "UDP",
        "name": "leoip",
        "port": 1886
    },
    {
        "protocol": "UDP",
        "name": "filex-lport",
        "port": 1887
    },
    {
        "protocol": "UDP",
        "name": "ncconfig",
        "port": 1888
    },
    {
        "protocol": "UDP",
        "name": "unify-adapter",
        "port": 1889
    },
    {
        "protocol": "UDP",
        "name": "wilkenlistener",
        "port": 1890
    },
    {
        "protocol": "UDP",
        "name": "childkey-notif",
        "port": 1891
    },
    {
        "protocol": "UDP",
        "name": "childkey-ctrl",
        "port": 1892
    },
    {
        "protocol": "UDP",
        "name": "elad",
        "port": 1893
    },
    {
        "protocol": "UDP",
        "name": "o2server-port",
        "port": 1894
    },
    {
        "protocol": "UDP",
        "name": "b-novative-ls",
        "port": 1896
    },
    {
        "protocol": "UDP",
        "name": "metaagent",
        "port": 1897
    },
    {
        "protocol": "UDP",
        "name": "cymtec-port",
        "port": 1898
    },
    {
        "protocol": "UDP",
        "name": "mc2studios",
        "port": 1899
    },
    {
        "protocol": "UDP",
        "name": "ssdp",
        "port": 1900
    },
    {
        "protocol": "UDP",
        "name": "fjicl-tep-a",
        "port": 1901
    },
    {
        "protocol": "UDP",
        "name": "fjicl-tep-b",
        "port": 1902
    },
    {
        "protocol": "UDP",
        "name": "linkname",
        "port": 1903
    },
    {
        "protocol": "UDP",
        "name": "fjicl-tep-c",
        "port": 1904
    },
    {
        "protocol": "UDP",
        "name": "sugp",
        "port": 1905
    },
    {
        "protocol": "UDP",
        "name": "tpmd",
        "port": 1906
    },
    {
        "protocol": "UDP",
        "name": "intrastar",
        "port": 1907
    },
    {
        "protocol": "UDP",
        "name": "dawn",
        "port": 1908
    },
    {
        "protocol": "UDP",
        "name": "global-wlink",
        "port": 1909
    },
    {
        "protocol": "UDP",
        "name": "ultrabac",
        "port": 1910
    },
    {
        "protocol": "UDP",
        "name": "mtp",
        "port": 1911
    },
    {
        "protocol": "UDP",
        "name": "rhp-iibp",
        "port": 1912
    },
    {
        "protocol": "UDP",
        "name": "armadp",
        "port": 1913
    },
    {
        "protocol": "UDP",
        "name": "elm-momentum",
        "port": 1914
    },
    {
        "protocol": "UDP",
        "name": "facelink",
        "port": 1915
    },
    {
        "protocol": "UDP",
        "name": "persona",
        "port": 1916
    },
    {
        "protocol": "UDP",
        "name": "noagent",
        "port": 1917
    },
    {
        "protocol": "UDP",
        "name": "can-nds",
        "port": 1918
    },
    {
        "protocol": "UDP",
        "name": "can-dch",
        "port": 1919
    },
    {
        "protocol": "UDP",
        "name": "can-ferret",
        "port": 1920
    },
    {
        "protocol": "UDP",
        "name": "noadmin",
        "port": 1921
    },
    {
        "protocol": "UDP",
        "name": "tapestry",
        "port": 1922
    },
    {
        "protocol": "UDP",
        "name": "spice",
        "port": 1923
    },
    {
        "protocol": "UDP",
        "name": "xiip",
        "port": 1924
    },
    {
        "protocol": "UDP",
        "name": "discovery-port",
        "port": 1925
    },
    {
        "protocol": "UDP",
        "name": "egs",
        "port": 1926
    },
    {
        "protocol": "UDP",
        "name": "videte-cipc",
        "port": 1927
    },
    {
        "protocol": "UDP",
        "name": "emsd-port",
        "port": 1928
    },
    {
        "protocol": "UDP",
        "name": "bandwiz-system",
        "port": 1929
    },
    {
        "protocol": "UDP",
        "name": "driveappserver",
        "port": 1930
    },
    {
        "protocol": "UDP",
        "name": "amdsched",
        "port": 1931
    },
    {
        "protocol": "UDP",
        "name": "ctt-broker",
        "port": 1932
    },
    {
        "protocol": "UDP",
        "name": "xmapi",
        "port": 1933
    },
    {
        "protocol": "UDP",
        "name": "xaapi",
        "port": 1934
    },
    {
        "protocol": "UDP",
        "name": "macromedia-fcs",
        "port": 1935
    },
    {
        "protocol": "UDP",
        "name": "jetcmeserver",
        "port": 1936
    },
    {
        "protocol": "UDP",
        "name": "jwserver",
        "port": 1937
    },
    {
        "protocol": "UDP",
        "name": "jwclient",
        "port": 1938
    },
    {
        "protocol": "UDP",
        "name": "jvserver",
        "port": 1939
    },
    {
        "protocol": "UDP",
        "name": "jvclient",
        "port": 1940
    },
    {
        "protocol": "UDP",
        "name": "dic-aida",
        "port": 1941
    },
    {
        "protocol": "UDP",
        "name": "res",
        "port": 1942
    },
    {
        "protocol": "UDP",
        "name": "beeyond-media",
        "port": 1943
    },
    {
        "protocol": "UDP",
        "name": "close-combat",
        "port": 1944
    },
    {
        "protocol": "UDP",
        "name": "dialogic-elmd",
        "port": 1945
    },
    {
        "protocol": "UDP",
        "name": "tekpls",
        "port": 1946
    },
    {
        "protocol": "UDP",
        "name": "hlserver",
        "port": 1947
    },
    {
        "protocol": "UDP",
        "name": "eye2eye",
        "port": 1948
    },
    {
        "protocol": "UDP",
        "name": "ismaeasdaqlive",
        "port": 1949
    },
    {
        "protocol": "UDP",
        "name": "ismaeasdaqtest",
        "port": 1950
    },
    {
        "protocol": "UDP",
        "name": "bcs-lmserver",
        "port": 1951
    },
    {
        "protocol": "UDP",
        "name": "mpnjsc",
        "port": 1952
    },
    {
        "protocol": "UDP",
        "name": "rapidbase",
        "port": 1953
    },
    {
        "protocol": "UDP",
        "name": "abr-basic",
        "port": 1954
    },
    {
        "protocol": "UDP",
        "name": "abr-secure",
        "port": 1955
    },
    {
        "protocol": "UDP",
        "name": "vrtl-vmf-ds",
        "port": 1956
    },
    {
        "protocol": "UDP",
        "name": "unix-status",
        "port": 1957
    },
    {
        "protocol": "UDP",
        "name": "dxadmind",
        "port": 1958
    },
    {
        "protocol": "UDP",
        "name": "simp-all",
        "port": 1959
    },
    {
        "protocol": "UDP",
        "name": "nasmanager",
        "port": 1960
    },
    {
        "protocol": "UDP",
        "name": "bts-appserver",
        "port": 1961
    },
    {
        "protocol": "UDP",
        "name": "biap-mp",
        "port": 1962
    },
    {
        "protocol": "UDP",
        "name": "webmachine",
        "port": 1963
    },
    {
        "protocol": "UDP",
        "name": "solid-e-engine",
        "port": 1964
    },
    {
        "protocol": "UDP",
        "name": "tivoli-npm",
        "port": 1965
    },
    {
        "protocol": "UDP",
        "name": "slush",
        "port": 1966
    },
    {
        "protocol": "UDP",
        "name": "sns-quote",
        "port": 1967
    },
    {
        "protocol": "UDP",
        "name": "lipsinc",
        "port": 1968
    },
    {
        "protocol": "UDP",
        "name": "lipsinc1",
        "port": 1969
    },
    {
        "protocol": "UDP",
        "name": "netop-rc",
        "port": 1970
    },
    {
        "protocol": "UDP",
        "name": "netop-school",
        "port": 1971
    },
    {
        "protocol": "UDP",
        "name": "intersys-cache",
        "port": 1972
    },
    {
        "protocol": "UDP",
        "name": "dlsrap",
        "port": 1973
    },
    {
        "protocol": "UDP",
        "name": "drp",
        "port": 1974
    },
    {
        "protocol": "UDP",
        "name": "tcoflashagent",
        "port": 1975
    },
    {
        "protocol": "UDP",
        "name": "tcoregagent",
        "port": 1976
    },
    {
        "protocol": "UDP",
        "name": "tcoaddressbook",
        "port": 1977
    },
    {
        "protocol": "UDP",
        "name": "unisql",
        "port": 1978
    },
    {
        "protocol": "UDP",
        "name": "unisql-java",
        "port": 1979
    },
    {
        "protocol": "UDP",
        "name": "pearldoc-xact",
        "port": 1980
    },
    {
        "protocol": "UDP",
        "name": "p2pq",
        "port": 1981
    },
    {
        "protocol": "UDP",
        "name": "estamp",
        "port": 1982
    },
    {
        "protocol": "UDP",
        "name": "lhtp",
        "port": 1983
    },
    {
        "protocol": "UDP",
        "name": "bb",
        "port": 1984
    },
    {
        "protocol": "UDP",
        "name": "hsrp",
        "port": 1985
    },
    {
        "protocol": "UDP",
        "name": "licensedaemon",
        "port": 1986
    },
    {
        "protocol": "UDP",
        "name": "tr-rsrb-p1",
        "port": 1987
    },
    {
        "protocol": "UDP",
        "name": "tr-rsrb-p2",
        "port": 1988
    },
    {
        "protocol": "UDP",
        "name": "tr-rsrb-p3",
        "port": 1989
    },
    {
        "protocol": "UDP",
        "name": "mshnet",
        "port": 1989
    },
    {
        "protocol": "UDP",
        "name": "stun-p1",
        "port": 1990
    },
    {
        "protocol": "UDP",
        "name": "stun-p2",
        "port": 1991
    },
    {
        "protocol": "UDP",
        "name": "ipsendmsg",
        "port": 1992
    },
    {
        "protocol": "UDP",
        "name": "snmp-tcp-port",
        "port": 1993
    },
    {
        "protocol": "UDP",
        "name": "stun-port",
        "port": 1994
    },
    {
        "protocol": "UDP",
        "name": "perf-port",
        "port": 1995
    },
    {
        "protocol": "UDP",
        "name": "tr-rsrb-port",
        "port": 1996
    },
    {
        "protocol": "UDP",
        "name": "gdp-port",
        "port": 1997
    },
    {
        "protocol": "UDP",
        "name": "x25-svc-port",
        "port": 1998
    },
    {
        "protocol": "UDP",
        "name": "tcp-id-port",
        "port": 1999
    },
    {
        "protocol": "UDP",
        "name": "cisco-sccp",
        "port": 2000
    },
    {
        "protocol": "UDP",
        "name": "wizard",
        "port": 2001
    },
    {
        "protocol": "UDP",
        "name": "globe",
        "port": 2002
    },
    {
        "protocol": "UDP",
        "name": "emce",
        "port": 2004
    },
    {
        "protocol": "UDP",
        "name": "oracle",
        "port": 2005
    },
    {
        "protocol": "UDP",
        "name": "raid-cd",
        "port": 2006
    },
    {
        "protocol": "UDP",
        "name": "raid-am",
        "port": 2007
    },
    {
        "protocol": "UDP",
        "name": "terminaldb",
        "port": 2008
    },
    {
        "protocol": "UDP",
        "name": "whosockami",
        "port": 2009
    },
    {
        "protocol": "UDP",
        "name": "pipe_server",
        "port": 2010
    },
    {
        "protocol": "UDP",
        "name": "servserv",
        "port": 2011
    },
    {
        "protocol": "UDP",
        "name": "raid-ac",
        "port": 2012
    },
    {
        "protocol": "UDP",
        "name": "raid-cd",
        "port": 2013
    },
    {
        "protocol": "UDP",
        "name": "raid-sf",
        "port": 2014
    },
    {
        "protocol": "UDP",
        "name": "raid-cs",
        "port": 2015
    },
    {
        "protocol": "UDP",
        "name": "bootserver",
        "port": 2016
    },
    {
        "protocol": "UDP",
        "name": "bootclient",
        "port": 2017
    },
    {
        "protocol": "UDP",
        "name": "rellpack",
        "port": 2018
    },
    {
        "protocol": "UDP",
        "name": "about",
        "port": 2019
    },
    {
        "protocol": "UDP",
        "name": "xinupageserver",
        "port": 2020
    },
    {
        "protocol": "UDP",
        "name": "xinuexpansion1",
        "port": 2021
    },
    {
        "protocol": "UDP",
        "name": "xinuexpansion2",
        "port": 2022
    },
    {
        "protocol": "UDP",
        "name": "xinuexpansion3",
        "port": 2023
    },
    {
        "protocol": "UDP",
        "name": "xinuexpansion4",
        "port": 2024
    },
    {
        "protocol": "UDP",
        "name": "xribs",
        "port": 2025
    },
    {
        "protocol": "UDP",
        "name": "scrabble",
        "port": 2026
    },
    {
        "protocol": "UDP",
        "name": "shadowserver",
        "port": 2027
    },
    {
        "protocol": "UDP",
        "name": "submitserver",
        "port": 2028
    },
    {
        "protocol": "UDP",
        "name": "hsrpv6",
        "port": 2029
    },
    {
        "protocol": "UDP",
        "name": "device2",
        "port": 2030
    },
    {
        "protocol": "UDP",
        "name": "mobrien-chat",
        "port": 2031
    },
    {
        "protocol": "UDP",
        "name": "blackboard",
        "port": 2032
    },
    {
        "protocol": "UDP",
        "name": "glogger",
        "port": 2033
    },
    {
        "protocol": "UDP",
        "name": "scoremgr",
        "port": 2034
    },
    {
        "protocol": "UDP",
        "name": "imsldoc",
        "port": 2035
    },
    {
        "protocol": "UDP",
        "name": "e-dpnet",
        "port": 2036
    },
    {
        "protocol": "UDP",
        "name": "p2plus",
        "port": 2037
    },
    {
        "protocol": "UDP",
        "name": "objectmanager",
        "port": 2038
    },
    {
        "protocol": "UDP",
        "name": "prizma",
        "port": 2039
    },
    {
        "protocol": "UDP",
        "name": "lam",
        "port": 2040
    },
    {
        "protocol": "UDP",
        "name": "interbase",
        "port": 2041
    },
    {
        "protocol": "UDP",
        "name": "isis",
        "port": 2042
    },
    {
        "protocol": "UDP",
        "name": "isis-bcast",
        "port": 2043
    },
    {
        "protocol": "UDP",
        "name": "rimsl",
        "port": 2044
    },
    {
        "protocol": "UDP",
        "name": "cdfunc",
        "port": 2045
    },
    {
        "protocol": "UDP",
        "name": "sdfunc",
        "port": 2046
    },
    {
        "protocol": "UDP",
        "name": "dls",
        "port": 2047
    },
    {
        "protocol": "UDP",
        "name": "dls-monitor",
        "port": 2048
    },
    {
        "protocol": "UDP",
        "name": "nfs",
        "port": 2049
    },
    {
        "protocol": "UDP",
        "name": "av-emb-config",
        "port": 2050
    },
    {
        "protocol": "UDP",
        "name": "epnsdp",
        "port": 2051
    },
    {
        "protocol": "UDP",
        "name": "clearvisn",
        "port": 2052
    },
    {
        "protocol": "UDP",
        "name": "lot105-ds-upd",
        "port": 2053
    },
    {
        "protocol": "UDP",
        "name": "weblogin",
        "port": 2054
    },
    {
        "protocol": "UDP",
        "name": "iop",
        "port": 2055
    },
    {
        "protocol": "UDP",
        "name": "omnisky",
        "port": 2056
    },
    {
        "protocol": "UDP",
        "name": "rich-cp",
        "port": 2057
    },
    {
        "protocol": "UDP",
        "name": "newwavesearch",
        "port": 2058
    },
    {
        "protocol": "UDP",
        "name": "bmc-messaging",
        "port": 2059
    },
    {
        "protocol": "UDP",
        "name": "teleniumdaemon",
        "port": 2060
    },
    {
        "protocol": "UDP",
        "name": "netmount",
        "port": 2061
    },
    {
        "protocol": "UDP",
        "name": "icg-swp",
        "port": 2062
    },
    {
        "protocol": "UDP",
        "name": "icg-bridge",
        "port": 2063
    },
    {
        "protocol": "UDP",
        "name": "icg-iprelay",
        "port": 2064
    },
    {
        "protocol": "UDP",
        "name": "dlsrpn",
        "port": 2065
    },
    {
        "protocol": "UDP",
        "name": "dlswpn",
        "port": 2067
    },
    {
        "protocol": "UDP",
        "name": "avauthsrvprtcl",
        "port": 2068
    },
    {
        "protocol": "UDP",
        "name": "event-port",
        "port": 2069
    },
    {
        "protocol": "UDP",
        "name": "ah-esp-encap",
        "port": 2070
    },
    {
        "protocol": "UDP",
        "name": "acp-port",
        "port": 2071
    },
    {
        "protocol": "UDP",
        "name": "msync",
        "port": 2072
    },
    {
        "protocol": "UDP",
        "name": "gxs-data-port",
        "port": 2073
    },
    {
        "protocol": "UDP",
        "name": "vrtl-vmf-sa",
        "port": 2074
    },
    {
        "protocol": "UDP",
        "name": "newlixengine",
        "port": 2075
    },
    {
        "protocol": "UDP",
        "name": "newlixconfig",
        "port": 2076
    },
    {
        "protocol": "UDP",
        "name": "trellisagt",
        "port": 2077
    },
    {
        "protocol": "UDP",
        "name": "trellissvr",
        "port": 2078
    },
    {
        "protocol": "UDP",
        "name": "idware-router",
        "port": 2079
    },
    {
        "protocol": "UDP",
        "name": "autodesk-nlm",
        "port": 2080
    },
    {
        "protocol": "UDP",
        "name": "kme-trap-port",
        "port": 2081
    },
    {
        "protocol": "UDP",
        "name": "infowave",
        "port": 2082
    },
    {
        "protocol": "UDP",
        "name": "radsec",
        "port": 2083
    },
    {
        "protocol": "UDP",
        "name": "sunclustergeo",
        "port": 2084
    },
    {
        "protocol": "UDP",
        "name": "ada-cip",
        "port": 2085
    },
    {
        "protocol": "UDP",
        "name": "gnunet",
        "port": 2086
    },
    {
        "protocol": "UDP",
        "name": "eli",
        "port": 2087
    },
    {
        "protocol": "UDP",
        "name": "ip-blf",
        "port": 2088
    },
    {
        "protocol": "UDP",
        "name": "sep",
        "port": 2089
    },
    {
        "protocol": "UDP",
        "name": "lrp",
        "port": 2090
    },
    {
        "protocol": "UDP",
        "name": "prp",
        "port": 2091
    },
    {
        "protocol": "UDP",
        "name": "descent3",
        "port": 2092
    },
    {
        "protocol": "UDP",
        "name": "nbx-cc",
        "port": 2093
    },
    {
        "protocol": "UDP",
        "name": "nbx-au",
        "port": 2094
    },
    {
        "protocol": "UDP",
        "name": "nbx-ser",
        "port": 2095
    },
    {
        "protocol": "UDP",
        "name": "nbx-dir",
        "port": 2096
    },
    {
        "protocol": "UDP",
        "name": "jetformpreview",
        "port": 2097
    },
    {
        "protocol": "UDP",
        "name": "dialog-port",
        "port": 2098
    },
    {
        "protocol": "UDP",
        "name": "h2250-annex-g",
        "port": 2099
    },
    {
        "protocol": "UDP",
        "name": "amiganetfs",
        "port": 2100
    },
    {
        "protocol": "UDP",
        "name": "rtcm-sc104",
        "port": 2101
    },
    {
        "protocol": "UDP",
        "name": "zephyr-srv",
        "port": 2102
    },
    {
        "protocol": "UDP",
        "name": "zephyr-clt",
        "port": 2103
    },
    {
        "protocol": "UDP",
        "name": "zephyr-hm",
        "port": 2104
    },
    {
        "protocol": "UDP",
        "name": "minipay",
        "port": 2105
    },
    {
        "protocol": "UDP",
        "name": "mzap",
        "port": 2106
    },
    {
        "protocol": "UDP",
        "name": "bintec-admin",
        "port": 2107
    },
    {
        "protocol": "UDP",
        "name": "comcam",
        "port": 2108
    },
    {
        "protocol": "UDP",
        "name": "ergolight",
        "port": 2109
    },
    {
        "protocol": "UDP",
        "name": "umsp",
        "port": 2110
    },
    {
        "protocol": "UDP",
        "name": "dsatp",
        "port": 2111
    },
    {
        "protocol": "UDP",
        "name": "idonix-metanet",
        "port": 2112
    },
    {
        "protocol": "UDP",
        "name": "hsl-storm",
        "port": 2113
    },
    {
        "protocol": "UDP",
        "name": "newheights",
        "port": 2114
    },
    {
        "protocol": "UDP",
        "name": "kdm",
        "port": 2115
    },
    {
        "protocol": "UDP",
        "name": "ccowcmr",
        "port": 2116
    },
    {
        "protocol": "UDP",
        "name": "mentaclient",
        "port": 2117
    },
    {
        "protocol": "UDP",
        "name": "mentaserver",
        "port": 2118
    },
    {
        "protocol": "UDP",
        "name": "gsigatekeeper",
        "port": 2119
    },
    {
        "protocol": "UDP",
        "name": "qencp",
        "port": 2120
    },
    {
        "protocol": "UDP",
        "name": "scientia-ssdb",
        "port": 2121
    },
    {
        "protocol": "UDP",
        "name": "caupc-remote",
        "port": 2122
    },
    {
        "protocol": "UDP",
        "name": "gtp-control",
        "port": 2123
    },
    {
        "protocol": "UDP",
        "name": "elatelink",
        "port": 2124
    },
    {
        "protocol": "UDP",
        "name": "lockstep",
        "port": 2125
    },
    {
        "protocol": "UDP",
        "name": "pktcable-cops",
        "port": 2126
    },
    {
        "protocol": "UDP",
        "name": "index-pc-wb",
        "port": 2127
    },
    {
        "protocol": "UDP",
        "name": "net-steward",
        "port": 2128
    },
    {
        "protocol": "UDP",
        "name": "cs-live",
        "port": 2129
    },
    {
        "protocol": "UDP",
        "name": "swc-xds",
        "port": 2130
    },
    {
        "protocol": "UDP",
        "name": "avantageb2b",
        "port": 2131
    },
    {
        "protocol": "UDP",
        "name": "avail-epmap",
        "port": 2132
    },
    {
        "protocol": "UDP",
        "name": "zymed-zpp",
        "port": 2133
    },
    {
        "protocol": "UDP",
        "name": "avenue",
        "port": 2134
    },
    {
        "protocol": "UDP",
        "name": "gris",
        "port": 2135
    },
    {
        "protocol": "UDP",
        "name": "appworxsrv",
        "port": 2136
    },
    {
        "protocol": "UDP",
        "name": "connect",
        "port": 2137
    },
    {
        "protocol": "UDP",
        "name": "unbind-cluster",
        "port": 2138
    },
    {
        "protocol": "UDP",
        "name": "ias-auth",
        "port": 2139
    },
    {
        "protocol": "UDP",
        "name": "ias-reg",
        "port": 2140
    },
    {
        "protocol": "UDP",
        "name": "ias-admind",
        "port": 2141
    },
    {
        "protocol": "UDP",
        "name": "tdm-over-ip",
        "port": 2142
    },
    {
        "protocol": "UDP",
        "name": "lv-jc",
        "port": 2143
    },
    {
        "protocol": "UDP",
        "name": "lv-ffx",
        "port": 2144
    },
    {
        "protocol": "UDP",
        "name": "lv-pici",
        "port": 2145
    },
    {
        "protocol": "UDP",
        "name": "lv-not",
        "port": 2146
    },
    {
        "protocol": "UDP",
        "name": "lv-auth",
        "port": 2147
    },
    {
        "protocol": "UDP",
        "name": "veritas-ucl",
        "port": 2148
    },
    {
        "protocol": "UDP",
        "name": "acptsys",
        "port": 2149
    },
    {
        "protocol": "UDP",
        "name": "dynamic3d",
        "port": 2150
    },
    {
        "protocol": "UDP",
        "name": "docent",
        "port": 2151
    },
    {
        "protocol": "UDP",
        "name": "gtp-user",
        "port": 2152
    },
    {
        "protocol": "UDP",
        "name": "gdbremote",
        "port": 2159
    },
    {
        "protocol": "UDP",
        "name": "apc-2160",
        "port": 2160
    },
    {
        "protocol": "UDP",
        "name": "apc-2161",
        "port": 2161
    },
    {
        "protocol": "UDP",
        "name": "navisphere",
        "port": 2162
    },
    {
        "protocol": "UDP",
        "name": "navisphere-sec",
        "port": 2163
    },
    {
        "protocol": "UDP",
        "name": "ddns-v3",
        "port": 2164
    },
    {
        "protocol": "UDP",
        "name": "x-bone-api",
        "port": 2165
    },
    {
        "protocol": "UDP",
        "name": "iwserver",
        "port": 2166
    },
    {
        "protocol": "UDP",
        "name": "raw-serial",
        "port": 2167
    },
    {
        "protocol": "UDP",
        "name": "easy-soft-mux",
        "port": 2168
    },
    {
        "protocol": "UDP",
        "name": "archisfcp",
        "port": 2169
    },
    {
        "protocol": "UDP",
        "name": "eyetv",
        "port": 2170
    },
    {
        "protocol": "UDP",
        "name": "msfw-storage",
        "port": 2171
    },
    {
        "protocol": "UDP",
        "name": "msfw-s-storage",
        "port": 2172
    },
    {
        "protocol": "UDP",
        "name": "msfw-replica",
        "port": 2173
    },
    {
        "protocol": "UDP",
        "name": "msfw-array",
        "port": 2174
    },
    {
        "protocol": "UDP",
        "name": "airsync",
        "port": 2175
    },
    {
        "protocol": "UDP",
        "name": "rapi",
        "port": 2176
    },
    {
        "protocol": "UDP",
        "name": "qwave",
        "port": 2177
    },
    {
        "protocol": "UDP",
        "name": "bitspeer",
        "port": 2178
    },
    {
        "protocol": "UDP",
        "name": "mc-gt-srv",
        "port": 2180
    },
    {
        "protocol": "UDP",
        "name": "eforward",
        "port": 2181
    },
    {
        "protocol": "UDP",
        "name": "cgn-stat",
        "port": 2182
    },
    {
        "protocol": "UDP",
        "name": "cgn-config",
        "port": 2183
    },
    {
        "protocol": "UDP",
        "name": "nvd",
        "port": 2184
    },
    {
        "protocol": "UDP",
        "name": "onbase-dds",
        "port": 2185
    },
    {
        "protocol": "UDP",
        "name": "tivoconnect",
        "port": 2190
    },
    {
        "protocol": "UDP",
        "name": "tvbus",
        "port": 2191
    },
    {
        "protocol": "UDP",
        "name": "asdis",
        "port": 2192
    },
    {
        "protocol": "UDP",
        "name": "mnp-exchange",
        "port": 2197
    },
    {
        "protocol": "UDP",
        "name": "onehome-remote",
        "port": 2198
    },
    {
        "protocol": "UDP",
        "name": "onehome-help",
        "port": 2199
    },
    {
        "protocol": "UDP",
        "name": "ici",
        "port": 2200
    },
    {
        "protocol": "UDP",
        "name": "ats",
        "port": 2201
    },
    {
        "protocol": "UDP",
        "name": "imtc-map",
        "port": 2202
    },
    {
        "protocol": "UDP",
        "name": "b2-runtime",
        "port": 2203
    },
    {
        "protocol": "UDP",
        "name": "b2-license",
        "port": 2204
    },
    {
        "protocol": "UDP",
        "name": "jps",
        "port": 2205
    },
    {
        "protocol": "UDP",
        "name": "hpocbus",
        "port": 2206
    },
    {
        "protocol": "UDP",
        "name": "kali",
        "port": 2213
    },
    {
        "protocol": "UDP",
        "name": "rpi",
        "port": 2214
    },
    {
        "protocol": "UDP",
        "name": "ipcore",
        "port": 2215
    },
    {
        "protocol": "UDP",
        "name": "vtu-comms",
        "port": 2216
    },
    {
        "protocol": "UDP",
        "name": "gotodevice",
        "port": 2217
    },
    {
        "protocol": "UDP",
        "name": "bounzza",
        "port": 2218
    },
    {
        "protocol": "UDP",
        "name": "netiq-ncap",
        "port": 2219
    },
    {
        "protocol": "UDP",
        "name": "netiq",
        "port": 2220
    },
    {
        "protocol": "UDP",
        "name": "rockwell-csp1",
        "port": 2221
    },
    {
        "protocol": "UDP",
        "name": "rockwell-csp2",
        "port": 2222
    },
    {
        "protocol": "UDP",
        "name": "rockwell-csp3",
        "port": 2223
    },
    {
        "protocol": "UDP",
        "name": "di-drm",
        "port": 2226
    },
    {
        "protocol": "UDP",
        "name": "di-msg",
        "port": 2227
    },
    {
        "protocol": "UDP",
        "name": "ehome-ms",
        "port": 2228
    },
    {
        "protocol": "UDP",
        "name": "datalens",
        "port": 2229
    },
    {
        "protocol": "UDP",
        "name": "ivs-video",
        "port": 2232
    },
    {
        "protocol": "UDP",
        "name": "infocrypt",
        "port": 2233
    },
    {
        "protocol": "UDP",
        "name": "directplay",
        "port": 2234
    },
    {
        "protocol": "UDP",
        "name": "sercomm-wlink",
        "port": 2235
    },
    {
        "protocol": "UDP",
        "name": "nani",
        "port": 2236
    },
    {
        "protocol": "UDP",
        "name": "optech-port1-lm",
        "port": 2237
    },
    {
        "protocol": "UDP",
        "name": "aviva-sna",
        "port": 2238
    },
    {
        "protocol": "UDP",
        "name": "imagequery",
        "port": 2239
    },
    {
        "protocol": "UDP",
        "name": "recipe",
        "port": 2240
    },
    {
        "protocol": "UDP",
        "name": "ivsd",
        "port": 2241
    },
    {
        "protocol": "UDP",
        "name": "foliocorp",
        "port": 2242
    },
    {
        "protocol": "UDP",
        "name": "magicom",
        "port": 2243
    },
    {
        "protocol": "UDP",
        "name": "nmsserver",
        "port": 2244
    },
    {
        "protocol": "UDP",
        "name": "hao",
        "port": 2245
    },
    {
        "protocol": "UDP",
        "name": "pc-mta-addrmap",
        "port": 2246
    },
    {
        "protocol": "UDP",
        "name": "antidotemgrsvr",
        "port": 2247
    },
    {
        "protocol": "UDP",
        "name": "ums",
        "port": 2248
    },
    {
        "protocol": "UDP",
        "name": "rfmp",
        "port": 2249
    },
    {
        "protocol": "UDP",
        "name": "remote-collab",
        "port": 2250
    },
    {
        "protocol": "UDP",
        "name": "dif-port",
        "port": 2251
    },
    {
        "protocol": "UDP",
        "name": "njenet-ssl",
        "port": 2252
    },
    {
        "protocol": "UDP",
        "name": "dtv-chan-req",
        "port": 2253
    },
    {
        "protocol": "UDP",
        "name": "seispoc",
        "port": 2254
    },
    {
        "protocol": "UDP",
        "name": "vrtp",
        "port": 2255
    },
    {
        "protocol": "UDP",
        "name": "pcc-mfp",
        "port": 2256
    },
    {
        "protocol": "UDP",
        "name": "apc-2260",
        "port": 2260
    },
    {
        "protocol": "UDP",
        "name": "comotionmaster",
        "port": 2261
    },
    {
        "protocol": "UDP",
        "name": "comotionback",
        "port": 2262
    },
    {
        "protocol": "UDP",
        "name": "mfserver",
        "port": 2266
    },
    {
        "protocol": "UDP",
        "name": "ontobroker",
        "port": 2267
    },
    {
        "protocol": "UDP",
        "name": "amt",
        "port": 2268
    },
    {
        "protocol": "UDP",
        "name": "mikey",
        "port": 2269
    },
    {
        "protocol": "UDP",
        "name": "starschool",
        "port": 2270
    },
    {
        "protocol": "UDP",
        "name": "mmcals",
        "port": 2271
    },
    {
        "protocol": "UDP",
        "name": "mmcal",
        "port": 2272
    },
    {
        "protocol": "UDP",
        "name": "mysql-im",
        "port": 2273
    },
    {
        "protocol": "UDP",
        "name": "pcttunnell",
        "port": 2274
    },
    {
        "protocol": "UDP",
        "name": "ibridge-data",
        "port": 2275
    },
    {
        "protocol": "UDP",
        "name": "ibridge-mgmt",
        "port": 2276
    },
    {
        "protocol": "UDP",
        "name": "bluectrlproxy",
        "port": 2277
    },
    {
        "protocol": "UDP",
        "name": "xmquery",
        "port": 2279
    },
    {
        "protocol": "UDP",
        "name": "lnvpoller",
        "port": 2280
    },
    {
        "protocol": "UDP",
        "name": "lnvconsole",
        "port": 2281
    },
    {
        "protocol": "UDP",
        "name": "lnvalarm",
        "port": 2282
    },
    {
        "protocol": "UDP",
        "name": "lnvstatus",
        "port": 2283
    },
    {
        "protocol": "UDP",
        "name": "lnvmaps",
        "port": 2284
    },
    {
        "protocol": "UDP",
        "name": "lnvmailmon",
        "port": 2285
    },
    {
        "protocol": "UDP",
        "name": "nas-metering",
        "port": 2286
    },
    {
        "protocol": "UDP",
        "name": "dna",
        "port": 2287
    },
    {
        "protocol": "UDP",
        "name": "netml",
        "port": 2288
    },
    {
        "protocol": "UDP",
        "name": "dict-lookup",
        "port": 2289
    },
    {
        "protocol": "UDP",
        "name": "sonus-logging",
        "port": 2290
    },
    {
        "protocol": "UDP",
        "name": "konshus-lm",
        "port": 2294
    },
    {
        "protocol": "UDP",
        "name": "advant-lm",
        "port": 2295
    },
    {
        "protocol": "UDP",
        "name": "theta-lm",
        "port": 2296
    },
    {
        "protocol": "UDP",
        "name": "d2k-datamover1",
        "port": 2297
    },
    {
        "protocol": "UDP",
        "name": "d2k-datamover2",
        "port": 2298
    },
    {
        "protocol": "UDP",
        "name": "pc-telecommute",
        "port": 2299
    },
    {
        "protocol": "UDP",
        "name": "cvmmon",
        "port": 2300
    },
    {
        "protocol": "UDP",
        "name": "cpq-wbem",
        "port": 2301
    },
    {
        "protocol": "UDP",
        "name": "binderysupport",
        "port": 2302
    },
    {
        "protocol": "UDP",
        "name": "proxy-gateway",
        "port": 2303
    },
    {
        "protocol": "UDP",
        "name": "attachmate-uts",
        "port": 2304
    },
    {
        "protocol": "UDP",
        "name": "mt-scaleserver",
        "port": 2305
    },
    {
        "protocol": "UDP",
        "name": "tappi-boxnet",
        "port": 2306
    },
    {
        "protocol": "UDP",
        "name": "pehelp",
        "port": 2307
    },
    {
        "protocol": "UDP",
        "name": "sdhelp",
        "port": 2308
    },
    {
        "protocol": "UDP",
        "name": "sdserver",
        "port": 2309
    },
    {
        "protocol": "UDP",
        "name": "sdclient",
        "port": 2310
    },
    {
        "protocol": "UDP",
        "name": "messageservice",
        "port": 2311
    },
    {
        "protocol": "UDP",
        "name": "iapp",
        "port": 2313
    },
    {
        "protocol": "UDP",
        "name": "cr-websystems",
        "port": 2314
    },
    {
        "protocol": "UDP",
        "name": "precise-sft",
        "port": 2315
    },
    {
        "protocol": "UDP",
        "name": "sent-lm",
        "port": 2316
    },
    {
        "protocol": "UDP",
        "name": "attachmate-g32",
        "port": 2317
    },
    {
        "protocol": "UDP",
        "name": "cadencecontrol",
        "port": 2318
    },
    {
        "protocol": "UDP",
        "name": "infolibria",
        "port": 2319
    },
    {
        "protocol": "UDP",
        "name": "siebel-ns",
        "port": 2320
    },
    {
        "protocol": "UDP",
        "name": "rdlap",
        "port": 2321
    },
    {
        "protocol": "UDP",
        "name": "ofsd",
        "port": 2322
    },
    {
        "protocol": "UDP",
        "name": "3d-nfsd",
        "port": 2323
    },
    {
        "protocol": "UDP",
        "name": "cosmocall",
        "port": 2324
    },
    {
        "protocol": "UDP",
        "name": "designspace-lm",
        "port": 2325
    },
    {
        "protocol": "UDP",
        "name": "idcp",
        "port": 2326
    },
    {
        "protocol": "UDP",
        "name": "xingcsm",
        "port": 2327
    },
    {
        "protocol": "UDP",
        "name": "netrix-sftm",
        "port": 2328
    },
    {
        "protocol": "UDP",
        "name": "nvd",
        "port": 2329
    },
    {
        "protocol": "UDP",
        "name": "tscchat",
        "port": 2330
    },
    {
        "protocol": "UDP",
        "name": "agentview",
        "port": 2331
    },
    {
        "protocol": "UDP",
        "name": "rcc-host",
        "port": 2332
    },
    {
        "protocol": "UDP",
        "name": "snapp",
        "port": 2333
    },
    {
        "protocol": "UDP",
        "name": "ace-client",
        "port": 2334
    },
    {
        "protocol": "UDP",
        "name": "ace-proxy",
        "port": 2335
    },
    {
        "protocol": "UDP",
        "name": "appleugcontrol",
        "port": 2336
    },
    {
        "protocol": "UDP",
        "name": "ideesrv",
        "port": 2337
    },
    {
        "protocol": "UDP",
        "name": "norton-lambert",
        "port": 2338
    },
    {
        "protocol": "UDP",
        "name": "3com-webview",
        "port": 2339
    },
    {
        "protocol": "UDP",
        "name": "wrs_registry",
        "port": 2340
    },
    {
        "protocol": "UDP",
        "name": "xiostatus",
        "port": 2341
    },
    {
        "protocol": "UDP",
        "name": "manage-exec",
        "port": 2342
    },
    {
        "protocol": "UDP",
        "name": "nati-logos",
        "port": 2343
    },
    {
        "protocol": "UDP",
        "name": "fcmsys",
        "port": 2344
    },
    {
        "protocol": "UDP",
        "name": "dbm",
        "port": 2345
    },
    {
        "protocol": "UDP",
        "name": "redstorm_join",
        "port": 2346
    },
    {
        "protocol": "UDP",
        "name": "redstorm_find",
        "port": 2347
    },
    {
        "protocol": "UDP",
        "name": "redstorm_info",
        "port": 2348
    },
    {
        "protocol": "UDP",
        "name": "redstorm_diag",
        "port": 2349
    },
    {
        "protocol": "UDP",
        "name": "psbserver",
        "port": 2350
    },
    {
        "protocol": "UDP",
        "name": "psrserver",
        "port": 2351
    },
    {
        "protocol": "UDP",
        "name": "pslserver",
        "port": 2352
    },
    {
        "protocol": "UDP",
        "name": "pspserver",
        "port": 2353
    },
    {
        "protocol": "UDP",
        "name": "psprserver",
        "port": 2354
    },
    {
        "protocol": "UDP",
        "name": "psdbserver",
        "port": 2355
    },
    {
        "protocol": "UDP",
        "name": "gxtelmd",
        "port": 2356
    },
    {
        "protocol": "UDP",
        "name": "unihub-server",
        "port": 2357
    },
    {
        "protocol": "UDP",
        "name": "futrix",
        "port": 2358
    },
    {
        "protocol": "UDP",
        "name": "flukeserver",
        "port": 2359
    },
    {
        "protocol": "UDP",
        "name": "nexstorindltd",
        "port": 2360
    },
    {
        "protocol": "UDP",
        "name": "tl1",
        "port": 2361
    },
    {
        "protocol": "UDP",
        "name": "digiman",
        "port": 2362
    },
    {
        "protocol": "UDP",
        "name": "mediacntrlnfsd",
        "port": 2363
    },
    {
        "protocol": "UDP",
        "name": "oi-2000",
        "port": 2364
    },
    {
        "protocol": "UDP",
        "name": "dbref",
        "port": 2365
    },
    {
        "protocol": "UDP",
        "name": "qip-login",
        "port": 2366
    },
    {
        "protocol": "UDP",
        "name": "service-ctrl",
        "port": 2367
    },
    {
        "protocol": "UDP",
        "name": "opentable",
        "port": 2368
    },
    {
        "protocol": "UDP",
        "name": "acs2000-dsp",
        "port": 2369
    },
    {
        "protocol": "UDP",
        "name": "l3-hbmon",
        "port": 2370
    },
    {
        "protocol": "UDP",
        "name": "worldwire",
        "port": 2371
    },
    {
        "protocol": "UDP",
        "name": "compaq-https",
        "port": 2381
    },
    {
        "protocol": "UDP",
        "name": "ms-olap3",
        "port": 2382
    },
    {
        "protocol": "UDP",
        "name": "ms-olap4",
        "port": 2383
    },
    {
        "protocol": "UDP",
        "name": "sd-capacity",
        "port": 2384
    },
    {
        "protocol": "UDP",
        "name": "sd-data",
        "port": 2385
    },
    {
        "protocol": "UDP",
        "name": "virtualtape",
        "port": 2386
    },
    {
        "protocol": "UDP",
        "name": "vsamredirector",
        "port": 2387
    },
    {
        "protocol": "UDP",
        "name": "mynahautostart",
        "port": 2388
    },
    {
        "protocol": "UDP",
        "name": "ovsessionmgr",
        "port": 2389
    },
    {
        "protocol": "UDP",
        "name": "rsmtp",
        "port": 2390
    },
    {
        "protocol": "UDP",
        "name": "3com-net-mgmt",
        "port": 2391
    },
    {
        "protocol": "UDP",
        "name": "tacticalauth",
        "port": 2392
    },
    {
        "protocol": "UDP",
        "name": "ms-olap1",
        "port": 2393
    },
    {
        "protocol": "UDP",
        "name": "ms-olap2",
        "port": 2394
    },
    {
        "protocol": "UDP",
        "name": "lan900_remote",
        "port": 2395
    },
    {
        "protocol": "UDP",
        "name": "wusage",
        "port": 2396
    },
    {
        "protocol": "UDP",
        "name": "ncl",
        "port": 2397
    },
    {
        "protocol": "UDP",
        "name": "orbiter",
        "port": 2398
    },
    {
        "protocol": "UDP",
        "name": "fmpro-fdal",
        "port": 2399
    },
    {
        "protocol": "UDP",
        "name": "opequus-server",
        "port": 2400
    },
    {
        "protocol": "UDP",
        "name": "cvspserver",
        "port": 2401
    },
    {
        "protocol": "UDP",
        "name": "taskmaster2000",
        "port": 2402
    },
    {
        "protocol": "UDP",
        "name": "taskmaster2000",
        "port": 2403
    },
    {
        "protocol": "UDP",
        "name": "iec-104",
        "port": 2404
    },
    {
        "protocol": "UDP",
        "name": "trc-netpoll",
        "port": 2405
    },
    {
        "protocol": "UDP",
        "name": "jediserver",
        "port": 2406
    },
    {
        "protocol": "UDP",
        "name": "orion",
        "port": 2407
    },
    {
        "protocol": "UDP",
        "name": "optimanet",
        "port": 2408
    },
    {
        "protocol": "UDP",
        "name": "sns-protocol",
        "port": 2409
    },
    {
        "protocol": "UDP",
        "name": "vrts-registry",
        "port": 2410
    },
    {
        "protocol": "UDP",
        "name": "netwave-ap-mgmt",
        "port": 2411
    },
    {
        "protocol": "UDP",
        "name": "cdn",
        "port": 2412
    },
    {
        "protocol": "UDP",
        "name": "orion-rmi-reg",
        "port": 2413
    },
    {
        "protocol": "UDP",
        "name": "beeyond",
        "port": 2414
    },
    {
        "protocol": "UDP",
        "name": "codima-rtp",
        "port": 2415
    },
    {
        "protocol": "UDP",
        "name": "rmtserver",
        "port": 2416
    },
    {
        "protocol": "UDP",
        "name": "composit-server",
        "port": 2417
    },
    {
        "protocol": "UDP",
        "name": "cas",
        "port": 2418
    },
    {
        "protocol": "UDP",
        "name": "attachmate-s2s",
        "port": 2419
    },
    {
        "protocol": "UDP",
        "name": "dslremote-mgmt",
        "port": 2420
    },
    {
        "protocol": "UDP",
        "name": "g-talk",
        "port": 2421
    },
    {
        "protocol": "UDP",
        "name": "crmsbits",
        "port": 2422
    },
    {
        "protocol": "UDP",
        "name": "rnrp",
        "port": 2423
    },
    {
        "protocol": "UDP",
        "name": "kofax-svr",
        "port": 2424
    },
    {
        "protocol": "UDP",
        "name": "fjitsuappmgr",
        "port": 2425
    },
    {
        "protocol": "UDP",
        "name": "mgcp-gateway",
        "port": 2427
    },
    {
        "protocol": "UDP",
        "name": "ott",
        "port": 2428
    },
    {
        "protocol": "UDP",
        "name": "ft-role",
        "port": 2429
    },
    {
        "protocol": "UDP",
        "name": "venus",
        "port": 2430
    },
    {
        "protocol": "UDP",
        "name": "venus-se",
        "port": 2431
    },
    {
        "protocol": "UDP",
        "name": "codasrv",
        "port": 2432
    },
    {
        "protocol": "UDP",
        "name": "codasrv-se",
        "port": 2433
    },
    {
        "protocol": "UDP",
        "name": "pxc-epmap",
        "port": 2434
    },
    {
        "protocol": "UDP",
        "name": "optilogic",
        "port": 2435
    },
    {
        "protocol": "UDP",
        "name": "topx",
        "port": 2436
    },
    {
        "protocol": "UDP",
        "name": "unicontrol",
        "port": 2437
    },
    {
        "protocol": "UDP",
        "name": "msp",
        "port": 2438
    },
    {
        "protocol": "UDP",
        "name": "sybasedbsynch",
        "port": 2439
    },
    {
        "protocol": "UDP",
        "name": "spearway",
        "port": 2440
    },
    {
        "protocol": "UDP",
        "name": "pvsw-inet",
        "port": 2441
    },
    {
        "protocol": "UDP",
        "name": "netangel",
        "port": 2442
    },
    {
        "protocol": "UDP",
        "name": "powerclientcsf",
        "port": 2443
    },
    {
        "protocol": "UDP",
        "name": "btpp2sectrans",
        "port": 2444
    },
    {
        "protocol": "UDP",
        "name": "dtn1",
        "port": 2445
    },
    {
        "protocol": "UDP",
        "name": "bues_service",
        "port": 2446
    },
    {
        "protocol": "UDP",
        "name": "ovwdb",
        "port": 2447
    },
    {
        "protocol": "UDP",
        "name": "hpppssvr",
        "port": 2448
    },
    {
        "protocol": "UDP",
        "name": "ratl",
        "port": 2449
    },
    {
        "protocol": "UDP",
        "name": "netadmin",
        "port": 2450
    },
    {
        "protocol": "UDP",
        "name": "netchat",
        "port": 2451
    },
    {
        "protocol": "UDP",
        "name": "snifferclient",
        "port": 2452
    },
    {
        "protocol": "UDP",
        "name": "madge-ltd",
        "port": 2453
    },
    {
        "protocol": "UDP",
        "name": "indx-dds",
        "port": 2454
    },
    {
        "protocol": "UDP",
        "name": "wago-io-system",
        "port": 2455
    },
    {
        "protocol": "UDP",
        "name": "altav-remmgt",
        "port": 2456
    },
    {
        "protocol": "UDP",
        "name": "rapido-ip",
        "port": 2457
    },
    {
        "protocol": "UDP",
        "name": "griffin",
        "port": 2458
    },
    {
        "protocol": "UDP",
        "name": "community",
        "port": 2459
    },
    {
        "protocol": "UDP",
        "name": "ms-theater",
        "port": 2460
    },
    {
        "protocol": "UDP",
        "name": "qadmifoper",
        "port": 2461
    },
    {
        "protocol": "UDP",
        "name": "qadmifevent",
        "port": 2462
    },
    {
        "protocol": "UDP",
        "name": "symbios-raid",
        "port": 2463
    },
    {
        "protocol": "UDP",
        "name": "direcpc-si",
        "port": 2464
    },
    {
        "protocol": "UDP",
        "name": "lbm",
        "port": 2465
    },
    {
        "protocol": "UDP",
        "name": "lbf",
        "port": 2466
    },
    {
        "protocol": "UDP",
        "name": "high-criteria",
        "port": 2467
    },
    {
        "protocol": "UDP",
        "name": "qip-msgd",
        "port": 2468
    },
    {
        "protocol": "UDP",
        "name": "mti-tcs-comm",
        "port": 2469
    },
    {
        "protocol": "UDP",
        "name": "taskman-port",
        "port": 2470
    },
    {
        "protocol": "UDP",
        "name": "seaodbc",
        "port": 2471
    },
    {
        "protocol": "UDP",
        "name": "c3",
        "port": 2472
    },
    {
        "protocol": "UDP",
        "name": "aker-cdp",
        "port": 2473
    },
    {
        "protocol": "UDP",
        "name": "vitalanalysis",
        "port": 2474
    },
    {
        "protocol": "UDP",
        "name": "ace-server",
        "port": 2475
    },
    {
        "protocol": "UDP",
        "name": "ace-svr-prop",
        "port": 2476
    },
    {
        "protocol": "UDP",
        "name": "ssm-cvs",
        "port": 2477
    },
    {
        "protocol": "UDP",
        "name": "ssm-cssps",
        "port": 2478
    },
    {
        "protocol": "UDP",
        "name": "ssm-els",
        "port": 2479
    },
    {
        "protocol": "UDP",
        "name": "lingwood",
        "port": 2480
    },
    {
        "protocol": "UDP",
        "name": "giop",
        "port": 2481
    },
    {
        "protocol": "UDP",
        "name": "giop-ssl",
        "port": 2482
    },
    {
        "protocol": "UDP",
        "name": "ttc",
        "port": 2483
    },
    {
        "protocol": "UDP",
        "name": "ttc-ssl",
        "port": 2484
    },
    {
        "protocol": "UDP",
        "name": "netobjects1",
        "port": 2485
    },
    {
        "protocol": "UDP",
        "name": "netobjects2",
        "port": 2486
    },
    {
        "protocol": "UDP",
        "name": "pns",
        "port": 2487
    },
    {
        "protocol": "UDP",
        "name": "moy-corp",
        "port": 2488
    },
    {
        "protocol": "UDP",
        "name": "tsilb",
        "port": 2489
    },
    {
        "protocol": "UDP",
        "name": "qip-qdhcp",
        "port": 2490
    },
    {
        "protocol": "UDP",
        "name": "conclave-cpp",
        "port": 2491
    },
    {
        "protocol": "UDP",
        "name": "groove",
        "port": 2492
    },
    {
        "protocol": "UDP",
        "name": "talarian-mqs",
        "port": 2493
    },
    {
        "protocol": "UDP",
        "name": "bmc-ar",
        "port": 2494
    },
    {
        "protocol": "UDP",
        "name": "fast-rem-serv",
        "port": 2495
    },
    {
        "protocol": "UDP",
        "name": "dirgis",
        "port": 2496
    },
    {
        "protocol": "UDP",
        "name": "quaddb",
        "port": 2497
    },
    {
        "protocol": "UDP",
        "name": "odn-castraq",
        "port": 2498
    },
    {
        "protocol": "UDP",
        "name": "unicontrol",
        "port": 2499
    },
    {
        "protocol": "UDP",
        "name": "rtsserv",
        "port": 2500
    },
    {
        "protocol": "UDP",
        "name": "rtsclient",
        "port": 2501
    },
    {
        "protocol": "UDP",
        "name": "kentrox-prot",
        "port": 2502
    },
    {
        "protocol": "UDP",
        "name": "nms-dpnss",
        "port": 2503
    },
    {
        "protocol": "UDP",
        "name": "wlbs",
        "port": 2504
    },
    {
        "protocol": "UDP",
        "name": "ppcontrol",
        "port": 2505
    },
    {
        "protocol": "UDP",
        "name": "jbroker",
        "port": 2506
    },
    {
        "protocol": "UDP",
        "name": "spock",
        "port": 2507
    },
    {
        "protocol": "UDP",
        "name": "jdatastore",
        "port": 2508
    },
    {
        "protocol": "UDP",
        "name": "fjmpss",
        "port": 2509
    },
    {
        "protocol": "UDP",
        "name": "fjappmgrbulk",
        "port": 2510
    },
    {
        "protocol": "UDP",
        "name": "metastorm",
        "port": 2511
    },
    {
        "protocol": "UDP",
        "name": "citrixima",
        "port": 2512
    },
    {
        "protocol": "UDP",
        "name": "citrixadmin",
        "port": 2513
    },
    {
        "protocol": "UDP",
        "name": "facsys-ntp",
        "port": 2514
    },
    {
        "protocol": "UDP",
        "name": "facsys-router",
        "port": 2515
    },
    {
        "protocol": "UDP",
        "name": "maincontrol",
        "port": 2516
    },
    {
        "protocol": "UDP",
        "name": "call-sig-trans",
        "port": 2517
    },
    {
        "protocol": "UDP",
        "name": "willy",
        "port": 2518
    },
    {
        "protocol": "UDP",
        "name": "globmsgsvc",
        "port": 2519
    },
    {
        "protocol": "UDP",
        "name": "pvsw",
        "port": 2520
    },
    {
        "protocol": "UDP",
        "name": "adaptecmgr",
        "port": 2521
    },
    {
        "protocol": "UDP",
        "name": "windb",
        "port": 2522
    },
    {
        "protocol": "UDP",
        "name": "qke-llc-v3",
        "port": 2523
    },
    {
        "protocol": "UDP",
        "name": "optiwave-lm",
        "port": 2524
    },
    {
        "protocol": "UDP",
        "name": "ms-v-worlds",
        "port": 2525
    },
    {
        "protocol": "UDP",
        "name": "ema-sent-lm",
        "port": 2526
    },
    {
        "protocol": "UDP",
        "name": "iqserver",
        "port": 2527
    },
    {
        "protocol": "UDP",
        "name": "ncr_ccl",
        "port": 2528
    },
    {
        "protocol": "UDP",
        "name": "utsftp",
        "port": 2529
    },
    {
        "protocol": "UDP",
        "name": "vrcommerce",
        "port": 2530
    },
    {
        "protocol": "UDP",
        "name": "ito-e-gui",
        "port": 2531
    },
    {
        "protocol": "UDP",
        "name": "ovtopmd",
        "port": 2532
    },
    {
        "protocol": "UDP",
        "name": "snifferserver",
        "port": 2533
    },
    {
        "protocol": "UDP",
        "name": "combox-web-acc",
        "port": 2534
    },
    {
        "protocol": "UDP",
        "name": "madcap",
        "port": 2535
    },
    {
        "protocol": "UDP",
        "name": "btpp2audctr1",
        "port": 2536
    },
    {
        "protocol": "UDP",
        "name": "upgrade",
        "port": 2537
    },
    {
        "protocol": "UDP",
        "name": "vnwk-prapi",
        "port": 2538
    },
    {
        "protocol": "UDP",
        "name": "vsiadmin",
        "port": 2539
    },
    {
        "protocol": "UDP",
        "name": "lonworks",
        "port": 2540
    },
    {
        "protocol": "UDP",
        "name": "lonworks2",
        "port": 2541
    },
    {
        "protocol": "UDP",
        "name": "davinci",
        "port": 2542
    },
    {
        "protocol": "UDP",
        "name": "reftek",
        "port": 2543
    },
    {
        "protocol": "UDP",
        "name": "novell-zen",
        "port": 2544
    },
    {
        "protocol": "UDP",
        "name": "sis-emt",
        "port": 2545
    },
    {
        "protocol": "UDP",
        "name": "vytalvaultbrtp",
        "port": 2546
    },
    {
        "protocol": "UDP",
        "name": "vytalvaultvsmp",
        "port": 2547
    },
    {
        "protocol": "UDP",
        "name": "vytalvaultpipe",
        "port": 2548
    },
    {
        "protocol": "UDP",
        "name": "ipass",
        "port": 2549
    },
    {
        "protocol": "UDP",
        "name": "ads",
        "port": 2550
    },
    {
        "protocol": "UDP",
        "name": "isg-uda-server",
        "port": 2551
    },
    {
        "protocol": "UDP",
        "name": "call-logging",
        "port": 2552
    },
    {
        "protocol": "UDP",
        "name": "efidiningport",
        "port": 2553
    },
    {
        "protocol": "UDP",
        "name": "vcnet-link-v10",
        "port": 2554
    },
    {
        "protocol": "UDP",
        "name": "compaq-wcp",
        "port": 2555
    },
    {
        "protocol": "UDP",
        "name": "nicetec-nmsvc",
        "port": 2556
    },
    {
        "protocol": "UDP",
        "name": "nicetec-mgmt",
        "port": 2557
    },
    {
        "protocol": "UDP",
        "name": "pclemultimedia",
        "port": 2558
    },
    {
        "protocol": "UDP",
        "name": "lstp",
        "port": 2559
    },
    {
        "protocol": "UDP",
        "name": "labrat",
        "port": 2560
    },
    {
        "protocol": "UDP",
        "name": "mosaixcc",
        "port": 2561
    },
    {
        "protocol": "UDP",
        "name": "delibo",
        "port": 2562
    },
    {
        "protocol": "UDP",
        "name": "cti-redwood",
        "port": 2563
    },
    {
        "protocol": "UDP",
        "name": "coord-svr",
        "port": 2565
    },
    {
        "protocol": "UDP",
        "name": "pcs-pcw",
        "port": 2566
    },
    {
        "protocol": "UDP",
        "name": "clp",
        "port": 2567
    },
    {
        "protocol": "UDP",
        "name": "spamtrap",
        "port": 2568
    },
    {
        "protocol": "UDP",
        "name": "sonuscallsig",
        "port": 2569
    },
    {
        "protocol": "UDP",
        "name": "hs-port",
        "port": 2570
    },
    {
        "protocol": "UDP",
        "name": "cecsvc",
        "port": 2571
    },
    {
        "protocol": "UDP",
        "name": "ibp",
        "port": 2572
    },
    {
        "protocol": "UDP",
        "name": "trustestablish",
        "port": 2573
    },
    {
        "protocol": "UDP",
        "name": "blockade-bpsp",
        "port": 2574
    },
    {
        "protocol": "UDP",
        "name": "hl7",
        "port": 2575
    },
    {
        "protocol": "UDP",
        "name": "tclprodebugger",
        "port": 2576
    },
    {
        "protocol": "UDP",
        "name": "scipticslsrvr",
        "port": 2577
    },
    {
        "protocol": "UDP",
        "name": "rvs-isdn-dcp",
        "port": 2578
    },
    {
        "protocol": "UDP",
        "name": "mpfoncl",
        "port": 2579
    },
    {
        "protocol": "UDP",
        "name": "tributary",
        "port": 2580
    },
    {
        "protocol": "UDP",
        "name": "argis-te",
        "port": 2581
    },
    {
        "protocol": "UDP",
        "name": "argis-ds",
        "port": 2582
    },
    {
        "protocol": "UDP",
        "name": "mon",
        "port": 2583
    },
    {
        "protocol": "UDP",
        "name": "cyaserv",
        "port": 2584
    },
    {
        "protocol": "UDP",
        "name": "netx-server",
        "port": 2585
    },
    {
        "protocol": "UDP",
        "name": "netx-agent",
        "port": 2586
    },
    {
        "protocol": "UDP",
        "name": "masc",
        "port": 2587
    },
    {
        "protocol": "UDP",
        "name": "privilege",
        "port": 2588
    },
    {
        "protocol": "UDP",
        "name": "quartus-tcl",
        "port": 2589
    },
    {
        "protocol": "UDP",
        "name": "idotdist",
        "port": 2590
    },
    {
        "protocol": "UDP",
        "name": "maytagshuffle",
        "port": 2591
    },
    {
        "protocol": "UDP",
        "name": "netrek",
        "port": 2592
    },
    {
        "protocol": "UDP",
        "name": "mns-mail",
        "port": 2593
    },
    {
        "protocol": "UDP",
        "name": "dts",
        "port": 2594
    },
    {
        "protocol": "UDP",
        "name": "worldfusion1",
        "port": 2595
    },
    {
        "protocol": "UDP",
        "name": "worldfusion2",
        "port": 2596
    },
    {
        "protocol": "UDP",
        "name": "homesteadglory",
        "port": 2597
    },
    {
        "protocol": "UDP",
        "name": "citriximaclient",
        "port": 2598
    },
    {
        "protocol": "UDP",
        "name": "snapd",
        "port": 2599
    },
    {
        "protocol": "UDP",
        "name": "hpstgmgr",
        "port": 2600
    },
    {
        "protocol": "UDP",
        "name": "discp-client",
        "port": 2601
    },
    {
        "protocol": "UDP",
        "name": "discp-server",
        "port": 2602
    },
    {
        "protocol": "UDP",
        "name": "servicemeter",
        "port": 2603
    },
    {
        "protocol": "UDP",
        "name": "nsc-ccs",
        "port": 2604
    },
    {
        "protocol": "UDP",
        "name": "nsc-posa",
        "port": 2605
    },
    {
        "protocol": "UDP",
        "name": "netmon",
        "port": 2606
    },
    {
        "protocol": "UDP",
        "name": "connection",
        "port": 2607
    },
    {
        "protocol": "UDP",
        "name": "wag-service",
        "port": 2608
    },
    {
        "protocol": "UDP",
        "name": "system-monitor",
        "port": 2609
    },
    {
        "protocol": "UDP",
        "name": "versa-tek",
        "port": 2610
    },
    {
        "protocol": "UDP",
        "name": "lionhead",
        "port": 2611
    },
    {
        "protocol": "UDP",
        "name": "qpasa-agent",
        "port": 2612
    },
    {
        "protocol": "UDP",
        "name": "smntubootstrap",
        "port": 2613
    },
    {
        "protocol": "UDP",
        "name": "neveroffline",
        "port": 2614
    },
    {
        "protocol": "UDP",
        "name": "firepower",
        "port": 2615
    },
    {
        "protocol": "UDP",
        "name": "appswitch-emp",
        "port": 2616
    },
    {
        "protocol": "UDP",
        "name": "cmadmin",
        "port": 2617
    },
    {
        "protocol": "UDP",
        "name": "priority-e-com",
        "port": 2618
    },
    {
        "protocol": "UDP",
        "name": "bruce",
        "port": 2619
    },
    {
        "protocol": "UDP",
        "name": "lpsrecommender",
        "port": 2620
    },
    {
        "protocol": "UDP",
        "name": "miles-apart",
        "port": 2621
    },
    {
        "protocol": "UDP",
        "name": "metricadbc",
        "port": 2622
    },
    {
        "protocol": "UDP",
        "name": "lmdp",
        "port": 2623
    },
    {
        "protocol": "UDP",
        "name": "aria",
        "port": 2624
    },
    {
        "protocol": "UDP",
        "name": "blwnkl-port",
        "port": 2625
    },
    {
        "protocol": "UDP",
        "name": "gbjd816",
        "port": 2626
    },
    {
        "protocol": "UDP",
        "name": "moshebeeri",
        "port": 2627
    },
    {
        "protocol": "UDP",
        "name": "dict",
        "port": 2628
    },
    {
        "protocol": "UDP",
        "name": "sitaraserver",
        "port": 2629
    },
    {
        "protocol": "UDP",
        "name": "sitaramgmt",
        "port": 2630
    },
    {
        "protocol": "UDP",
        "name": "sitaradir",
        "port": 2631
    },
    {
        "protocol": "UDP",
        "name": "irdg-post",
        "port": 2632
    },
    {
        "protocol": "UDP",
        "name": "interintelli",
        "port": 2633
    },
    {
        "protocol": "UDP",
        "name": "pk-electronics",
        "port": 2634
    },
    {
        "protocol": "UDP",
        "name": "backburner",
        "port": 2635
    },
    {
        "protocol": "UDP",
        "name": "solve",
        "port": 2636
    },
    {
        "protocol": "UDP",
        "name": "imdocsvc",
        "port": 2637
    },
    {
        "protocol": "UDP",
        "name": "sybaseanywhere",
        "port": 2638
    },
    {
        "protocol": "UDP",
        "name": "aminet",
        "port": 2639
    },
    {
        "protocol": "UDP",
        "name": "sai_sentlm",
        "port": 2640
    },
    {
        "protocol": "UDP",
        "name": "hdl-srv",
        "port": 2641
    },
    {
        "protocol": "UDP",
        "name": "tragic",
        "port": 2642
    },
    {
        "protocol": "UDP",
        "name": "gte-samp",
        "port": 2643
    },
    {
        "protocol": "UDP",
        "name": "travsoft-ipx-t",
        "port": 2644
    },
    {
        "protocol": "UDP",
        "name": "novell-ipx-cmd",
        "port": 2645
    },
    {
        "protocol": "UDP",
        "name": "and-lm",
        "port": 2646
    },
    {
        "protocol": "UDP",
        "name": "syncserver",
        "port": 2647
    },
    {
        "protocol": "UDP",
        "name": "upsnotifyprot",
        "port": 2648
    },
    {
        "protocol": "UDP",
        "name": "vpsipport",
        "port": 2649
    },
    {
        "protocol": "UDP",
        "name": "eristwoguns",
        "port": 2650
    },
    {
        "protocol": "UDP",
        "name": "ebinsite",
        "port": 2651
    },
    {
        "protocol": "UDP",
        "name": "interpathpanel",
        "port": 2652
    },
    {
        "protocol": "UDP",
        "name": "sonus",
        "port": 2653
    },
    {
        "protocol": "UDP",
        "name": "corel_vncadmin",
        "port": 2654
    },
    {
        "protocol": "UDP",
        "name": "unglue",
        "port": 2655
    },
    {
        "protocol": "UDP",
        "name": "kana",
        "port": 2656
    },
    {
        "protocol": "UDP",
        "name": "sns-dispatcher",
        "port": 2657
    },
    {
        "protocol": "UDP",
        "name": "sns-admin",
        "port": 2658
    },
    {
        "protocol": "UDP",
        "name": "sns-query",
        "port": 2659
    },
    {
        "protocol": "UDP",
        "name": "gcmonitor",
        "port": 2660
    },
    {
        "protocol": "UDP",
        "name": "olhost",
        "port": 2661
    },
    {
        "protocol": "UDP",
        "name": "bintec-capi",
        "port": 2662
    },
    {
        "protocol": "UDP",
        "name": "bintec-tapi",
        "port": 2663
    },
    {
        "protocol": "UDP",
        "name": "patrol-mq-gm",
        "port": 2664
    },
    {
        "protocol": "UDP",
        "name": "patrol-mq-nm",
        "port": 2665
    },
    {
        "protocol": "UDP",
        "name": "extensis",
        "port": 2666
    },
    {
        "protocol": "UDP",
        "name": "alarm-clock-s",
        "port": 2667
    },
    {
        "protocol": "UDP",
        "name": "alarm-clock-c",
        "port": 2668
    },
    {
        "protocol": "UDP",
        "name": "toad",
        "port": 2669
    },
    {
        "protocol": "UDP",
        "name": "tve-announce",
        "port": 2670
    },
    {
        "protocol": "UDP",
        "name": "newlixreg",
        "port": 2671
    },
    {
        "protocol": "UDP",
        "name": "nhserver",
        "port": 2672
    },
    {
        "protocol": "UDP",
        "name": "firstcall42",
        "port": 2673
    },
    {
        "protocol": "UDP",
        "name": "ewnn",
        "port": 2674
    },
    {
        "protocol": "UDP",
        "name": "ttc-etap",
        "port": 2675
    },
    {
        "protocol": "UDP",
        "name": "simslink",
        "port": 2676
    },
    {
        "protocol": "UDP",
        "name": "gadgetgate1way",
        "port": 2677
    },
    {
        "protocol": "UDP",
        "name": "gadgetgate2way",
        "port": 2678
    },
    {
        "protocol": "UDP",
        "name": "syncserverssl",
        "port": 2679
    },
    {
        "protocol": "UDP",
        "name": "pxc-sapxom",
        "port": 2680
    },
    {
        "protocol": "UDP",
        "name": "mpnjsomb",
        "port": 2681
    },
    {
        "protocol": "UDP",
        "name": "ncdloadbalance",
        "port": 2683
    },
    {
        "protocol": "UDP",
        "name": "mpnjsosv",
        "port": 2684
    },
    {
        "protocol": "UDP",
        "name": "mpnjsocl",
        "port": 2685
    },
    {
        "protocol": "UDP",
        "name": "mpnjsomg",
        "port": 2686
    },
    {
        "protocol": "UDP",
        "name": "pq-lic-mgmt",
        "port": 2687
    },
    {
        "protocol": "UDP",
        "name": "md-cg-http",
        "port": 2688
    },
    {
        "protocol": "UDP",
        "name": "fastlynx",
        "port": 2689
    },
    {
        "protocol": "UDP",
        "name": "hp-nnm-data",
        "port": 2690
    },
    {
        "protocol": "UDP",
        "name": "itinternet",
        "port": 2691
    },
    {
        "protocol": "UDP",
        "name": "admins-lms",
        "port": 2692
    },
    {
        "protocol": "UDP",
        "name": "pwrsevent",
        "port": 2694
    },
    {
        "protocol": "UDP",
        "name": "vspread",
        "port": 2695
    },
    {
        "protocol": "UDP",
        "name": "unifyadmin",
        "port": 2696
    },
    {
        "protocol": "UDP",
        "name": "oce-snmp-trap",
        "port": 2697
    },
    {
        "protocol": "UDP",
        "name": "mck-ivpip",
        "port": 2698
    },
    {
        "protocol": "UDP",
        "name": "csoft-plusclnt",
        "port": 2699
    },
    {
        "protocol": "UDP",
        "name": "tqdata",
        "port": 2700
    },
    {
        "protocol": "UDP",
        "name": "sms-rcinfo",
        "port": 2701
    },
    {
        "protocol": "UDP",
        "name": "sms-xfer",
        "port": 2702
    },
    {
        "protocol": "UDP",
        "name": "sms-chat",
        "port": 2703
    },
    {
        "protocol": "UDP",
        "name": "sms-remctrl",
        "port": 2704
    },
    {
        "protocol": "UDP",
        "name": "sds-admin",
        "port": 2705
    },
    {
        "protocol": "UDP",
        "name": "ncdmirroring",
        "port": 2706
    },
    {
        "protocol": "UDP",
        "name": "emcsymapiport",
        "port": 2707
    },
    {
        "protocol": "UDP",
        "name": "banyan-net",
        "port": 2708
    },
    {
        "protocol": "UDP",
        "name": "supermon",
        "port": 2709
    },
    {
        "protocol": "UDP",
        "name": "sso-service",
        "port": 2710
    },
    {
        "protocol": "UDP",
        "name": "sso-control",
        "port": 2711
    },
    {
        "protocol": "UDP",
        "name": "aocp",
        "port": 2712
    },
    {
        "protocol": "UDP",
        "name": "raven1",
        "port": 2713
    },
    {
        "protocol": "UDP",
        "name": "raven2",
        "port": 2714
    },
    {
        "protocol": "UDP",
        "name": "hpstgmgr2",
        "port": 2715
    },
    {
        "protocol": "UDP",
        "name": "inova-ip-disco",
        "port": 2716
    },
    {
        "protocol": "UDP",
        "name": "pn-requester",
        "port": 2717
    },
    {
        "protocol": "UDP",
        "name": "pn-requester2",
        "port": 2718
    },
    {
        "protocol": "UDP",
        "name": "scan-change",
        "port": 2719
    },
    {
        "protocol": "UDP",
        "name": "wkars",
        "port": 2720
    },
    {
        "protocol": "UDP",
        "name": "smart-diagnose",
        "port": 2721
    },
    {
        "protocol": "UDP",
        "name": "proactivesrvr",
        "port": 2722
    },
    {
        "protocol": "UDP",
        "name": "watchdognt",
        "port": 2723
    },
    {
        "protocol": "UDP",
        "name": "qotps",
        "port": 2724
    },
    {
        "protocol": "UDP",
        "name": "msolap-ptp2",
        "port": 2725
    },
    {
        "protocol": "UDP",
        "name": "tams",
        "port": 2726
    },
    {
        "protocol": "UDP",
        "name": "mgcp-callagent",
        "port": 2727
    },
    {
        "protocol": "UDP",
        "name": "sqdr",
        "port": 2728
    },
    {
        "protocol": "UDP",
        "name": "tcim-control",
        "port": 2729
    },
    {
        "protocol": "UDP",
        "name": "nec-raidplus",
        "port": 2730
    },
    {
        "protocol": "UDP",
        "name": "fyre-messanger",
        "port": 2731
    },
    {
        "protocol": "UDP",
        "name": "g5m",
        "port": 2732
    },
    {
        "protocol": "UDP",
        "name": "signet-ctf",
        "port": 2733
    },
    {
        "protocol": "UDP",
        "name": "ccs-software",
        "port": 2734
    },
    {
        "protocol": "UDP",
        "name": "netiq-mc",
        "port": 2735
    },
    {
        "protocol": "UDP",
        "name": "radwiz-nms-srv",
        "port": 2736
    },
    {
        "protocol": "UDP",
        "name": "srp-feedback",
        "port": 2737
    },
    {
        "protocol": "UDP",
        "name": "ndl-tcp-ois-gw",
        "port": 2738
    },
    {
        "protocol": "UDP",
        "name": "tn-timing",
        "port": 2739
    },
    {
        "protocol": "UDP",
        "name": "alarm",
        "port": 2740
    },
    {
        "protocol": "UDP",
        "name": "tsb",
        "port": 2741
    },
    {
        "protocol": "UDP",
        "name": "tsb2",
        "port": 2742
    },
    {
        "protocol": "UDP",
        "name": "murx",
        "port": 2743
    },
    {
        "protocol": "UDP",
        "name": "honyaku",
        "port": 2744
    },
    {
        "protocol": "UDP",
        "name": "urbisnet",
        "port": 2745
    },
    {
        "protocol": "UDP",
        "name": "cpudpencap",
        "port": 2746
    },
    {
        "protocol": "UDP",
        "name": "fjippol-swrly",
        "port": 2747
    },
    {
        "protocol": "UDP",
        "name": "fjippol-polsvr",
        "port": 2748
    },
    {
        "protocol": "UDP",
        "name": "fjippol-cnsl",
        "port": 2749
    },
    {
        "protocol": "UDP",
        "name": "fjippol-port1",
        "port": 2750
    },
    {
        "protocol": "UDP",
        "name": "fjippol-port2",
        "port": 2751
    },
    {
        "protocol": "UDP",
        "name": "rsisysaccess",
        "port": 2752
    },
    {
        "protocol": "UDP",
        "name": "de-spot",
        "port": 2753
    },
    {
        "protocol": "UDP",
        "name": "apollo-cc",
        "port": 2754
    },
    {
        "protocol": "UDP",
        "name": "expresspay",
        "port": 2755
    },
    {
        "protocol": "UDP",
        "name": "simplement-tie",
        "port": 2756
    },
    {
        "protocol": "UDP",
        "name": "cnrp",
        "port": 2757
    },
    {
        "protocol": "UDP",
        "name": "apollo-status",
        "port": 2758
    },
    {
        "protocol": "UDP",
        "name": "apollo-gms",
        "port": 2759
    },
    {
        "protocol": "UDP",
        "name": "sabams",
        "port": 2760
    },
    {
        "protocol": "UDP",
        "name": "dicom-iscl",
        "port": 2761
    },
    {
        "protocol": "UDP",
        "name": "dicom-tls",
        "port": 2762
    },
    {
        "protocol": "UDP",
        "name": "desktop-dna",
        "port": 2763
    },
    {
        "protocol": "UDP",
        "name": "data-insurance",
        "port": 2764
    },
    {
        "protocol": "UDP",
        "name": "qip-audup",
        "port": 2765
    },
    {
        "protocol": "UDP",
        "name": "compaq-scp",
        "port": 2766
    },
    {
        "protocol": "UDP",
        "name": "uadtc",
        "port": 2767
    },
    {
        "protocol": "UDP",
        "name": "uacs",
        "port": 2768
    },
    {
        "protocol": "UDP",
        "name": "exce",
        "port": 2769
    },
    {
        "protocol": "UDP",
        "name": "veronica",
        "port": 2770
    },
    {
        "protocol": "UDP",
        "name": "vergencecm",
        "port": 2771
    },
    {
        "protocol": "UDP",
        "name": "auris",
        "port": 2772
    },
    {
        "protocol": "UDP",
        "name": "rbakcup1",
        "port": 2773
    },
    {
        "protocol": "UDP",
        "name": "rbakcup2",
        "port": 2774
    },
    {
        "protocol": "UDP",
        "name": "smpp",
        "port": 2775
    },
    {
        "protocol": "UDP",
        "name": "ridgeway1",
        "port": 2776
    },
    {
        "protocol": "UDP",
        "name": "ridgeway2",
        "port": 2777
    },
    {
        "protocol": "UDP",
        "name": "gwen-sonya",
        "port": 2778
    },
    {
        "protocol": "UDP",
        "name": "lbc-sync",
        "port": 2779
    },
    {
        "protocol": "UDP",
        "name": "lbc-control",
        "port": 2780
    },
    {
        "protocol": "UDP",
        "name": "whosells",
        "port": 2781
    },
    {
        "protocol": "UDP",
        "name": "everydayrc",
        "port": 2782
    },
    {
        "protocol": "UDP",
        "name": "aises",
        "port": 2783
    },
    {
        "protocol": "UDP",
        "name": "www-dev",
        "port": 2784
    },
    {
        "protocol": "UDP",
        "name": "aic-np",
        "port": 2785
    },
    {
        "protocol": "UDP",
        "name": "aic-oncrpc",
        "port": 2786
    },
    {
        "protocol": "UDP",
        "name": "piccolo",
        "port": 2787
    },
    {
        "protocol": "UDP",
        "name": "fryeserv",
        "port": 2788
    },
    {
        "protocol": "UDP",
        "name": "media-agent",
        "port": 2789
    },
    {
        "protocol": "UDP",
        "name": "plgproxy",
        "port": 2790
    },
    {
        "protocol": "UDP",
        "name": "mtport-regist",
        "port": 2791
    },
    {
        "protocol": "UDP",
        "name": "f5-globalsite",
        "port": 2792
    },
    {
        "protocol": "UDP",
        "name": "initlsmsad",
        "port": 2793
    },
    {
        "protocol": "UDP",
        "name": "aaftp",
        "port": 2794
    },
    {
        "protocol": "UDP",
        "name": "livestats",
        "port": 2795
    },
    {
        "protocol": "UDP",
        "name": "ac-tech",
        "port": 2796
    },
    {
        "protocol": "UDP",
        "name": "esp-encap",
        "port": 2797
    },
    {
        "protocol": "UDP",
        "name": "tmesis-upshot",
        "port": 2798
    },
    {
        "protocol": "UDP",
        "name": "icon-discover",
        "port": 2799
    },
    {
        "protocol": "UDP",
        "name": "acc-raid",
        "port": 2800
    },
    {
        "protocol": "UDP",
        "name": "igcp",
        "port": 2801
    },
    {
        "protocol": "UDP",
        "name": "veritas-udp1",
        "port": 2802
    },
    {
        "protocol": "UDP",
        "name": "btprjctrl",
        "port": 2803
    },
    {
        "protocol": "UDP",
        "name": "dvr-esm",
        "port": 2804
    },
    {
        "protocol": "UDP",
        "name": "wta-wsp-s",
        "port": 2805
    },
    {
        "protocol": "UDP",
        "name": "cspuni",
        "port": 2806
    },
    {
        "protocol": "UDP",
        "name": "cspmulti",
        "port": 2807
    },
    {
        "protocol": "UDP",
        "name": "j-lan-p",
        "port": 2808
    },
    {
        "protocol": "UDP",
        "name": "corbaloc",
        "port": 2809
    },
    {
        "protocol": "UDP",
        "name": "netsteward",
        "port": 2810
    },
    {
        "protocol": "UDP",
        "name": "gsiftp",
        "port": 2811
    },
    {
        "protocol": "UDP",
        "name": "atmtcp",
        "port": 2812
    },
    {
        "protocol": "UDP",
        "name": "llm-pass",
        "port": 2813
    },
    {
        "protocol": "UDP",
        "name": "llm-csv",
        "port": 2814
    },
    {
        "protocol": "UDP",
        "name": "lbc-measure",
        "port": 2815
    },
    {
        "protocol": "UDP",
        "name": "lbc-watchdog",
        "port": 2816
    },
    {
        "protocol": "UDP",
        "name": "nmsigport",
        "port": 2817
    },
    {
        "protocol": "UDP",
        "name": "rmlnk",
        "port": 2818
    },
    {
        "protocol": "UDP",
        "name": "fc-faultnotify",
        "port": 2819
    },
    {
        "protocol": "UDP",
        "name": "univision",
        "port": 2820
    },
    {
        "protocol": "UDP",
        "name": "vrts-at-port",
        "port": 2821
    },
    {
        "protocol": "UDP",
        "name": "ka0wuc",
        "port": 2822
    },
    {
        "protocol": "UDP",
        "name": "cqg-netlan",
        "port": 2823
    },
    {
        "protocol": "UDP",
        "name": "cqg-netlan-1",
        "port": 2824
    },
    {
        "protocol": "UDP",
        "name": "slc-systemlog",
        "port": 2826
    },
    {
        "protocol": "UDP",
        "name": "slc-ctrlrloops",
        "port": 2827
    },
    {
        "protocol": "UDP",
        "name": "itm-lm",
        "port": 2828
    },
    {
        "protocol": "UDP",
        "name": "silkp1",
        "port": 2829
    },
    {
        "protocol": "UDP",
        "name": "silkp2",
        "port": 2830
    },
    {
        "protocol": "UDP",
        "name": "silkp3",
        "port": 2831
    },
    {
        "protocol": "UDP",
        "name": "silkp4",
        "port": 2832
    },
    {
        "protocol": "UDP",
        "name": "glishd",
        "port": 2833
    },
    {
        "protocol": "UDP",
        "name": "evtp",
        "port": 2834
    },
    {
        "protocol": "UDP",
        "name": "evtp-data",
        "port": 2835
    },
    {
        "protocol": "UDP",
        "name": "catalyst",
        "port": 2836
    },
    {
        "protocol": "UDP",
        "name": "repliweb",
        "port": 2837
    },
    {
        "protocol": "UDP",
        "name": "starbot",
        "port": 2838
    },
    {
        "protocol": "UDP",
        "name": "nmsigport",
        "port": 2839
    },
    {
        "protocol": "UDP",
        "name": "l3-exprt",
        "port": 2840
    },
    {
        "protocol": "UDP",
        "name": "l3-ranger",
        "port": 2841
    },
    {
        "protocol": "UDP",
        "name": "l3-hawk",
        "port": 2842
    },
    {
        "protocol": "UDP",
        "name": "pdnet",
        "port": 2843
    },
    {
        "protocol": "UDP",
        "name": "bpcp-poll",
        "port": 2844
    },
    {
        "protocol": "UDP",
        "name": "bpcp-trap",
        "port": 2845
    },
    {
        "protocol": "UDP",
        "name": "aimpp-hello",
        "port": 2846
    },
    {
        "protocol": "UDP",
        "name": "aimpp-port-req",
        "port": 2847
    },
    {
        "protocol": "UDP",
        "name": "amt-blc-port",
        "port": 2848
    },
    {
        "protocol": "UDP",
        "name": "fxp",
        "port": 2849
    },
    {
        "protocol": "UDP",
        "name": "metaconsole",
        "port": 2850
    },
    {
        "protocol": "UDP",
        "name": "webemshttp",
        "port": 2851
    },
    {
        "protocol": "UDP",
        "name": "bears-01",
        "port": 2852
    },
    {
        "protocol": "UDP",
        "name": "ispipes",
        "port": 2853
    },
    {
        "protocol": "UDP",
        "name": "infomover",
        "port": 2854
    },
    {
        "protocol": "UDP",
        "name": "cesdinv",
        "port": 2856
    },
    {
        "protocol": "UDP",
        "name": "simctlp",
        "port": 2857
    },
    {
        "protocol": "UDP",
        "name": "ecnp",
        "port": 2858
    },
    {
        "protocol": "UDP",
        "name": "activememory",
        "port": 2859
    },
    {
        "protocol": "UDP",
        "name": "dialpad-voice1",
        "port": 2860
    },
    {
        "protocol": "UDP",
        "name": "dialpad-voice2",
        "port": 2861
    },
    {
        "protocol": "UDP",
        "name": "ttg-protocol",
        "port": 2862
    },
    {
        "protocol": "UDP",
        "name": "sonardata",
        "port": 2863
    },
    {
        "protocol": "UDP",
        "name": "astromed-main",
        "port": 2864
    },
    {
        "protocol": "UDP",
        "name": "pit-vpn",
        "port": 2865
    },
    {
        "protocol": "UDP",
        "name": "iwlistener",
        "port": 2866
    },
    {
        "protocol": "UDP",
        "name": "esps-portal",
        "port": 2867
    },
    {
        "protocol": "UDP",
        "name": "npep-messaging",
        "port": 2868
    },
    {
        "protocol": "UDP",
        "name": "icslap",
        "port": 2869
    },
    {
        "protocol": "UDP",
        "name": "daishi",
        "port": 2870
    },
    {
        "protocol": "UDP",
        "name": "msi-selectplay",
        "port": 2871
    },
    {
        "protocol": "UDP",
        "name": "radix",
        "port": 2872
    },
    {
        "protocol": "UDP",
        "name": "dxmessagebase1",
        "port": 2874
    },
    {
        "protocol": "UDP",
        "name": "dxmessagebase2",
        "port": 2875
    },
    {
        "protocol": "UDP",
        "name": "sps-tunnel",
        "port": 2876
    },
    {
        "protocol": "UDP",
        "name": "bluelance",
        "port": 2877
    },
    {
        "protocol": "UDP",
        "name": "aap",
        "port": 2878
    },
    {
        "protocol": "UDP",
        "name": "ucentric-ds",
        "port": 2879
    },
    {
        "protocol": "UDP",
        "name": "synapse",
        "port": 2880
    },
    {
        "protocol": "UDP",
        "name": "ndsp",
        "port": 2881
    },
    {
        "protocol": "UDP",
        "name": "ndtp",
        "port": 2882
    },
    {
        "protocol": "UDP",
        "name": "ndnp",
        "port": 2883
    },
    {
        "protocol": "UDP",
        "name": "flashmsg",
        "port": 2884
    },
    {
        "protocol": "UDP",
        "name": "topflow",
        "port": 2885
    },
    {
        "protocol": "UDP",
        "name": "responselogic",
        "port": 2886
    },
    {
        "protocol": "UDP",
        "name": "aironetddp",
        "port": 2887
    },
    {
        "protocol": "UDP",
        "name": "spcsdlobby",
        "port": 2888
    },
    {
        "protocol": "UDP",
        "name": "rsom",
        "port": 2889
    },
    {
        "protocol": "UDP",
        "name": "cspclmulti",
        "port": 2890
    },
    {
        "protocol": "UDP",
        "name": "cinegrfx-elmd",
        "port": 2891
    },
    {
        "protocol": "UDP",
        "name": "snifferdata",
        "port": 2892
    },
    {
        "protocol": "UDP",
        "name": "vseconnector",
        "port": 2893
    },
    {
        "protocol": "UDP",
        "name": "abacus-remote",
        "port": 2894
    },
    {
        "protocol": "UDP",
        "name": "natuslink",
        "port": 2895
    },
    {
        "protocol": "UDP",
        "name": "ecovisiong6-1",
        "port": 2896
    },
    {
        "protocol": "UDP",
        "name": "citrix-rtmp",
        "port": 2897
    },
    {
        "protocol": "UDP",
        "name": "appliance-cfg",
        "port": 2898
    },
    {
        "protocol": "UDP",
        "name": "powergemplus",
        "port": 2899
    },
    {
        "protocol": "UDP",
        "name": "quicksuite",
        "port": 2900
    },
    {
        "protocol": "UDP",
        "name": "allstorcns",
        "port": 2901
    },
    {
        "protocol": "UDP",
        "name": "netaspi",
        "port": 2902
    },
    {
        "protocol": "UDP",
        "name": "suitcase",
        "port": 2903
    },
    {
        "protocol": "UDP",
        "name": "m2ua",
        "port": 2904
    },
    {
        "protocol": "UDP",
        "name": "caller9",
        "port": 2906
    },
    {
        "protocol": "UDP",
        "name": "webmethods-b2b",
        "port": 2907
    },
    {
        "protocol": "UDP",
        "name": "mao",
        "port": 2908
    },
    {
        "protocol": "UDP",
        "name": "funk-dialout",
        "port": 2909
    },
    {
        "protocol": "UDP",
        "name": "tdaccess",
        "port": 2910
    },
    {
        "protocol": "UDP",
        "name": "blockade",
        "port": 2911
    },
    {
        "protocol": "UDP",
        "name": "epicon",
        "port": 2912
    },
    {
        "protocol": "UDP",
        "name": "boosterware",
        "port": 2913
    },
    {
        "protocol": "UDP",
        "name": "gamelobby",
        "port": 2914
    },
    {
        "protocol": "UDP",
        "name": "tksocket",
        "port": 2915
    },
    {
        "protocol": "UDP",
        "name": "elvin_server",
        "port": 2916
    },
    {
        "protocol": "UDP",
        "name": "elvin_client",
        "port": 2917
    },
    {
        "protocol": "UDP",
        "name": "kastenchasepad",
        "port": 2918
    },
    {
        "protocol": "UDP",
        "name": "roboer",
        "port": 2919
    },
    {
        "protocol": "UDP",
        "name": "roboeda",
        "port": 2920
    },
    {
        "protocol": "UDP",
        "name": "cesdcdman",
        "port": 2921
    },
    {
        "protocol": "UDP",
        "name": "cesdcdtrn",
        "port": 2922
    },
    {
        "protocol": "UDP",
        "name": "wta-wsp-wtp-s",
        "port": 2923
    },
    {
        "protocol": "UDP",
        "name": "precise-vip",
        "port": 2924
    },
    {
        "protocol": "UDP",
        "name": "mobile-file-dl",
        "port": 2926
    },
    {
        "protocol": "UDP",
        "name": "unimobilectrl",
        "port": 2927
    },
    {
        "protocol": "UDP",
        "name": "redstone-cpss",
        "port": 2928
    },
    {
        "protocol": "UDP",
        "name": "amx-webadmin",
        "port": 2929
    },
    {
        "protocol": "UDP",
        "name": "amx-weblinx",
        "port": 2930
    },
    {
        "protocol": "UDP",
        "name": "circle-x",
        "port": 2931
    },
    {
        "protocol": "UDP",
        "name": "incp",
        "port": 2932
    },
    {
        "protocol": "UDP",
        "name": "4-tieropmgw",
        "port": 2933
    },
    {
        "protocol": "UDP",
        "name": "4-tieropmcli",
        "port": 2934
    },
    {
        "protocol": "UDP",
        "name": "qtp",
        "port": 2935
    },
    {
        "protocol": "UDP",
        "name": "otpatch",
        "port": 2936
    },
    {
        "protocol": "UDP",
        "name": "pnaconsult-lm",
        "port": 2937
    },
    {
        "protocol": "UDP",
        "name": "sm-pas-1",
        "port": 2938
    },
    {
        "protocol": "UDP",
        "name": "sm-pas-2",
        "port": 2939
    },
    {
        "protocol": "UDP",
        "name": "sm-pas-3",
        "port": 2940
    },
    {
        "protocol": "UDP",
        "name": "sm-pas-4",
        "port": 2941
    },
    {
        "protocol": "UDP",
        "name": "sm-pas-5",
        "port": 2942
    },
    {
        "protocol": "UDP",
        "name": "ttnrepository",
        "port": 2943
    },
    {
        "protocol": "UDP",
        "name": "megaco-h248",
        "port": 2944
    },
    {
        "protocol": "UDP",
        "name": "h248-binary",
        "port": 2945
    },
    {
        "protocol": "UDP",
        "name": "fjsvmpor",
        "port": 2946
    },
    {
        "protocol": "UDP",
        "name": "gpsd",
        "port": 2947
    },
    {
        "protocol": "UDP",
        "name": "wap-push",
        "port": 2948
    },
    {
        "protocol": "UDP",
        "name": "wap-pushsecure",
        "port": 2949
    },
    {
        "protocol": "UDP",
        "name": "esip",
        "port": 2950
    },
    {
        "protocol": "UDP",
        "name": "ottp",
        "port": 2951
    },
    {
        "protocol": "UDP",
        "name": "mpfwsas",
        "port": 2952
    },
    {
        "protocol": "UDP",
        "name": "ovalarmsrv",
        "port": 2953
    },
    {
        "protocol": "UDP",
        "name": "ovalarmsrv-cmd",
        "port": 2954
    },
    {
        "protocol": "UDP",
        "name": "csnotify",
        "port": 2955
    },
    {
        "protocol": "UDP",
        "name": "ovrimosdbman",
        "port": 2956
    },
    {
        "protocol": "UDP",
        "name": "jmact5",
        "port": 2957
    },
    {
        "protocol": "UDP",
        "name": "jmact6",
        "port": 2958
    },
    {
        "protocol": "UDP",
        "name": "rmopagt",
        "port": 2959
    },
    {
        "protocol": "UDP",
        "name": "dfoxserver",
        "port": 2960
    },
    {
        "protocol": "UDP",
        "name": "boldsoft-lm",
        "port": 2961
    },
    {
        "protocol": "UDP",
        "name": "iph-policy-cli",
        "port": 2962
    },
    {
        "protocol": "UDP",
        "name": "iph-policy-adm",
        "port": 2963
    },
    {
        "protocol": "UDP",
        "name": "bullant-srap",
        "port": 2964
    },
    {
        "protocol": "UDP",
        "name": "bullant-rap",
        "port": 2965
    },
    {
        "protocol": "UDP",
        "name": "idp-infotrieve",
        "port": 2966
    },
    {
        "protocol": "UDP",
        "name": "ssc-agent",
        "port": 2967
    },
    {
        "protocol": "UDP",
        "name": "enpp",
        "port": 2968
    },
    {
        "protocol": "UDP",
        "name": "essp",
        "port": 2969
    },
    {
        "protocol": "UDP",
        "name": "index-net",
        "port": 2970
    },
    {
        "protocol": "UDP",
        "name": "netclip",
        "port": 2971
    },
    {
        "protocol": "UDP",
        "name": "pmsm-webrctl",
        "port": 2972
    },
    {
        "protocol": "UDP",
        "name": "svnetworks",
        "port": 2973
    },
    {
        "protocol": "UDP",
        "name": "signal",
        "port": 2974
    },
    {
        "protocol": "UDP",
        "name": "fjmpcm",
        "port": 2975
    },
    {
        "protocol": "UDP",
        "name": "cns-srv-port",
        "port": 2976
    },
    {
        "protocol": "UDP",
        "name": "ttc-etap-ns",
        "port": 2977
    },
    {
        "protocol": "UDP",
        "name": "ttc-etap-ds",
        "port": 2978
    },
    {
        "protocol": "UDP",
        "name": "h263-video",
        "port": 2979
    },
    {
        "protocol": "UDP",
        "name": "wimd",
        "port": 2980
    },
    {
        "protocol": "UDP",
        "name": "mylxamport",
        "port": 2981
    },
    {
        "protocol": "UDP",
        "name": "iwb-whiteboard",
        "port": 2982
    },
    {
        "protocol": "UDP",
        "name": "netplan",
        "port": 2983
    },
    {
        "protocol": "UDP",
        "name": "hpidsadmin",
        "port": 2984
    },
    {
        "protocol": "UDP",
        "name": "hpidsagent",
        "port": 2985
    },
    {
        "protocol": "UDP",
        "name": "stonefalls",
        "port": 2986
    },
    {
        "protocol": "UDP",
        "name": "identify",
        "port": 2987
    },
    {
        "protocol": "UDP",
        "name": "hippad",
        "port": 2988
    },
    {
        "protocol": "UDP",
        "name": "zarkov",
        "port": 2989
    },
    {
        "protocol": "UDP",
        "name": "boscap",
        "port": 2990
    },
    {
        "protocol": "UDP",
        "name": "wkstn-mon",
        "port": 2991
    },
    {
        "protocol": "UDP",
        "name": "itb301",
        "port": 2992
    },
    {
        "protocol": "UDP",
        "name": "veritas-vis1",
        "port": 2993
    },
    {
        "protocol": "UDP",
        "name": "veritas-vis2",
        "port": 2994
    },
    {
        "protocol": "UDP",
        "name": "idrs",
        "port": 2995
    },
    {
        "protocol": "UDP",
        "name": "vsixml",
        "port": 2996
    },
    {
        "protocol": "UDP",
        "name": "rebol",
        "port": 2997
    },
    {
        "protocol": "UDP",
        "name": "realsecure",
        "port": 2998
    },
    {
        "protocol": "UDP",
        "name": "remoteware-un",
        "port": 2999
    },
    {
        "protocol": "UDP",
        "name": "remoteware-cl",
        "port": 3000
    },
    {
        "protocol": "UDP",
        "name": "redwood-broker",
        "port": 3001
    },
    {
        "protocol": "UDP",
        "name": "remoteware-srv",
        "port": 3002
    },
    {
        "protocol": "UDP",
        "name": "cgms",
        "port": 3003
    },
    {
        "protocol": "UDP",
        "name": "csoftragent",
        "port": 3004
    },
    {
        "protocol": "UDP",
        "name": "geniuslm",
        "port": 3005
    },
    {
        "protocol": "UDP",
        "name": "ii-admin",
        "port": 3006
    },
    {
        "protocol": "UDP",
        "name": "lotusmtap",
        "port": 3007
    },
    {
        "protocol": "UDP",
        "name": "midnight-tech",
        "port": 3008
    },
    {
        "protocol": "UDP",
        "name": "pxc-ntfy",
        "port": 3009
    },
    {
        "protocol": "UDP",
        "name": "ping-pong",
        "port": 3010
    },
    {
        "protocol": "UDP",
        "name": "trusted-web",
        "port": 3011
    },
    {
        "protocol": "UDP",
        "name": "twsdss",
        "port": 3012
    },
    {
        "protocol": "UDP",
        "name": "gilatskysurfer",
        "port": 3013
    },
    {
        "protocol": "UDP",
        "name": "broker_service",
        "port": 3014
    },
    {
        "protocol": "UDP",
        "name": "nati-dstp",
        "port": 3015
    },
    {
        "protocol": "UDP",
        "name": "notify_srvr",
        "port": 3016
    },
    {
        "protocol": "UDP",
        "name": "event_listener",
        "port": 3017
    },
    {
        "protocol": "UDP",
        "name": "srvc_registry",
        "port": 3018
    },
    {
        "protocol": "UDP",
        "name": "resource_mgr",
        "port": 3019
    },
    {
        "protocol": "UDP",
        "name": "cifs",
        "port": 3020
    },
    {
        "protocol": "UDP",
        "name": "agriserver",
        "port": 3021
    },
    {
        "protocol": "UDP",
        "name": "csregagent",
        "port": 3022
    },
    {
        "protocol": "UDP",
        "name": "magicnotes",
        "port": 3023
    },
    {
        "protocol": "UDP",
        "name": "nds_sso",
        "port": 3024
    },
    {
        "protocol": "UDP",
        "name": "arepa-raft",
        "port": 3025
    },
    {
        "protocol": "UDP",
        "name": "agri-gateway",
        "port": 3026
    },
    {
        "protocol": "UDP",
        "name": "LiebDevMgmt_C",
        "port": 3027
    },
    {
        "protocol": "UDP",
        "name": "LiebDevMgmt_DM",
        "port": 3028
    },
    {
        "protocol": "UDP",
        "name": "LiebDevMgmt_A",
        "port": 3029
    },
    {
        "protocol": "UDP",
        "name": "arepa-cas",
        "port": 3030
    },
    {
        "protocol": "UDP",
        "name": "eppc",
        "port": 3031
    },
    {
        "protocol": "UDP",
        "name": "redwood-chat",
        "port": 3032
    },
    {
        "protocol": "UDP",
        "name": "pdb",
        "port": 3033
    },
    {
        "protocol": "UDP",
        "name": "osmosis-aeea",
        "port": 3034
    },
    {
        "protocol": "UDP",
        "name": "fjsv-gssagt",
        "port": 3035
    },
    {
        "protocol": "UDP",
        "name": "hagel-dump",
        "port": 3036
    },
    {
        "protocol": "UDP",
        "name": "hp-san-mgmt",
        "port": 3037
    },
    {
        "protocol": "UDP",
        "name": "santak-ups",
        "port": 3038
    },
    {
        "protocol": "UDP",
        "name": "cogitate",
        "port": 3039
    },
    {
        "protocol": "UDP",
        "name": "tomato-springs",
        "port": 3040
    },
    {
        "protocol": "UDP",
        "name": "di-traceware",
        "port": 3041
    },
    {
        "protocol": "UDP",
        "name": "journee",
        "port": 3042
    },
    {
        "protocol": "UDP",
        "name": "brp",
        "port": 3043
    },
    {
        "protocol": "UDP",
        "name": "epp",
        "port": 3044
    },
    {
        "protocol": "UDP",
        "name": "responsenet",
        "port": 3045
    },
    {
        "protocol": "UDP",
        "name": "di-ase",
        "port": 3046
    },
    {
        "protocol": "UDP",
        "name": "hlserver",
        "port": 3047
    },
    {
        "protocol": "UDP",
        "name": "pctrader",
        "port": 3048
    },
    {
        "protocol": "UDP",
        "name": "nsws",
        "port": 3049
    },
    {
        "protocol": "UDP",
        "name": "gds_db",
        "port": 3050
    },
    {
        "protocol": "UDP",
        "name": "galaxy-server",
        "port": 3051
    },
    {
        "protocol": "UDP",
        "name": "apc-3052",
        "port": 3052
    },
    {
        "protocol": "UDP",
        "name": "dsom-server",
        "port": 3053
    },
    {
        "protocol": "UDP",
        "name": "amt-cnf-prot",
        "port": 3054
    },
    {
        "protocol": "UDP",
        "name": "policyserver",
        "port": 3055
    },
    {
        "protocol": "UDP",
        "name": "cdl-server",
        "port": 3056
    },
    {
        "protocol": "UDP",
        "name": "goahead-fldup",
        "port": 3057
    },
    {
        "protocol": "UDP",
        "name": "videobeans",
        "port": 3058
    },
    {
        "protocol": "UDP",
        "name": "qsoft",
        "port": 3059
    },
    {
        "protocol": "UDP",
        "name": "interserver",
        "port": 3060
    },
    {
        "protocol": "UDP",
        "name": "cautcpd",
        "port": 3061
    },
    {
        "protocol": "UDP",
        "name": "ncacn-ip-tcp",
        "port": 3062
    },
    {
        "protocol": "UDP",
        "name": "ncadg-ip-udp",
        "port": 3063
    },
    {
        "protocol": "UDP",
        "name": "rprt",
        "port": 3064
    },
    {
        "protocol": "UDP",
        "name": "slinterbase",
        "port": 3065
    },
    {
        "protocol": "UDP",
        "name": "netattachsdmp",
        "port": 3066
    },
    {
        "protocol": "UDP",
        "name": "fjhpjp",
        "port": 3067
    },
    {
        "protocol": "UDP",
        "name": "ls3bcast",
        "port": 3068
    },
    {
        "protocol": "UDP",
        "name": "ls3",
        "port": 3069
    },
    {
        "protocol": "UDP",
        "name": "mgxswitch",
        "port": 3070
    },
    {
        "protocol": "UDP",
        "name": "csd-mgmt-port",
        "port": 3071
    },
    {
        "protocol": "UDP",
        "name": "csd-monitor",
        "port": 3072
    },
    {
        "protocol": "UDP",
        "name": "vcrp",
        "port": 3073
    },
    {
        "protocol": "UDP",
        "name": "xbox",
        "port": 3074
    },
    {
        "protocol": "UDP",
        "name": "orbix-locator",
        "port": 3075
    },
    {
        "protocol": "UDP",
        "name": "orbix-config",
        "port": 3076
    },
    {
        "protocol": "UDP",
        "name": "orbix-loc-ssl",
        "port": 3077
    },
    {
        "protocol": "UDP",
        "name": "orbix-cfg-ssl",
        "port": 3078
    },
    {
        "protocol": "UDP",
        "name": "lv-frontpanel",
        "port": 3079
    },
    {
        "protocol": "UDP",
        "name": "stm_pproc",
        "port": 3080
    },
    {
        "protocol": "UDP",
        "name": "tl1-lv",
        "port": 3081
    },
    {
        "protocol": "UDP",
        "name": "tl1-raw",
        "port": 3082
    },
    {
        "protocol": "UDP",
        "name": "tl1-telnet",
        "port": 3083
    },
    {
        "protocol": "UDP",
        "name": "itm-mccs",
        "port": 3084
    },
    {
        "protocol": "UDP",
        "name": "pcihreq",
        "port": 3085
    },
    {
        "protocol": "UDP",
        "name": "jdl-dbkitchen",
        "port": 3086
    },
    {
        "protocol": "UDP",
        "name": "asoki-sma",
        "port": 3087
    },
    {
        "protocol": "UDP",
        "name": "xdtp",
        "port": 3088
    },
    {
        "protocol": "UDP",
        "name": "ptk-alink",
        "port": 3089
    },
    {
        "protocol": "UDP",
        "name": "rtss",
        "port": 3090
    },
    {
        "protocol": "UDP",
        "name": "1ci-smcs",
        "port": 3091
    },
    {
        "protocol": "UDP",
        "name": "njfss",
        "port": 3092
    },
    {
        "protocol": "UDP",
        "name": "rapidmq-center",
        "port": 3093
    },
    {
        "protocol": "UDP",
        "name": "rapidmq-reg",
        "port": 3094
    },
    {
        "protocol": "UDP",
        "name": "panasas",
        "port": 3095
    },
    {
        "protocol": "UDP",
        "name": "ndl-aps",
        "port": 3096
    },
    {
        "protocol": "UDP",
        "name": "umm-port",
        "port": 3098
    },
    {
        "protocol": "UDP",
        "name": "chmd",
        "port": 3099
    },
    {
        "protocol": "UDP",
        "name": "opcon-xps",
        "port": 3100
    },
    {
        "protocol": "UDP",
        "name": "hp-pxpib",
        "port": 3101
    },
    {
        "protocol": "UDP",
        "name": "slslavemon",
        "port": 3102
    },
    {
        "protocol": "UDP",
        "name": "autocuesmi",
        "port": 3103
    },
    {
        "protocol": "UDP",
        "name": "autocuetime",
        "port": 3104
    },
    {
        "protocol": "UDP",
        "name": "cardbox",
        "port": 3105
    },
    {
        "protocol": "UDP",
        "name": "cardbox-http",
        "port": 3106
    },
    {
        "protocol": "UDP",
        "name": "business",
        "port": 3107
    },
    {
        "protocol": "UDP",
        "name": "geolocate",
        "port": 3108
    },
    {
        "protocol": "UDP",
        "name": "personnel",
        "port": 3109
    },
    {
        "protocol": "UDP",
        "name": "sim-control",
        "port": 3110
    },
    {
        "protocol": "UDP",
        "name": "wsynch",
        "port": 3111
    },
    {
        "protocol": "UDP",
        "name": "ksysguard",
        "port": 3112
    },
    {
        "protocol": "UDP",
        "name": "cs-auth-svr",
        "port": 3113
    },
    {
        "protocol": "UDP",
        "name": "ccmad",
        "port": 3114
    },
    {
        "protocol": "UDP",
        "name": "mctet-master",
        "port": 3115
    },
    {
        "protocol": "UDP",
        "name": "mctet-gateway",
        "port": 3116
    },
    {
        "protocol": "UDP",
        "name": "mctet-jserv",
        "port": 3117
    },
    {
        "protocol": "UDP",
        "name": "pkagent",
        "port": 3118
    },
    {
        "protocol": "UDP",
        "name": "d2000kernel",
        "port": 3119
    },
    {
        "protocol": "UDP",
        "name": "d2000webserver",
        "port": 3120
    },
    {
        "protocol": "UDP",
        "name": "vtr-emulator",
        "port": 3122
    },
    {
        "protocol": "UDP",
        "name": "edix",
        "port": 3123
    },
    {
        "protocol": "UDP",
        "name": "beacon-port",
        "port": 3124
    },
    {
        "protocol": "UDP",
        "name": "a13-an",
        "port": 3125
    },
    {
        "protocol": "UDP",
        "name": "ms-dotnetster",
        "port": 3126
    },
    {
        "protocol": "UDP",
        "name": "ctx-bridge",
        "port": 3127
    },
    {
        "protocol": "UDP",
        "name": "ndl-aas",
        "port": 3128
    },
    {
        "protocol": "UDP",
        "name": "netport-id",
        "port": 3129
    },
    {
        "protocol": "UDP",
        "name": "icpv2",
        "port": 3130
    },
    {
        "protocol": "UDP",
        "name": "netbookmark",
        "port": 3131
    },
    {
        "protocol": "UDP",
        "name": "ms-rule-engine",
        "port": 3132
    },
    {
        "protocol": "UDP",
        "name": "prism-deploy",
        "port": 3133
    },
    {
        "protocol": "UDP",
        "name": "ecp",
        "port": 3134
    },
    {
        "protocol": "UDP",
        "name": "peerbook-port",
        "port": 3135
    },
    {
        "protocol": "UDP",
        "name": "grubd",
        "port": 3136
    },
    {
        "protocol": "UDP",
        "name": "rtnt-1",
        "port": 3137
    },
    {
        "protocol": "UDP",
        "name": "rtnt-2",
        "port": 3138
    },
    {
        "protocol": "UDP",
        "name": "incognitorv",
        "port": 3139
    },
    {
        "protocol": "UDP",
        "name": "ariliamulti",
        "port": 3140
    },
    {
        "protocol": "UDP",
        "name": "vmodem",
        "port": 3141
    },
    {
        "protocol": "UDP",
        "name": "rdc-wh-eos",
        "port": 3142
    },
    {
        "protocol": "UDP",
        "name": "seaview",
        "port": 3143
    },
    {
        "protocol": "UDP",
        "name": "tarantella",
        "port": 3144
    },
    {
        "protocol": "UDP",
        "name": "csi-lfap",
        "port": 3145
    },
    {
        "protocol": "UDP",
        "name": "bears-02",
        "port": 3146
    },
    {
        "protocol": "UDP",
        "name": "rfio",
        "port": 3147
    },
    {
        "protocol": "UDP",
        "name": "nm-game-admin",
        "port": 3148
    },
    {
        "protocol": "UDP",
        "name": "nm-game-server",
        "port": 3149
    },
    {
        "protocol": "UDP",
        "name": "nm-asses-admin",
        "port": 3150
    },
    {
        "protocol": "UDP",
        "name": "nm-assessor",
        "port": 3151
    },
    {
        "protocol": "UDP",
        "name": "feitianrockey",
        "port": 3152
    },
    {
        "protocol": "UDP",
        "name": "s8-client-port",
        "port": 3153
    },
    {
        "protocol": "UDP",
        "name": "ccmrmi",
        "port": 3154
    },
    {
        "protocol": "UDP",
        "name": "jpegmpeg",
        "port": 3155
    },
    {
        "protocol": "UDP",
        "name": "indura",
        "port": 3156
    },
    {
        "protocol": "UDP",
        "name": "e3consultants",
        "port": 3157
    },
    {
        "protocol": "UDP",
        "name": "stvp",
        "port": 3158
    },
    {
        "protocol": "UDP",
        "name": "navegaweb-port",
        "port": 3159
    },
    {
        "protocol": "UDP",
        "name": "tip-app-server",
        "port": 3160
    },
    {
        "protocol": "UDP",
        "name": "doc1lm",
        "port": 3161
    },
    {
        "protocol": "UDP",
        "name": "sflm",
        "port": 3162
    },
    {
        "protocol": "UDP",
        "name": "res-sap",
        "port": 3163
    },
    {
        "protocol": "UDP",
        "name": "imprs",
        "port": 3164
    },
    {
        "protocol": "UDP",
        "name": "newgenpay",
        "port": 3165
    },
    {
        "protocol": "UDP",
        "name": "qrepos",
        "port": 3166
    },
    {
        "protocol": "UDP",
        "name": "poweroncontact",
        "port": 3167
    },
    {
        "protocol": "UDP",
        "name": "poweronnud",
        "port": 3168
    },
    {
        "protocol": "UDP",
        "name": "serverview-as",
        "port": 3169
    },
    {
        "protocol": "UDP",
        "name": "serverview-asn",
        "port": 3170
    },
    {
        "protocol": "UDP",
        "name": "serverview-gf",
        "port": 3171
    },
    {
        "protocol": "UDP",
        "name": "serverview-rm",
        "port": 3172
    },
    {
        "protocol": "UDP",
        "name": "serverview-icc",
        "port": 3173
    },
    {
        "protocol": "UDP",
        "name": "armi-server",
        "port": 3174
    },
    {
        "protocol": "UDP",
        "name": "t1-e1-over-ip",
        "port": 3175
    },
    {
        "protocol": "UDP",
        "name": "ars-master",
        "port": 3176
    },
    {
        "protocol": "UDP",
        "name": "phonex-port",
        "port": 3177
    },
    {
        "protocol": "UDP",
        "name": "radclientport",
        "port": 3178
    },
    {
        "protocol": "UDP",
        "name": "h2gf-w-2m",
        "port": 3179
    },
    {
        "protocol": "UDP",
        "name": "mc-brk-srv",
        "port": 3180
    },
    {
        "protocol": "UDP",
        "name": "bmcpatrolagent",
        "port": 3181
    },
    {
        "protocol": "UDP",
        "name": "bmcpatrolrnvu",
        "port": 3182
    },
    {
        "protocol": "UDP",
        "name": "cops-tls",
        "port": 3183
    },
    {
        "protocol": "UDP",
        "name": "apogeex-port",
        "port": 3184
    },
    {
        "protocol": "UDP",
        "name": "smpppd",
        "port": 3185
    },
    {
        "protocol": "UDP",
        "name": "iiw-port",
        "port": 3186
    },
    {
        "protocol": "UDP",
        "name": "odi-port",
        "port": 3187
    },
    {
        "protocol": "UDP",
        "name": "brcm-comm-port",
        "port": 3188
    },
    {
        "protocol": "UDP",
        "name": "pcle-infex",
        "port": 3189
    },
    {
        "protocol": "UDP",
        "name": "csvr-proxy",
        "port": 3190
    },
    {
        "protocol": "UDP",
        "name": "csvr-sslproxy",
        "port": 3191
    },
    {
        "protocol": "UDP",
        "name": "firemonrcc",
        "port": 3192
    },
    {
        "protocol": "UDP",
        "name": "spandataport",
        "port": 3193
    },
    {
        "protocol": "UDP",
        "name": "magbind",
        "port": 3194
    },
    {
        "protocol": "UDP",
        "name": "ncu-1",
        "port": 3195
    },
    {
        "protocol": "UDP",
        "name": "ncu-2",
        "port": 3196
    },
    {
        "protocol": "UDP",
        "name": "embrace-dp-s",
        "port": 3197
    },
    {
        "protocol": "UDP",
        "name": "embrace-dp-c",
        "port": 3198
    },
    {
        "protocol": "UDP",
        "name": "dmod-workspace",
        "port": 3199
    },
    {
        "protocol": "UDP",
        "name": "tick-port",
        "port": 3200
    },
    {
        "protocol": "UDP",
        "name": "cpq-tasksmart",
        "port": 3201
    },
    {
        "protocol": "UDP",
        "name": "intraintra",
        "port": 3202
    },
    {
        "protocol": "UDP",
        "name": "netwatcher-mon",
        "port": 3203
    },
    {
        "protocol": "UDP",
        "name": "netwatcher-db",
        "port": 3204
    },
    {
        "protocol": "UDP",
        "name": "isns",
        "port": 3205
    },
    {
        "protocol": "UDP",
        "name": "ironmail",
        "port": 3206
    },
    {
        "protocol": "UDP",
        "name": "vx-auth-port",
        "port": 3207
    },
    {
        "protocol": "UDP",
        "name": "pfu-prcallback",
        "port": 3208
    },
    {
        "protocol": "UDP",
        "name": "netwkpathengine",
        "port": 3209
    },
    {
        "protocol": "UDP",
        "name": "flamenco-proxy",
        "port": 3210
    },
    {
        "protocol": "UDP",
        "name": "avsecuremgmt",
        "port": 3211
    },
    {
        "protocol": "UDP",
        "name": "surveyinst",
        "port": 3212
    },
    {
        "protocol": "UDP",
        "name": "neon24x7",
        "port": 3213
    },
    {
        "protocol": "UDP",
        "name": "jmq-daemon-1",
        "port": 3214
    },
    {
        "protocol": "UDP",
        "name": "jmq-daemon-2",
        "port": 3215
    },
    {
        "protocol": "UDP",
        "name": "ferrari-foam",
        "port": 3216
    },
    {
        "protocol": "UDP",
        "name": "unite",
        "port": 3217
    },
    {
        "protocol": "UDP",
        "name": "smartpackets",
        "port": 3218
    },
    {
        "protocol": "UDP",
        "name": "wms-messenger",
        "port": 3219
    },
    {
        "protocol": "UDP",
        "name": "xnm-ssl",
        "port": 3220
    },
    {
        "protocol": "UDP",
        "name": "xnm-clear-text",
        "port": 3221
    },
    {
        "protocol": "UDP",
        "name": "glbp",
        "port": 3222
    },
    {
        "protocol": "UDP",
        "name": "digivote",
        "port": 3223
    },
    {
        "protocol": "UDP",
        "name": "aes-discovery",
        "port": 3224
    },
    {
        "protocol": "UDP",
        "name": "fcip-port",
        "port": 3225
    },
    {
        "protocol": "UDP",
        "name": "isi-irp",
        "port": 3226
    },
    {
        "protocol": "UDP",
        "name": "dwnmshttp",
        "port": 3227
    },
    {
        "protocol": "UDP",
        "name": "dwmsgserver",
        "port": 3228
    },
    {
        "protocol": "UDP",
        "name": "global-cd-port",
        "port": 3229
    },
    {
        "protocol": "UDP",
        "name": "sftdst-port",
        "port": 3230
    },
    {
        "protocol": "UDP",
        "name": "dsnl",
        "port": 3231
    },
    {
        "protocol": "UDP",
        "name": "mdtp",
        "port": 3232
    },
    {
        "protocol": "UDP",
        "name": "whisker",
        "port": 3233
    },
    {
        "protocol": "UDP",
        "name": "alchemy",
        "port": 3234
    },
    {
        "protocol": "UDP",
        "name": "mdap-port",
        "port": 3235
    },
    {
        "protocol": "UDP",
        "name": "apparenet-ts",
        "port": 3236
    },
    {
        "protocol": "UDP",
        "name": "apparenet-tps",
        "port": 3237
    },
    {
        "protocol": "UDP",
        "name": "apparenet-as",
        "port": 3238
    },
    {
        "protocol": "UDP",
        "name": "apparenet-ui",
        "port": 3239
    },
    {
        "protocol": "UDP",
        "name": "triomotion",
        "port": 3240
    },
    {
        "protocol": "UDP",
        "name": "sysorb",
        "port": 3241
    },
    {
        "protocol": "UDP",
        "name": "sdp-id-port",
        "port": 3242
    },
    {
        "protocol": "UDP",
        "name": "timelot",
        "port": 3243
    },
    {
        "protocol": "UDP",
        "name": "onesaf",
        "port": 3244
    },
    {
        "protocol": "UDP",
        "name": "vieo-fe",
        "port": 3245
    },
    {
        "protocol": "UDP",
        "name": "dvt-system",
        "port": 3246
    },
    {
        "protocol": "UDP",
        "name": "dvt-data",
        "port": 3247
    },
    {
        "protocol": "UDP",
        "name": "procos-lm",
        "port": 3248
    },
    {
        "protocol": "UDP",
        "name": "ssp",
        "port": 3249
    },
    {
        "protocol": "UDP",
        "name": "hicp",
        "port": 3250
    },
    {
        "protocol": "UDP",
        "name": "sysscanner",
        "port": 3251
    },
    {
        "protocol": "UDP",
        "name": "dhe",
        "port": 3252
    },
    {
        "protocol": "UDP",
        "name": "pda-data",
        "port": 3253
    },
    {
        "protocol": "UDP",
        "name": "pda-sys",
        "port": 3254
    },
    {
        "protocol": "UDP",
        "name": "semaphore",
        "port": 3255
    },
    {
        "protocol": "UDP",
        "name": "cpqrpm-agent",
        "port": 3256
    },
    {
        "protocol": "UDP",
        "name": "cpqrpm-server",
        "port": 3257
    },
    {
        "protocol": "UDP",
        "name": "ivecon-port",
        "port": 3258
    },
    {
        "protocol": "UDP",
        "name": "epncdp2",
        "port": 3259
    },
    {
        "protocol": "UDP",
        "name": "iscsi-target",
        "port": 3260
    },
    {
        "protocol": "UDP",
        "name": "winshadow",
        "port": 3261
    },
    {
        "protocol": "UDP",
        "name": "necp",
        "port": 3262
    },
    {
        "protocol": "UDP",
        "name": "ecolor-imager",
        "port": 3263
    },
    {
        "protocol": "UDP",
        "name": "ccmail",
        "port": 3264
    },
    {
        "protocol": "UDP",
        "name": "altav-tunnel",
        "port": 3265
    },
    {
        "protocol": "UDP",
        "name": "ns-cfg-server",
        "port": 3266
    },
    {
        "protocol": "UDP",
        "name": "ibm-dial-out",
        "port": 3267
    },
    {
        "protocol": "UDP",
        "name": "msft-gc",
        "port": 3268
    },
    {
        "protocol": "UDP",
        "name": "msft-gc-ssl",
        "port": 3269
    },
    {
        "protocol": "UDP",
        "name": "verismart",
        "port": 3270
    },
    {
        "protocol": "UDP",
        "name": "csoft-prev",
        "port": 3271
    },
    {
        "protocol": "UDP",
        "name": "user-manager",
        "port": 3272
    },
    {
        "protocol": "UDP",
        "name": "sxmp",
        "port": 3273
    },
    {
        "protocol": "UDP",
        "name": "ordinox-server",
        "port": 3274
    },
    {
        "protocol": "UDP",
        "name": "samd",
        "port": 3275
    },
    {
        "protocol": "UDP",
        "name": "maxim-asics",
        "port": 3276
    },
    {
        "protocol": "UDP",
        "name": "awg-proxy",
        "port": 3277
    },
    {
        "protocol": "UDP",
        "name": "lkcmserver",
        "port": 3278
    },
    {
        "protocol": "UDP",
        "name": "admind",
        "port": 3279
    },
    {
        "protocol": "UDP",
        "name": "vs-server",
        "port": 3280
    },
    {
        "protocol": "UDP",
        "name": "sysopt",
        "port": 3281
    },
    {
        "protocol": "UDP",
        "name": "datusorb",
        "port": 3282
    },
    {
        "protocol": "UDP",
        "name": "net-assistant",
        "port": 3283
    },
    {
        "protocol": "UDP",
        "name": "4talk",
        "port": 3284
    },
    {
        "protocol": "UDP",
        "name": "plato",
        "port": 3285
    },
    {
        "protocol": "UDP",
        "name": "e-net",
        "port": 3286
    },
    {
        "protocol": "UDP",
        "name": "directvdata",
        "port": 3287
    },
    {
        "protocol": "UDP",
        "name": "cops",
        "port": 3288
    },
    {
        "protocol": "UDP",
        "name": "enpc",
        "port": 3289
    },
    {
        "protocol": "UDP",
        "name": "caps-lm",
        "port": 3290
    },
    {
        "protocol": "UDP",
        "name": "sah-lm",
        "port": 3291
    },
    {
        "protocol": "UDP",
        "name": "cart-o-rama",
        "port": 3292
    },
    {
        "protocol": "UDP",
        "name": "fg-fps",
        "port": 3293
    },
    {
        "protocol": "UDP",
        "name": "fg-gip",
        "port": 3294
    },
    {
        "protocol": "UDP",
        "name": "dyniplookup",
        "port": 3295
    },
    {
        "protocol": "UDP",
        "name": "rib-slm",
        "port": 3296
    },
    {
        "protocol": "UDP",
        "name": "cytel-lm",
        "port": 3297
    },
    {
        "protocol": "UDP",
        "name": "deskview",
        "port": 3298
    },
    {
        "protocol": "UDP",
        "name": "pdrncs",
        "port": 3299
    },
    {
        "protocol": "UDP",
        "name": "mcs-fastmail",
        "port": 3302
    },
    {
        "protocol": "UDP",
        "name": "opsession-clnt",
        "port": 3303
    },
    {
        "protocol": "UDP",
        "name": "opsession-srvr",
        "port": 3304
    },
    {
        "protocol": "UDP",
        "name": "odette-ftp",
        "port": 3305
    },
    {
        "protocol": "UDP",
        "name": "mysql",
        "port": 3306
    },
    {
        "protocol": "UDP",
        "name": "opsession-prxy",
        "port": 3307
    },
    {
        "protocol": "UDP",
        "name": "tns-server",
        "port": 3308
    },
    {
        "protocol": "UDP",
        "name": "tns-adv",
        "port": 3309
    },
    {
        "protocol": "UDP",
        "name": "dyna-access",
        "port": 3310
    },
    {
        "protocol": "UDP",
        "name": "mcns-tel-ret",
        "port": 3311
    },
    {
        "protocol": "UDP",
        "name": "appman-server",
        "port": 3312
    },
    {
        "protocol": "UDP",
        "name": "uorb",
        "port": 3313
    },
    {
        "protocol": "UDP",
        "name": "uohost",
        "port": 3314
    },
    {
        "protocol": "UDP",
        "name": "cdid",
        "port": 3315
    },
    {
        "protocol": "UDP",
        "name": "aicc-cmi",
        "port": 3316
    },
    {
        "protocol": "UDP",
        "name": "vsaiport",
        "port": 3317
    },
    {
        "protocol": "UDP",
        "name": "ssrip",
        "port": 3318
    },
    {
        "protocol": "UDP",
        "name": "sdt-lmd",
        "port": 3319
    },
    {
        "protocol": "UDP",
        "name": "officelink2000",
        "port": 3320
    },
    {
        "protocol": "UDP",
        "name": "vnsstr",
        "port": 3321
    },
    {
        "protocol": "UDP",
        "name": "active-net",
        "port": 3322
    },
    {
        "protocol": "UDP",
        "name": "active-net",
        "port": 3323
    },
    {
        "protocol": "UDP",
        "name": "active-net",
        "port": 3324
    },
    {
        "protocol": "UDP",
        "name": "active-net",
        "port": 3325
    },
    {
        "protocol": "UDP",
        "name": "sftu",
        "port": 3326
    },
    {
        "protocol": "UDP",
        "name": "bbars",
        "port": 3327
    },
    {
        "protocol": "UDP",
        "name": "egptlm",
        "port": 3328
    },
    {
        "protocol": "UDP",
        "name": "hp-device-disc",
        "port": 3329
    },
    {
        "protocol": "UDP",
        "name": "mcs-calypsoicf",
        "port": 3330
    },
    {
        "protocol": "UDP",
        "name": "mcs-messaging",
        "port": 3331
    },
    {
        "protocol": "UDP",
        "name": "mcs-mailsvr",
        "port": 3332
    },
    {
        "protocol": "UDP",
        "name": "dec-notes",
        "port": 3333
    },
    {
        "protocol": "UDP",
        "name": "directv-web",
        "port": 3334
    },
    {
        "protocol": "UDP",
        "name": "directv-soft",
        "port": 3335
    },
    {
        "protocol": "UDP",
        "name": "directv-tick",
        "port": 3336
    },
    {
        "protocol": "UDP",
        "name": "directv-catlg",
        "port": 3337
    },
    {
        "protocol": "UDP",
        "name": "anet-b",
        "port": 3338
    },
    {
        "protocol": "UDP",
        "name": "anet-l",
        "port": 3339
    },
    {
        "protocol": "UDP",
        "name": "anet-m",
        "port": 3340
    },
    {
        "protocol": "UDP",
        "name": "anet-h",
        "port": 3341
    },
    {
        "protocol": "UDP",
        "name": "webtie",
        "port": 3342
    },
    {
        "protocol": "UDP",
        "name": "ms-cluster-net",
        "port": 3343
    },
    {
        "protocol": "UDP",
        "name": "bnt-manager",
        "port": 3344
    },
    {
        "protocol": "UDP",
        "name": "influence",
        "port": 3345
    },
    {
        "protocol": "UDP",
        "name": "trnsprntproxy",
        "port": 3346
    },
    {
        "protocol": "UDP",
        "name": "phoenix-rpc",
        "port": 3347
    },
    {
        "protocol": "UDP",
        "name": "pangolin-laser",
        "port": 3348
    },
    {
        "protocol": "UDP",
        "name": "chevinservices",
        "port": 3349
    },
    {
        "protocol": "UDP",
        "name": "findviatv",
        "port": 3350
    },
    {
        "protocol": "UDP",
        "name": "btrieve",
        "port": 3351
    },
    {
        "protocol": "UDP",
        "name": "ssql",
        "port": 3352
    },
    {
        "protocol": "UDP",
        "name": "fatpipe",
        "port": 3353
    },
    {
        "protocol": "UDP",
        "name": "suitjd",
        "port": 3354
    },
    {
        "protocol": "UDP",
        "name": "ordinox-dbase",
        "port": 3355
    },
    {
        "protocol": "UDP",
        "name": "upnotifyps",
        "port": 3356
    },
    {
        "protocol": "UDP",
        "name": "adtech-test",
        "port": 3357
    },
    {
        "protocol": "UDP",
        "name": "mpsysrmsvr",
        "port": 3358
    },
    {
        "protocol": "UDP",
        "name": "wg-netforce",
        "port": 3359
    },
    {
        "protocol": "UDP",
        "name": "kv-server",
        "port": 3360
    },
    {
        "protocol": "UDP",
        "name": "kv-agent",
        "port": 3361
    },
    {
        "protocol": "UDP",
        "name": "dj-ilm",
        "port": 3362
    },
    {
        "protocol": "UDP",
        "name": "nati-vi-server",
        "port": 3363
    },
    {
        "protocol": "UDP",
        "name": "creativeserver",
        "port": 3364
    },
    {
        "protocol": "UDP",
        "name": "contentserver",
        "port": 3365
    },
    {
        "protocol": "UDP",
        "name": "creativepartnr",
        "port": 3366
    },
    {
        "protocol": "UDP",
        "name": "satvid-datalnk",
        "port": 3367
    },
    {
        "protocol": "UDP",
        "name": "satvid-datalnk",
        "port": 3368
    },
    {
        "protocol": "UDP",
        "name": "satvid-datalnk",
        "port": 3369
    },
    {
        "protocol": "UDP",
        "name": "satvid-datalnk",
        "port": 3370
    },
    {
        "protocol": "UDP",
        "name": "satvid-datalnk",
        "port": 3371
    },
    {
        "protocol": "UDP",
        "name": "tip2",
        "port": 3372
    },
    {
        "protocol": "UDP",
        "name": "lavenir-lm",
        "port": 3373
    },
    {
        "protocol": "UDP",
        "name": "cluster-disc",
        "port": 3374
    },
    {
        "protocol": "UDP",
        "name": "vsnm-agent",
        "port": 3375
    },
    {
        "protocol": "UDP",
        "name": "cdbroker",
        "port": 3376
    },
    {
        "protocol": "UDP",
        "name": "cogsys-lm",
        "port": 3377
    },
    {
        "protocol": "UDP",
        "name": "wsicopy",
        "port": 3378
    },
    {
        "protocol": "UDP",
        "name": "socorfs",
        "port": 3379
    },
    {
        "protocol": "UDP",
        "name": "sns-channels",
        "port": 3380
    },
    {
        "protocol": "UDP",
        "name": "geneous",
        "port": 3381
    },
    {
        "protocol": "UDP",
        "name": "fujitsu-neat",
        "port": 3382
    },
    {
        "protocol": "UDP",
        "name": "esp-lm",
        "port": 3383
    },
    {
        "protocol": "UDP",
        "name": "hp-clic",
        "port": 3384
    },
    {
        "protocol": "UDP",
        "name": "qnxnetman",
        "port": 3385
    },
    {
        "protocol": "UDP",
        "name": "gprs-sig",
        "port": 3386
    },
    {
        "protocol": "UDP",
        "name": "backroomnet",
        "port": 3387
    },
    {
        "protocol": "UDP",
        "name": "cbserver",
        "port": 3388
    },
    {
        "protocol": "UDP",
        "name": "ms-wbt-server",
        "port": 3389
    },
    {
        "protocol": "UDP",
        "name": "dsc",
        "port": 3390
    },
    {
        "protocol": "UDP",
        "name": "savant",
        "port": 3391
    },
    {
        "protocol": "UDP",
        "name": "efi-lm",
        "port": 3392
    },
    {
        "protocol": "UDP",
        "name": "d2k-tapestry1",
        "port": 3393
    },
    {
        "protocol": "UDP",
        "name": "d2k-tapestry2",
        "port": 3394
    },
    {
        "protocol": "UDP",
        "name": "dyna-lm",
        "port": 3395
    },
    {
        "protocol": "UDP",
        "name": "printer_agent",
        "port": 3396
    },
    {
        "protocol": "UDP",
        "name": "cloanto-lm",
        "port": 3397
    },
    {
        "protocol": "UDP",
        "name": "mercantile",
        "port": 3398
    },
    {
        "protocol": "UDP",
        "name": "csms",
        "port": 3399
    },
    {
        "protocol": "UDP",
        "name": "csms2",
        "port": 3400
    },
    {
        "protocol": "UDP",
        "name": "filecast",
        "port": 3401
    },
    {
        "protocol": "UDP",
        "name": "fxaengine-net",
        "port": 3402
    },
    {
        "protocol": "UDP",
        "name": "copysnap",
        "port": 3403
    },
    {
        "protocol": "UDP",
        "name": "nokia-ann-ch1",
        "port": 3405
    },
    {
        "protocol": "UDP",
        "name": "nokia-ann-ch2",
        "port": 3406
    },
    {
        "protocol": "UDP",
        "name": "ldap-admin",
        "port": 3407
    },
    {
        "protocol": "UDP",
        "name": "issapi",
        "port": 3408
    },
    {
        "protocol": "UDP",
        "name": "networklens",
        "port": 3409
    },
    {
        "protocol": "UDP",
        "name": "networklenss",
        "port": 3410
    },
    {
        "protocol": "UDP",
        "name": "biolink-auth",
        "port": 3411
    },
    {
        "protocol": "UDP",
        "name": "xmlblaster",
        "port": 3412
    },
    {
        "protocol": "UDP",
        "name": "svnet",
        "port": 3413
    },
    {
        "protocol": "UDP",
        "name": "wip-port",
        "port": 3414
    },
    {
        "protocol": "UDP",
        "name": "bcinameservice",
        "port": 3415
    },
    {
        "protocol": "UDP",
        "name": "commandport",
        "port": 3416
    },
    {
        "protocol": "UDP",
        "name": "csvr",
        "port": 3417
    },
    {
        "protocol": "UDP",
        "name": "rnmap",
        "port": 3418
    },
    {
        "protocol": "UDP",
        "name": "softaudit",
        "port": 3419
    },
    {
        "protocol": "UDP",
        "name": "ifcp-port",
        "port": 3420
    },
    {
        "protocol": "UDP",
        "name": "bmap",
        "port": 3421
    },
    {
        "protocol": "UDP",
        "name": "rusb-sys-port",
        "port": 3422
    },
    {
        "protocol": "UDP",
        "name": "xtrm",
        "port": 3423
    },
    {
        "protocol": "UDP",
        "name": "xtrms",
        "port": 3424
    },
    {
        "protocol": "UDP",
        "name": "agps-port",
        "port": 3425
    },
    {
        "protocol": "UDP",
        "name": "arkivio",
        "port": 3426
    },
    {
        "protocol": "UDP",
        "name": "websphere-snmp",
        "port": 3427
    },
    {
        "protocol": "UDP",
        "name": "twcss",
        "port": 3428
    },
    {
        "protocol": "UDP",
        "name": "gcsp",
        "port": 3429
    },
    {
        "protocol": "UDP",
        "name": "ssdispatch",
        "port": 3430
    },
    {
        "protocol": "UDP",
        "name": "ndl-als",
        "port": 3431
    },
    {
        "protocol": "UDP",
        "name": "osdcp",
        "port": 3432
    },
    {
        "protocol": "UDP",
        "name": "alta-smp",
        "port": 3433
    },
    {
        "protocol": "UDP",
        "name": "opencm",
        "port": 3434
    },
    {
        "protocol": "UDP",
        "name": "pacom",
        "port": 3435
    },
    {
        "protocol": "UDP",
        "name": "gc-config",
        "port": 3436
    },
    {
        "protocol": "UDP",
        "name": "autocueds",
        "port": 3437
    },
    {
        "protocol": "UDP",
        "name": "spiral-admin",
        "port": 3438
    },
    {
        "protocol": "UDP",
        "name": "hri-port",
        "port": 3439
    },
    {
        "protocol": "UDP",
        "name": "ans-console",
        "port": 3440
    },
    {
        "protocol": "UDP",
        "name": "connect-client",
        "port": 3441
    },
    {
        "protocol": "UDP",
        "name": "connect-server",
        "port": 3442
    },
    {
        "protocol": "UDP",
        "name": "ov-nnm-websrv",
        "port": 3443
    },
    {
        "protocol": "UDP",
        "name": "denali-server",
        "port": 3444
    },
    {
        "protocol": "UDP",
        "name": "monp",
        "port": 3445
    },
    {
        "protocol": "UDP",
        "name": "3comfaxrpc",
        "port": 3446
    },
    {
        "protocol": "UDP",
        "name": "cddn",
        "port": 3447
    },
    {
        "protocol": "UDP",
        "name": "dnc-port",
        "port": 3448
    },
    {
        "protocol": "UDP",
        "name": "hotu-chat",
        "port": 3449
    },
    {
        "protocol": "UDP",
        "name": "castorproxy",
        "port": 3450
    },
    {
        "protocol": "UDP",
        "name": "asam",
        "port": 3451
    },
    {
        "protocol": "UDP",
        "name": "sabp-signal",
        "port": 3452
    },
    {
        "protocol": "UDP",
        "name": "pscupd",
        "port": 3453
    },
    {
        "protocol": "UDP",
        "name": "prsvp",
        "port": 3455
    },
    {
        "protocol": "UDP",
        "name": "vat",
        "port": 3456
    },
    {
        "protocol": "UDP",
        "name": "vat-control",
        "port": 3457
    },
    {
        "protocol": "UDP",
        "name": "d3winosfi",
        "port": 3458
    },
    {
        "protocol": "UDP",
        "name": "integral",
        "port": 3459
    },
    {
        "protocol": "UDP",
        "name": "edm-manager",
        "port": 3460
    },
    {
        "protocol": "UDP",
        "name": "edm-stager",
        "port": 3461
    },
    {
        "protocol": "UDP",
        "name": "edm-std-notify",
        "port": 3462
    },
    {
        "protocol": "UDP",
        "name": "edm-adm-notify",
        "port": 3463
    },
    {
        "protocol": "UDP",
        "name": "edm-mgr-sync",
        "port": 3464
    },
    {
        "protocol": "UDP",
        "name": "edm-mgr-cntrl",
        "port": 3465
    },
    {
        "protocol": "UDP",
        "name": "workflow",
        "port": 3466
    },
    {
        "protocol": "UDP",
        "name": "rcst",
        "port": 3467
    },
    {
        "protocol": "UDP",
        "name": "ttcmremotectrl",
        "port": 3468
    },
    {
        "protocol": "UDP",
        "name": "pluribus",
        "port": 3469
    },
    {
        "protocol": "UDP",
        "name": "jt400",
        "port": 3470
    },
    {
        "protocol": "UDP",
        "name": "jt400-ssl",
        "port": 3471
    },
    {
        "protocol": "UDP",
        "name": "jaugsremotec-1",
        "port": 3472
    },
    {
        "protocol": "UDP",
        "name": "jaugsremotec-2",
        "port": 3473
    },
    {
        "protocol": "UDP",
        "name": "ttntspauto",
        "port": 3474
    },
    {
        "protocol": "UDP",
        "name": "genisar-port",
        "port": 3475
    },
    {
        "protocol": "UDP",
        "name": "nppmp",
        "port": 3476
    },
    {
        "protocol": "UDP",
        "name": "ecomm",
        "port": 3477
    },
    {
        "protocol": "UDP",
        "name": "nat-stun-port",
        "port": 3478
    },
    {
        "protocol": "UDP",
        "name": "twrpc",
        "port": 3479
    },
    {
        "protocol": "UDP",
        "name": "plethora",
        "port": 3480
    },
    {
        "protocol": "UDP",
        "name": "cleanerliverc",
        "port": 3481
    },
    {
        "protocol": "UDP",
        "name": "vulture",
        "port": 3482
    },
    {
        "protocol": "UDP",
        "name": "slim-devices",
        "port": 3483
    },
    {
        "protocol": "UDP",
        "name": "gbs-stp",
        "port": 3484
    },
    {
        "protocol": "UDP",
        "name": "celatalk",
        "port": 3485
    },
    {
        "protocol": "UDP",
        "name": "ifsf-hb-port",
        "port": 3486
    },
    {
        "protocol": "UDP",
        "name": "ltctcp",
        "port": 3487
    },
    {
        "protocol": "UDP",
        "name": "fs-rh-srv",
        "port": 3488
    },
    {
        "protocol": "UDP",
        "name": "dtp-dia",
        "port": 3489
    },
    {
        "protocol": "UDP",
        "name": "colubris",
        "port": 3490
    },
    {
        "protocol": "UDP",
        "name": "swr-port",
        "port": 3491
    },
    {
        "protocol": "UDP",
        "name": "tvdumtray-port",
        "port": 3492
    },
    {
        "protocol": "UDP",
        "name": "nut",
        "port": 3493
    },
    {
        "protocol": "UDP",
        "name": "ibm3494",
        "port": 3494
    },
    {
        "protocol": "UDP",
        "name": "seclayer-tcp",
        "port": 3495
    },
    {
        "protocol": "UDP",
        "name": "seclayer-tls",
        "port": 3496
    },
    {
        "protocol": "UDP",
        "name": "ipether232port",
        "port": 3497
    },
    {
        "protocol": "UDP",
        "name": "dashpas-port",
        "port": 3498
    },
    {
        "protocol": "UDP",
        "name": "sccip-media",
        "port": 3499
    },
    {
        "protocol": "UDP",
        "name": "rtmp-port",
        "port": 3500
    },
    {
        "protocol": "UDP",
        "name": "isoft-p2p",
        "port": 3501
    },
    {
        "protocol": "UDP",
        "name": "avinstalldisc",
        "port": 3502
    },
    {
        "protocol": "UDP",
        "name": "lsp-ping",
        "port": 3503
    },
    {
        "protocol": "UDP",
        "name": "ironstorm",
        "port": 3504
    },
    {
        "protocol": "UDP",
        "name": "ccmcomm",
        "port": 3505
    },
    {
        "protocol": "UDP",
        "name": "apc-3506",
        "port": 3506
    },
    {
        "protocol": "UDP",
        "name": "nesh-broker",
        "port": 3507
    },
    {
        "protocol": "UDP",
        "name": "interactionweb",
        "port": 3508
    },
    {
        "protocol": "UDP",
        "name": "vt-ssl",
        "port": 3509
    },
    {
        "protocol": "UDP",
        "name": "xss-port",
        "port": 3510
    },
    {
        "protocol": "UDP",
        "name": "webmail-2",
        "port": 3511
    },
    {
        "protocol": "UDP",
        "name": "aztec",
        "port": 3512
    },
    {
        "protocol": "UDP",
        "name": "arcpd",
        "port": 3513
    },
    {
        "protocol": "UDP",
        "name": "must-p2p",
        "port": 3514
    },
    {
        "protocol": "UDP",
        "name": "must-backplane",
        "port": 3515
    },
    {
        "protocol": "UDP",
        "name": "smartcard-port",
        "port": 3516
    },
    {
        "protocol": "UDP",
        "name": "802-11-iapp",
        "port": 3517
    },
    {
        "protocol": "UDP",
        "name": "artifact-msg",
        "port": 3518
    },
    {
        "protocol": "UDP",
        "name": "galileo",
        "port": 3519
    },
    {
        "protocol": "UDP",
        "name": "galileolog",
        "port": 3520
    },
    {
        "protocol": "UDP",
        "name": "mc3ss",
        "port": 3521
    },
    {
        "protocol": "UDP",
        "name": "nssocketport",
        "port": 3522
    },
    {
        "protocol": "UDP",
        "name": "odeumservlink",
        "port": 3523
    },
    {
        "protocol": "UDP",
        "name": "ecmport",
        "port": 3524
    },
    {
        "protocol": "UDP",
        "name": "eisport",
        "port": 3525
    },
    {
        "protocol": "UDP",
        "name": "starquiz-port",
        "port": 3526
    },
    {
        "protocol": "UDP",
        "name": "beserver-msg-q",
        "port": 3527
    },
    {
        "protocol": "UDP",
        "name": "jboss-iiop",
        "port": 3528
    },
    {
        "protocol": "UDP",
        "name": "jboss-iiop-ssl",
        "port": 3529
    },
    {
        "protocol": "UDP",
        "name": "gf",
        "port": 3530
    },
    {
        "protocol": "UDP",
        "name": "joltid",
        "port": 3531
    },
    {
        "protocol": "UDP",
        "name": "raven-rmp",
        "port": 3532
    },
    {
        "protocol": "UDP",
        "name": "urld-port",
        "port": 3533
    },
    {
        "protocol": "UDP",
        "name": "ms-la",
        "port": 3535
    },
    {
        "protocol": "UDP",
        "name": "snac",
        "port": 3536
    },
    {
        "protocol": "UDP",
        "name": "ni-visa-remote",
        "port": 3537
    },
    {
        "protocol": "UDP",
        "name": "ibm-diradm",
        "port": 3538
    },
    {
        "protocol": "UDP",
        "name": "ibm-diradm-ssl",
        "port": 3539
    },
    {
        "protocol": "UDP",
        "name": "pnrp-port",
        "port": 3540
    },
    {
        "protocol": "UDP",
        "name": "voispeed-port",
        "port": 3541
    },
    {
        "protocol": "UDP",
        "name": "hacl-monitor",
        "port": 3542
    },
    {
        "protocol": "UDP",
        "name": "qftest-lookup",
        "port": 3543
    },
    {
        "protocol": "UDP",
        "name": "teredo",
        "port": 3544
    },
    {
        "protocol": "UDP",
        "name": "camac",
        "port": 3545
    },
    {
        "protocol": "UDP",
        "name": "symantec-sim",
        "port": 3547
    },
    {
        "protocol": "UDP",
        "name": "interworld",
        "port": 3548
    },
    {
        "protocol": "UDP",
        "name": "tellumat-nms",
        "port": 3549
    },
    {
        "protocol": "UDP",
        "name": "ssmpp",
        "port": 3550
    },
    {
        "protocol": "UDP",
        "name": "apcupsd",
        "port": 3551
    },
    {
        "protocol": "UDP",
        "name": "taserver",
        "port": 3552
    },
    {
        "protocol": "UDP",
        "name": "rbr-discovery",
        "port": 3553
    },
    {
        "protocol": "UDP",
        "name": "questnotify",
        "port": 3554
    },
    {
        "protocol": "UDP",
        "name": "razor",
        "port": 3555
    },
    {
        "protocol": "UDP",
        "name": "sky-transport",
        "port": 3556
    },
    {
        "protocol": "UDP",
        "name": "personalos-001",
        "port": 3557
    },
    {
        "protocol": "UDP",
        "name": "mcp-port",
        "port": 3558
    },
    {
        "protocol": "UDP",
        "name": "cctv-port",
        "port": 3559
    },
    {
        "protocol": "UDP",
        "name": "iniserve-port",
        "port": 3560
    },
    {
        "protocol": "UDP",
        "name": "bmc-onekey",
        "port": 3561
    },
    {
        "protocol": "UDP",
        "name": "sdbproxy",
        "port": 3562
    },
    {
        "protocol": "UDP",
        "name": "watcomdebug",
        "port": 3563
    },
    {
        "protocol": "UDP",
        "name": "esimport",
        "port": 3564
    },
    {
        "protocol": "UDP",
        "name": "quest-launcher",
        "port": 3566
    },
    {
        "protocol": "UDP",
        "name": "emware-oft",
        "port": 3567
    },
    {
        "protocol": "UDP",
        "name": "emware-epss",
        "port": 3568
    },
    {
        "protocol": "UDP",
        "name": "mbg-ctrl",
        "port": 3569
    },
    {
        "protocol": "UDP",
        "name": "mccwebsvr-port",
        "port": 3570
    },
    {
        "protocol": "UDP",
        "name": "megardsvr-port",
        "port": 3571
    },
    {
        "protocol": "UDP",
        "name": "megaregsvrport",
        "port": 3572
    },
    {
        "protocol": "UDP",
        "name": "tag-ups-1",
        "port": 3573
    },
    {
        "protocol": "UDP",
        "name": "dmaf-server",
        "port": 3574
    },
    {
        "protocol": "UDP",
        "name": "ccm-port",
        "port": 3575
    },
    {
        "protocol": "UDP",
        "name": "cmc-port",
        "port": 3576
    },
    {
        "protocol": "UDP",
        "name": "config-port",
        "port": 3577
    },
    {
        "protocol": "UDP",
        "name": "data-port",
        "port": 3578
    },
    {
        "protocol": "UDP",
        "name": "ttat3lb",
        "port": 3579
    },
    {
        "protocol": "UDP",
        "name": "nati-svrloc",
        "port": 3580
    },
    {
        "protocol": "UDP",
        "name": "kfxaclicensing",
        "port": 3581
    },
    {
        "protocol": "UDP",
        "name": "press",
        "port": 3582
    },
    {
        "protocol": "UDP",
        "name": "canex-watch",
        "port": 3583
    },
    {
        "protocol": "UDP",
        "name": "u-dbap",
        "port": 3584
    },
    {
        "protocol": "UDP",
        "name": "emprise-lls",
        "port": 3585
    },
    {
        "protocol": "UDP",
        "name": "emprise-lsc",
        "port": 3586
    },
    {
        "protocol": "UDP",
        "name": "p2pgroup",
        "port": 3587
    },
    {
        "protocol": "UDP",
        "name": "sentinel",
        "port": 3588
    },
    {
        "protocol": "UDP",
        "name": "isomair",
        "port": 3589
    },
    {
        "protocol": "UDP",
        "name": "wv-csp-sms",
        "port": 3590
    },
    {
        "protocol": "UDP",
        "name": "gtrack-server",
        "port": 3591
    },
    {
        "protocol": "UDP",
        "name": "gtrack-ne",
        "port": 3592
    },
    {
        "protocol": "UDP",
        "name": "bpmd",
        "port": 3593
    },
    {
        "protocol": "UDP",
        "name": "mediaspace",
        "port": 3594
    },
    {
        "protocol": "UDP",
        "name": "shareapp",
        "port": 3595
    },
    {
        "protocol": "UDP",
        "name": "iw-mmogame",
        "port": 3596
    },
    {
        "protocol": "UDP",
        "name": "a14",
        "port": 3597
    },
    {
        "protocol": "UDP",
        "name": "a15",
        "port": 3598
    },
    {
        "protocol": "UDP",
        "name": "quasar-server",
        "port": 3599
    },
    {
        "protocol": "UDP",
        "name": "trap-daemon",
        "port": 3600
    },
    {
        "protocol": "UDP",
        "name": "visinet-gui",
        "port": 3601
    },
    {
        "protocol": "UDP",
        "name": "infiniswitchcl",
        "port": 3602
    },
    {
        "protocol": "UDP",
        "name": "int-rcv-cntrl",
        "port": 3603
    },
    {
        "protocol": "UDP",
        "name": "bmc-jmx-port",
        "port": 3604
    },
    {
        "protocol": "UDP",
        "name": "comcam-io",
        "port": 3605
    },
    {
        "protocol": "UDP",
        "name": "splitlock",
        "port": 3606
    },
    {
        "protocol": "UDP",
        "name": "precise-i3",
        "port": 3607
    },
    {
        "protocol": "UDP",
        "name": "trendchip-dcp",
        "port": 3608
    },
    {
        "protocol": "UDP",
        "name": "cpdi-pidas-cm",
        "port": 3609
    },
    {
        "protocol": "UDP",
        "name": "echonet",
        "port": 3610
    },
    {
        "protocol": "UDP",
        "name": "six-degrees",
        "port": 3611
    },
    {
        "protocol": "UDP",
        "name": "hp-dataprotect",
        "port": 3612
    },
    {
        "protocol": "UDP",
        "name": "alaris-disc",
        "port": 3613
    },
    {
        "protocol": "UDP",
        "name": "sigma-port",
        "port": 3614
    },
    {
        "protocol": "UDP",
        "name": "start-network",
        "port": 3615
    },
    {
        "protocol": "UDP",
        "name": "cd3o-protocol",
        "port": 3616
    },
    {
        "protocol": "UDP",
        "name": "sharp-server",
        "port": 3617
    },
    {
        "protocol": "UDP",
        "name": "aairnet-1",
        "port": 3618
    },
    {
        "protocol": "UDP",
        "name": "aairnet-2",
        "port": 3619
    },
    {
        "protocol": "UDP",
        "name": "ep-pcp",
        "port": 3620
    },
    {
        "protocol": "UDP",
        "name": "ep-nsp",
        "port": 3621
    },
    {
        "protocol": "UDP",
        "name": "ff-lr-port",
        "port": 3622
    },
    {
        "protocol": "UDP",
        "name": "haipe-discover",
        "port": 3623
    },
    {
        "protocol": "UDP",
        "name": "dist-upgrade",
        "port": 3624
    },
    {
        "protocol": "UDP",
        "name": "volley",
        "port": 3625
    },
    {
        "protocol": "UDP",
        "name": "bvcdaemon-port",
        "port": 3626
    },
    {
        "protocol": "UDP",
        "name": "jamserverport",
        "port": 3627
    },
    {
        "protocol": "UDP",
        "name": "ept-machine",
        "port": 3628
    },
    {
        "protocol": "UDP",
        "name": "escvpnet",
        "port": 3629
    },
    {
        "protocol": "UDP",
        "name": "cs-remote-db",
        "port": 3630
    },
    {
        "protocol": "UDP",
        "name": "cs-services",
        "port": 3631
    },
    {
        "protocol": "UDP",
        "name": "distcc",
        "port": 3632
    },
    {
        "protocol": "UDP",
        "name": "wacp",
        "port": 3633
    },
    {
        "protocol": "UDP",
        "name": "hlibmgr",
        "port": 3634
    },
    {
        "protocol": "UDP",
        "name": "sdo",
        "port": 3635
    },
    {
        "protocol": "UDP",
        "name": "opscenter",
        "port": 3636
    },
    {
        "protocol": "UDP",
        "name": "scservp",
        "port": 3637
    },
    {
        "protocol": "UDP",
        "name": "ehp-backup",
        "port": 3638
    },
    {
        "protocol": "UDP",
        "name": "xap-ha",
        "port": 3639
    },
    {
        "protocol": "UDP",
        "name": "netplay-port1",
        "port": 3640
    },
    {
        "protocol": "UDP",
        "name": "netplay-port2",
        "port": 3641
    },
    {
        "protocol": "UDP",
        "name": "juxml-port",
        "port": 3642
    },
    {
        "protocol": "UDP",
        "name": "audiojuggler",
        "port": 3643
    },
    {
        "protocol": "UDP",
        "name": "ssowatch",
        "port": 3644
    },
    {
        "protocol": "UDP",
        "name": "cyc",
        "port": 3645
    },
    {
        "protocol": "UDP",
        "name": "xss-srv-port",
        "port": 3646
    },
    {
        "protocol": "UDP",
        "name": "splitlock-gw",
        "port": 3647
    },
    {
        "protocol": "UDP",
        "name": "fjcp",
        "port": 3648
    },
    {
        "protocol": "UDP",
        "name": "nmmp",
        "port": 3649
    },
    {
        "protocol": "UDP",
        "name": "prismiq-plugin",
        "port": 3650
    },
    {
        "protocol": "UDP",
        "name": "xrpc-registry",
        "port": 3651
    },
    {
        "protocol": "UDP",
        "name": "vxcrnbuport",
        "port": 3652
    },
    {
        "protocol": "UDP",
        "name": "tsp",
        "port": 3653
    },
    {
        "protocol": "UDP",
        "name": "vaprtm",
        "port": 3654
    },
    {
        "protocol": "UDP",
        "name": "abatemgr",
        "port": 3655
    },
    {
        "protocol": "UDP",
        "name": "abatjss",
        "port": 3656
    },
    {
        "protocol": "UDP",
        "name": "immedianet-bcn",
        "port": 3657
    },
    {
        "protocol": "UDP",
        "name": "ps-ams",
        "port": 3658
    },
    {
        "protocol": "UDP",
        "name": "apple-sasl",
        "port": 3659
    },
    {
        "protocol": "UDP",
        "name": "can-nds-ssl",
        "port": 3660
    },
    {
        "protocol": "UDP",
        "name": "can-ferret-ssl",
        "port": 3661
    },
    {
        "protocol": "UDP",
        "name": "pserver",
        "port": 3662
    },
    {
        "protocol": "UDP",
        "name": "dtp",
        "port": 3663
    },
    {
        "protocol": "UDP",
        "name": "ups-engine",
        "port": 3664
    },
    {
        "protocol": "UDP",
        "name": "ent-engine",
        "port": 3665
    },
    {
        "protocol": "UDP",
        "name": "eserver-pap",
        "port": 3666
    },
    {
        "protocol": "UDP",
        "name": "infoexch",
        "port": 3667
    },
    {
        "protocol": "UDP",
        "name": "dell-rm-port",
        "port": 3668
    },
    {
        "protocol": "UDP",
        "name": "casanswmgmt",
        "port": 3669
    },
    {
        "protocol": "UDP",
        "name": "smile",
        "port": 3670
    },
    {
        "protocol": "UDP",
        "name": "efcp",
        "port": 3671
    },
    {
        "protocol": "UDP",
        "name": "lispworks-orb",
        "port": 3672
    },
    {
        "protocol": "UDP",
        "name": "mediavault-gui",
        "port": 3673
    },
    {
        "protocol": "UDP",
        "name": "wininstall-ipc",
        "port": 3674
    },
    {
        "protocol": "UDP",
        "name": "calltrax",
        "port": 3675
    },
    {
        "protocol": "UDP",
        "name": "va-pacbase",
        "port": 3676
    },
    {
        "protocol": "UDP",
        "name": "roverlog",
        "port": 3677
    },
    {
        "protocol": "UDP",
        "name": "ipr-dglt",
        "port": 3678
    },
    {
        "protocol": "UDP",
        "name": "newton-dock",
        "port": 3679
    },
    {
        "protocol": "UDP",
        "name": "npds-tracker",
        "port": 3680
    },
    {
        "protocol": "UDP",
        "name": "bts-x73",
        "port": 3681
    },
    {
        "protocol": "UDP",
        "name": "cas-mapi",
        "port": 3682
    },
    {
        "protocol": "UDP",
        "name": "bmc-ea",
        "port": 3683
    },
    {
        "protocol": "UDP",
        "name": "faxstfx-port",
        "port": 3684
    },
    {
        "protocol": "UDP",
        "name": "dsx-agent",
        "port": 3685
    },
    {
        "protocol": "UDP",
        "name": "tnmpv2",
        "port": 3686
    },
    {
        "protocol": "UDP",
        "name": "simple-push",
        "port": 3687
    },
    {
        "protocol": "UDP",
        "name": "simple-push-s",
        "port": 3688
    },
    {
        "protocol": "UDP",
        "name": "daap",
        "port": 3689
    },
    {
        "protocol": "UDP",
        "name": "svn",
        "port": 3690
    },
    {
        "protocol": "UDP",
        "name": "magaya-network",
        "port": 3691
    },
    {
        "protocol": "UDP",
        "name": "intelsync",
        "port": 3692
    },
    {
        "protocol": "UDP",
        "name": "gttp",
        "port": 3693
    },
    {
        "protocol": "UDP",
        "name": "vpntpp",
        "port": 3694
    },
    {
        "protocol": "UDP",
        "name": "bmc-data-coll",
        "port": 3695
    },
    {
        "protocol": "UDP",
        "name": "telnetcpcd",
        "port": 3696
    },
    {
        "protocol": "UDP",
        "name": "nw-license",
        "port": 3697
    },
    {
        "protocol": "UDP",
        "name": "sagectlpanel",
        "port": 3698
    },
    {
        "protocol": "UDP",
        "name": "kpn-icw",
        "port": 3699
    },
    {
        "protocol": "UDP",
        "name": "lrs-paging",
        "port": 3700
    },
    {
        "protocol": "UDP",
        "name": "netcelera",
        "port": 3701
    },
    {
        "protocol": "UDP",
        "name": "ws-discovery",
        "port": 3702
    },
    {
        "protocol": "UDP",
        "name": "adobeserver-3",
        "port": 3703
    },
    {
        "protocol": "UDP",
        "name": "adobeserver-4",
        "port": 3704
    },
    {
        "protocol": "UDP",
        "name": "adobeserver-5",
        "port": 3705
    },
    {
        "protocol": "UDP",
        "name": "rt-event",
        "port": 3706
    },
    {
        "protocol": "UDP",
        "name": "rt-event-s",
        "port": 3707
    },
    {
        "protocol": "UDP",
        "name": "sun-as-iiops",
        "port": 3708
    },
    {
        "protocol": "UDP",
        "name": "ca-idms",
        "port": 3709
    },
    {
        "protocol": "UDP",
        "name": "portgate-auth",
        "port": 3710
    },
    {
        "protocol": "UDP",
        "name": "edb-server2",
        "port": 3711
    },
    {
        "protocol": "UDP",
        "name": "sentinel-ent",
        "port": 3712
    },
    {
        "protocol": "UDP",
        "name": "tftps",
        "port": 3713
    },
    {
        "protocol": "UDP",
        "name": "delos-dms",
        "port": 3714
    },
    {
        "protocol": "UDP",
        "name": "anoto-rendezv",
        "port": 3715
    },
    {
        "protocol": "UDP",
        "name": "wv-csp-sms-cir",
        "port": 3716
    },
    {
        "protocol": "UDP",
        "name": "wv-csp-udp-cir",
        "port": 3717
    },
    {
        "protocol": "UDP",
        "name": "opus-services",
        "port": 3718
    },
    {
        "protocol": "UDP",
        "name": "itelserverport",
        "port": 3719
    },
    {
        "protocol": "UDP",
        "name": "ufastro-instr",
        "port": 3720
    },
    {
        "protocol": "UDP",
        "name": "xsync",
        "port": 3721
    },
    {
        "protocol": "UDP",
        "name": "xserveraid",
        "port": 3722
    },
    {
        "protocol": "UDP",
        "name": "sychrond",
        "port": 3723
    },
    {
        "protocol": "UDP",
        "name": "blizwow",
        "port": 3724
    },
    {
        "protocol": "UDP",
        "name": "na-er-tip",
        "port": 3725
    },
    {
        "protocol": "UDP",
        "name": "array-manager",
        "port": 3726
    },
    {
        "protocol": "UDP",
        "name": "e-mdu",
        "port": 3727
    },
    {
        "protocol": "UDP",
        "name": "e-woa",
        "port": 3728
    },
    {
        "protocol": "UDP",
        "name": "fksp-audit",
        "port": 3729
    },
    {
        "protocol": "UDP",
        "name": "client-ctrl",
        "port": 3730
    },
    {
        "protocol": "UDP",
        "name": "smap",
        "port": 3731
    },
    {
        "protocol": "UDP",
        "name": "m-wnn",
        "port": 3732
    },
    {
        "protocol": "UDP",
        "name": "multip-msg",
        "port": 3733
    },
    {
        "protocol": "UDP",
        "name": "synel-data",
        "port": 3734
    },
    {
        "protocol": "UDP",
        "name": "pwdis",
        "port": 3735
    },
    {
        "protocol": "UDP",
        "name": "rs-rmi",
        "port": 3736
    },
    {
        "protocol": "UDP",
        "name": "versatalk",
        "port": 3738
    },
    {
        "protocol": "UDP",
        "name": "launchbird-lm",
        "port": 3739
    },
    {
        "protocol": "UDP",
        "name": "heartbeat",
        "port": 3740
    },
    {
        "protocol": "UDP",
        "name": "wysdma",
        "port": 3741
    },
    {
        "protocol": "UDP",
        "name": "cst-port",
        "port": 3742
    },
    {
        "protocol": "UDP",
        "name": "ipcs-command",
        "port": 3743
    },
    {
        "protocol": "UDP",
        "name": "sasg",
        "port": 3744
    },
    {
        "protocol": "UDP",
        "name": "gw-call-port",
        "port": 3745
    },
    {
        "protocol": "UDP",
        "name": "linktest",
        "port": 3746
    },
    {
        "protocol": "UDP",
        "name": "linktest-s",
        "port": 3747
    },
    {
        "protocol": "UDP",
        "name": "webdata",
        "port": 3748
    },
    {
        "protocol": "UDP",
        "name": "cimtrak",
        "port": 3749
    },
    {
        "protocol": "UDP",
        "name": "cbos-ip-port",
        "port": 3750
    },
    {
        "protocol": "UDP",
        "name": "gprs-cube",
        "port": 3751
    },
    {
        "protocol": "UDP",
        "name": "vipremoteagent",
        "port": 3752
    },
    {
        "protocol": "UDP",
        "name": "nattyserver",
        "port": 3753
    },
    {
        "protocol": "UDP",
        "name": "timestenbroker",
        "port": 3754
    },
    {
        "protocol": "UDP",
        "name": "sas-remote-hlp",
        "port": 3755
    },
    {
        "protocol": "UDP",
        "name": "canon-capt",
        "port": 3756
    },
    {
        "protocol": "UDP",
        "name": "grf-port",
        "port": 3757
    },
    {
        "protocol": "UDP",
        "name": "apw-registry",
        "port": 3758
    },
    {
        "protocol": "UDP",
        "name": "exapt-lmgr",
        "port": 3759
    },
    {
        "protocol": "UDP",
        "name": "adtempusclient",
        "port": 3760
    },
    {
        "protocol": "UDP",
        "name": "gsakmp",
        "port": 3761
    },
    {
        "protocol": "UDP",
        "name": "gbs-smp",
        "port": 3762
    },
    {
        "protocol": "UDP",
        "name": "xo-wave",
        "port": 3763
    },
    {
        "protocol": "UDP",
        "name": "mni-prot-rout",
        "port": 3764
    },
    {
        "protocol": "UDP",
        "name": "rtraceroute",
        "port": 3765
    },
    {
        "protocol": "UDP",
        "name": "listmgr-port",
        "port": 3767
    },
    {
        "protocol": "UDP",
        "name": "rblcheckd",
        "port": 3768
    },
    {
        "protocol": "UDP",
        "name": "haipe-otnk",
        "port": 3769
    },
    {
        "protocol": "UDP",
        "name": "cindycollab",
        "port": 3770
    },
    {
        "protocol": "UDP",
        "name": "paging-port",
        "port": 3771
    },
    {
        "protocol": "UDP",
        "name": "ctp",
        "port": 3772
    },
    {
        "protocol": "UDP",
        "name": "ctdhercules",
        "port": 3773
    },
    {
        "protocol": "UDP",
        "name": "zicom",
        "port": 3774
    },
    {
        "protocol": "UDP",
        "name": "ispmmgr",
        "port": 3775
    },
    {
        "protocol": "UDP",
        "name": "dvcprov-port",
        "port": 3776
    },
    {
        "protocol": "UDP",
        "name": "jibe-eb",
        "port": 3777
    },
    {
        "protocol": "UDP",
        "name": "c-h-it-port",
        "port": 3778
    },
    {
        "protocol": "UDP",
        "name": "cognima",
        "port": 3779
    },
    {
        "protocol": "UDP",
        "name": "nnp",
        "port": 3780
    },
    {
        "protocol": "UDP",
        "name": "abcvoice-port",
        "port": 3781
    },
    {
        "protocol": "UDP",
        "name": "iso-tp0s",
        "port": 3782
    },
    {
        "protocol": "UDP",
        "name": "bim-pem",
        "port": 3783
    },
    {
        "protocol": "UDP",
        "name": "bfd-control",
        "port": 3784
    },
    {
        "protocol": "UDP",
        "name": "bfd-echo",
        "port": 3785
    },
    {
        "protocol": "UDP",
        "name": "upstriggervsw",
        "port": 3786
    },
    {
        "protocol": "UDP",
        "name": "fintrx",
        "port": 3787
    },
    {
        "protocol": "UDP",
        "name": "isrp-port",
        "port": 3788
    },
    {
        "protocol": "UDP",
        "name": "remotedeploy",
        "port": 3789
    },
    {
        "protocol": "UDP",
        "name": "quickbooksrds",
        "port": 3790
    },
    {
        "protocol": "UDP",
        "name": "tvnetworkvideo",
        "port": 3791
    },
    {
        "protocol": "UDP",
        "name": "sitewatch",
        "port": 3792
    },
    {
        "protocol": "UDP",
        "name": "dcsoftware",
        "port": 3793
    },
    {
        "protocol": "UDP",
        "name": "jaus",
        "port": 3794
    },
    {
        "protocol": "UDP",
        "name": "myblast",
        "port": 3795
    },
    {
        "protocol": "UDP",
        "name": "spw-dialer",
        "port": 3796
    },
    {
        "protocol": "UDP",
        "name": "idps",
        "port": 3797
    },
    {
        "protocol": "UDP",
        "name": "minilock",
        "port": 3798
    },
    {
        "protocol": "UDP",
        "name": "radius-dynauth",
        "port": 3799
    },
    {
        "protocol": "UDP",
        "name": "pwgpsi",
        "port": 3800
    },
    {
        "protocol": "UDP",
        "name": "vhd",
        "port": 3802
    },
    {
        "protocol": "UDP",
        "name": "soniqsync",
        "port": 3803
    },
    {
        "protocol": "UDP",
        "name": "iqnet-port",
        "port": 3804
    },
    {
        "protocol": "UDP",
        "name": "tcpdataserver",
        "port": 3805
    },
    {
        "protocol": "UDP",
        "name": "wsmlb",
        "port": 3806
    },
    {
        "protocol": "UDP",
        "name": "spugna",
        "port": 3807
    },
    {
        "protocol": "UDP",
        "name": "sun-as-iiops-ca",
        "port": 3808
    },
    {
        "protocol": "UDP",
        "name": "apocd",
        "port": 3809
    },
    {
        "protocol": "UDP",
        "name": "wlanauth",
        "port": 3810
    },
    {
        "protocol": "UDP",
        "name": "amp",
        "port": 3811
    },
    {
        "protocol": "UDP",
        "name": "neto-wol-server",
        "port": 3812
    },
    {
        "protocol": "UDP",
        "name": "rap-ip",
        "port": 3813
    },
    {
        "protocol": "UDP",
        "name": "neto-dcs",
        "port": 3814
    },
    {
        "protocol": "UDP",
        "name": "lansurveyorxml",
        "port": 3815
    },
    {
        "protocol": "UDP",
        "name": "sunlps-http",
        "port": 3816
    },
    {
        "protocol": "UDP",
        "name": "tapeware",
        "port": 3817
    },
    {
        "protocol": "UDP",
        "name": "crinis-hb",
        "port": 3818
    },
    {
        "protocol": "UDP",
        "name": "epl-slp",
        "port": 3819
    },
    {
        "protocol": "UDP",
        "name": "scp",
        "port": 3820
    },
    {
        "protocol": "UDP",
        "name": "pmcp",
        "port": 3821
    },
    {
        "protocol": "UDP",
        "name": "acp-discovery",
        "port": 3822
    },
    {
        "protocol": "UDP",
        "name": "acp-conduit",
        "port": 3823
    },
    {
        "protocol": "UDP",
        "name": "acp-policy",
        "port": 3824
    },
    {
        "protocol": "UDP",
        "name": "markem-dcp",
        "port": 3836
    },
    {
        "protocol": "UDP",
        "name": "mkm-discovery",
        "port": 3837
    },
    {
        "protocol": "UDP",
        "name": "sos",
        "port": 3838
    },
    {
        "protocol": "UDP",
        "name": "amx-rms",
        "port": 3839
    },
    {
        "protocol": "UDP",
        "name": "flirtmitmir",
        "port": 3840
    },
    {
        "protocol": "UDP",
        "name": "zfirm-shiprush3",
        "port": 3841
    },
    {
        "protocol": "UDP",
        "name": "nhci",
        "port": 3842
    },
    {
        "protocol": "UDP",
        "name": "quest-agent",
        "port": 3843
    },
    {
        "protocol": "UDP",
        "name": "rnm",
        "port": 3844
    },
    {
        "protocol": "UDP",
        "name": "v-one-spp",
        "port": 3845
    },
    {
        "protocol": "UDP",
        "name": "an-pcp",
        "port": 3846
    },
    {
        "protocol": "UDP",
        "name": "msfw-control",
        "port": 3847
    },
    {
        "protocol": "UDP",
        "name": "item",
        "port": 3848
    },
    {
        "protocol": "UDP",
        "name": "spw-dnspreload",
        "port": 3849
    },
    {
        "protocol": "UDP",
        "name": "qtms-bootstrap",
        "port": 3850
    },
    {
        "protocol": "UDP",
        "name": "spectraport",
        "port": 3851
    },
    {
        "protocol": "UDP",
        "name": "sse-app-config",
        "port": 3852
    },
    {
        "protocol": "UDP",
        "name": "sscan",
        "port": 3853
    },
    {
        "protocol": "UDP",
        "name": "stryker-com",
        "port": 3854
    },
    {
        "protocol": "UDP",
        "name": "opentrac",
        "port": 3855
    },
    {
        "protocol": "UDP",
        "name": "informer",
        "port": 3856
    },
    {
        "protocol": "UDP",
        "name": "trap-port",
        "port": 3857
    },
    {
        "protocol": "UDP",
        "name": "trap-port-mom",
        "port": 3858
    },
    {
        "protocol": "UDP",
        "name": "nav-port",
        "port": 3859
    },
    {
        "protocol": "UDP",
        "name": "ewlm",
        "port": 3860
    },
    {
        "protocol": "UDP",
        "name": "winshadow-hd",
        "port": 3861
    },
    {
        "protocol": "UDP",
        "name": "giga-pocket",
        "port": 3862
    },
    {
        "protocol": "UDP",
        "name": "asap-tcp",
        "port": 3863
    },
    {
        "protocol": "UDP",
        "name": "asap-tcp-tls",
        "port": 3864
    },
    {
        "protocol": "UDP",
        "name": "xpl",
        "port": 3865
    },
    {
        "protocol": "UDP",
        "name": "dzdaemon",
        "port": 3866
    },
    {
        "protocol": "UDP",
        "name": "dzoglserver",
        "port": 3867
    },
    {
        "protocol": "UDP",
        "name": "ovsam-mgmt",
        "port": 3869
    },
    {
        "protocol": "UDP",
        "name": "ovsam-d-agent",
        "port": 3870
    },
    {
        "protocol": "UDP",
        "name": "avocent-adsap",
        "port": 3871
    },
    {
        "protocol": "UDP",
        "name": "oem-agent",
        "port": 3872
    },
    {
        "protocol": "UDP",
        "name": "fagordnc",
        "port": 3873
    },
    {
        "protocol": "UDP",
        "name": "sixxsconfig",
        "port": 3874
    },
    {
        "protocol": "UDP",
        "name": "pnbscada",
        "port": 3875
    },
    {
        "protocol": "UDP",
        "name": "dl_agent",
        "port": 3876
    },
    {
        "protocol": "UDP",
        "name": "xmpcr-interface",
        "port": 3877
    },
    {
        "protocol": "UDP",
        "name": "fotogcad",
        "port": 3878
    },
    {
        "protocol": "UDP",
        "name": "appss-lm",
        "port": 3879
    },
    {
        "protocol": "UDP",
        "name": "microgrid",
        "port": 3880
    },
    {
        "protocol": "UDP",
        "name": "idac",
        "port": 3881
    },
    {
        "protocol": "UDP",
        "name": "msdts1",
        "port": 3882
    },
    {
        "protocol": "UDP",
        "name": "vrpn",
        "port": 3883
    },
    {
        "protocol": "UDP",
        "name": "softrack-meter",
        "port": 3884
    },
    {
        "protocol": "UDP",
        "name": "topflow-ssl",
        "port": 3885
    },
    {
        "protocol": "UDP",
        "name": "nei-management",
        "port": 3886
    },
    {
        "protocol": "UDP",
        "name": "ciphire-data",
        "port": 3887
    },
    {
        "protocol": "UDP",
        "name": "ciphire-serv",
        "port": 3888
    },
    {
        "protocol": "UDP",
        "name": "dandv-tester",
        "port": 3889
    },
    {
        "protocol": "UDP",
        "name": "ndsconnect",
        "port": 3890
    },
    {
        "protocol": "UDP",
        "name": "rtc-pm-port",
        "port": 3891
    },
    {
        "protocol": "UDP",
        "name": "pcc-image-port",
        "port": 3892
    },
    {
        "protocol": "UDP",
        "name": "cgi-starapi",
        "port": 3893
    },
    {
        "protocol": "UDP",
        "name": "syam-agent",
        "port": 3894
    },
    {
        "protocol": "UDP",
        "name": "syam-smc",
        "port": 3895
    },
    {
        "protocol": "UDP",
        "name": "sdo-tls",
        "port": 3896
    },
    {
        "protocol": "UDP",
        "name": "sdo-ssh",
        "port": 3897
    },
    {
        "protocol": "UDP",
        "name": "senip",
        "port": 3898
    },
    {
        "protocol": "UDP",
        "name": "itv-control",
        "port": 3899
    },
    {
        "protocol": "UDP",
        "name": "udt_os",
        "port": 3900
    },
    {
        "protocol": "UDP",
        "name": "nimsh",
        "port": 3901
    },
    {
        "protocol": "UDP",
        "name": "nimaux",
        "port": 3902
    },
    {
        "protocol": "UDP",
        "name": "charsetmgr",
        "port": 3903
    },
    {
        "protocol": "UDP",
        "name": "omnilink-port",
        "port": 3904
    },
    {
        "protocol": "UDP",
        "name": "mupdate",
        "port": 3905
    },
    {
        "protocol": "UDP",
        "name": "topovista-data",
        "port": 3906
    },
    {
        "protocol": "UDP",
        "name": "imoguia-port",
        "port": 3907
    },
    {
        "protocol": "UDP",
        "name": "hppronetman",
        "port": 3908
    },
    {
        "protocol": "UDP",
        "name": "surfcontrolcpa",
        "port": 3909
    },
    {
        "protocol": "UDP",
        "name": "prnrequest",
        "port": 3910
    },
    {
        "protocol": "UDP",
        "name": "prnstatus",
        "port": 3911
    },
    {
        "protocol": "UDP",
        "name": "gbmt-stars",
        "port": 3912
    },
    {
        "protocol": "UDP",
        "name": "listcrt-port",
        "port": 3913
    },
    {
        "protocol": "UDP",
        "name": "listcrt-port-2",
        "port": 3914
    },
    {
        "protocol": "UDP",
        "name": "agcat",
        "port": 3915
    },
    {
        "protocol": "UDP",
        "name": "wysdmc",
        "port": 3916
    },
    {
        "protocol": "UDP",
        "name": "aftmux",
        "port": 3917
    },
    {
        "protocol": "UDP",
        "name": "pktcablemmcops",
        "port": 3918
    },
    {
        "protocol": "UDP",
        "name": "hyperip",
        "port": 3919
    },
    {
        "protocol": "UDP",
        "name": "exasoftport1",
        "port": 3920
    },
    {
        "protocol": "UDP",
        "name": "herodotus-net",
        "port": 3921
    },
    {
        "protocol": "UDP",
        "name": "sor-update",
        "port": 3922
    },
    {
        "protocol": "UDP",
        "name": "symb-sb-port",
        "port": 3923
    },
    {
        "protocol": "UDP",
        "name": "mpl-gprs-port",
        "port": 3924
    },
    {
        "protocol": "UDP",
        "name": "zmp",
        "port": 3925
    },
    {
        "protocol": "UDP",
        "name": "winport",
        "port": 3926
    },
    {
        "protocol": "UDP",
        "name": "natdataservice",
        "port": 3927
    },
    {
        "protocol": "UDP",
        "name": "netboot-pxe",
        "port": 3928
    },
    {
        "protocol": "UDP",
        "name": "smauth-port",
        "port": 3929
    },
    {
        "protocol": "UDP",
        "name": "syam-webserver",
        "port": 3930
    },
    {
        "protocol": "UDP",
        "name": "msr-plugin-port",
        "port": 3931
    },
    {
        "protocol": "UDP",
        "name": "dyn-site",
        "port": 3932
    },
    {
        "protocol": "UDP",
        "name": "plbserve-port",
        "port": 3933
    },
    {
        "protocol": "UDP",
        "name": "sunfm-port",
        "port": 3934
    },
    {
        "protocol": "UDP",
        "name": "sdp-portmapper",
        "port": 3935
    },
    {
        "protocol": "UDP",
        "name": "mailprox",
        "port": 3936
    },
    {
        "protocol": "UDP",
        "name": "dvbservdscport",
        "port": 3937
    },
    {
        "protocol": "UDP",
        "name": "dbcontrol_agent",
        "port": 3938
    },
    {
        "protocol": "UDP",
        "name": "aamp",
        "port": 3939
    },
    {
        "protocol": "UDP",
        "name": "xecp-node",
        "port": 3940
    },
    {
        "protocol": "UDP",
        "name": "homeportal-web",
        "port": 3941
    },
    {
        "protocol": "UDP",
        "name": "srdp",
        "port": 3942
    },
    {
        "protocol": "UDP",
        "name": "tig",
        "port": 3943
    },
    {
        "protocol": "UDP",
        "name": "sops",
        "port": 3944
    },
    {
        "protocol": "UDP",
        "name": "emcads",
        "port": 3945
    },
    {
        "protocol": "UDP",
        "name": "backupedge",
        "port": 3946
    },
    {
        "protocol": "UDP",
        "name": "ccp",
        "port": 3947
    },
    {
        "protocol": "UDP",
        "name": "apdap",
        "port": 3948
    },
    {
        "protocol": "UDP",
        "name": "drip",
        "port": 3949
    },
    {
        "protocol": "UDP",
        "name": "namemunge",
        "port": 3950
    },
    {
        "protocol": "UDP",
        "name": "pwgippfax",
        "port": 3951
    },
    {
        "protocol": "UDP",
        "name": "i3-sessionmgr",
        "port": 3952
    },
    {
        "protocol": "UDP",
        "name": "xmlink-connect",
        "port": 3953
    },
    {
        "protocol": "UDP",
        "name": "adrep",
        "port": 3954
    },
    {
        "protocol": "UDP",
        "name": "p2pcommunity",
        "port": 3955
    },
    {
        "protocol": "UDP",
        "name": "gvcp",
        "port": 3956
    },
    {
        "protocol": "UDP",
        "name": "mqe-broker",
        "port": 3957
    },
    {
        "protocol": "UDP",
        "name": "mqe-agent",
        "port": 3958
    },
    {
        "protocol": "UDP",
        "name": "treehopper",
        "port": 3959
    },
    {
        "protocol": "UDP",
        "name": "bess",
        "port": 3960
    },
    {
        "protocol": "UDP",
        "name": "proaxess",
        "port": 3961
    },
    {
        "protocol": "UDP",
        "name": "sbi-agent",
        "port": 3962
    },
    {
        "protocol": "UDP",
        "name": "thrp",
        "port": 3963
    },
    {
        "protocol": "UDP",
        "name": "sasggprs",
        "port": 3964
    },
    {
        "protocol": "UDP",
        "name": "ati-ip-to-ncpe",
        "port": 3965
    },
    {
        "protocol": "UDP",
        "name": "bflckmgr",
        "port": 3966
    },
    {
        "protocol": "UDP",
        "name": "ppsms",
        "port": 3967
    },
    {
        "protocol": "UDP",
        "name": "ianywhere-dbns",
        "port": 3968
    },
    {
        "protocol": "UDP",
        "name": "landmarks",
        "port": 3969
    },
    {
        "protocol": "UDP",
        "name": "cobraclient",
        "port": 3970
    },
    {
        "protocol": "UDP",
        "name": "cobraserver",
        "port": 3971
    },
    {
        "protocol": "UDP",
        "name": "iconp",
        "port": 3972
    },
    {
        "protocol": "UDP",
        "name": "progistics",
        "port": 3973
    },
    {
        "protocol": "UDP",
        "name": "citysearch",
        "port": 3974
    },
    {
        "protocol": "UDP",
        "name": "airshot",
        "port": 3975
    },
    {
        "protocol": "UDP",
        "name": "mapper-nodemgr",
        "port": 3984
    },
    {
        "protocol": "UDP",
        "name": "mapper-mapethd",
        "port": 3985
    },
    {
        "protocol": "UDP",
        "name": "mapper-ws_ethd",
        "port": 3986
    },
    {
        "protocol": "UDP",
        "name": "centerline",
        "port": 3987
    },
    {
        "protocol": "UDP",
        "name": "dcs-config",
        "port": 3988
    },
    {
        "protocol": "UDP",
        "name": "bv-queryengine",
        "port": 3989
    },
    {
        "protocol": "UDP",
        "name": "bv-is",
        "port": 3990
    },
    {
        "protocol": "UDP",
        "name": "bv-smcsrv",
        "port": 3991
    },
    {
        "protocol": "UDP",
        "name": "bv-ds",
        "port": 3992
    },
    {
        "protocol": "UDP",
        "name": "bv-agent",
        "port": 3993
    },
    {
        "protocol": "UDP",
        "name": "iss-mgmt-ssl",
        "port": 3995
    },
    {
        "protocol": "UDP",
        "name": "abcsoftware",
        "port": 3996
    },
    {
        "protocol": "UDP",
        "name": "agentsease-db",
        "port": 3997
    },
    {
        "protocol": "UDP",
        "name": "terabase",
        "port": 4000
    },
    {
        "protocol": "UDP",
        "name": "newoak",
        "port": 4001
    },
    {
        "protocol": "UDP",
        "name": "pxc-spvr-ft",
        "port": 4002
    },
    {
        "protocol": "UDP",
        "name": "pxc-splr-ft",
        "port": 4003
    },
    {
        "protocol": "UDP",
        "name": "pxc-roid",
        "port": 4004
    },
    {
        "protocol": "UDP",
        "name": "pxc-pin",
        "port": 4005
    },
    {
        "protocol": "UDP",
        "name": "pxc-spvr",
        "port": 4006
    },
    {
        "protocol": "UDP",
        "name": "pxc-splr",
        "port": 4007
    },
    {
        "protocol": "UDP",
        "name": "netcheque",
        "port": 4008
    },
    {
        "protocol": "UDP",
        "name": "chimera-hwm",
        "port": 4009
    },
    {
        "protocol": "UDP",
        "name": "samsung-unidex",
        "port": 4010
    },
    {
        "protocol": "UDP",
        "name": "altserviceboot",
        "port": 4011
    },
    {
        "protocol": "UDP",
        "name": "pda-gate",
        "port": 4012
    },
    {
        "protocol": "UDP",
        "name": "acl-manager",
        "port": 4013
    },
    {
        "protocol": "UDP",
        "name": "taiclock",
        "port": 4014
    },
    {
        "protocol": "UDP",
        "name": "talarian-mcast1",
        "port": 4015
    },
    {
        "protocol": "UDP",
        "name": "talarian-mcast2",
        "port": 4016
    },
    {
        "protocol": "UDP",
        "name": "talarian-mcast3",
        "port": 4017
    },
    {
        "protocol": "UDP",
        "name": "talarian-mcast4",
        "port": 4018
    },
    {
        "protocol": "UDP",
        "name": "talarian-mcast5",
        "port": 4019
    },
    {
        "protocol": "UDP",
        "name": "trap",
        "port": 4020
    },
    {
        "protocol": "UDP",
        "name": "nexus-portal",
        "port": 4021
    },
    {
        "protocol": "UDP",
        "name": "dnox",
        "port": 4022
    },
    {
        "protocol": "UDP",
        "name": "esnm-zoning",
        "port": 4023
    },
    {
        "protocol": "UDP",
        "name": "tnp1-port",
        "port": 4024
    },
    {
        "protocol": "UDP",
        "name": "partimage",
        "port": 4025
    },
    {
        "protocol": "UDP",
        "name": "as-debug",
        "port": 4026
    },
    {
        "protocol": "UDP",
        "name": "bxp",
        "port": 4027
    },
    {
        "protocol": "UDP",
        "name": "dtserver-port",
        "port": 4028
    },
    {
        "protocol": "UDP",
        "name": "ip-qsig",
        "port": 4029
    },
    {
        "protocol": "UDP",
        "name": "jdmn-port",
        "port": 4030
    },
    {
        "protocol": "UDP",
        "name": "suucp",
        "port": 4031
    },
    {
        "protocol": "UDP",
        "name": "vrts-auth-port",
        "port": 4032
    },
    {
        "protocol": "UDP",
        "name": "sanavigator",
        "port": 4033
    },
    {
        "protocol": "UDP",
        "name": "ubxd",
        "port": 4034
    },
    {
        "protocol": "UDP",
        "name": "wap-push-http",
        "port": 4035
    },
    {
        "protocol": "UDP",
        "name": "wap-push-https",
        "port": 4036
    },
    {
        "protocol": "UDP",
        "name": "ravehd",
        "port": 4037
    },
    {
        "protocol": "UDP",
        "name": "yo-main",
        "port": 4040
    },
    {
        "protocol": "UDP",
        "name": "houston",
        "port": 4041
    },
    {
        "protocol": "UDP",
        "name": "ldxp",
        "port": 4042
    },
    {
        "protocol": "UDP",
        "name": "nirp",
        "port": 4043
    },
    {
        "protocol": "UDP",
        "name": "ltp",
        "port": 4044
    },
    {
        "protocol": "UDP",
        "name": "npp",
        "port": 4045
    },
    {
        "protocol": "UDP",
        "name": "acp-proto",
        "port": 4046
    },
    {
        "protocol": "UDP",
        "name": "ctp-state",
        "port": 4047
    },
    {
        "protocol": "UDP",
        "name": "wafs",
        "port": 4049
    },
    {
        "protocol": "UDP",
        "name": "cisco-wafs",
        "port": 4050
    },
    {
        "protocol": "UDP",
        "name": "bre",
        "port": 4096
    },
    {
        "protocol": "UDP",
        "name": "patrolview",
        "port": 4097
    },
    {
        "protocol": "UDP",
        "name": "drmsfsd",
        "port": 4098
    },
    {
        "protocol": "UDP",
        "name": "dpcp",
        "port": 4099
    },
    {
        "protocol": "UDP",
        "name": "igo-incognito",
        "port": 4100
    },
    {
        "protocol": "UDP",
        "name": "brlp-0",
        "port": 4101
    },
    {
        "protocol": "UDP",
        "name": "brlp-1",
        "port": 4102
    },
    {
        "protocol": "UDP",
        "name": "brlp-2",
        "port": 4103
    },
    {
        "protocol": "UDP",
        "name": "brlp-3",
        "port": 4104
    },
    {
        "protocol": "UDP",
        "name": "xgrid",
        "port": 4111
    },
    {
        "protocol": "UDP",
        "name": "jomamqmonitor",
        "port": 4114
    },
    {
        "protocol": "UDP",
        "name": "nuts_dem",
        "port": 4132
    },
    {
        "protocol": "UDP",
        "name": "nuts_bootp",
        "port": 4133
    },
    {
        "protocol": "UDP",
        "name": "nifty-hmi",
        "port": 4134
    },
    {
        "protocol": "UDP",
        "name": "nettest",
        "port": 4138
    },
    {
        "protocol": "UDP",
        "name": "thrtx",
        "port": 4139
    },
    {
        "protocol": "UDP",
        "name": "oirtgsvc",
        "port": 4141
    },
    {
        "protocol": "UDP",
        "name": "oidocsvc",
        "port": 4142
    },
    {
        "protocol": "UDP",
        "name": "oidsr",
        "port": 4143
    },
    {
        "protocol": "UDP",
        "name": "vvr-control",
        "port": 4145
    },
    {
        "protocol": "UDP",
        "name": "atlinks",
        "port": 4154
    },
    {
        "protocol": "UDP",
        "name": "jini-discovery",
        "port": 4160
    },
    {
        "protocol": "UDP",
        "name": "omscontact",
        "port": 4161
    },
    {
        "protocol": "UDP",
        "name": "omstopology",
        "port": 4162
    },
    {
        "protocol": "UDP",
        "name": "eims-admin",
        "port": 4199
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4200
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4201
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4202
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4203
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4204
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4205
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4206
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4207
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4208
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4209
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4210
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4211
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4212
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4213
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4214
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4215
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4216
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4217
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4218
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4219
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4220
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4221
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4222
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4223
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4224
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4225
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4226
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4227
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4228
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4229
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4230
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4231
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4232
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4233
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4234
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4235
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4236
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4237
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4238
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4239
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4240
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4241
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4242
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4243
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4244
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4245
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4246
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4247
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4248
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4249
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4250
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4251
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4252
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4253
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4254
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4255
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4256
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4257
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4258
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4259
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4260
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4261
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4262
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4263
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4264
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4265
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4266
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4267
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4268
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4269
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4270
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4271
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4272
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4273
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4274
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4275
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4276
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4277
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4278
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4279
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4280
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4281
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4282
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4283
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4284
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4285
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4286
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4287
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4288
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4289
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4290
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4291
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4292
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4293
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4294
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4295
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4296
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4297
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4298
    },
    {
        "protocol": "UDP",
        "name": "vrml-multi-use",
        "port": 4299
    },
    {
        "protocol": "UDP",
        "name": "corelccam",
        "port": 4300
    },
    {
        "protocol": "UDP",
        "name": "rwhois",
        "port": 4321
    },
    {
        "protocol": "UDP",
        "name": "unicall",
        "port": 4343
    },
    {
        "protocol": "UDP",
        "name": "vinainstall",
        "port": 4344
    },
    {
        "protocol": "UDP",
        "name": "m4-network-as",
        "port": 4345
    },
    {
        "protocol": "UDP",
        "name": "elanlm",
        "port": 4346
    },
    {
        "protocol": "UDP",
        "name": "lansurveyor",
        "port": 4347
    },
    {
        "protocol": "UDP",
        "name": "itose",
        "port": 4348
    },
    {
        "protocol": "UDP",
        "name": "fsportmap",
        "port": 4349
    },
    {
        "protocol": "UDP",
        "name": "net-device",
        "port": 4350
    },
    {
        "protocol": "UDP",
        "name": "plcy-net-svcs",
        "port": 4351
    },
    {
        "protocol": "UDP",
        "name": "pjlink",
        "port": 4352
    },
    {
        "protocol": "UDP",
        "name": "f5-iquery",
        "port": 4353
    },
    {
        "protocol": "UDP",
        "name": "qsnet-trans",
        "port": 4354
    },
    {
        "protocol": "UDP",
        "name": "qsnet-workst",
        "port": 4355
    },
    {
        "protocol": "UDP",
        "name": "qsnet-assist",
        "port": 4356
    },
    {
        "protocol": "UDP",
        "name": "qsnet-cond",
        "port": 4357
    },
    {
        "protocol": "UDP",
        "name": "qsnet-nucl",
        "port": 4358
    },
    {
        "protocol": "UDP",
        "name": "epmd",
        "port": 4369
    },
    {
        "protocol": "UDP",
        "name": "ds-srv",
        "port": 4400
    },
    {
        "protocol": "UDP",
        "name": "ds-srvr",
        "port": 4401
    },
    {
        "protocol": "UDP",
        "name": "ds-clnt",
        "port": 4402
    },
    {
        "protocol": "UDP",
        "name": "ds-user",
        "port": 4403
    },
    {
        "protocol": "UDP",
        "name": "ds-admin",
        "port": 4404
    },
    {
        "protocol": "UDP",
        "name": "ds-mail",
        "port": 4405
    },
    {
        "protocol": "UDP",
        "name": "ds-slp",
        "port": 4406
    },
    {
        "protocol": "UDP",
        "name": "beacon-port-2",
        "port": 4426
    },
    {
        "protocol": "UDP",
        "name": "saris",
        "port": 4442
    },
    {
        "protocol": "UDP",
        "name": "pharos",
        "port": 4443
    },
    {
        "protocol": "UDP",
        "name": "nv-video",
        "port": 4444
    },
    {
        "protocol": "UDP",
        "name": "upnotifyp",
        "port": 4445
    },
    {
        "protocol": "UDP",
        "name": "n1-fwp",
        "port": 4446
    },
    {
        "protocol": "UDP",
        "name": "n1-rmgmt",
        "port": 4447
    },
    {
        "protocol": "UDP",
        "name": "asc-slmd",
        "port": 4448
    },
    {
        "protocol": "UDP",
        "name": "privatewire",
        "port": 4449
    },
    {
        "protocol": "UDP",
        "name": "camp",
        "port": 4450
    },
    {
        "protocol": "UDP",
        "name": "ctisystemmsg",
        "port": 4451
    },
    {
        "protocol": "UDP",
        "name": "ctiprogramload",
        "port": 4452
    },
    {
        "protocol": "UDP",
        "name": "nssalertmgr",
        "port": 4453
    },
    {
        "protocol": "UDP",
        "name": "nssagentmgr",
        "port": 4454
    },
    {
        "protocol": "UDP",
        "name": "prchat-user",
        "port": 4455
    },
    {
        "protocol": "UDP",
        "name": "prchat-server",
        "port": 4456
    },
    {
        "protocol": "UDP",
        "name": "prRegister",
        "port": 4457
    },
    {
        "protocol": "UDP",
        "name": "hpssmgmt",
        "port": 4484
    },
    {
        "protocol": "UDP",
        "name": "ipsec-nat-t",
        "port": 4500
    },
    {
        "protocol": "UDP",
        "name": "ehs",
        "port": 4535
    },
    {
        "protocol": "UDP",
        "name": "ehs-ssl",
        "port": 4536
    },
    {
        "protocol": "UDP",
        "name": "wssauthsvc",
        "port": 4537
    },
    {
        "protocol": "UDP",
        "name": "worldscores",
        "port": 4545
    },
    {
        "protocol": "UDP",
        "name": "sf-lm",
        "port": 4546
    },
    {
        "protocol": "UDP",
        "name": "lanner-lm",
        "port": 4547
    },
    {
        "protocol": "UDP",
        "name": "synchromesh",
        "port": 4548
    },
    {
        "protocol": "UDP",
        "name": "aegate",
        "port": 4549
    },
    {
        "protocol": "UDP",
        "name": "rsip",
        "port": 4555
    },
    {
        "protocol": "UDP",
        "name": "hylafax",
        "port": 4559
    },
    {
        "protocol": "UDP",
        "name": "tram",
        "port": 4567
    },
    {
        "protocol": "UDP",
        "name": "bmc-reporting",
        "port": 4568
    },
    {
        "protocol": "UDP",
        "name": "iax",
        "port": 4569
    },
    {
        "protocol": "UDP",
        "name": "a21-an-1xbs",
        "port": 4597
    },
    {
        "protocol": "UDP",
        "name": "a16-an-an",
        "port": 4598
    },
    {
        "protocol": "UDP",
        "name": "a17-an-an",
        "port": 4599
    },
    {
        "protocol": "UDP",
        "name": "piranha1",
        "port": 4600
    },
    {
        "protocol": "UDP",
        "name": "piranha2",
        "port": 4601
    },
    {
        "protocol": "UDP",
        "name": "playsta2-app",
        "port": 4658
    },
    {
        "protocol": "UDP",
        "name": "playsta2-lob",
        "port": 4659
    },
    {
        "protocol": "UDP",
        "name": "smaclmgr",
        "port": 4660
    },
    {
        "protocol": "UDP",
        "name": "kar2ouche",
        "port": 4661
    },
    {
        "protocol": "UDP",
        "name": "oms",
        "port": 4662
    },
    {
        "protocol": "UDP",
        "name": "noteit",
        "port": 4663
    },
    {
        "protocol": "UDP",
        "name": "ems",
        "port": 4664
    },
    {
        "protocol": "UDP",
        "name": "contclientms",
        "port": 4665
    },
    {
        "protocol": "UDP",
        "name": "eportcomm",
        "port": 4666
    },
    {
        "protocol": "UDP",
        "name": "mmacomm",
        "port": 4667
    },
    {
        "protocol": "UDP",
        "name": "mmaeds",
        "port": 4668
    },
    {
        "protocol": "UDP",
        "name": "eportcommdata",
        "port": 4669
    },
    {
        "protocol": "UDP",
        "name": "acter",
        "port": 4671
    },
    {
        "protocol": "UDP",
        "name": "rfa",
        "port": 4672
    },
    {
        "protocol": "UDP",
        "name": "cxws",
        "port": 4673
    },
    {
        "protocol": "UDP",
        "name": "appiq-mgmt",
        "port": 4674
    },
    {
        "protocol": "UDP",
        "name": "dhct-status",
        "port": 4675
    },
    {
        "protocol": "UDP",
        "name": "dhct-alerts",
        "port": 4676
    },
    {
        "protocol": "UDP",
        "name": "bcs",
        "port": 4677
    },
    {
        "protocol": "UDP",
        "name": "traversal",
        "port": 4678
    },
    {
        "protocol": "UDP",
        "name": "mgesupervision",
        "port": 4679
    },
    {
        "protocol": "UDP",
        "name": "mgemanagement",
        "port": 4680
    },
    {
        "protocol": "UDP",
        "name": "parliant",
        "port": 4681
    },
    {
        "protocol": "UDP",
        "name": "finisar",
        "port": 4682
    },
    {
        "protocol": "UDP",
        "name": "spike",
        "port": 4683
    },
    {
        "protocol": "UDP",
        "name": "rfid-rp1",
        "port": 4684
    },
    {
        "protocol": "UDP",
        "name": "autopac",
        "port": 4685
    },
    {
        "protocol": "UDP",
        "name": "msp-os",
        "port": 4686
    },
    {
        "protocol": "UDP",
        "name": "nst",
        "port": 4687
    },
    {
        "protocol": "UDP",
        "name": "mobile-p2p",
        "port": 4688
    },
    {
        "protocol": "UDP",
        "name": "altovacentral",
        "port": 4689
    },
    {
        "protocol": "UDP",
        "name": "prelude",
        "port": 4690
    },
    {
        "protocol": "UDP",
        "name": "monotone",
        "port": 4691
    },
    {
        "protocol": "UDP",
        "name": "conspiracy",
        "port": 4692
    },
    {
        "protocol": "UDP",
        "name": "ipdr-sp",
        "port": 4737
    },
    {
        "protocol": "UDP",
        "name": "solera-lpn",
        "port": 4738
    },
    {
        "protocol": "UDP",
        "name": "ipfix",
        "port": 4739
    },
    {
        "protocol": "UDP",
        "name": "openhpid",
        "port": 4743
    },
    {
        "protocol": "UDP",
        "name": "ssad",
        "port": 4750
    },
    {
        "protocol": "UDP",
        "name": "spocp",
        "port": 4751
    },
    {
        "protocol": "UDP",
        "name": "snap",
        "port": 4752
    },
    {
        "protocol": "UDP",
        "name": "bfd-multi-ctl",
        "port": 4784
    },
    {
        "protocol": "UDP",
        "name": "iims",
        "port": 4800
    },
    {
        "protocol": "UDP",
        "name": "iwec",
        "port": 4801
    },
    {
        "protocol": "UDP",
        "name": "ilss",
        "port": 4802
    },
    {
        "protocol": "UDP",
        "name": "htcp",
        "port": 4827
    },
    {
        "protocol": "UDP",
        "name": "varadero-0",
        "port": 4837
    },
    {
        "protocol": "UDP",
        "name": "varadero-1",
        "port": 4838
    },
    {
        "protocol": "UDP",
        "name": "varadero-2",
        "port": 4839
    },
    {
        "protocol": "UDP",
        "name": "appserv-http",
        "port": 4848
    },
    {
        "protocol": "UDP",
        "name": "appserv-https",
        "port": 4849
    },
    {
        "protocol": "UDP",
        "name": "sun-as-nodeagt",
        "port": 4850
    },
    {
        "protocol": "UDP",
        "name": "phrelay",
        "port": 4868
    },
    {
        "protocol": "UDP",
        "name": "phrelaydbg",
        "port": 4869
    },
    {
        "protocol": "UDP",
        "name": "cc-tracking",
        "port": 4870
    },
    {
        "protocol": "UDP",
        "name": "wired",
        "port": 4871
    },
    {
        "protocol": "UDP",
        "name": "abbs",
        "port": 4885
    },
    {
        "protocol": "UDP",
        "name": "lyskom",
        "port": 4894
    },
    {
        "protocol": "UDP",
        "name": "radmin-port",
        "port": 4899
    },
    {
        "protocol": "UDP",
        "name": "hfcs",
        "port": 4900
    },
    {
        "protocol": "UDP",
        "name": "munin",
        "port": 4949
    },
    {
        "protocol": "UDP",
        "name": "pwgwims",
        "port": 4951
    },
    {
        "protocol": "UDP",
        "name": "sagxtsds",
        "port": 4952
    },
    {
        "protocol": "UDP",
        "name": "ccss-qmm",
        "port": 4969
    },
    {
        "protocol": "UDP",
        "name": "ccss-qsm",
        "port": 4970
    },
    {
        "protocol": "UDP",
        "name": "smar-se-port1",
        "port": 4987
    },
    {
        "protocol": "UDP",
        "name": "smar-se-port2",
        "port": 4988
    },
    {
        "protocol": "UDP",
        "name": "parallel",
        "port": 4989
    },
    {
        "protocol": "UDP",
        "name": "hfcs-manager",
        "port": 4999
    },
    {
        "protocol": "UDP",
        "name": "commplex-main",
        "port": 5000
    },
    {
        "protocol": "UDP",
        "name": "commplex-link",
        "port": 5001
    },
    {
        "protocol": "UDP",
        "name": "rfe",
        "port": 5002
    },
    {
        "protocol": "UDP",
        "name": "fmpro-internal",
        "port": 5003
    },
    {
        "protocol": "UDP",
        "name": "avt-profile-1",
        "port": 5004
    },
    {
        "protocol": "UDP",
        "name": "avt-profile-2",
        "port": 5005
    },
    {
        "protocol": "UDP",
        "name": "wsm-server",
        "port": 5006
    },
    {
        "protocol": "UDP",
        "name": "wsm-server-ssl",
        "port": 5007
    },
    {
        "protocol": "UDP",
        "name": "synapsis-edge",
        "port": 5008
    },
    {
        "protocol": "UDP",
        "name": "winfs",
        "port": 5009
    },
    {
        "protocol": "UDP",
        "name": "telelpathstart",
        "port": 5010
    },
    {
        "protocol": "UDP",
        "name": "telelpathattack",
        "port": 5011
    },
    {
        "protocol": "UDP",
        "name": "zenginkyo-1",
        "port": 5020
    },
    {
        "protocol": "UDP",
        "name": "zenginkyo-2",
        "port": 5021
    },
    {
        "protocol": "UDP",
        "name": "mice",
        "port": 5022
    },
    {
        "protocol": "UDP",
        "name": "htuilsrv",
        "port": 5023
    },
    {
        "protocol": "UDP",
        "name": "scpi-telnet",
        "port": 5024
    },
    {
        "protocol": "UDP",
        "name": "scpi-raw",
        "port": 5025
    },
    {
        "protocol": "UDP",
        "name": "strexec-d",
        "port": 5026
    },
    {
        "protocol": "UDP",
        "name": "strexec-s",
        "port": 5027
    },
    {
        "protocol": "UDP",
        "name": "asnaacceler8db",
        "port": 5042
    },
    {
        "protocol": "UDP",
        "name": "swxadmin",
        "port": 5043
    },
    {
        "protocol": "UDP",
        "name": "lxi-evntsvc",
        "port": 5044
    },
    {
        "protocol": "UDP",
        "name": "mmcc",
        "port": 5050
    },
    {
        "protocol": "UDP",
        "name": "ita-agent",
        "port": 5051
    },
    {
        "protocol": "UDP",
        "name": "ita-manager",
        "port": 5052
    },
    {
        "protocol": "UDP",
        "name": "unot",
        "port": 5055
    },
    {
        "protocol": "UDP",
        "name": "intecom-ps1",
        "port": 5056
    },
    {
        "protocol": "UDP",
        "name": "intecom-ps2",
        "port": 5057
    },
    {
        "protocol": "UDP",
        "name": "sip",
        "port": 5060
    },
    {
        "protocol": "UDP",
        "name": "sip-tls",
        "port": 5061
    },
    {
        "protocol": "UDP",
        "name": "ca-1",
        "port": 5064
    },
    {
        "protocol": "UDP",
        "name": "ca-2",
        "port": 5065
    },
    {
        "protocol": "UDP",
        "name": "stanag-5066",
        "port": 5066
    },
    {
        "protocol": "UDP",
        "name": "authentx",
        "port": 5067
    },
    {
        "protocol": "UDP",
        "name": "i-net-2000-npr",
        "port": 5069
    },
    {
        "protocol": "UDP",
        "name": "vtsas",
        "port": 5070
    },
    {
        "protocol": "UDP",
        "name": "powerschool",
        "port": 5071
    },
    {
        "protocol": "UDP",
        "name": "ayiya",
        "port": 5072
    },
    {
        "protocol": "UDP",
        "name": "tag-pm",
        "port": 5073
    },
    {
        "protocol": "UDP",
        "name": "alesquery",
        "port": 5074
    },
    {
        "protocol": "UDP",
        "name": "sentinel-lm",
        "port": 5093
    },
    {
        "protocol": "UDP",
        "name": "sentlm-srv2srv",
        "port": 5099
    },
    {
        "protocol": "UDP",
        "name": "socalia",
        "port": 5100
    },
    {
        "protocol": "UDP",
        "name": "talarian-tcp",
        "port": 5101
    },
    {
        "protocol": "UDP",
        "name": "oms-nonsecure",
        "port": 5102
    },
    {
        "protocol": "UDP",
        "name": "pm-cmdsvr",
        "port": 5112
    },
    {
        "protocol": "UDP",
        "name": "nbt-pc",
        "port": 5133
    },
    {
        "protocol": "UDP",
        "name": "ctsd",
        "port": 5137
    },
    {
        "protocol": "UDP",
        "name": "rmonitor_secure",
        "port": 5145
    },
    {
        "protocol": "UDP",
        "name": "atmp",
        "port": 5150
    },
    {
        "protocol": "UDP",
        "name": "esri_sde",
        "port": 5151
    },
    {
        "protocol": "UDP",
        "name": "sde-discovery",
        "port": 5152
    },
    {
        "protocol": "UDP",
        "name": "bzflag",
        "port": 5154
    },
    {
        "protocol": "UDP",
        "name": "asctrl-agent",
        "port": 5155
    },
    {
        "protocol": "UDP",
        "name": "ife_icorp",
        "port": 5165
    },
    {
        "protocol": "UDP",
        "name": "winpcs",
        "port": 5166
    },
    {
        "protocol": "UDP",
        "name": "scte104",
        "port": 5167
    },
    {
        "protocol": "UDP",
        "name": "scte30",
        "port": 5168
    },
    {
        "protocol": "UDP",
        "name": "aol",
        "port": 5190
    },
    {
        "protocol": "UDP",
        "name": "aol-1",
        "port": 5191
    },
    {
        "protocol": "UDP",
        "name": "aol-2",
        "port": 5192
    },
    {
        "protocol": "UDP",
        "name": "aol-3",
        "port": 5193
    },
    {
        "protocol": "UDP",
        "name": "targus-getdata",
        "port": 5200
    },
    {
        "protocol": "UDP",
        "name": "targus-getdata1",
        "port": 5201
    },
    {
        "protocol": "UDP",
        "name": "targus-getdata2",
        "port": 5202
    },
    {
        "protocol": "UDP",
        "name": "targus-getdata3",
        "port": 5203
    },
    {
        "protocol": "UDP",
        "name": "xmpp-client",
        "port": 5222
    },
    {
        "protocol": "UDP",
        "name": "hp-server",
        "port": 5225
    },
    {
        "protocol": "UDP",
        "name": "hp-status",
        "port": 5226
    },
    {
        "protocol": "UDP",
        "name": "eenet",
        "port": 5234
    },
    {
        "protocol": "UDP",
        "name": "padl2sim",
        "port": 5236
    },
    {
        "protocol": "UDP",
        "name": "igateway",
        "port": 5250
    },
    {
        "protocol": "UDP",
        "name": "caevms",
        "port": 5251
    },
    {
        "protocol": "UDP",
        "name": "movaz-ssc",
        "port": 5252
    },
    {
        "protocol": "UDP",
        "name": "3com-njack-1",
        "port": 5264
    },
    {
        "protocol": "UDP",
        "name": "3com-njack-2",
        "port": 5265
    },
    {
        "protocol": "UDP",
        "name": "xmpp-server",
        "port": 5269
    },
    {
        "protocol": "UDP",
        "name": "pk",
        "port": 5272
    },
    {
        "protocol": "UDP",
        "name": "transmit-port",
        "port": 5282
    },
    {
        "protocol": "UDP",
        "name": "hacl-hb",
        "port": 5300
    },
    {
        "protocol": "UDP",
        "name": "hacl-gs",
        "port": 5301
    },
    {
        "protocol": "UDP",
        "name": "hacl-cfg",
        "port": 5302
    },
    {
        "protocol": "UDP",
        "name": "hacl-probe",
        "port": 5303
    },
    {
        "protocol": "UDP",
        "name": "hacl-local",
        "port": 5304
    },
    {
        "protocol": "UDP",
        "name": "hacl-test",
        "port": 5305
    },
    {
        "protocol": "UDP",
        "name": "sun-mc-grp",
        "port": 5306
    },
    {
        "protocol": "UDP",
        "name": "sco-aip",
        "port": 5307
    },
    {
        "protocol": "UDP",
        "name": "cfengine",
        "port": 5308
    },
    {
        "protocol": "UDP",
        "name": "jprinter",
        "port": 5309
    },
    {
        "protocol": "UDP",
        "name": "outlaws",
        "port": 5310
    },
    {
        "protocol": "UDP",
        "name": "permabit-cs",
        "port": 5312
    },
    {
        "protocol": "UDP",
        "name": "rrdp",
        "port": 5313
    },
    {
        "protocol": "UDP",
        "name": "opalis-rbt-ipc",
        "port": 5314
    },
    {
        "protocol": "UDP",
        "name": "hacl-poll",
        "port": 5315
    },
    {
        "protocol": "UDP",
        "name": "kfserver",
        "port": 5343
    },
    {
        "protocol": "UDP",
        "name": "xkotodrcp",
        "port": 5344
    },
    {
        "protocol": "UDP",
        "name": "nat-pmp",
        "port": 5351
    },
    {
        "protocol": "UDP",
        "name": "dns-llq",
        "port": 5352
    },
    {
        "protocol": "UDP",
        "name": "mdns",
        "port": 5353
    },
    {
        "protocol": "UDP",
        "name": "mdnsresponder",
        "port": 5354
    },
    {
        "protocol": "UDP",
        "name": "llmnr",
        "port": 5355
    },
    {
        "protocol": "UDP",
        "name": "ms-smlbiz",
        "port": 5356
    },
    {
        "protocol": "UDP",
        "name": "wsdapi",
        "port": 5357
    },
    {
        "protocol": "UDP",
        "name": "wsdapi-s",
        "port": 5358
    },
    {
        "protocol": "UDP",
        "name": "stresstester",
        "port": 5397
    },
    {
        "protocol": "UDP",
        "name": "elektron-admin",
        "port": 5398
    },
    {
        "protocol": "UDP",
        "name": "securitychase",
        "port": 5399
    },
    {
        "protocol": "UDP",
        "name": "excerpt",
        "port": 5400
    },
    {
        "protocol": "UDP",
        "name": "excerpts",
        "port": 5401
    },
    {
        "protocol": "UDP",
        "name": "mftp",
        "port": 5402
    },
    {
        "protocol": "UDP",
        "name": "hpoms-ci-lstn",
        "port": 5403
    },
    {
        "protocol": "UDP",
        "name": "hpoms-dps-lstn",
        "port": 5404
    },
    {
        "protocol": "UDP",
        "name": "netsupport",
        "port": 5405
    },
    {
        "protocol": "UDP",
        "name": "systemics-sox",
        "port": 5406
    },
    {
        "protocol": "UDP",
        "name": "foresyte-clear",
        "port": 5407
    },
    {
        "protocol": "UDP",
        "name": "foresyte-sec",
        "port": 5408
    },
    {
        "protocol": "UDP",
        "name": "salient-dtasrv",
        "port": 5409
    },
    {
        "protocol": "UDP",
        "name": "salient-usrmgr",
        "port": 5410
    },
    {
        "protocol": "UDP",
        "name": "actnet",
        "port": 5411
    },
    {
        "protocol": "UDP",
        "name": "continuus",
        "port": 5412
    },
    {
        "protocol": "UDP",
        "name": "wwiotalk",
        "port": 5413
    },
    {
        "protocol": "UDP",
        "name": "statusd",
        "port": 5414
    },
    {
        "protocol": "UDP",
        "name": "ns-server",
        "port": 5415
    },
    {
        "protocol": "UDP",
        "name": "sns-gateway",
        "port": 5416
    },
    {
        "protocol": "UDP",
        "name": "sns-agent",
        "port": 5417
    },
    {
        "protocol": "UDP",
        "name": "mcntp",
        "port": 5418
    },
    {
        "protocol": "UDP",
        "name": "dj-ice",
        "port": 5419
    },
    {
        "protocol": "UDP",
        "name": "cylink-c",
        "port": 5420
    },
    {
        "protocol": "UDP",
        "name": "netsupport2",
        "port": 5421
    },
    {
        "protocol": "UDP",
        "name": "salient-mux",
        "port": 5422
    },
    {
        "protocol": "UDP",
        "name": "virtualuser",
        "port": 5423
    },
    {
        "protocol": "UDP",
        "name": "beyond-remote",
        "port": 5424
    },
    {
        "protocol": "UDP",
        "name": "br-channel",
        "port": 5425
    },
    {
        "protocol": "UDP",
        "name": "devbasic",
        "port": 5426
    },
    {
        "protocol": "UDP",
        "name": "sco-peer-tta",
        "port": 5427
    },
    {
        "protocol": "UDP",
        "name": "telaconsole",
        "port": 5428
    },
    {
        "protocol": "UDP",
        "name": "base",
        "port": 5429
    },
    {
        "protocol": "UDP",
        "name": "radec-corp",
        "port": 5430
    },
    {
        "protocol": "UDP",
        "name": "park-agent",
        "port": 5431
    },
    {
        "protocol": "UDP",
        "name": "postgresql",
        "port": 5432
    },
    {
        "protocol": "UDP",
        "name": "pyrrho",
        "port": 5433
    },
    {
        "protocol": "UDP",
        "name": "sgi-arrayd",
        "port": 5434
    },
    {
        "protocol": "UDP",
        "name": "dttl",
        "port": 5435
    },
    {
        "protocol": "UDP",
        "name": "surebox",
        "port": 5453
    },
    {
        "protocol": "UDP",
        "name": "apc-5454",
        "port": 5454
    },
    {
        "protocol": "UDP",
        "name": "apc-5455",
        "port": 5455
    },
    {
        "protocol": "UDP",
        "name": "apc-5456",
        "port": 5456
    },
    {
        "protocol": "UDP",
        "name": "silkmeter",
        "port": 5461
    },
    {
        "protocol": "UDP",
        "name": "ttl-publisher",
        "port": 5462
    },
    {
        "protocol": "UDP",
        "name": "ttlpriceproxy",
        "port": 5463
    },
    {
        "protocol": "UDP",
        "name": "netops-broker",
        "port": 5465
    },
    {
        "protocol": "UDP",
        "name": "fcp-addr-srvr1",
        "port": 5500
    },
    {
        "protocol": "UDP",
        "name": "fcp-addr-srvr2",
        "port": 5501
    },
    {
        "protocol": "UDP",
        "name": "fcp-srvr-inst1",
        "port": 5502
    },
    {
        "protocol": "UDP",
        "name": "fcp-srvr-inst2",
        "port": 5503
    },
    {
        "protocol": "UDP",
        "name": "fcp-cics-gw1",
        "port": 5504
    },
    {
        "protocol": "UDP",
        "name": "sgi-eventmond",
        "port": 5553
    },
    {
        "protocol": "UDP",
        "name": "sgi-esphttp",
        "port": 5554
    },
    {
        "protocol": "UDP",
        "name": "personal-agent",
        "port": 5555
    },
    {
        "protocol": "UDP",
        "name": "freeciv",
        "port": 5556
    },
    {
        "protocol": "UDP",
        "name": "udpplus",
        "port": 5566
    },
    {
        "protocol": "UDP",
        "name": "emware-moap",
        "port": 5567
    },
    {
        "protocol": "UDP",
        "name": "bis-web",
        "port": 5584
    },
    {
        "protocol": "UDP",
        "name": "bis-sync",
        "port": 5585
    },
    {
        "protocol": "UDP",
        "name": "esinstall",
        "port": 5599
    },
    {
        "protocol": "UDP",
        "name": "esmmanager",
        "port": 5600
    },
    {
        "protocol": "UDP",
        "name": "esmagent",
        "port": 5601
    },
    {
        "protocol": "UDP",
        "name": "a1-msc",
        "port": 5602
    },
    {
        "protocol": "UDP",
        "name": "a1-bs",
        "port": 5603
    },
    {
        "protocol": "UDP",
        "name": "a3-sdunode",
        "port": 5604
    },
    {
        "protocol": "UDP",
        "name": "a4-sdunode",
        "port": 5605
    },
    {
        "protocol": "UDP",
        "name": "ninaf",
        "port": 5627
    },
    {
        "protocol": "UDP",
        "name": "pcanywheredata",
        "port": 5631
    },
    {
        "protocol": "UDP",
        "name": "pcanywherestat",
        "port": 5632
    },
    {
        "protocol": "UDP",
        "name": "beorl",
        "port": 5633
    },
    {
        "protocol": "UDP",
        "name": "amqp",
        "port": 5672
    },
    {
        "protocol": "UDP",
        "name": "jms",
        "port": 5673
    },
    {
        "protocol": "UDP",
        "name": "hyperscsi-port",
        "port": 5674
    },
    {
        "protocol": "UDP",
        "name": "v5ua",
        "port": 5675
    },
    {
        "protocol": "UDP",
        "name": "raadmin",
        "port": 5676
    },
    {
        "protocol": "UDP",
        "name": "questdb2-lnchr",
        "port": 5677
    },
    {
        "protocol": "UDP",
        "name": "rrac",
        "port": 5678
    },
    {
        "protocol": "UDP",
        "name": "dccm",
        "port": 5679
    },
    {
        "protocol": "UDP",
        "name": "auriga-router",
        "port": 5680
    },
    {
        "protocol": "UDP",
        "name": "ggz",
        "port": 5688
    },
    {
        "protocol": "UDP",
        "name": "proshareaudio",
        "port": 5713
    },
    {
        "protocol": "UDP",
        "name": "prosharevideo",
        "port": 5714
    },
    {
        "protocol": "UDP",
        "name": "prosharedata",
        "port": 5715
    },
    {
        "protocol": "UDP",
        "name": "prosharerequest",
        "port": 5716
    },
    {
        "protocol": "UDP",
        "name": "prosharenotify",
        "port": 5717
    },
    {
        "protocol": "UDP",
        "name": "ms-licensing",
        "port": 5720
    },
    {
        "protocol": "UDP",
        "name": "dtpt",
        "port": 5721
    },
    {
        "protocol": "UDP",
        "name": "openmail",
        "port": 5729
    },
    {
        "protocol": "UDP",
        "name": "unieng",
        "port": 5730
    },
    {
        "protocol": "UDP",
        "name": "ida-discover1",
        "port": 5741
    },
    {
        "protocol": "UDP",
        "name": "ida-discover2",
        "port": 5742
    },
    {
        "protocol": "UDP",
        "name": "watchdoc-pod",
        "port": 5743
    },
    {
        "protocol": "UDP",
        "name": "watchdoc",
        "port": 5744
    },
    {
        "protocol": "UDP",
        "name": "fcopy-server",
        "port": 5745
    },
    {
        "protocol": "UDP",
        "name": "fcopys-server",
        "port": 5746
    },
    {
        "protocol": "UDP",
        "name": "tunatic",
        "port": 5747
    },
    {
        "protocol": "UDP",
        "name": "tunalyzer",
        "port": 5748
    },
    {
        "protocol": "UDP",
        "name": "openmailg",
        "port": 5755
    },
    {
        "protocol": "UDP",
        "name": "x500ms",
        "port": 5757
    },
    {
        "protocol": "UDP",
        "name": "openmailns",
        "port": 5766
    },
    {
        "protocol": "UDP",
        "name": "s-openmail",
        "port": 5767
    },
    {
        "protocol": "UDP",
        "name": "openmailpxy",
        "port": 5768
    },
    {
        "protocol": "UDP",
        "name": "spramsca",
        "port": 5769
    },
    {
        "protocol": "UDP",
        "name": "spramsd",
        "port": 5770
    },
    {
        "protocol": "UDP",
        "name": "netagent",
        "port": 5771
    },
    {
        "protocol": "UDP",
        "name": "dali-port",
        "port": 5777
    },
    {
        "protocol": "UDP",
        "name": "icmpd",
        "port": 5813
    },
    {
        "protocol": "UDP",
        "name": "spt-automation",
        "port": 5814
    },
    {
        "protocol": "UDP",
        "name": "icpp",
        "port": 5815
    },
    {
        "protocol": "UDP",
        "name": "wherehoo",
        "port": 5859
    },
    {
        "protocol": "UDP",
        "name": "ppsuitemsg",
        "port": 5863
    },
    {
        "protocol": "UDP",
        "name": "indy",
        "port": 5963
    },
    {
        "protocol": "UDP",
        "name": "mppolicy-v5",
        "port": 5968
    },
    {
        "protocol": "UDP",
        "name": "mppolicy-mgr",
        "port": 5969
    },
    {
        "protocol": "UDP",
        "name": "wbem-rmi",
        "port": 5987
    },
    {
        "protocol": "UDP",
        "name": "wbem-http",
        "port": 5988
    },
    {
        "protocol": "UDP",
        "name": "wbem-https",
        "port": 5989
    },
    {
        "protocol": "UDP",
        "name": "wbem-exp-https",
        "port": 5990
    },
    {
        "protocol": "UDP",
        "name": "nuxsl",
        "port": 5991
    },
    {
        "protocol": "UDP",
        "name": "consul-insight",
        "port": 5992
    },
    {
        "protocol": "UDP",
        "name": "cvsup",
        "port": 5999
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6000
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6001
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6002
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6003
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6004
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6005
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6006
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6007
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6008
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6009
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6010
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6011
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6012
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6013
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6014
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6015
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6016
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6017
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6018
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6019
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6020
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6021
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6022
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6023
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6024
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6025
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6026
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6027
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6028
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6029
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6030
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6031
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6032
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6033
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6034
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6035
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6036
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6037
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6038
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6039
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6040
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6041
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6042
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6043
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6044
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6045
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6046
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6047
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6048
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6049
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6050
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6051
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6052
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6053
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6054
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6055
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6056
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6057
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6058
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6059
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6060
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6061
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6062
    },
    {
        "protocol": "UDP",
        "name": "x11",
        "port": 6063
    },
    {
        "protocol": "UDP",
        "name": "ndl-ahp-svc",
        "port": 6064
    },
    {
        "protocol": "UDP",
        "name": "winpharaoh",
        "port": 6065
    },
    {
        "protocol": "UDP",
        "name": "ewctsp",
        "port": 6066
    },
    {
        "protocol": "UDP",
        "name": "srb",
        "port": 6067
    },
    {
        "protocol": "UDP",
        "name": "gsmp",
        "port": 6068
    },
    {
        "protocol": "UDP",
        "name": "trip",
        "port": 6069
    },
    {
        "protocol": "UDP",
        "name": "messageasap",
        "port": 6070
    },
    {
        "protocol": "UDP",
        "name": "ssdtp",
        "port": 6071
    },
    {
        "protocol": "UDP",
        "name": "diagnose-proc",
        "port": 6072
    },
    {
        "protocol": "UDP",
        "name": "directplay8",
        "port": 6073
    },
    {
        "protocol": "UDP",
        "name": "max",
        "port": 6074
    },
    {
        "protocol": "UDP",
        "name": "konspire2b",
        "port": 6085
    },
    {
        "protocol": "UDP",
        "name": "synchronet-db",
        "port": 6100
    },
    {
        "protocol": "UDP",
        "name": "synchronet-rtc",
        "port": 6101
    },
    {
        "protocol": "UDP",
        "name": "synchronet-upd",
        "port": 6102
    },
    {
        "protocol": "UDP",
        "name": "rets",
        "port": 6103
    },
    {
        "protocol": "UDP",
        "name": "dbdb",
        "port": 6104
    },
    {
        "protocol": "UDP",
        "name": "primaserver",
        "port": 6105
    },
    {
        "protocol": "UDP",
        "name": "mpsserver",
        "port": 6106
    },
    {
        "protocol": "UDP",
        "name": "etc-control",
        "port": 6107
    },
    {
        "protocol": "UDP",
        "name": "sercomm-scadmin",
        "port": 6108
    },
    {
        "protocol": "UDP",
        "name": "globecast-id",
        "port": 6109
    },
    {
        "protocol": "UDP",
        "name": "softcm",
        "port": 6110
    },
    {
        "protocol": "UDP",
        "name": "spc",
        "port": 6111
    },
    {
        "protocol": "UDP",
        "name": "dtspcd",
        "port": 6112
    },
    {
        "protocol": "UDP",
        "name": "bex-webadmin",
        "port": 6122
    },
    {
        "protocol": "UDP",
        "name": "backup-express",
        "port": 6123
    },
    {
        "protocol": "UDP",
        "name": "nbt-wol",
        "port": 6133
    },
    {
        "protocol": "UDP",
        "name": "meta-corp",
        "port": 6141
    },
    {
        "protocol": "UDP",
        "name": "aspentec-lm",
        "port": 6142
    },
    {
        "protocol": "UDP",
        "name": "watershed-lm",
        "port": 6143
    },
    {
        "protocol": "UDP",
        "name": "statsci1-lm",
        "port": 6144
    },
    {
        "protocol": "UDP",
        "name": "statsci2-lm",
        "port": 6145
    },
    {
        "protocol": "UDP",
        "name": "lonewolf-lm",
        "port": 6146
    },
    {
        "protocol": "UDP",
        "name": "montage-lm",
        "port": 6147
    },
    {
        "protocol": "UDP",
        "name": "ricardo-lm",
        "port": 6148
    },
    {
        "protocol": "UDP",
        "name": "tal-pod",
        "port": 6149
    },
    {
        "protocol": "UDP",
        "name": "patrol-ism",
        "port": 6161
    },
    {
        "protocol": "UDP",
        "name": "patrol-coll",
        "port": 6162
    },
    {
        "protocol": "UDP",
        "name": "pscribe",
        "port": 6163
    },
    {
        "protocol": "UDP",
        "name": "crip",
        "port": 6253
    },
    {
        "protocol": "UDP",
        "name": "bmc-grx",
        "port": 6300
    },
    {
        "protocol": "UDP",
        "name": "emp-server1",
        "port": 6321
    },
    {
        "protocol": "UDP",
        "name": "emp-server2",
        "port": 6322
    },
    {
        "protocol": "UDP",
        "name": "sflow",
        "port": 6343
    },
    {
        "protocol": "UDP",
        "name": "gnutella-svc",
        "port": 6346
    },
    {
        "protocol": "UDP",
        "name": "gnutella-rtr",
        "port": 6347
    },
    {
        "protocol": "UDP",
        "name": "metatude-mds",
        "port": 6382
    },
    {
        "protocol": "UDP",
        "name": "clariion-evr01",
        "port": 6389
    },
    {
        "protocol": "UDP",
        "name": "info-aps",
        "port": 6400
    },
    {
        "protocol": "UDP",
        "name": "info-was",
        "port": 6401
    },
    {
        "protocol": "UDP",
        "name": "info-eventsvr",
        "port": 6402
    },
    {
        "protocol": "UDP",
        "name": "info-cachesvr",
        "port": 6403
    },
    {
        "protocol": "UDP",
        "name": "info-filesvr",
        "port": 6404
    },
    {
        "protocol": "UDP",
        "name": "info-pagesvr",
        "port": 6405
    },
    {
        "protocol": "UDP",
        "name": "info-processvr",
        "port": 6406
    },
    {
        "protocol": "UDP",
        "name": "reserved1",
        "port": 6407
    },
    {
        "protocol": "UDP",
        "name": "reserved2",
        "port": 6408
    },
    {
        "protocol": "UDP",
        "name": "reserved3",
        "port": 6409
    },
    {
        "protocol": "UDP",
        "name": "reserved4",
        "port": 6410
    },
    {
        "protocol": "UDP",
        "name": "nim-vdrshell",
        "port": 6420
    },
    {
        "protocol": "UDP",
        "name": "nim-wan",
        "port": 6421
    },
    {
        "protocol": "UDP",
        "name": "lvision-lm",
        "port": 6471
    },
    {
        "protocol": "UDP",
        "name": "boks",
        "port": 6500
    },
    {
        "protocol": "UDP",
        "name": "boks_servc",
        "port": 6501
    },
    {
        "protocol": "UDP",
        "name": "boks_servm",
        "port": 6502
    },
    {
        "protocol": "UDP",
        "name": "boks_clntd",
        "port": 6503
    },
    {
        "protocol": "UDP",
        "name": "badm_priv",
        "port": 6505
    },
    {
        "protocol": "UDP",
        "name": "badm_pub",
        "port": 6506
    },
    {
        "protocol": "UDP",
        "name": "bdir_priv",
        "port": 6507
    },
    {
        "protocol": "UDP",
        "name": "bdir_pub",
        "port": 6508
    },
    {
        "protocol": "UDP",
        "name": "mgcs-mfp-port",
        "port": 6509
    },
    {
        "protocol": "UDP",
        "name": "mcer-port",
        "port": 6510
    },
    {
        "protocol": "UDP",
        "name": "lds-distrib",
        "port": 6543
    },
    {
        "protocol": "UDP",
        "name": "lds-dump",
        "port": 6544
    },
    {
        "protocol": "UDP",
        "name": "apc-6547",
        "port": 6547
    },
    {
        "protocol": "UDP",
        "name": "apc-6548",
        "port": 6548
    },
    {
        "protocol": "UDP",
        "name": "apc-6549",
        "port": 6549
    },
    {
        "protocol": "UDP",
        "name": "fg-sysupdate",
        "port": 6550
    },
    {
        "protocol": "UDP",
        "name": "xdsxdm",
        "port": 6558
    },
    {
        "protocol": "UDP",
        "name": "sane-port",
        "port": 6566
    },
    {
        "protocol": "UDP",
        "name": "affiliate",
        "port": 6579
    },
    {
        "protocol": "UDP",
        "name": "parsec-master",
        "port": 6580
    },
    {
        "protocol": "UDP",
        "name": "parsec-peer",
        "port": 6581
    },
    {
        "protocol": "UDP",
        "name": "parsec-game",
        "port": 6582
    },
    {
        "protocol": "UDP",
        "name": "joaJewelSuite",
        "port": 6583
    },
    {
        "protocol": "UDP",
        "name": "odette-ftps",
        "port": 6619
    },
    {
        "protocol": "UDP",
        "name": "kftp-data",
        "port": 6620
    },
    {
        "protocol": "UDP",
        "name": "kftp",
        "port": 6621
    },
    {
        "protocol": "UDP",
        "name": "mcftp",
        "port": 6622
    },
    {
        "protocol": "UDP",
        "name": "ktelnet",
        "port": 6623
    },
    {
        "protocol": "UDP",
        "name": "nexgen",
        "port": 6627
    },
    {
        "protocol": "UDP",
        "name": "afesc-mc",
        "port": 6628
    },
    {
        "protocol": "UDP",
        "name": "ircu",
        "port": 6665
    },
    {
        "protocol": "UDP",
        "name": "ircu",
        "port": 6666
    },
    {
        "protocol": "UDP",
        "name": "ircu",
        "port": 6667
    },
    {
        "protocol": "UDP",
        "name": "ircu",
        "port": 6668
    },
    {
        "protocol": "UDP",
        "name": "ircu",
        "port": 6669
    },
    {
        "protocol": "UDP",
        "name": "vocaltec-gold",
        "port": 6670
    },
    {
        "protocol": "UDP",
        "name": "vision_server",
        "port": 6672
    },
    {
        "protocol": "UDP",
        "name": "vision_elmd",
        "port": 6673
    },
    {
        "protocol": "UDP",
        "name": "kti-icad-srvr",
        "port": 6701
    },
    {
        "protocol": "UDP",
        "name": "e-design-net",
        "port": 6702
    },
    {
        "protocol": "UDP",
        "name": "e-design-web",
        "port": 6703
    },
    {
        "protocol": "UDP",
        "name": "ibprotocol",
        "port": 6714
    },
    {
        "protocol": "UDP",
        "name": "fibotrader-com",
        "port": 6715
    },
    {
        "protocol": "UDP",
        "name": "bmc-perf-agent",
        "port": 6767
    },
    {
        "protocol": "UDP",
        "name": "bmc-perf-mgrd",
        "port": 6768
    },
    {
        "protocol": "UDP",
        "name": "adi-gxp-srvprt",
        "port": 6769
    },
    {
        "protocol": "UDP",
        "name": "plysrv-http",
        "port": 6770
    },
    {
        "protocol": "UDP",
        "name": "plysrv-https",
        "port": 6771
    },
    {
        "protocol": "UDP",
        "name": "hnmp",
        "port": 6776
    },
    {
        "protocol": "UDP",
        "name": "smc-jmx",
        "port": 6786
    },
    {
        "protocol": "UDP",
        "name": "smc-admin",
        "port": 6787
    },
    {
        "protocol": "UDP",
        "name": "smc-http",
        "port": 6788
    },
    {
        "protocol": "UDP",
        "name": "smc-https",
        "port": 6789
    },
    {
        "protocol": "UDP",
        "name": "hnmp",
        "port": 6790
    },
    {
        "protocol": "UDP",
        "name": "hnm",
        "port": 6791
    },
    {
        "protocol": "UDP",
        "name": "ambit-lm",
        "port": 6831
    },
    {
        "protocol": "UDP",
        "name": "netmo-default",
        "port": 6841
    },
    {
        "protocol": "UDP",
        "name": "netmo-http",
        "port": 6842
    },
    {
        "protocol": "UDP",
        "name": "iccrushmore",
        "port": 6850
    },
    {
        "protocol": "UDP",
        "name": "muse",
        "port": 6888
    },
    {
        "protocol": "UDP",
        "name": "bioserver",
        "port": 6946
    },
    {
        "protocol": "UDP",
        "name": "jmact3",
        "port": 6961
    },
    {
        "protocol": "UDP",
        "name": "jmevt2",
        "port": 6962
    },
    {
        "protocol": "UDP",
        "name": "swismgr1",
        "port": 6963
    },
    {
        "protocol": "UDP",
        "name": "swismgr2",
        "port": 6964
    },
    {
        "protocol": "UDP",
        "name": "swistrap",
        "port": 6965
    },
    {
        "protocol": "UDP",
        "name": "swispol",
        "port": 6966
    },
    {
        "protocol": "UDP",
        "name": "acmsoda",
        "port": 6969
    },
    {
        "protocol": "UDP",
        "name": "iatp-highpri",
        "port": 6998
    },
    {
        "protocol": "UDP",
        "name": "iatp-normalpri",
        "port": 6999
    },
    {
        "protocol": "UDP",
        "name": "afs3-fileserver",
        "port": 7000
    },
    {
        "protocol": "UDP",
        "name": "afs3-callback",
        "port": 7001
    },
    {
        "protocol": "UDP",
        "name": "afs3-prserver",
        "port": 7002
    },
    {
        "protocol": "UDP",
        "name": "afs3-vlserver",
        "port": 7003
    },
    {
        "protocol": "UDP",
        "name": "afs3-kaserver",
        "port": 7004
    },
    {
        "protocol": "UDP",
        "name": "afs3-volser",
        "port": 7005
    },
    {
        "protocol": "UDP",
        "name": "afs3-errors",
        "port": 7006
    },
    {
        "protocol": "UDP",
        "name": "afs3-bos",
        "port": 7007
    },
    {
        "protocol": "UDP",
        "name": "afs3-update",
        "port": 7008
    },
    {
        "protocol": "UDP",
        "name": "afs3-rmtsys",
        "port": 7009
    },
    {
        "protocol": "UDP",
        "name": "ups-onlinet",
        "port": 7010
    },
    {
        "protocol": "UDP",
        "name": "talon-disc",
        "port": 7011
    },
    {
        "protocol": "UDP",
        "name": "talon-engine",
        "port": 7012
    },
    {
        "protocol": "UDP",
        "name": "microtalon-dis",
        "port": 7013
    },
    {
        "protocol": "UDP",
        "name": "microtalon-com",
        "port": 7014
    },
    {
        "protocol": "UDP",
        "name": "talon-webserver",
        "port": 7015
    },
    {
        "protocol": "UDP",
        "name": "dpserve",
        "port": 7020
    },
    {
        "protocol": "UDP",
        "name": "dpserveadmin",
        "port": 7021
    },
    {
        "protocol": "UDP",
        "name": "ctdp",
        "port": 7022
    },
    {
        "protocol": "UDP",
        "name": "ct2nmcs",
        "port": 7023
    },
    {
        "protocol": "UDP",
        "name": "vmsvc",
        "port": 7024
    },
    {
        "protocol": "UDP",
        "name": "vmsvc-2",
        "port": 7025
    },
    {
        "protocol": "UDP",
        "name": "op-probe",
        "port": 7030
    },
    {
        "protocol": "UDP",
        "name": "arcp",
        "port": 7070
    },
    {
        "protocol": "UDP",
        "name": "lazy-ptop",
        "port": 7099
    },
    {
        "protocol": "UDP",
        "name": "font-service",
        "port": 7100
    },
    {
        "protocol": "UDP",
        "name": "virprot-lm",
        "port": 7121
    },
    {
        "protocol": "UDP",
        "name": "scenidm",
        "port": 7128
    },
    {
        "protocol": "UDP",
        "name": "cabsm-comm",
        "port": 7161
    },
    {
        "protocol": "UDP",
        "name": "caistoragemgr",
        "port": 7162
    },
    {
        "protocol": "UDP",
        "name": "cacsambroker",
        "port": 7163
    },
    {
        "protocol": "UDP",
        "name": "clutild",
        "port": 7174
    },
    {
        "protocol": "UDP",
        "name": "fodms",
        "port": 7200
    },
    {
        "protocol": "UDP",
        "name": "dlip",
        "port": 7201
    },
    {
        "protocol": "UDP",
        "name": "ramp",
        "port": 7227
    },
    {
        "protocol": "UDP",
        "name": "watchme-7272",
        "port": 7272
    },
    {
        "protocol": "UDP",
        "name": "oma-rlp",
        "port": 7273
    },
    {
        "protocol": "UDP",
        "name": "oma-rlp-s",
        "port": 7274
    },
    {
        "protocol": "UDP",
        "name": "oma-ulp",
        "port": 7275
    },
    {
        "protocol": "UDP",
        "name": "itactionserver1",
        "port": 7280
    },
    {
        "protocol": "UDP",
        "name": "itactionserver2",
        "port": 7281
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7300
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7301
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7302
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7303
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7304
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7305
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7304
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7305
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7306
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7307
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7308
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7309
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7310
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7311
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7312
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7313
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7314
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7315
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7316
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7317
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7318
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7319
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7320
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7321
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7322
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7323
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7324
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7325
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7326
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7327
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7328
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7329
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7330
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7331
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7332
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7333
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7334
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7335
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7336
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7337
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7338
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7339
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7340
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7341
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7342
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7343
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7344
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7345
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7346
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7347
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7348
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7349
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7350
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7351
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7352
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7353
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7354
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7355
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7356
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7357
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7358
    },
    {
        "protocol": "UDP",
        "name": "swx",
        "port": 7359
    },
    {
        "protocol": "UDP",
        "name": "lcm-server",
        "port": 7365
    },
    {
        "protocol": "UDP",
        "name": "mindfilesys",
        "port": 7391
    },
    {
        "protocol": "UDP",
        "name": "mrssrendezvous",
        "port": 7392
    },
    {
        "protocol": "UDP",
        "name": "nfoldman",
        "port": 7393
    },
    {
        "protocol": "UDP",
        "name": "winqedit",
        "port": 7395
    },
    {
        "protocol": "UDP",
        "name": "hexarc",
        "port": 7397
    },
    {
        "protocol": "UDP",
        "name": "rtps-discovery",
        "port": 7400
    },
    {
        "protocol": "UDP",
        "name": "rtps-dd-ut",
        "port": 7401
    },
    {
        "protocol": "UDP",
        "name": "rtps-dd-mt",
        "port": 7402
    },
    {
        "protocol": "UDP",
        "name": "mtportmon",
        "port": 7421
    },
    {
        "protocol": "UDP",
        "name": "pmdmgr",
        "port": 7426
    },
    {
        "protocol": "UDP",
        "name": "oveadmgr",
        "port": 7427
    },
    {
        "protocol": "UDP",
        "name": "ovladmgr",
        "port": 7428
    },
    {
        "protocol": "UDP",
        "name": "opi-sock",
        "port": 7429
    },
    {
        "protocol": "UDP",
        "name": "xmpv7",
        "port": 7430
    },
    {
        "protocol": "UDP",
        "name": "pmd",
        "port": 7431
    },
    {
        "protocol": "UDP",
        "name": "faximum",
        "port": 7437
    },
    {
        "protocol": "UDP",
        "name": "telops-lmd",
        "port": 7491
    },
    {
        "protocol": "UDP",
        "name": "silhouette",
        "port": 7500
    },
    {
        "protocol": "UDP",
        "name": "ovbus",
        "port": 7501
    },
    {
        "protocol": "UDP",
        "name": "ovhpas",
        "port": 7510
    },
    {
        "protocol": "UDP",
        "name": "pafec-lm",
        "port": 7511
    },
    {
        "protocol": "UDP",
        "name": "atul",
        "port": 7543
    },
    {
        "protocol": "UDP",
        "name": "nta-ds",
        "port": 7544
    },
    {
        "protocol": "UDP",
        "name": "nta-us",
        "port": 7545
    },
    {
        "protocol": "UDP",
        "name": "cfs",
        "port": 7546
    },
    {
        "protocol": "UDP",
        "name": "cwmp",
        "port": 7547
    },
    {
        "protocol": "UDP",
        "name": "tidp",
        "port": 7548
    },
    {
        "protocol": "UDP",
        "name": "sncp",
        "port": 7560
    },
    {
        "protocol": "UDP",
        "name": "vsi-omega",
        "port": 7566
    },
    {
        "protocol": "UDP",
        "name": "aries-kfinder",
        "port": 7570
    },
    {
        "protocol": "UDP",
        "name": "sun-lm",
        "port": 7588
    },
    {
        "protocol": "UDP",
        "name": "indi",
        "port": 7624
    },
    {
        "protocol": "UDP",
        "name": "soap-http",
        "port": 7627
    },
    {
        "protocol": "UDP",
        "name": "pmdfmgt",
        "port": 7633
    },
    {
        "protocol": "UDP",
        "name": "imqtunnels",
        "port": 7674
    },
    {
        "protocol": "UDP",
        "name": "imqtunnel",
        "port": 7675
    },
    {
        "protocol": "UDP",
        "name": "imqbrokerd",
        "port": 7676
    },
    {
        "protocol": "UDP",
        "name": "sun-user-https",
        "port": 7677
    },
    {
        "protocol": "UDP",
        "name": "klio",
        "port": 7697
    },
    {
        "protocol": "UDP",
        "name": "sync-em7",
        "port": 7707
    },
    {
        "protocol": "UDP",
        "name": "scinet",
        "port": 7708
    },
    {
        "protocol": "UDP",
        "name": "medimageportal",
        "port": 7720
    },
    {
        "protocol": "UDP",
        "name": "nitrogen",
        "port": 7725
    },
    {
        "protocol": "UDP",
        "name": "freezexservice",
        "port": 7726
    },
    {
        "protocol": "UDP",
        "name": "trident-data",
        "port": 7727
    },
    {
        "protocol": "UDP",
        "name": "aiagent",
        "port": 7738
    },
    {
        "protocol": "UDP",
        "name": "sstp-1",
        "port": 7743
    },
    {
        "protocol": "UDP",
        "name": "cbt",
        "port": 7777
    },
    {
        "protocol": "UDP",
        "name": "interwise",
        "port": 7778
    },
    {
        "protocol": "UDP",
        "name": "vstat",
        "port": 7779
    },
    {
        "protocol": "UDP",
        "name": "accu-lmgr",
        "port": 7781
    },
    {
        "protocol": "UDP",
        "name": "minivend",
        "port": 7786
    },
    {
        "protocol": "UDP",
        "name": "popup-reminders",
        "port": 7787
    },
    {
        "protocol": "UDP",
        "name": "office-tools",
        "port": 7789
    },
    {
        "protocol": "UDP",
        "name": "q3ade",
        "port": 7794
    },
    {
        "protocol": "UDP",
        "name": "pnet-conn",
        "port": 7797
    },
    {
        "protocol": "UDP",
        "name": "pnet-enc",
        "port": 7798
    },
    {
        "protocol": "UDP",
        "name": "asr",
        "port": 7800
    },
    {
        "protocol": "UDP",
        "name": "apc-7845",
        "port": 7845
    },
    {
        "protocol": "UDP",
        "name": "apc-7846",
        "port": 7846
    },
    {
        "protocol": "UDP",
        "name": "ubroker",
        "port": 7887
    },
    {
        "protocol": "UDP",
        "name": "tnos-sp",
        "port": 7901
    },
    {
        "protocol": "UDP",
        "name": "tnos-dp",
        "port": 7902
    },
    {
        "protocol": "UDP",
        "name": "tnos-dps",
        "port": 7903
    },
    {
        "protocol": "UDP",
        "name": "qo-secure",
        "port": 7913
    },
    {
        "protocol": "UDP",
        "name": "t2-drm",
        "port": 7932
    },
    {
        "protocol": "UDP",
        "name": "t2-brm",
        "port": 7933
    },
    {
        "protocol": "UDP",
        "name": "supercell",
        "port": 7967
    },
    {
        "protocol": "UDP",
        "name": "micromuse-ncps",
        "port": 7979
    },
    {
        "protocol": "UDP",
        "name": "quest-vista",
        "port": 7980
    },
    {
        "protocol": "UDP",
        "name": "irdmi2",
        "port": 7999
    },
    {
        "protocol": "UDP",
        "name": "irdmi",
        "port": 8000
    },
    {
        "protocol": "UDP",
        "name": "vcom-tunnel",
        "port": 8001
    },
    {
        "protocol": "UDP",
        "name": "teradataordbms",
        "port": 8002
    },
    {
        "protocol": "UDP",
        "name": "http-alt",
        "port": 8008
    },
    {
        "protocol": "UDP",
        "name": "intu-ec-svcdisc",
        "port": 8020
    },
    {
        "protocol": "UDP",
        "name": "intu-ec-client",
        "port": 8021
    },
    {
        "protocol": "UDP",
        "name": "oa-system",
        "port": 8022
    },
    {
        "protocol": "UDP",
        "name": "pro-ed",
        "port": 8032
    },
    {
        "protocol": "UDP",
        "name": "mindprint",
        "port": 8033
    },
    {
        "protocol": "UDP",
        "name": "http-alt",
        "port": 8080
    },
    {
        "protocol": "UDP",
        "name": "sunproxyadmin",
        "port": 8081
    },
    {
        "protocol": "UDP",
        "name": "us-cli",
        "port": 8082
    },
    {
        "protocol": "UDP",
        "name": "us-srv",
        "port": 8083
    },
    {
        "protocol": "UDP",
        "name": "radan-http",
        "port": 8088
    },
    {
        "protocol": "UDP",
        "name": "xprint-server",
        "port": 8100
    },
    {
        "protocol": "UDP",
        "name": "mtl8000-matrix",
        "port": 8115
    },
    {
        "protocol": "UDP",
        "name": "cp-cluster",
        "port": 8116
    },
    {
        "protocol": "UDP",
        "name": "privoxy",
        "port": 8118
    },
    {
        "protocol": "UDP",
        "name": "apollo-data",
        "port": 8121
    },
    {
        "protocol": "UDP",
        "name": "apollo-admin",
        "port": 8122
    },
    {
        "protocol": "UDP",
        "name": "paycash-online",
        "port": 8128
    },
    {
        "protocol": "UDP",
        "name": "paycash-wbp",
        "port": 8129
    },
    {
        "protocol": "UDP",
        "name": "indigo-vrmi",
        "port": 8130
    },
    {
        "protocol": "UDP",
        "name": "indigo-vbcp",
        "port": 8131
    },
    {
        "protocol": "UDP",
        "name": "dbabble",
        "port": 8132
    },
    {
        "protocol": "UDP",
        "name": "isdd",
        "port": 8148
    },
    {
        "protocol": "UDP",
        "name": "patrol",
        "port": 8160
    },
    {
        "protocol": "UDP",
        "name": "patrol-snmp",
        "port": 8161
    },
    {
        "protocol": "UDP",
        "name": "vvr-data",
        "port": 8199
    },
    {
        "protocol": "UDP",
        "name": "trivnet1",
        "port": 8200
    },
    {
        "protocol": "UDP",
        "name": "trivnet2",
        "port": 8201
    },
    {
        "protocol": "UDP",
        "name": "lm-perfworks",
        "port": 8204
    },
    {
        "protocol": "UDP",
        "name": "lm-instmgr",
        "port": 8205
    },
    {
        "protocol": "UDP",
        "name": "lm-dta",
        "port": 8206
    },
    {
        "protocol": "UDP",
        "name": "lm-sserver",
        "port": 8207
    },
    {
        "protocol": "UDP",
        "name": "lm-webwatcher",
        "port": 8208
    },
    {
        "protocol": "UDP",
        "name": "rexecj",
        "port": 8230
    },
    {
        "protocol": "UDP",
        "name": "server-find",
        "port": 8351
    },
    {
        "protocol": "UDP",
        "name": "cruise-enum",
        "port": 8376
    },
    {
        "protocol": "UDP",
        "name": "cruise-swroute",
        "port": 8377
    },
    {
        "protocol": "UDP",
        "name": "cruise-config",
        "port": 8378
    },
    {
        "protocol": "UDP",
        "name": "cruise-diags",
        "port": 8379
    },
    {
        "protocol": "UDP",
        "name": "cruise-update",
        "port": 8380
    },
    {
        "protocol": "UDP",
        "name": "m2mservices",
        "port": 8383
    },
    {
        "protocol": "UDP",
        "name": "cvd",
        "port": 8400
    },
    {
        "protocol": "UDP",
        "name": "sabarsd",
        "port": 8401
    },
    {
        "protocol": "UDP",
        "name": "abarsd",
        "port": 8402
    },
    {
        "protocol": "UDP",
        "name": "admind",
        "port": 8403
    },
    {
        "protocol": "UDP",
        "name": "espeech",
        "port": 8416
    },
    {
        "protocol": "UDP",
        "name": "espeech-rtp",
        "port": 8417
    },
    {
        "protocol": "UDP",
        "name": "pcsync-https",
        "port": 8443
    },
    {
        "protocol": "UDP",
        "name": "pcsync-http",
        "port": 8444
    },
    {
        "protocol": "UDP",
        "name": "npmp",
        "port": 8450
    },
    {
        "protocol": "UDP",
        "name": "vp2p",
        "port": 8473
    },
    {
        "protocol": "UDP",
        "name": "noteshare",
        "port": 8474
    },
    {
        "protocol": "UDP",
        "name": "fde",
        "port": 8500
    },
    {
        "protocol": "UDP",
        "name": "rtsp-alt",
        "port": 8554
    },
    {
        "protocol": "UDP",
        "name": "d-fence",
        "port": 8555
    },
    {
        "protocol": "UDP",
        "name": "emware-admin",
        "port": 8567
    },
    {
        "protocol": "UDP",
        "name": "asterix",
        "port": 8600
    },
    {
        "protocol": "UDP",
        "name": "canon-bjnp1",
        "port": 8611
    },
    {
        "protocol": "UDP",
        "name": "canon-bjnp2",
        "port": 8612
    },
    {
        "protocol": "UDP",
        "name": "canon-bjnp3",
        "port": 8613
    },
    {
        "protocol": "UDP",
        "name": "canon-bjnp4",
        "port": 8614
    },
    {
        "protocol": "UDP",
        "name": "sun-as-jmxrmi",
        "port": 8686
    },
    {
        "protocol": "UDP",
        "name": "vnyx",
        "port": 8699
    },
    {
        "protocol": "UDP",
        "name": "ibus",
        "port": 8733
    },
    {
        "protocol": "UDP",
        "name": "mc-appserver",
        "port": 8763
    },
    {
        "protocol": "UDP",
        "name": "openqueue",
        "port": 8764
    },
    {
        "protocol": "UDP",
        "name": "ultraseek-http",
        "port": 8765
    },
    {
        "protocol": "UDP",
        "name": "dpap",
        "port": 8770
    },
    {
        "protocol": "UDP",
        "name": "msgclnt",
        "port": 8786
    },
    {
        "protocol": "UDP",
        "name": "msgsrvr",
        "port": 8787
    },
    {
        "protocol": "UDP",
        "name": "sunwebadmin",
        "port": 8800
    },
    {
        "protocol": "UDP",
        "name": "truecm",
        "port": 8804
    },
    {
        "protocol": "UDP",
        "name": "dxspider",
        "port": 8873
    },
    {
        "protocol": "UDP",
        "name": "cddbp-alt",
        "port": 8880
    },
    {
        "protocol": "UDP",
        "name": "ddi-udp-1",
        "port": 8888
    },
    {
        "protocol": "UDP",
        "name": "ddi-udp-2",
        "port": 8889
    },
    {
        "protocol": "UDP",
        "name": "ddi-udp-3",
        "port": 8890
    },
    {
        "protocol": "UDP",
        "name": "ddi-udp-4",
        "port": 8891
    },
    {
        "protocol": "UDP",
        "name": "ddi-udp-5",
        "port": 8892
    },
    {
        "protocol": "UDP",
        "name": "ddi-udp-6",
        "port": 8893
    },
    {
        "protocol": "UDP",
        "name": "ddi-udp-7",
        "port": 8894
    },
    {
        "protocol": "UDP",
        "name": "jmb-cds1",
        "port": 8900
    },
    {
        "protocol": "UDP",
        "name": "jmb-cds2",
        "port": 8901
    },
    {
        "protocol": "UDP",
        "name": "manyone-http",
        "port": 8910
    },
    {
        "protocol": "UDP",
        "name": "manyone-xml",
        "port": 8911
    },
    {
        "protocol": "UDP",
        "name": "wcbackup",
        "port": 8912
    },
    {
        "protocol": "UDP",
        "name": "dragonfly",
        "port": 8913
    },
    {
        "protocol": "UDP",
        "name": "cumulus-admin",
        "port": 8954
    },
    {
        "protocol": "UDP",
        "name": "sunwebadmins",
        "port": 8989
    },
    {
        "protocol": "UDP",
        "name": "bctp",
        "port": 8999
    },
    {
        "protocol": "UDP",
        "name": "cslistener",
        "port": 9000
    },
    {
        "protocol": "UDP",
        "name": "etlservicemgr",
        "port": 9001
    },
    {
        "protocol": "UDP",
        "name": "dynamid",
        "port": 9002
    },
    {
        "protocol": "UDP",
        "name": "pichat",
        "port": 9009
    },
    {
        "protocol": "UDP",
        "name": "tambora",
        "port": 9020
    },
    {
        "protocol": "UDP",
        "name": "panagolin-ident",
        "port": 9021
    },
    {
        "protocol": "UDP",
        "name": "paragent",
        "port": 9022
    },
    {
        "protocol": "UDP",
        "name": "swa-1",
        "port": 9023
    },
    {
        "protocol": "UDP",
        "name": "swa-2",
        "port": 9024
    },
    {
        "protocol": "UDP",
        "name": "swa-3",
        "port": 9025
    },
    {
        "protocol": "UDP",
        "name": "swa-4",
        "port": 9026
    },
    {
        "protocol": "UDP",
        "name": "glrpc",
        "port": 9080
    },
    {
        "protocol": "UDP",
        "name": "sqlexec",
        "port": 9088
    },
    {
        "protocol": "UDP",
        "name": "sqlexec-ssl",
        "port": 9089
    },
    {
        "protocol": "UDP",
        "name": "websm",
        "port": 9090
    },
    {
        "protocol": "UDP",
        "name": "xmltec-xmlmail",
        "port": 9091
    },
    {
        "protocol": "UDP",
        "name": "XmlIpcRegSvc",
        "port": 9092
    },
    {
        "protocol": "UDP",
        "name": "hp-pdl-datastr",
        "port": 9100
    },
    {
        "protocol": "UDP",
        "name": "bacula-dir",
        "port": 9101
    },
    {
        "protocol": "UDP",
        "name": "bacula-fd",
        "port": 9102
    },
    {
        "protocol": "UDP",
        "name": "bacula-sd",
        "port": 9103
    },
    {
        "protocol": "UDP",
        "name": "peerwire",
        "port": 9104
    },
    {
        "protocol": "UDP",
        "name": "mxit",
        "port": 9119
    },
    {
        "protocol": "UDP",
        "name": "dddp",
        "port": 9131
    },
    {
        "protocol": "UDP",
        "name": "netlock1",
        "port": 9160
    },
    {
        "protocol": "UDP",
        "name": "netlock2",
        "port": 9161
    },
    {
        "protocol": "UDP",
        "name": "netlock3",
        "port": 9162
    },
    {
        "protocol": "UDP",
        "name": "netlock4",
        "port": 9163
    },
    {
        "protocol": "UDP",
        "name": "netlock5",
        "port": 9164
    },
    {
        "protocol": "UDP",
        "name": "sun-as-jpda",
        "port": 9191
    },
    {
        "protocol": "UDP",
        "name": "wap-wsp",
        "port": 9200
    },
    {
        "protocol": "UDP",
        "name": "wap-wsp-wtp",
        "port": 9201
    },
    {
        "protocol": "UDP",
        "name": "wap-wsp-s",
        "port": 9202
    },
    {
        "protocol": "UDP",
        "name": "wap-wsp-wtp-s",
        "port": 9203
    },
    {
        "protocol": "UDP",
        "name": "wap-vcard",
        "port": 9204
    },
    {
        "protocol": "UDP",
        "name": "wap-vcal",
        "port": 9205
    },
    {
        "protocol": "UDP",
        "name": "wap-vcard-s",
        "port": 9206
    },
    {
        "protocol": "UDP",
        "name": "wap-vcal-s",
        "port": 9207
    },
    {
        "protocol": "UDP",
        "name": "rjcdb-vcards",
        "port": 9208
    },
    {
        "protocol": "UDP",
        "name": "almobile-system",
        "port": 9209
    },
    {
        "protocol": "UDP",
        "name": "oma-mlp",
        "port": 9210
    },
    {
        "protocol": "UDP",
        "name": "oma-mlp-s",
        "port": 9211
    },
    {
        "protocol": "UDP",
        "name": "serverviewdbms",
        "port": 9212
    },
    {
        "protocol": "UDP",
        "name": "serverstart",
        "port": 9213
    },
    {
        "protocol": "UDP",
        "name": "ipdcesgbs",
        "port": 9214
    },
    {
        "protocol": "UDP",
        "name": "insis",
        "port": 9215
    },
    {
        "protocol": "UDP",
        "name": "fsc-port",
        "port": 9217
    },
    {
        "protocol": "UDP",
        "name": "teamcoherence",
        "port": 9222
    },
    {
        "protocol": "UDP",
        "name": "swtp-port1",
        "port": 9281
    },
    {
        "protocol": "UDP",
        "name": "swtp-port2",
        "port": 9282
    },
    {
        "protocol": "UDP",
        "name": "callwaveiam",
        "port": 9283
    },
    {
        "protocol": "UDP",
        "name": "visd",
        "port": 9284
    },
    {
        "protocol": "UDP",
        "name": "n2h2server",
        "port": 9285
    },
    {
        "protocol": "UDP",
        "name": "cumulus",
        "port": 9287
    },
    {
        "protocol": "UDP",
        "name": "armtechdaemon",
        "port": 9292
    },
    {
        "protocol": "UDP",
        "name": "secure-ts",
        "port": 9318
    },
    {
        "protocol": "UDP",
        "name": "guibase",
        "port": 9321
    },
    {
        "protocol": "UDP",
        "name": "mpidcmgr",
        "port": 9343
    },
    {
        "protocol": "UDP",
        "name": "mphlpdmc",
        "port": 9344
    },
    {
        "protocol": "UDP",
        "name": "ctechlicensing",
        "port": 9346
    },
    {
        "protocol": "UDP",
        "name": "fjdmimgr",
        "port": 9374
    },
    {
        "protocol": "UDP",
        "name": "fjinvmgr",
        "port": 9396
    },
    {
        "protocol": "UDP",
        "name": "mpidcagt",
        "port": 9397
    },
    {
        "protocol": "UDP",
        "name": "git",
        "port": 9418
    },
    {
        "protocol": "UDP",
        "name": "ismserver",
        "port": 9500
    },
    {
        "protocol": "UDP",
        "name": "mngsuite",
        "port": 9535
    },
    {
        "protocol": "UDP",
        "name": "trispen-sra",
        "port": 9555
    },
    {
        "protocol": "UDP",
        "name": "ldgateway",
        "port": 9592
    },
    {
        "protocol": "UDP",
        "name": "cba8",
        "port": 9593
    },
    {
        "protocol": "UDP",
        "name": "msgsys",
        "port": 9594
    },
    {
        "protocol": "UDP",
        "name": "pds",
        "port": 9595
    },
    {
        "protocol": "UDP",
        "name": "mercury-disc",
        "port": 9596
    },
    {
        "protocol": "UDP",
        "name": "pd-admin",
        "port": 9597
    },
    {
        "protocol": "UDP",
        "name": "vscp",
        "port": 9598
    },
    {
        "protocol": "UDP",
        "name": "robix",
        "port": 9599
    },
    {
        "protocol": "UDP",
        "name": "micromuse-ncpw",
        "port": 9600
    },
    {
        "protocol": "UDP",
        "name": "streamcomm-ds",
        "port": 9612
    },
    {
        "protocol": "UDP",
        "name": "board-roar",
        "port": 9700
    },
    {
        "protocol": "UDP",
        "name": "l5nas-parchan",
        "port": 9747
    },
    {
        "protocol": "UDP",
        "name": "board-voip",
        "port": 9750
    },
    {
        "protocol": "UDP",
        "name": "rasadv",
        "port": 9753
    },
    {
        "protocol": "UDP",
        "name": "davsrc",
        "port": 9800
    },
    {
        "protocol": "UDP",
        "name": "sstp-2",
        "port": 9801
    },
    {
        "protocol": "UDP",
        "name": "davsrcs",
        "port": 9802
    },
    {
        "protocol": "UDP",
        "name": "sapv1",
        "port": 9875
    },
    {
        "protocol": "UDP",
        "name": "sd",
        "port": 9876
    },
    {
        "protocol": "UDP",
        "name": "cyborg-systems",
        "port": 9888
    },
    {
        "protocol": "UDP",
        "name": "monkeycom",
        "port": 9898
    },
    {
        "protocol": "UDP",
        "name": "sctp-tunneling",
        "port": 9899
    },
    {
        "protocol": "UDP",
        "name": "iua",
        "port": 9900
    },
    {
        "protocol": "UDP",
        "name": "domaintime",
        "port": 9909
    },
    {
        "protocol": "UDP",
        "name": "sype-transport",
        "port": 9911
    },
    {
        "protocol": "UDP",
        "name": "apc-9950",
        "port": 9950
    },
    {
        "protocol": "UDP",
        "name": "apc-9951",
        "port": 9951
    },
    {
        "protocol": "UDP",
        "name": "apc-9952",
        "port": 9952
    },
    {
        "protocol": "UDP",
        "name": "acis",
        "port": 9953
    },
    {
        "protocol": "UDP",
        "name": "osm-appsrvr",
        "port": 9990
    },
    {
        "protocol": "UDP",
        "name": "osm-oev",
        "port": 9991
    },
    {
        "protocol": "UDP",
        "name": "palace-1",
        "port": 9992
    },
    {
        "protocol": "UDP",
        "name": "palace-2",
        "port": 9993
    },
    {
        "protocol": "UDP",
        "name": "palace-3",
        "port": 9994
    },
    {
        "protocol": "UDP",
        "name": "palace-4",
        "port": 9995
    },
    {
        "protocol": "UDP",
        "name": "palace-5",
        "port": 9996
    },
    {
        "protocol": "UDP",
        "name": "palace-6",
        "port": 9997
    },
    {
        "protocol": "UDP",
        "name": "distinct32",
        "port": 9998
    },
    {
        "protocol": "UDP",
        "name": "distinct",
        "port": 9999
    },
    {
        "protocol": "UDP",
        "name": "ndmp",
        "port": 10000
    },
    {
        "protocol": "UDP",
        "name": "scp-config",
        "port": 10001
    },
    {
        "protocol": "UDP",
        "name": "mvs-capacity",
        "port": 10007
    },
    {
        "protocol": "UDP",
        "name": "octopus",
        "port": 10008
    },
    {
        "protocol": "UDP",
        "name": "amanda",
        "port": 10080
    },
    {
        "protocol": "UDP",
        "name": "famdc",
        "port": 10081
    },
    {
        "protocol": "UDP",
        "name": "zabbix-agent",
        "port": 10050
    },
    {
        "protocol": "UDP",
        "name": "zabbix-trapper",
        "port": 10051
    },
    {
        "protocol": "UDP",
        "name": "itap-ddtp",
        "port": 10100
    },
    {
        "protocol": "UDP",
        "name": "ezmeeting-2",
        "port": 10101
    },
    {
        "protocol": "UDP",
        "name": "ezproxy-2",
        "port": 10102
    },
    {
        "protocol": "UDP",
        "name": "ezrelay",
        "port": 10103
    },
    {
        "protocol": "UDP",
        "name": "bctp-server",
        "port": 10107
    },
    {
        "protocol": "UDP",
        "name": "netiq-endpoint",
        "port": 10113
    },
    {
        "protocol": "UDP",
        "name": "netiq-qcheck",
        "port": 10114
    },
    {
        "protocol": "UDP",
        "name": "netiq-endpt",
        "port": 10115
    },
    {
        "protocol": "UDP",
        "name": "netiq-voipa",
        "port": 10116
    },
    {
        "protocol": "UDP",
        "name": "bmc-perf-sd",
        "port": 10128
    },
    {
        "protocol": "UDP",
        "name": "qb-db-server",
        "port": 10160
    },
    {
        "protocol": "UDP",
        "name": "apollo-relay",
        "port": 10252
    },
    {
        "protocol": "UDP",
        "name": "axis-wimp-port",
        "port": 10260
    },
    {
        "protocol": "UDP",
        "name": "blocks",
        "port": 10288
    },
    {
        "protocol": "UDP",
        "name": "lpdg",
        "port": 10805
    },
    {
        "protocol": "UDP",
        "name": "rmiaux",
        "port": 10990
    },
    {
        "protocol": "UDP",
        "name": "irisa",
        "port": 11000
    },
    {
        "protocol": "UDP",
        "name": "metasys",
        "port": 11001
    },
    {
        "protocol": "UDP",
        "name": "vce",
        "port": 11111
    },
    {
        "protocol": "UDP",
        "name": "dicom",
        "port": 11112
    },
    {
        "protocol": "UDP",
        "name": "suncacao-snmp",
        "port": 11161
    },
    {
        "protocol": "UDP",
        "name": "suncacao-jmxmp",
        "port": 11162
    },
    {
        "protocol": "UDP",
        "name": "suncacao-rmi",
        "port": 11163
    },
    {
        "protocol": "UDP",
        "name": "suncacao-csa",
        "port": 11164
    },
    {
        "protocol": "UDP",
        "name": "suncacao-websvc",
        "port": 11165
    },
    {
        "protocol": "UDP",
        "name": "smsqp",
        "port": 11201
    },
    {
        "protocol": "UDP",
        "name": "imip",
        "port": 11319
    },
    {
        "protocol": "UDP",
        "name": "imip-channels",
        "port": 11320
    },
    {
        "protocol": "UDP",
        "name": "arena-server",
        "port": 11321
    },
    {
        "protocol": "UDP",
        "name": "atm-uhas",
        "port": 11367
    },
    {
        "protocol": "UDP",
        "name": "hkp",
        "port": 11371
    },
    {
        "protocol": "UDP",
        "name": "tempest-port",
        "port": 11600
    },
    {
        "protocol": "UDP",
        "name": "h323callsigalt",
        "port": 11720
    },
    {
        "protocol": "UDP",
        "name": "intrepid-ssl",
        "port": 11751
    },
    {
        "protocol": "UDP",
        "name": "sysinfo-sp",
        "port": 11967
    },
    {
        "protocol": "UDP",
        "name": "entextxid",
        "port": 12000
    },
    {
        "protocol": "UDP",
        "name": "entextnetwk",
        "port": 12001
    },
    {
        "protocol": "UDP",
        "name": "entexthigh",
        "port": 12002
    },
    {
        "protocol": "UDP",
        "name": "entextmed",
        "port": 12003
    },
    {
        "protocol": "UDP",
        "name": "entextlow",
        "port": 12004
    },
    {
        "protocol": "UDP",
        "name": "dbisamserver1",
        "port": 12005
    },
    {
        "protocol": "UDP",
        "name": "dbisamserver2",
        "port": 12006
    },
    {
        "protocol": "UDP",
        "name": "accuracer",
        "port": 12007
    },
    {
        "protocol": "UDP",
        "name": "accuracer-dbms",
        "port": 12008
    },
    {
        "protocol": "UDP",
        "name": "vipera",
        "port": 12012
    },
    {
        "protocol": "UDP",
        "name": "rets-ssl",
        "port": 12109
    },
    {
        "protocol": "UDP",
        "name": "nupaper-ss",
        "port": 12121
    },
    {
        "protocol": "UDP",
        "name": "cawas",
        "port": 12168
    },
    {
        "protocol": "UDP",
        "name": "hivep",
        "port": 12172
    },
    {
        "protocol": "UDP",
        "name": "linogridengine",
        "port": 12300
    },
    {
        "protocol": "UDP",
        "name": "warehouse-sss",
        "port": 12321
    },
    {
        "protocol": "UDP",
        "name": "warehouse",
        "port": 12322
    },
    {
        "protocol": "UDP",
        "name": "italk",
        "port": 12345
    },
    {
        "protocol": "UDP",
        "name": "tsaf",
        "port": 12753
    },
    {
        "protocol": "UDP",
        "name": "i-zipqd",
        "port": 13160
    },
    {
        "protocol": "UDP",
        "name": "powwow-client",
        "port": 13223
    },
    {
        "protocol": "UDP",
        "name": "powwow-server",
        "port": 13224
    },
    {
        "protocol": "UDP",
        "name": "bprd",
        "port": 13720
    },
    {
        "protocol": "UDP",
        "name": "bpdbm",
        "port": 13721
    },
    {
        "protocol": "UDP",
        "name": "bpjava-msvc",
        "port": 13722
    },
    {
        "protocol": "UDP",
        "name": "vnetd",
        "port": 13724
    },
    {
        "protocol": "UDP",
        "name": "bpcd",
        "port": 13782
    },
    {
        "protocol": "UDP",
        "name": "vopied",
        "port": 13783
    },
    {
        "protocol": "UDP",
        "name": "nbdb",
        "port": 13785
    },
    {
        "protocol": "UDP",
        "name": "nomdb",
        "port": 13786
    },
    {
        "protocol": "UDP",
        "name": "dsmcc-config",
        "port": 13818
    },
    {
        "protocol": "UDP",
        "name": "dsmcc-session",
        "port": 13819
    },
    {
        "protocol": "UDP",
        "name": "dsmcc-passthru",
        "port": 13820
    },
    {
        "protocol": "UDP",
        "name": "dsmcc-download",
        "port": 13821
    },
    {
        "protocol": "UDP",
        "name": "dsmcc-ccp",
        "port": 13822
    },
    {
        "protocol": "UDP",
        "name": "sua",
        "port": 14001
    },
    {
        "protocol": "UDP",
        "name": "sage-best-com1",
        "port": 14033
    },
    {
        "protocol": "UDP",
        "name": "sage-best-com2",
        "port": 14034
    },
    {
        "protocol": "UDP",
        "name": "vcs-app",
        "port": 14141
    },
    {
        "protocol": "UDP",
        "name": "gcm-app",
        "port": 14145
    },
    {
        "protocol": "UDP",
        "name": "vrts-tdd",
        "port": 14149
    },
    {
        "protocol": "UDP",
        "name": "vad",
        "port": 14154
    },
    {
        "protocol": "UDP",
        "name": "hde-lcesrvr-1",
        "port": 14936
    },
    {
        "protocol": "UDP",
        "name": "hde-lcesrvr-2",
        "port": 14937
    },
    {
        "protocol": "UDP",
        "name": "hydap",
        "port": 15000
    },
    {
        "protocol": "UDP",
        "name": "xpilot",
        "port": 15345
    },
    {
        "protocol": "UDP",
        "name": "ptp",
        "port": 15740
    },
    {
        "protocol": "UDP",
        "name": "3link",
        "port": 15363
    },
    {
        "protocol": "UDP",
        "name": "sun-sea-port",
        "port": 16161
    },
    {
        "protocol": "UDP",
        "name": "etb4j",
        "port": 16309
    },
    {
        "protocol": "UDP",
        "name": "netserialext1",
        "port": 16360
    },
    {
        "protocol": "UDP",
        "name": "netserialext2",
        "port": 16361
    },
    {
        "protocol": "UDP",
        "name": "netserialext3",
        "port": 16367
    },
    {
        "protocol": "UDP",
        "name": "netserialext4",
        "port": 16368
    },
    {
        "protocol": "UDP",
        "name": "connected",
        "port": 16384
    },
    {
        "protocol": "UDP",
        "name": "intel-rci-mp",
        "port": 16991
    },
    {
        "protocol": "UDP",
        "name": "amt-soap-http",
        "port": 16992
    },
    {
        "protocol": "UDP",
        "name": "amt-soap-https",
        "port": 16993
    },
    {
        "protocol": "UDP",
        "name": "amt-redir-tcp",
        "port": 16994
    },
    {
        "protocol": "UDP",
        "name": "amt-redir-tls",
        "port": 16995
    },
    {
        "protocol": "UDP",
        "name": "isode-dua",
        "port": 17007
    },
    {
        "protocol": "UDP",
        "name": "soundsvirtual",
        "port": 17185
    },
    {
        "protocol": "UDP",
        "name": "chipper",
        "port": 17219
    },
    {
        "protocol": "UDP",
        "name": "ssh-mgmt",
        "port": 17235
    },
    {
        "protocol": "UDP",
        "name": "ea",
        "port": 17729
    },
    {
        "protocol": "UDP",
        "name": "zep",
        "port": 17754
    },
    {
        "protocol": "UDP",
        "name": "biimenu",
        "port": 18000
    },
    {
        "protocol": "UDP",
        "name": "opsec-cvp",
        "port": 18181
    },
    {
        "protocol": "UDP",
        "name": "opsec-ufp",
        "port": 18182
    },
    {
        "protocol": "UDP",
        "name": "opsec-sam",
        "port": 18183
    },
    {
        "protocol": "UDP",
        "name": "opsec-lea",
        "port": 18184
    },
    {
        "protocol": "UDP",
        "name": "opsec-omi",
        "port": 18185
    },
    {
        "protocol": "UDP",
        "name": "ohsc",
        "port": 18186
    },
    {
        "protocol": "UDP",
        "name": "opsec-ela",
        "port": 18187
    },
    {
        "protocol": "UDP",
        "name": "checkpoint-rtm",
        "port": 18241
    },
    {
        "protocol": "UDP",
        "name": "ac-cluster",
        "port": 18463
    },
    {
        "protocol": "UDP",
        "name": "ique",
        "port": 18769
    },
    {
        "protocol": "UDP",
        "name": "infotos",
        "port": 18881
    },
    {
        "protocol": "UDP",
        "name": "apc-necmp",
        "port": 18888
    },
    {
        "protocol": "UDP",
        "name": "igrid",
        "port": 19000
    },
    {
        "protocol": "UDP",
        "name": "opsec-uaa",
        "port": 19191
    },
    {
        "protocol": "UDP",
        "name": "ua-secureagent",
        "port": 19194
    },
    {
        "protocol": "UDP",
        "name": "keysrvr",
        "port": 19283
    },
    {
        "protocol": "UDP",
        "name": "keyshadow",
        "port": 19315
    },
    {
        "protocol": "UDP",
        "name": "mtrgtrans",
        "port": 19398
    },
    {
        "protocol": "UDP",
        "name": "hp-sco",
        "port": 19410
    },
    {
        "protocol": "UDP",
        "name": "hp-sca",
        "port": 19411
    },
    {
        "protocol": "UDP",
        "name": "hp-sessmon",
        "port": 19412
    },
    {
        "protocol": "UDP",
        "name": "fxuptp",
        "port": 19539
    },
    {
        "protocol": "UDP",
        "name": "sxuptp",
        "port": 19540
    },
    {
        "protocol": "UDP",
        "name": "jcp",
        "port": 19541
    },
    {
        "protocol": "UDP",
        "name": "dnp",
        "port": 20000
    },
    {
        "protocol": "UDP",
        "name": "microsan",
        "port": 20001
    },
    {
        "protocol": "UDP",
        "name": "commtact-http",
        "port": 20002
    },
    {
        "protocol": "UDP",
        "name": "commtact-https",
        "port": 20003
    },
    {
        "protocol": "UDP",
        "name": "opendeploy",
        "port": 20014
    },
    {
        "protocol": "UDP",
        "name": "nburn_id",
        "port": 20034
    },
    {
        "protocol": "UDP",
        "name": "ipdtp-port",
        "port": 20202
    },
    {
        "protocol": "UDP",
        "name": "ipulse-ics",
        "port": 20222
    },
    {
        "protocol": "UDP",
        "name": "track",
        "port": 20670
    },
    {
        "protocol": "UDP",
        "name": "athand-mmp",
        "port": 20999
    },
    {
        "protocol": "UDP",
        "name": "irtrans",
        "port": 21000
    },
    {
        "protocol": "UDP",
        "name": "vofr-gateway",
        "port": 21590
    },
    {
        "protocol": "UDP",
        "name": "tvpm",
        "port": 21800
    },
    {
        "protocol": "UDP",
        "name": "webphone",
        "port": 21845
    },
    {
        "protocol": "UDP",
        "name": "netspeak-is",
        "port": 21846
    },
    {
        "protocol": "UDP",
        "name": "netspeak-cs",
        "port": 21847
    },
    {
        "protocol": "UDP",
        "name": "netspeak-acd",
        "port": 21848
    },
    {
        "protocol": "UDP",
        "name": "netspeak-cps",
        "port": 21849
    },
    {
        "protocol": "UDP",
        "name": "snapenetio",
        "port": 22000
    },
    {
        "protocol": "UDP",
        "name": "optocontrol",
        "port": 22001
    },
    {
        "protocol": "UDP",
        "name": "wnn6",
        "port": 22273
    },
    {
        "protocol": "UDP",
        "name": "vocaltec-phone",
        "port": 22555
    },
    {
        "protocol": "UDP",
        "name": "aws-brf",
        "port": 22800
    },
    {
        "protocol": "UDP",
        "name": "brf-gw",
        "port": 22951
    },
    {
        "protocol": "UDP",
        "name": "novar-dbase",
        "port": 23400
    },
    {
        "protocol": "UDP",
        "name": "novar-alarm",
        "port": 23401
    },
    {
        "protocol": "UDP",
        "name": "novar-global",
        "port": 23402
    },
    {
        "protocol": "UDP",
        "name": "med-ltp",
        "port": 24000
    },
    {
        "protocol": "UDP",
        "name": "med-fsp-rx",
        "port": 24001
    },
    {
        "protocol": "UDP",
        "name": "med-fsp-tx",
        "port": 24002
    },
    {
        "protocol": "UDP",
        "name": "med-supp",
        "port": 24003
    },
    {
        "protocol": "UDP",
        "name": "med-ovw",
        "port": 24004
    },
    {
        "protocol": "UDP",
        "name": "med-ci",
        "port": 24005
    },
    {
        "protocol": "UDP",
        "name": "med-net-svc",
        "port": 24006
    },
    {
        "protocol": "UDP",
        "name": "filesphere",
        "port": 24242
    },
    {
        "protocol": "UDP",
        "name": "vista-4gl",
        "port": 24249
    },
    {
        "protocol": "UDP",
        "name": "ild",
        "port": 24321
    },
    {
        "protocol": "UDP",
        "name": "intel_rci",
        "port": 24386
    },
    {
        "protocol": "UDP",
        "name": "binkp",
        "port": 24554
    },
    {
        "protocol": "UDP",
        "name": "flashfiler",
        "port": 24677
    },
    {
        "protocol": "UDP",
        "name": "proactivate",
        "port": 24678
    },
    {
        "protocol": "UDP",
        "name": "snip",
        "port": 24922
    },
    {
        "protocol": "UDP",
        "name": "icl-twobase1",
        "port": 25000
    },
    {
        "protocol": "UDP",
        "name": "icl-twobase2",
        "port": 25001
    },
    {
        "protocol": "UDP",
        "name": "icl-twobase3",
        "port": 25002
    },
    {
        "protocol": "UDP",
        "name": "icl-twobase4",
        "port": 25003
    },
    {
        "protocol": "UDP",
        "name": "icl-twobase5",
        "port": 25004
    },
    {
        "protocol": "UDP",
        "name": "icl-twobase6",
        "port": 25005
    },
    {
        "protocol": "UDP",
        "name": "icl-twobase7",
        "port": 25006
    },
    {
        "protocol": "UDP",
        "name": "icl-twobase8",
        "port": 25007
    },
    {
        "protocol": "UDP",
        "name": "icl-twobase9",
        "port": 25008
    },
    {
        "protocol": "UDP",
        "name": "icl-twobase10",
        "port": 25009
    },
    {
        "protocol": "UDP",
        "name": "vocaltec-hos",
        "port": 25793
    },
    {
        "protocol": "UDP",
        "name": "tasp-net",
        "port": 25900
    },
    {
        "protocol": "UDP",
        "name": "niobserver",
        "port": 25901
    },
    {
        "protocol": "UDP",
        "name": "niprobe",
        "port": 25903
    },
    {
        "protocol": "UDP",
        "name": "quake",
        "port": 26000
    },
    {
        "protocol": "UDP",
        "name": "wnn6-ds",
        "port": 26208
    },
    {
        "protocol": "UDP",
        "name": "ezproxy",
        "port": 26260
    },
    {
        "protocol": "UDP",
        "name": "ezmeeting",
        "port": 26261
    },
    {
        "protocol": "UDP",
        "name": "k3software-svr",
        "port": 26262
    },
    {
        "protocol": "UDP",
        "name": "k3software-cli",
        "port": 26263
    },
    {
        "protocol": "UDP",
        "name": "gserver",
        "port": 26264
    },
    {
        "protocol": "UDP",
        "name": "flex-lm",
        "port": 27000
    },
    {
        "protocol": "UDP",
        "name": "flex-lm",
        "port": 27001
    },
    {
        "protocol": "UDP",
        "name": "flex-lm",
        "port": 27002
    },
    {
        "protocol": "UDP",
        "name": "flex-lm",
        "port": 27003
    },
    {
        "protocol": "UDP",
        "name": "flex-lm",
        "port": 27004
    },
    {
        "protocol": "UDP",
        "name": "flex-lm",
        "port": 27005
    },
    {
        "protocol": "UDP",
        "name": "flex-lm",
        "port": 27006
    },
    {
        "protocol": "UDP",
        "name": "flex-lm",
        "port": 27007
    },
    {
        "protocol": "UDP",
        "name": "flex-lm",
        "port": 27008
    },
    {
        "protocol": "UDP",
        "name": "flex-lm",
        "port": 27009
    },
    {
        "protocol": "UDP",
        "name": "imagepump",
        "port": 27345
    },
    {
        "protocol": "UDP",
        "name": "kopek-httphead",
        "port": 27504
    },
    {
        "protocol": "UDP",
        "name": "ars-vista",
        "port": 27782
    },
    {
        "protocol": "UDP",
        "name": "tw-auth-key",
        "port": 27999
    },
    {
        "protocol": "UDP",
        "name": "nxlmd",
        "port": 28000
    },
    {
        "protocol": "UDP",
        "name": "siemensgsm",
        "port": 28240
    },
    {
        "protocol": "UDP",
        "name": "otmp",
        "port": 29167
    },
    {
        "protocol": "UDP",
        "name": "pago-services1",
        "port": 30001
    },
    {
        "protocol": "UDP",
        "name": "pago-services2",
        "port": 30002
    },
    {
        "protocol": "UDP",
        "name": "xqosd",
        "port": 31416
    },
    {
        "protocol": "UDP",
        "name": "tetrinet",
        "port": 31457
    },
    {
        "protocol": "UDP",
        "name": "lm-mon",
        "port": 31620
    },
    {
        "protocol": "UDP",
        "name": "gamesmith-port",
        "port": 31765
    },
    {
        "protocol": "UDP",
        "name": "t1distproc60",
        "port": 32249
    },
    {
        "protocol": "UDP",
        "name": "apm-link",
        "port": 32483
    },
    {
        "protocol": "UDP",
        "name": "sec-ntb-clnt",
        "port": 32635
    },
    {
        "protocol": "UDP",
        "name": "filenet-tms",
        "port": 32768
    },
    {
        "protocol": "UDP",
        "name": "filenet-rpc",
        "port": 32769
    },
    {
        "protocol": "UDP",
        "name": "filenet-nch",
        "port": 32770
    },
    {
        "protocol": "UDP",
        "name": "filenet-rmi",
        "port": 32771
    },
    {
        "protocol": "UDP",
        "name": "filenet-pa",
        "port": 32772
    },
    {
        "protocol": "UDP",
        "name": "filenet-cm",
        "port": 32773
    },
    {
        "protocol": "UDP",
        "name": "filenet-re",
        "port": 32774
    },
    {
        "protocol": "UDP",
        "name": "filenet-pch",
        "port": 32775
    },
    {
        "protocol": "UDP",
        "name": "idmgratm",
        "port": 32896
    },
    {
        "protocol": "UDP",
        "name": "diamondport",
        "port": 33331
    },
    {
        "protocol": "UDP",
        "name": "traceroute",
        "port": 33434
    },
    {
        "protocol": "UDP",
        "name": "snip-slave",
        "port": 33656
    },
    {
        "protocol": "UDP",
        "name": "turbonote-2",
        "port": 34249
    },
    {
        "protocol": "UDP",
        "name": "p-net-local",
        "port": 34378
    },
    {
        "protocol": "UDP",
        "name": "p-net-remote",
        "port": 34379
    },
    {
        "protocol": "UDP",
        "name": "profinet-rt",
        "port": 34962
    },
    {
        "protocol": "UDP",
        "name": "profinet-rtm",
        "port": 34963
    },
    {
        "protocol": "UDP",
        "name": "profinet-cm",
        "port": 34964
    },
    {
        "protocol": "UDP",
        "name": "ethercat",
        "port": 34980
    },
    {
        "protocol": "UDP",
        "name": "kastenxpipe",
        "port": 36865
    },
    {
        "protocol": "UDP",
        "name": "neckar",
        "port": 37475
    },
    {
        "protocol": "UDP",
        "name": "unisys-eportal",
        "port": 37654
    },
    {
        "protocol": "UDP",
        "name": "galaxy7-data",
        "port": 38201
    },
    {
        "protocol": "UDP",
        "name": "fairview",
        "port": 38202
    },
    {
        "protocol": "UDP",
        "name": "agpolicy",
        "port": 38203
    },
    {
        "protocol": "UDP",
        "name": "turbonote-1",
        "port": 39681
    },
    {
        "protocol": "UDP",
        "name": "cscp",
        "port": 40841
    },
    {
        "protocol": "UDP",
        "name": "csccredir",
        "port": 40842
    },
    {
        "protocol": "UDP",
        "name": "csccfirewall",
        "port": 40843
    },
    {
        "protocol": "UDP",
        "name": "fs-qos",
        "port": 41111
    },
    {
        "protocol": "UDP",
        "name": "crestron-cip",
        "port": 41794
    },
    {
        "protocol": "UDP",
        "name": "crestron-ctp",
        "port": 41795
    },
    {
        "protocol": "UDP",
        "name": "candp",
        "port": 42508
    },
    {
        "protocol": "UDP",
        "name": "candrp",
        "port": 42509
    },
    {
        "protocol": "UDP",
        "name": "caerpc",
        "port": 42510
    },
    {
        "protocol": "UDP",
        "name": "reachout",
        "port": 43188
    },
    {
        "protocol": "UDP",
        "name": "ndm-agent-port",
        "port": 43189
    },
    {
        "protocol": "UDP",
        "name": "ip-provision",
        "port": 43190
    },
    {
        "protocol": "UDP",
        "name": "ciscocsdb",
        "port": 43441
    },
    {
        "protocol": "UDP",
        "name": "pmcd",
        "port": 44321
    },
    {
        "protocol": "UDP",
        "name": "pmcdproxy",
        "port": 44322
    },
    {
        "protocol": "UDP",
        "name": "rbr-debug",
        "port": 44553
    },
    {
        "protocol": "UDP",
        "name": "rockwell-encap",
        "port": 44818
    },
    {
        "protocol": "UDP",
        "name": "invision-ag",
        "port": 45054
    },
    {
        "protocol": "UDP",
        "name": "eba",
        "port": 45678
    },
    {
        "protocol": "UDP",
        "name": "ssr-servermgr",
        "port": 45966
    },
    {
        "protocol": "UDP",
        "name": "mediabox",
        "port": 46999
    },
    {
        "protocol": "UDP",
        "name": "mbus",
        "port": 47000
    },
    {
        "protocol": "UDP",
        "name": "dbbrowse",
        "port": 47557
    },
    {
        "protocol": "UDP",
        "name": "directplaysrvr",
        "port": 47624
    },
    {
        "protocol": "UDP",
        "name": "ap",
        "port": 47806
    },
    {
        "protocol": "UDP",
        "name": "bacnet",
        "port": 47808
    },
    {
        "protocol": "UDP",
        "name": "nimcontroller",
        "port": 48000
    },
    {
        "protocol": "UDP",
        "name": "nimspooler",
        "port": 48001
    },
    {
        "protocol": "UDP",
        "name": "nimhub",
        "port": 48002
    },
    {
        "protocol": "UDP",
        "name": "nimgtw",
        "port": 48003
    },
    {
        "protocol": "UDP",
        "name": "com-bardac-dw",
        "port": 48556
    },
    {
        "protocol": "UDP",
        "name": "iqobject",
        "port": 48619
    },
    {
        "protocol": "TCP",
        "name": "TCP-mux",
        "port": 1
    },
    {
        "protocol": "TCP",
        "name": "compressnet",
        "port": 2
    },
    {
        "protocol": "TCP",
        "name": "compressnet",
        "port": 3
    },
    {
        "protocol": "TCP",
        "name": "rje",
        "port": 5
    },
    {
        "protocol": "TCP",
        "name": "echo",
        "port": 7
    },
    {
        "protocol": "TCP",
        "name": "discard",
        "port": 9
    },
    {
        "protocol": "TCP",
        "name": "systat",
        "port": 11
    },
    {
        "protocol": "TCP",
        "name": "daytime",
        "port": 13
    },
    {
        "protocol": "TCP",
        "name": "qotd",
        "port": 17
    },
    {
        "protocol": "TCP",
        "name": "msp",
        "port": 18
    },
    {
        "protocol": "TCP",
        "name": "chargen",
        "port": 19
    },
    {
        "protocol": "TCP",
        "name": "ftp-data",
        "port": 20
    },
    {
        "protocol": "TCP",
        "name": "ftp",
        "port": 21
    },
    {
        "protocol": "TCP",
        "name": "ssh",
        "port": 22
    },
    {
        "protocol": "TCP",
        "name": "telnet",
        "port": 23
    },
    {
        "protocol": "TCP",
        "name": "smtp",
        "port": 25
    },
    {
        "protocol": "TCP",
        "name": "nsw-fe",
        "port": 27
    },
    {
        "protocol": "TCP",
        "name": "msg-icp",
        "port": 29
    },
    {
        "protocol": "TCP",
        "name": "msg-auth",
        "port": 31
    },
    {
        "protocol": "TCP",
        "name": "dsp",
        "port": 33
    },
    {
        "protocol": "TCP",
        "name": "time",
        "port": 37
    },
    {
        "protocol": "TCP",
        "name": "rap",
        "port": 38
    },
    {
        "protocol": "TCP",
        "name": "rlp",
        "port": 39
    },
    {
        "protocol": "TCP",
        "name": "graphics",
        "port": 41
    },
    {
        "protocol": "TCP",
        "name": "nameserver",
        "port": 42
    },
    {
        "protocol": "TCP",
        "name": "nicname",
        "port": 43
    },
    {
        "protocol": "TCP",
        "name": "mpm-flags",
        "port": 44
    },
    {
        "protocol": "TCP",
        "name": "mpm",
        "port": 45
    },
    {
        "protocol": "TCP",
        "name": "mpm-snd",
        "port": 46
    },
    {
        "protocol": "TCP",
        "name": "ni-ftp",
        "port": 47
    },
    {
        "protocol": "TCP",
        "name": "auditd",
        "port": 48
    },
    {
        "protocol": "TCP",
        "name": "tacacs",
        "port": 49
    },
    {
        "protocol": "TCP",
        "name": "re-mail-ck",
        "port": 50
    },
    {
        "protocol": "TCP",
        "name": "la-maint",
        "port": 51
    },
    {
        "protocol": "TCP",
        "name": "xns-time",
        "port": 52
    },
    {
        "protocol": "TCP",
        "name": "domain",
        "port": 53
    },
    {
        "protocol": "TCP",
        "name": "xns-ch",
        "port": 54
    },
    {
        "protocol": "TCP",
        "name": "isi-gl",
        "port": 55
    },
    {
        "protocol": "TCP",
        "name": "xns-auth",
        "port": 56
    },
    {
        "protocol": "TCP",
        "name": "xns-mail",
        "port": 58
    },
    {
        "protocol": "TCP",
        "name": "ni-mail",
        "port": 61
    },
    {
        "protocol": "TCP",
        "name": "acas",
        "port": 62
    },
    {
        "protocol": "TCP",
        "name": "whois++",
        "port": 63
    },
    {
        "protocol": "TCP",
        "name": "covia",
        "port": 64
    },
    {
        "protocol": "TCP",
        "name": "tacacs-ds",
        "port": 65
    },
    {
        "protocol": "TCP",
        "name": "sql*net",
        "port": 66
    },
    {
        "protocol": "TCP",
        "name": "bootps",
        "port": 67
    },
    {
        "protocol": "TCP",
        "name": "bootpc",
        "port": 68
    },
    {
        "protocol": "TCP",
        "name": "tftp",
        "port": 69
    },
    {
        "protocol": "TCP",
        "name": "gopher",
        "port": 70
    },
    {
        "protocol": "TCP",
        "name": "netrjs-1",
        "port": 71
    },
    {
        "protocol": "TCP",
        "name": "netrjs-2",
        "port": 72
    },
    {
        "protocol": "TCP",
        "name": "netrjs-3",
        "port": 73
    },
    {
        "protocol": "TCP",
        "name": "netrjs-4",
        "port": 74
    },
    {
        "protocol": "TCP",
        "name": "deos",
        "port": 76
    },
    {
        "protocol": "TCP",
        "name": "vetTCP",
        "port": 78
    },
    {
        "protocol": "TCP",
        "name": "finger",
        "port": 79
    },
    {
        "protocol": "TCP",
        "name": "http",
        "port": 80
    },
    {
        "protocol": "TCP",
        "name": "hosts2-ns",
        "port": 81
    },
    {
        "protocol": "TCP",
        "name": "xfer",
        "port": 82
    },
    {
        "protocol": "TCP",
        "name": "mit-ml-dev",
        "port": 83
    },
    {
        "protocol": "TCP",
        "name": "ctf",
        "port": 84
    },
    {
        "protocol": "TCP",
        "name": "mit-ml-dev",
        "port": 85
    },
    {
        "protocol": "TCP",
        "name": "mfcobol",
        "port": 86
    },
    {
        "protocol": "TCP",
        "name": "kerberos",
        "port": 88
    },
    {
        "protocol": "TCP",
        "name": "su-mit-tg",
        "port": 89
    },
    {
        "protocol": "TCP",
        "name": "dnsix",
        "port": 90
    },
    {
        "protocol": "TCP",
        "name": "mit-dov",
        "port": 91
    },
    {
        "protocol": "TCP",
        "name": "npp",
        "port": 92
    },
    {
        "protocol": "TCP",
        "name": "dcp",
        "port": 93
    },
    {
        "protocol": "TCP",
        "name": "objcall",
        "port": 94
    },
    {
        "protocol": "TCP",
        "name": "supdup",
        "port": 95
    },
    {
        "protocol": "TCP",
        "name": "dixie",
        "port": 96
    },
    {
        "protocol": "TCP",
        "name": "swift-rvf",
        "port": 97
    },
    {
        "protocol": "TCP",
        "name": "tacnews",
        "port": 98
    },
    {
        "protocol": "TCP",
        "name": "metagram",
        "port": 99
    },
    {
        "protocol": "TCP",
        "name": "newacct",
        "port": 100
    },
    {
        "protocol": "TCP",
        "name": "hostname",
        "port": 101
    },
    {
        "protocol": "TCP",
        "name": "iso-tsap",
        "port": 102
    },
    {
        "protocol": "TCP",
        "name": "gppitnp",
        "port": 103
    },
    {
        "protocol": "TCP",
        "name": "acr-nema",
        "port": 104
    },
    {
        "protocol": "TCP",
        "name": "csnet-ns",
        "port": 105
    },
    {
        "protocol": "TCP",
        "name": "3com-tsmux",
        "port": 106
    },
    {
        "protocol": "TCP",
        "name": "rtelnet",
        "port": 107
    },
    {
        "protocol": "TCP",
        "name": "snagas",
        "port": 108
    },
    {
        "protocol": "TCP",
        "name": "pop2",
        "port": 109
    },
    {
        "protocol": "TCP",
        "name": "pop3",
        "port": 110
    },
    {
        "protocol": "TCP",
        "name": "sunrpc",
        "port": 111
    },
    {
        "protocol": "TCP",
        "name": "mcidas",
        "port": 112
    },
    {
        "protocol": "TCP",
        "name": "ident",
        "port": 113
    },
    {
        "protocol": "TCP",
        "name": "sftp",
        "port": 115
    },
    {
        "protocol": "TCP",
        "name": "ansanotify",
        "port": 116
    },
    {
        "protocol": "TCP",
        "name": "uucp-path",
        "port": 117
    },
    {
        "protocol": "TCP",
        "name": "sqlserv",
        "port": 118
    },
    {
        "protocol": "TCP",
        "name": "nntp",
        "port": 119
    },
    {
        "protocol": "TCP",
        "name": "cfdptkt",
        "port": 120
    },
    {
        "protocol": "TCP",
        "name": "erpc",
        "port": 121
    },
    {
        "protocol": "TCP",
        "name": "smakynet",
        "port": 122
    },
    {
        "protocol": "TCP",
        "name": "ntp",
        "port": 123
    },
    {
        "protocol": "TCP",
        "name": "ansatrader",
        "port": 124
    },
    {
        "protocol": "TCP",
        "name": "locus-map",
        "port": 125
    },
    {
        "protocol": "TCP",
        "name": "nxedit",
        "port": 126
    },
    {
        "protocol": "TCP",
        "name": "locus-con",
        "port": 127
    },
    {
        "protocol": "TCP",
        "name": "gss-xlicen",
        "port": 128
    },
    {
        "protocol": "TCP",
        "name": "pwdgen",
        "port": 129
    },
    {
        "protocol": "TCP",
        "name": "cisco-fna",
        "port": 130
    },
    {
        "protocol": "TCP",
        "name": "cisco-tna",
        "port": 131
    },
    {
        "protocol": "TCP",
        "name": "cisco-sys",
        "port": 132
    },
    {
        "protocol": "TCP",
        "name": "statsrv",
        "port": 133
    },
    {
        "protocol": "TCP",
        "name": "ingres-net",
        "port": 134
    },
    {
        "protocol": "TCP",
        "name": "epmap",
        "port": 135
    },
    {
        "protocol": "TCP",
        "name": "profile",
        "port": 136
    },
    {
        "protocol": "TCP",
        "name": "netbios-ns",
        "port": 137
    },
    {
        "protocol": "TCP",
        "name": "netbios-dgm",
        "port": 138
    },
    {
        "protocol": "TCP",
        "name": "netbios-ssn",
        "port": 139
    },
    {
        "protocol": "TCP",
        "name": "emfis-data",
        "port": 140
    },
    {
        "protocol": "TCP",
        "name": "emfis-cntl",
        "port": 141
    },
    {
        "protocol": "TCP",
        "name": "bl-idm",
        "port": 142
    },
    {
        "protocol": "TCP",
        "name": "imap",
        "port": 143
    },
    {
        "protocol": "TCP",
        "name": "uma",
        "port": 144
    },
    {
        "protocol": "TCP",
        "name": "uaac",
        "port": 145
    },
    {
        "protocol": "TCP",
        "name": "iso-tp0",
        "port": 146
    },
    {
        "protocol": "TCP",
        "name": "iso-ip",
        "port": 147
    },
    {
        "protocol": "TCP",
        "name": "jargon",
        "port": 148
    },
    {
        "protocol": "TCP",
        "name": "aed-512",
        "port": 149
    },
    {
        "protocol": "TCP",
        "name": "sql-net",
        "port": 150
    },
    {
        "protocol": "TCP",
        "name": "hems",
        "port": 151
    },
    {
        "protocol": "TCP",
        "name": "bftp",
        "port": 152
    },
    {
        "protocol": "TCP",
        "name": "sgmp",
        "port": 153
    },
    {
        "protocol": "TCP",
        "name": "netsc-prod",
        "port": 154
    },
    {
        "protocol": "TCP",
        "name": "netsc-dev",
        "port": 155
    },
    {
        "protocol": "TCP",
        "name": "sqlsrv",
        "port": 156
    },
    {
        "protocol": "TCP",
        "name": "knet-cmp",
        "port": 157
    },
    {
        "protocol": "TCP",
        "name": "pcmail-srv",
        "port": 158
    },
    {
        "protocol": "TCP",
        "name": "nss-routing",
        "port": 159
    },
    {
        "protocol": "TCP",
        "name": "sgmp-traps",
        "port": 160
    },
    {
        "protocol": "TCP",
        "name": "snmp",
        "port": 161
    },
    {
        "protocol": "TCP",
        "name": "snmptrap",
        "port": 162
    },
    {
        "protocol": "TCP",
        "name": "cmip-man",
        "port": 163
    },
    {
        "protocol": "TCP",
        "name": "cmip-agent",
        "port": 164
    },
    {
        "protocol": "TCP",
        "name": "xns-courier",
        "port": 165
    },
    {
        "protocol": "TCP",
        "name": "s-net",
        "port": 166
    },
    {
        "protocol": "TCP",
        "name": "namp",
        "port": 167
    },
    {
        "protocol": "TCP",
        "name": "rsvd",
        "port": 168
    },
    {
        "protocol": "TCP",
        "name": "send",
        "port": 169
    },
    {
        "protocol": "TCP",
        "name": "print-srv",
        "port": 170
    },
    {
        "protocol": "TCP",
        "name": "multiplex",
        "port": 171
    },
    {
        "protocol": "TCP",
        "name": "cl/1",
        "port": 172
    },
    {
        "protocol": "TCP",
        "name": "xyplex-mux",
        "port": 173
    },
    {
        "protocol": "TCP",
        "name": "mailq",
        "port": 174
    },
    {
        "protocol": "TCP",
        "name": "vmnet",
        "port": 175
    },
    {
        "protocol": "TCP",
        "name": "genrad-mux",
        "port": 176
    },
    {
        "protocol": "TCP",
        "name": "xdmcp",
        "port": 177
    },
    {
        "protocol": "TCP",
        "name": "nextstep",
        "port": 178
    },
    {
        "protocol": "TCP",
        "name": "bgp",
        "port": 179
    },
    {
        "protocol": "TCP",
        "name": "ris",
        "port": 180
    },
    {
        "protocol": "TCP",
        "name": "unify",
        "port": 181
    },
    {
        "protocol": "TCP",
        "name": "audit",
        "port": 182
    },
    {
        "protocol": "TCP",
        "name": "ocbinder",
        "port": 183
    },
    {
        "protocol": "TCP",
        "name": "ocserver",
        "port": 184
    },
    {
        "protocol": "TCP",
        "name": "remote-kis",
        "port": 185
    },
    {
        "protocol": "TCP",
        "name": "kis",
        "port": 186
    },
    {
        "protocol": "TCP",
        "name": "aci",
        "port": 187
    },
    {
        "protocol": "TCP",
        "name": "mumps",
        "port": 188
    },
    {
        "protocol": "TCP",
        "name": "qft",
        "port": 189
    },
    {
        "protocol": "TCP",
        "name": "gacp",
        "port": 190
    },
    {
        "protocol": "TCP",
        "name": "prospero",
        "port": 191
    },
    {
        "protocol": "TCP",
        "name": "osu-nms",
        "port": 192
    },
    {
        "protocol": "TCP",
        "name": "srmp",
        "port": 193
    },
    {
        "protocol": "TCP",
        "name": "irc",
        "port": 194
    },
    {
        "protocol": "TCP",
        "name": "dn6-nlm-aud",
        "port": 195
    },
    {
        "protocol": "TCP",
        "name": "dn6-smm-red",
        "port": 196
    },
    {
        "protocol": "TCP",
        "name": "dls",
        "port": 197
    },
    {
        "protocol": "TCP",
        "name": "dls-mon",
        "port": 198
    },
    {
        "protocol": "TCP",
        "name": "smux",
        "port": 199
    },
    {
        "protocol": "TCP",
        "name": "novastorbakcup",
        "port": 308
    },
    {
        "protocol": "TCP",
        "name": "entrusttime",
        "port": 309
    },
    {
        "protocol": "TCP",
        "name": "bhmds",
        "port": 310
    },
    {
        "protocol": "TCP",
        "name": "asip-webadmin",
        "port": 311
    },
    {
        "protocol": "TCP",
        "name": "vslmp",
        "port": 312
    },
    {
        "protocol": "TCP",
        "name": "magenta-logic",
        "port": 313
    },
    {
        "protocol": "TCP",
        "name": "opalis-robot",
        "port": 314
    },
    {
        "protocol": "TCP",
        "name": "dpsi",
        "port": 315
    },
    {
        "protocol": "TCP",
        "name": "decauth",
        "port": 316
    },
    {
        "protocol": "TCP",
        "name": "zannet",
        "port": 317
    },
    {
        "protocol": "TCP",
        "name": "pkix-timestamp",
        "port": 318
    },
    {
        "protocol": "TCP",
        "name": "ptp-event",
        "port": 319
    },
    {
        "protocol": "TCP",
        "name": "ptp-general",
        "port": 320
    },
    {
        "protocol": "TCP",
        "name": "pip",
        "port": 321
    },
    {
        "protocol": "TCP",
        "name": "rtsps",
        "port": 322
    },
    {
        "protocol": "TCP",
        "name": "texar",
        "port": 333
    },
    {
        "protocol": "TCP",
        "name": "pdap",
        "port": 344
    },
    {
        "protocol": "TCP",
        "name": "pawserv",
        "port": 345
    },
    {
        "protocol": "TCP",
        "name": "zserv",
        "port": 346
    },
    {
        "protocol": "TCP",
        "name": "fatserv",
        "port": 347
    },
    {
        "protocol": "TCP",
        "name": "csi-sgwp",
        "port": 348
    },
    {
        "protocol": "TCP",
        "name": "mftp",
        "port": 349
    },
    {
        "protocol": "TCP",
        "name": "matip-type-a",
        "port": 350
    },
    {
        "protocol": "TCP",
        "name": "matip-type-b",
        "port": 351
    },
    {
        "protocol": "TCP",
        "name": "dtag-ste-sb",
        "port": 352
    },
    {
        "protocol": "TCP",
        "name": "ndsauth",
        "port": 353
    },
    {
        "protocol": "TCP",
        "name": "bh611",
        "port": 354
    },
    {
        "protocol": "TCP",
        "name": "datex-asn",
        "port": 355
    },
    {
        "protocol": "TCP",
        "name": "cloanto-net-1",
        "port": 356
    },
    {
        "protocol": "TCP",
        "name": "bhevent",
        "port": 357
    },
    {
        "protocol": "TCP",
        "name": "shrinkwrap",
        "port": 358
    },
    {
        "protocol": "TCP",
        "name": "nsrmp",
        "port": 359
    },
    {
        "protocol": "TCP",
        "name": "scoi2odialog",
        "port": 360
    },
    {
        "protocol": "TCP",
        "name": "semantix",
        "port": 361
    },
    {
        "protocol": "TCP",
        "name": "srssend",
        "port": 362
    },
    {
        "protocol": "TCP",
        "name": "rsvp_tunnel",
        "port": 363
    },
    {
        "protocol": "TCP",
        "name": "aurora-cmgr",
        "port": 364
    },
    {
        "protocol": "TCP",
        "name": "dtk",
        "port": 365
    },
    {
        "protocol": "TCP",
        "name": "odmr",
        "port": 366
    },
    {
        "protocol": "TCP",
        "name": "mortgageware",
        "port": 367
    },
    {
        "protocol": "TCP",
        "name": "qbikgdp",
        "port": 368
    },
    {
        "protocol": "TCP",
        "name": "rpc2portmap",
        "port": 369
    },
    {
        "protocol": "TCP",
        "name": "codaauth2",
        "port": 370
    },
    {
        "protocol": "TCP",
        "name": "clearcase",
        "port": 371
    },
    {
        "protocol": "TCP",
        "name": "ulistproc",
        "port": 372
    },
    {
        "protocol": "TCP",
        "name": "legent-1",
        "port": 373
    },
    {
        "protocol": "TCP",
        "name": "legent-2",
        "port": 374
    },
    {
        "protocol": "TCP",
        "name": "hassle",
        "port": 375
    },
    {
        "protocol": "TCP",
        "name": "nip",
        "port": 376
    },
    {
        "protocol": "TCP",
        "name": "tnETOS",
        "port": 377
    },
    {
        "protocol": "TCP",
        "name": "dsETOS",
        "port": 378
    },
    {
        "protocol": "TCP",
        "name": "is99c",
        "port": 379
    },
    {
        "protocol": "TCP",
        "name": "is99s",
        "port": 380
    },
    {
        "protocol": "TCP",
        "name": "hp-collector",
        "port": 381
    },
    {
        "protocol": "TCP",
        "name": "hp-managed-node",
        "port": 382
    },
    {
        "protocol": "TCP",
        "name": "hp-alarm-mgr",
        "port": 383
    },
    {
        "protocol": "TCP",
        "name": "arns",
        "port": 384
    },
    {
        "protocol": "TCP",
        "name": "ibm-app",
        "port": 385
    },
    {
        "protocol": "TCP",
        "name": "asa",
        "port": 386
    },
    {
        "protocol": "TCP",
        "name": "aurp",
        "port": 387
    },
    {
        "protocol": "TCP",
        "name": "unidata-ldm",
        "port": 388
    },
    {
        "protocol": "TCP",
        "name": "ldap",
        "port": 389
    },
    {
        "protocol": "TCP",
        "name": "uis",
        "port": 390
    },
    {
        "protocol": "TCP",
        "name": "synotics-relay",
        "port": 391
    },
    {
        "protocol": "TCP",
        "name": "TCP",
        "port": 392
    },
    {
        "protocol": "TCP",
        "name": "meta5",
        "port": 393
    },
    {
        "protocol": "TCP",
        "name": "embl-ndt",
        "port": 394
    },
    {
        "protocol": "TCP",
        "name": "neTCP",
        "port": 395
    },
    {
        "protocol": "TCP",
        "name": "netware-ip",
        "port": 396
    },
    {
        "protocol": "TCP",
        "name": "mptn",
        "port": 397
    },
    {
        "protocol": "TCP",
        "name": "kryptolan",
        "port": 398
    },
    {
        "protocol": "TCP",
        "name": "iso-tsap-c2",
        "port": 399
    },
    {
        "protocol": "TCP",
        "name": "work-sol",
        "port": 400
    },
    {
        "protocol": "TCP",
        "name": "ups",
        "port": 401
    },
    {
        "protocol": "TCP",
        "name": "genie",
        "port": 402
    },
    {
        "protocol": "TCP",
        "name": "decap",
        "port": 403
    },
    {
        "protocol": "TCP",
        "name": "nced",
        "port": 404
    },
    {
        "protocol": "TCP",
        "name": "ncld",
        "port": 405
    },
    {
        "protocol": "TCP",
        "name": "imsp",
        "port": 406
    },
    {
        "protocol": "TCP",
        "name": "timbuktu",
        "port": 407
    },
    {
        "protocol": "TCP",
        "name": "prm-sm",
        "port": 408
    },
    {
        "protocol": "TCP",
        "name": "prm-nm",
        "port": 409
    },
    {
        "protocol": "TCP",
        "name": "decladebug",
        "port": 410
    },
    {
        "protocol": "TCP",
        "name": "rmt",
        "port": 411
    },
    {
        "protocol": "TCP",
        "name": "synoptics-trap",
        "port": 412
    },
    {
        "protocol": "TCP",
        "name": "smsp",
        "port": 413
    },
    {
        "protocol": "TCP",
        "name": "infoseek",
        "port": 414
    },
    {
        "protocol": "TCP",
        "name": "bnet",
        "port": 415
    },
    {
        "protocol": "TCP",
        "name": "silverplatter",
        "port": 416
    },
    {
        "protocol": "TCP",
        "name": "onmux",
        "port": 417
    },
    {
        "protocol": "TCP",
        "name": "hyper-g",
        "port": 418
    },
    {
        "protocol": "TCP",
        "name": "ariel1",
        "port": 419
    },
    {
        "protocol": "TCP",
        "name": "smpte",
        "port": 420
    },
    {
        "protocol": "TCP",
        "name": "ariel2",
        "port": 421
    },
    {
        "protocol": "TCP",
        "name": "ariel3",
        "port": 422
    },
    {
        "protocol": "TCP",
        "name": "opc-job-start",
        "port": 423
    },
    {
        "protocol": "TCP",
        "name": "opc-job-track",
        "port": 424
    },
    {
        "protocol": "TCP",
        "name": "icad-el",
        "port": 425
    },
    {
        "protocol": "TCP",
        "name": "smartsdp",
        "port": 426
    },
    {
        "protocol": "TCP",
        "name": "svrloc",
        "port": 427
    },
    {
        "protocol": "TCP",
        "name": "ocs_cmu",
        "port": 428
    },
    {
        "protocol": "TCP",
        "name": "ocs_amu",
        "port": 429
    },
    {
        "protocol": "TCP",
        "name": "utmpsd",
        "port": 430
    },
    {
        "protocol": "TCP",
        "name": "utmpcd",
        "port": 431
    },
    {
        "protocol": "TCP",
        "name": "iasd",
        "port": 432
    },
    {
        "protocol": "TCP",
        "name": "nnsp",
        "port": 433
    },
    {
        "protocol": "TCP",
        "name": "mobileip-agent",
        "port": 434
    },
    {
        "protocol": "TCP",
        "name": "mobilip-mn",
        "port": 435
    },
    {
        "protocol": "TCP",
        "name": "dna-cml",
        "port": 436
    },
    {
        "protocol": "TCP",
        "name": "comscm",
        "port": 437
    },
    {
        "protocol": "TCP",
        "name": "dsfgw",
        "port": 438
    },
    {
        "protocol": "TCP",
        "name": "dasp",
        "port": 439
    },
    {
        "protocol": "TCP",
        "name": "sgcp",
        "port": 440
    },
    {
        "protocol": "TCP",
        "name": "decvms-sysmgt",
        "port": 441
    },
    {
        "protocol": "TCP",
        "name": "cvc_hostd",
        "port": 442
    },
    {
        "protocol": "TCP",
        "name": "https",
        "port": 443
    },
    {
        "protocol": "TCP",
        "name": "snpp",
        "port": 444
    },
    {
        "protocol": "TCP",
        "name": "microsoft-ds",
        "port": 445
    },
    {
        "protocol": "TCP",
        "name": "ddm-rdb",
        "port": 446
    },
    {
        "protocol": "TCP",
        "name": "ddm-dfm",
        "port": 447
    },
    {
        "protocol": "TCP",
        "name": "ddm-ssl",
        "port": 448
    },
    {
        "protocol": "TCP",
        "name": "as-servermap",
        "port": 449
    },
    {
        "protocol": "TCP",
        "name": "tserver",
        "port": 450
    },
    {
        "protocol": "TCP",
        "name": "sfs-smp-net",
        "port": 451
    },
    {
        "protocol": "TCP",
        "name": "sfs-config",
        "port": 452
    },
    {
        "protocol": "TCP",
        "name": "creativeserver",
        "port": 453
    },
    {
        "protocol": "TCP",
        "name": "contentserver",
        "port": 454
    },
    {
        "protocol": "TCP",
        "name": "creativepartnr",
        "port": 455
    },
    {
        "protocol": "TCP",
        "name": "macon-TCP",
        "port": 456
    },
    {
        "protocol": "TCP",
        "name": "scohelp",
        "port": 457
    },
    {
        "protocol": "TCP",
        "name": "appleqtc",
        "port": 458
    },
    {
        "protocol": "TCP",
        "name": "ampr-rcmd",
        "port": 459
    },
    {
        "protocol": "TCP",
        "name": "skronk",
        "port": 460
    },
    {
        "protocol": "TCP",
        "name": "datasurfsrv",
        "port": 461
    },
    {
        "protocol": "TCP",
        "name": "datasurfsrvsec",
        "port": 462
    },
    {
        "protocol": "TCP",
        "name": "alpes",
        "port": 463
    },
    {
        "protocol": "TCP",
        "name": "kpasswd",
        "port": 464
    },
    {
        "protocol": "TCP",
        "name": "urd",
        "port": 465
    },
    {
        "protocol": "TCP",
        "name": "digital-vrc",
        "port": 466
    },
    {
        "protocol": "TCP",
        "name": "mylex-mapd",
        "port": 467
    },
    {
        "protocol": "TCP",
        "name": "photuris",
        "port": 468
    },
    {
        "protocol": "TCP",
        "name": "rcp",
        "port": 469
    },
    {
        "protocol": "TCP",
        "name": "scx-proxy",
        "port": 470
    },
    {
        "protocol": "TCP",
        "name": "mondex",
        "port": 471
    },
    {
        "protocol": "TCP",
        "name": "ljk-login",
        "port": 472
    },
    {
        "protocol": "TCP",
        "name": "hybrid-pop",
        "port": 473
    },
    {
        "protocol": "TCP",
        "name": "tn-tl-w1",
        "port": 474
    },
    {
        "protocol": "TCP",
        "name": "TCPnethaspsrv",
        "port": 475
    },
    {
        "protocol": "TCP",
        "name": "tn-tl-fd1",
        "port": 476
    },
    {
        "protocol": "TCP",
        "name": "ss7ns",
        "port": 477
    },
    {
        "protocol": "TCP",
        "name": "spsc",
        "port": 478
    },
    {
        "protocol": "TCP",
        "name": "iafserver",
        "port": 479
    },
    {
        "protocol": "TCP",
        "name": "iafdbase",
        "port": 480
    },
    {
        "protocol": "TCP",
        "name": "ph",
        "port": 481
    },
    {
        "protocol": "TCP",
        "name": "bgs-nsi",
        "port": 482
    },
    {
        "protocol": "TCP",
        "name": "ulpnet",
        "port": 483
    },
    {
        "protocol": "TCP",
        "name": "integra-sme",
        "port": 484
    },
    {
        "protocol": "TCP",
        "name": "powerburst",
        "port": 485
    },
    {
        "protocol": "TCP",
        "name": "avian",
        "port": 486
    },
    {
        "protocol": "TCP",
        "name": "saft",
        "port": 487
    },
    {
        "protocol": "TCP",
        "name": "gss-http",
        "port": 488
    },
    {
        "protocol": "TCP",
        "name": "nest-protocol",
        "port": 489
    },
    {
        "protocol": "TCP",
        "name": "micom-pfs",
        "port": 490
    },
    {
        "protocol": "TCP",
        "name": "go-login",
        "port": 491
    },
    {
        "protocol": "TCP",
        "name": "ticf-1",
        "port": 492
    },
    {
        "protocol": "TCP",
        "name": "ticf-2",
        "port": 493
    },
    {
        "protocol": "TCP",
        "name": "pov-ray",
        "port": 494
    },
    {
        "protocol": "TCP",
        "name": "intecourier",
        "port": 495
    },
    {
        "protocol": "TCP",
        "name": "pim-rp-disc",
        "port": 496
    },
    {
        "protocol": "TCP",
        "name": "dantz",
        "port": 497
    },
    {
        "protocol": "TCP",
        "name": "siam",
        "port": 498
    },
    {
        "protocol": "TCP",
        "name": "iso-ill",
        "port": 499
    },
    {
        "protocol": "TCP",
        "name": "isakmp",
        "port": 500
    },
    {
        "protocol": "TCP",
        "name": "stmf",
        "port": 501
    },
    {
        "protocol": "TCP",
        "name": "asa-appl-proto",
        "port": 502
    },
    {
        "protocol": "TCP",
        "name": "intrinsa",
        "port": 503
    },
    {
        "protocol": "TCP",
        "name": "citadel",
        "port": 504
    },
    {
        "protocol": "TCP",
        "name": "mailbox-lm",
        "port": 505
    },
    {
        "protocol": "TCP",
        "name": "ohimsrv",
        "port": 506
    },
    {
        "protocol": "TCP",
        "name": "crs",
        "port": 507
    },
    {
        "protocol": "TCP",
        "name": "xvttp",
        "port": 508
    },
    {
        "protocol": "TCP",
        "name": "snare",
        "port": 509
    },
    {
        "protocol": "TCP",
        "name": "fcp",
        "port": 510
    },
    {
        "protocol": "TCP",
        "name": "passgo",
        "port": 511
    },
    {
        "protocol": "TCP",
        "name": "exec",
        "port": 512
    },
    {
        "protocol": "TCP",
        "name": "login",
        "port": 513
    },
    {
        "protocol": "TCP",
        "name": "shell",
        "port": 514
    },
    {
        "protocol": "TCP",
        "name": "printer",
        "port": 515
    },
    {
        "protocol": "TCP",
        "name": "videotex",
        "port": 516
    },
    {
        "protocol": "TCP",
        "name": "talk",
        "port": 517
    },
    {
        "protocol": "TCP",
        "name": "ntalk",
        "port": 518
    },
    {
        "protocol": "TCP",
        "name": "utime",
        "port": 519
    },
    {
        "protocol": "TCP",
        "name": "efs",
        "port": 520
    },
    {
        "protocol": "TCP",
        "name": "ripng",
        "port": 521
    },
    {
        "protocol": "TCP",
        "name": "ulp",
        "port": 522
    },
    {
        "protocol": "TCP",
        "name": "ibm-db2",
        "port": 523
    },
    {
        "protocol": "TCP",
        "name": "ncp",
        "port": 524
    },
    {
        "protocol": "TCP",
        "name": "timed",
        "port": 525
    },
    {
        "protocol": "TCP",
        "name": "tempo",
        "port": 526
    },
    {
        "protocol": "TCP",
        "name": "stx",
        "port": 527
    },
    {
        "protocol": "TCP",
        "name": "custix",
        "port": 528
    },
    {
        "protocol": "TCP",
        "name": "irc-serv",
        "port": 529
    },
    {
        "protocol": "TCP",
        "name": "courier",
        "port": 530
    },
    {
        "protocol": "TCP",
        "name": "conference",
        "port": 531
    },
    {
        "protocol": "TCP",
        "name": "netnews",
        "port": 532
    },
    {
        "protocol": "TCP",
        "name": "netwall",
        "port": 533
    },
    {
        "protocol": "TCP",
        "name": "mm-admin",
        "port": 534
    },
    {
        "protocol": "TCP",
        "name": "iiop",
        "port": 535
    },
    {
        "protocol": "TCP",
        "name": "opalis-rdv",
        "port": 536
    },
    {
        "protocol": "TCP",
        "name": "nmsp",
        "port": 537
    },
    {
        "protocol": "TCP",
        "name": "gdomap",
        "port": 538
    },
    {
        "protocol": "TCP",
        "name": "apertus-ldp",
        "port": 539
    },
    {
        "protocol": "TCP",
        "name": "uucp",
        "port": 540
    },
    {
        "protocol": "TCP",
        "name": "uucp-rlogin",
        "port": 541
    },
    {
        "protocol": "TCP",
        "name": "commerce",
        "port": 542
    },
    {
        "protocol": "TCP",
        "name": "klogin",
        "port": 543
    },
    {
        "protocol": "TCP",
        "name": "kshell",
        "port": 544
    },
    {
        "protocol": "TCP",
        "name": "appleqtcsrvr",
        "port": 545
    },
    {
        "protocol": "TCP",
        "name": "dhcpv6-client",
        "port": 546
    },
    {
        "protocol": "TCP",
        "name": "dhcpv6-server",
        "port": 547
    },
    {
        "protocol": "TCP",
        "name": "afpoverTCP",
        "port": 548
    },
    {
        "protocol": "TCP",
        "name": "idfp",
        "port": 549
    },
    {
        "protocol": "TCP",
        "name": "new-rwho",
        "port": 550
    },
    {
        "protocol": "TCP",
        "name": "cybercash",
        "port": 551
    },
    {
        "protocol": "TCP",
        "name": "devshr-nts",
        "port": 552
    },
    {
        "protocol": "TCP",
        "name": "pirp",
        "port": 553
    },
    {
        "protocol": "TCP",
        "name": "rtsp",
        "port": 554
    },
    {
        "protocol": "TCP",
        "name": "dsf",
        "port": 555
    },
    {
        "protocol": "TCP",
        "name": "remotefs",
        "port": 556
    },
    {
        "protocol": "TCP",
        "name": "openvms-sysipc",
        "port": 557
    },
    {
        "protocol": "TCP",
        "name": "sdnskmp",
        "port": 558
    },
    {
        "protocol": "TCP",
        "name": "teedtap",
        "port": 559
    },
    {
        "protocol": "TCP",
        "name": "rmonitor",
        "port": 560
    },
    {
        "protocol": "TCP",
        "name": "monitor",
        "port": 561
    },
    {
        "protocol": "TCP",
        "name": "chshell",
        "port": 562
    },
    {
        "protocol": "TCP",
        "name": "nntps",
        "port": 563
    },
    {
        "protocol": "TCP",
        "name": "9pfs",
        "port": 564
    },
    {
        "protocol": "TCP",
        "name": "whoami",
        "port": 565
    },
    {
        "protocol": "TCP",
        "name": "streettalk",
        "port": 566
    },
    {
        "protocol": "TCP",
        "name": "banyan-rpc",
        "port": 567
    },
    {
        "protocol": "TCP",
        "name": "ms-shuttle",
        "port": 568
    },
    {
        "protocol": "TCP",
        "name": "ms-rome",
        "port": 569
    },
    {
        "protocol": "TCP",
        "name": "meter",
        "port": 570
    },
    {
        "protocol": "TCP",
        "name": "meter",
        "port": 571
    },
    {
        "protocol": "TCP",
        "name": "sonar",
        "port": 572
    },
    {
        "protocol": "TCP",
        "name": "banyan-vip",
        "port": 573
    },
    {
        "protocol": "TCP",
        "name": "ftp-agent",
        "port": 574
    },
    {
        "protocol": "TCP",
        "name": "vemmi",
        "port": 575
    },
    {
        "protocol": "TCP",
        "name": "ipcd",
        "port": 576
    },
    {
        "protocol": "TCP",
        "name": "vnas",
        "port": 577
    },
    {
        "protocol": "TCP",
        "name": "ipdd",
        "port": 578
    },
    {
        "protocol": "TCP",
        "name": "decbsrv",
        "port": 579
    },
    {
        "protocol": "TCP",
        "name": "sntp-heartbeat",
        "port": 580
    },
    {
        "protocol": "TCP",
        "name": "bdp",
        "port": 581
    },
    {
        "protocol": "TCP",
        "name": "scc-security",
        "port": 582
    },
    {
        "protocol": "TCP",
        "name": "philips-vc",
        "port": 583
    },
    {
        "protocol": "TCP",
        "name": "keyserver",
        "port": 584
    },
    {
        "protocol": "TCP",
        "name": "imap4-ssl",
        "port": 585
    },
    {
        "protocol": "TCP",
        "name": "password-chg",
        "port": 586
    },
    {
        "protocol": "TCP",
        "name": "submission",
        "port": 587
    },
    {
        "protocol": "TCP",
        "name": "cal",
        "port": 588
    },
    {
        "protocol": "TCP",
        "name": "eyelink",
        "port": 589
    },
    {
        "protocol": "TCP",
        "name": "tns-cml",
        "port": 590
    },
    {
        "protocol": "TCP",
        "name": "http-alt",
        "port": 591
    },
    {
        "protocol": "TCP",
        "name": "eudora-set",
        "port": 592
    },
    {
        "protocol": "TCP",
        "name": "http-rpc-epmap",
        "port": 593
    },
    {
        "protocol": "TCP",
        "name": "tpip",
        "port": 594
    },
    {
        "protocol": "TCP",
        "name": "cab-protocol",
        "port": 595
    },
    {
        "protocol": "TCP",
        "name": "smsd",
        "port": 596
    },
    {
        "protocol": "TCP",
        "name": "ptcnameservice",
        "port": 597
    },
    {
        "protocol": "TCP",
        "name": "sco-websrvrmg3",
        "port": 598
    },
    {
        "protocol": "TCP",
        "name": "acp",
        "port": 599
    },
    {
        "protocol": "TCP",
        "name": "ipcserver",
        "port": 600
    },
    {
        "protocol": "TCP",
        "name": "syslog-conn",
        "port": 601
    },
    {
        "protocol": "TCP",
        "name": "xmlrpc-beep",
        "port": 602
    },
    {
        "protocol": "TCP",
        "name": "idxp",
        "port": 603
    },
    {
        "protocol": "TCP",
        "name": "tunnel",
        "port": 604
    },
    {
        "protocol": "TCP",
        "name": "soap-beep",
        "port": 605
    },
    {
        "protocol": "TCP",
        "name": "urm",
        "port": 606
    },
    {
        "protocol": "TCP",
        "name": "nqs",
        "port": 607
    },
    {
        "protocol": "TCP",
        "name": "sift-uft",
        "port": 608
    },
    {
        "protocol": "TCP",
        "name": "npmp-trap",
        "port": 609
    },
    {
        "protocol": "TCP",
        "name": "npmp-local",
        "port": 610
    },
    {
        "protocol": "TCP",
        "name": "npmp-gui",
        "port": 611
    },
    {
        "protocol": "TCP",
        "name": "hmmp-ind",
        "port": 612
    },
    {
        "protocol": "TCP",
        "name": "hmmp-op",
        "port": 613
    },
    {
        "protocol": "TCP",
        "name": "sshell",
        "port": 614
    },
    {
        "protocol": "TCP",
        "name": "sco-inetmgr",
        "port": 615
    },
    {
        "protocol": "TCP",
        "name": "sco-sysmgr",
        "port": 616
    },
    {
        "protocol": "TCP",
        "name": "sco-dtmgr",
        "port": 617
    },
    {
        "protocol": "TCP",
        "name": "dei-icda",
        "port": 618
    },
    {
        "protocol": "TCP",
        "name": "compaq-evm",
        "port": 619
    },
    {
        "protocol": "TCP",
        "name": "sco-websrvrmgr",
        "port": 620
    },
    {
        "protocol": "TCP",
        "name": "escp-ip",
        "port": 621
    },
    {
        "protocol": "TCP",
        "name": "collaborator",
        "port": 622
    },
    {
        "protocol": "TCP",
        "name": "asf-rmcp",
        "port": 623
    },
    {
        "protocol": "TCP",
        "name": "cryptoadmin",
        "port": 624
    },
    {
        "protocol": "TCP",
        "name": "dec_dlm",
        "port": 625
    },
    {
        "protocol": "TCP",
        "name": "asia",
        "port": 626
    },
    {
        "protocol": "TCP",
        "name": "passgo-tivoli",
        "port": 627
    },
    {
        "protocol": "TCP",
        "name": "qmqp",
        "port": 628
    },
    {
        "protocol": "TCP",
        "name": "3com-amp3",
        "port": 629
    },
    {
        "protocol": "TCP",
        "name": "rda",
        "port": 630
    },
    {
        "protocol": "TCP",
        "name": "ipp",
        "port": 631
    },
    {
        "protocol": "TCP",
        "name": "bmpp",
        "port": 632
    },
    {
        "protocol": "TCP",
        "name": "servstat",
        "port": 633
    },
    {
        "protocol": "TCP",
        "name": "ginad",
        "port": 634
    },
    {
        "protocol": "TCP",
        "name": "rlzdbase",
        "port": 635
    },
    {
        "protocol": "TCP",
        "name": "ldaps",
        "port": 636
    },
    {
        "protocol": "TCP",
        "name": "lanserver",
        "port": 637
    },
    {
        "protocol": "TCP",
        "name": "mcns-sec",
        "port": 638
    },
    {
        "protocol": "TCP",
        "name": "msdp",
        "port": 639
    },
    {
        "protocol": "TCP",
        "name": "entrust-sps",
        "port": 640
    },
    {
        "protocol": "TCP",
        "name": "repcmd",
        "port": 641
    },
    {
        "protocol": "TCP",
        "name": "esro-emsdp",
        "port": 642
    },
    {
        "protocol": "TCP",
        "name": "sanity",
        "port": 643
    },
    {
        "protocol": "TCP",
        "name": "dwr",
        "port": 644
    },
    {
        "protocol": "TCP",
        "name": "pssc",
        "port": 645
    },
    {
        "protocol": "TCP",
        "name": "ldp",
        "port": 646
    },
    {
        "protocol": "TCP",
        "name": "dhcp-failover",
        "port": 647
    },
    {
        "protocol": "TCP",
        "name": "rrp",
        "port": 648
    },
    {
        "protocol": "TCP",
        "name": "cadview-3d",
        "port": 649
    },
    {
        "protocol": "TCP",
        "name": "obex",
        "port": 650
    },
    {
        "protocol": "TCP",
        "name": "ieee-mms",
        "port": 651
    },
    {
        "protocol": "TCP",
        "name": "hello-port",
        "port": 652
    },
    {
        "protocol": "TCP",
        "name": "repscmd",
        "port": 653
    },
    {
        "protocol": "TCP",
        "name": "aodv",
        "port": 654
    },
    {
        "protocol": "TCP",
        "name": "tinc",
        "port": 655
    },
    {
        "protocol": "TCP",
        "name": "spmp",
        "port": 656
    },
    {
        "protocol": "TCP",
        "name": "rmc",
        "port": 657
    },
    {
        "protocol": "TCP",
        "name": "tenfold",
        "port": 658
    },
    {
        "protocol": "TCP",
        "name": "mac-srvr-admin",
        "port": 660
    },
    {
        "protocol": "TCP",
        "name": "hap",
        "port": 661
    },
    {
        "protocol": "TCP",
        "name": "pftp",
        "port": 662
    },
    {
        "protocol": "TCP",
        "name": "purenoise",
        "port": 663
    },
    {
        "protocol": "TCP",
        "name": "asf-secure-rmcp",
        "port": 664
    },
    {
        "protocol": "TCP",
        "name": "sun-dr",
        "port": 665
    },
    {
        "protocol": "TCP",
        "name": "doom",
        "port": 666
    },
    {
        "protocol": "TCP",
        "name": "disclose",
        "port": 667
    },
    {
        "protocol": "TCP",
        "name": "mecomm",
        "port": 668
    },
    {
        "protocol": "TCP",
        "name": "meregister",
        "port": 669
    },
    {
        "protocol": "TCP",
        "name": "vacdsm-sws",
        "port": 670
    },
    {
        "protocol": "TCP",
        "name": "vacdsm-app",
        "port": 671
    },
    {
        "protocol": "TCP",
        "name": "vpps-qua",
        "port": 672
    },
    {
        "protocol": "TCP",
        "name": "cimplex",
        "port": 673
    },
    {
        "protocol": "TCP",
        "name": "acap",
        "port": 674
    },
    {
        "protocol": "TCP",
        "name": "dctp",
        "port": 675
    },
    {
        "protocol": "TCP",
        "name": "vpps-via",
        "port": 676
    },
    {
        "protocol": "TCP",
        "name": "vpp",
        "port": 677
    },
    {
        "protocol": "TCP",
        "name": "ggf-ncp",
        "port": 678
    },
    {
        "protocol": "TCP",
        "name": "mrm",
        "port": 679
    },
    {
        "protocol": "TCP",
        "name": "entrust-aaas",
        "port": 680
    },
    {
        "protocol": "TCP",
        "name": "entrust-aams",
        "port": 681
    },
    {
        "protocol": "TCP",
        "name": "xfr",
        "port": 682
    },
    {
        "protocol": "TCP",
        "name": "corba-iiop",
        "port": 683
    },
    {
        "protocol": "TCP",
        "name": "corba-iiop-ssl",
        "port": 684
    },
    {
        "protocol": "TCP",
        "name": "mdc-portmapper",
        "port": 685
    },
    {
        "protocol": "TCP",
        "name": "hcp-wismar",
        "port": 686
    },
    {
        "protocol": "TCP",
        "name": "asipregistry",
        "port": 687
    },
    {
        "protocol": "TCP",
        "name": "realm-rusd",
        "port": 688
    },
    {
        "protocol": "TCP",
        "name": "nmap",
        "port": 689
    },
    {
        "protocol": "TCP",
        "name": "vatp",
        "port": 690
    },
    {
        "protocol": "TCP",
        "name": "msexch-routing",
        "port": 691
    },
    {
        "protocol": "TCP",
        "name": "hyperwave-isp",
        "port": 692
    },
    {
        "protocol": "TCP",
        "name": "connendp",
        "port": 693
    },
    {
        "protocol": "TCP",
        "name": "ha-cluster",
        "port": 694
    },
    {
        "protocol": "TCP",
        "name": "ieee-mms-ssl",
        "port": 695
    },
    {
        "protocol": "TCP",
        "name": "rushd",
        "port": 696
    },
    {
        "protocol": "TCP",
        "name": "uuidgen",
        "port": 697
    },
    {
        "protocol": "TCP",
        "name": "olsr",
        "port": 698
    },
    {
        "protocol": "TCP",
        "name": "accessnetwork",
        "port": 699
    },
    {
        "protocol": "TCP",
        "name": "epp",
        "port": 700
    },
    {
        "protocol": "TCP",
        "name": "lmp",
        "port": 701
    },
    {
        "protocol": "TCP",
        "name": "iris-beep",
        "port": 702
    },
    {
        "protocol": "TCP",
        "name": "elcsd",
        "port": 704
    },
    {
        "protocol": "TCP",
        "name": "agentx",
        "port": 705
    },
    {
        "protocol": "TCP",
        "name": "silc",
        "port": 706
    },
    {
        "protocol": "TCP",
        "name": "borland-dsj",
        "port": 707
    },
    {
        "protocol": "TCP",
        "name": "entrust-kmsh",
        "port": 709
    },
    {
        "protocol": "TCP",
        "name": "entrust-ash",
        "port": 710
    },
    {
        "protocol": "TCP",
        "name": "cisco-tdp",
        "port": 711
    },
    {
        "protocol": "TCP",
        "name": "tbrpf",
        "port": 712
    },
    {
        "protocol": "TCP",
        "name": "netviewdm1",
        "port": 729
    },
    {
        "protocol": "TCP",
        "name": "netviewdm2",
        "port": 730
    },
    {
        "protocol": "TCP",
        "name": "netviewdm3",
        "port": 731
    },
    {
        "protocol": "TCP",
        "name": "netgw",
        "port": 741
    },
    {
        "protocol": "TCP",
        "name": "netrcs",
        "port": 742
    },
    {
        "protocol": "TCP",
        "name": "flexlm",
        "port": 744
    },
    {
        "protocol": "TCP",
        "name": "fujitsu-dev",
        "port": 747
    },
    {
        "protocol": "TCP",
        "name": "ris-cm",
        "port": 748
    },
    {
        "protocol": "TCP",
        "name": "kerberos-adm",
        "port": 749
    },
    {
        "protocol": "TCP",
        "name": "rfile",
        "port": 750
    },
    {
        "protocol": "TCP",
        "name": "pump",
        "port": 751
    },
    {
        "protocol": "TCP",
        "name": "qrh",
        "port": 752
    },
    {
        "protocol": "TCP",
        "name": "rrh",
        "port": 753
    },
    {
        "protocol": "TCP",
        "name": "tell",
        "port": 754
    },
    {
        "protocol": "TCP",
        "name": "nlogin",
        "port": 758
    },
    {
        "protocol": "TCP",
        "name": "con",
        "port": 759
    },
    {
        "protocol": "TCP",
        "name": "ns",
        "port": 760
    },
    {
        "protocol": "TCP",
        "name": "rxe",
        "port": 761
    },
    {
        "protocol": "TCP",
        "name": "quotad",
        "port": 762
    },
    {
        "protocol": "TCP",
        "name": "cycleserv",
        "port": 763
    },
    {
        "protocol": "TCP",
        "name": "omserv",
        "port": 764
    },
    {
        "protocol": "TCP",
        "name": "webster",
        "port": 765
    },
    {
        "protocol": "TCP",
        "name": "phonebook",
        "port": 767
    },
    {
        "protocol": "TCP",
        "name": "vid",
        "port": 769
    },
    {
        "protocol": "TCP",
        "name": "cadlock",
        "port": 770
    },
    {
        "protocol": "TCP",
        "name": "rtip",
        "port": 771
    },
    {
        "protocol": "TCP",
        "name": "cycleserv2",
        "port": 772
    },
    {
        "protocol": "TCP",
        "name": "submit",
        "port": 773
    },
    {
        "protocol": "TCP",
        "name": "rpasswd",
        "port": 774
    },
    {
        "protocol": "TCP",
        "name": "entomb",
        "port": 775
    },
    {
        "protocol": "TCP",
        "name": "wpages",
        "port": 776
    },
    {
        "protocol": "TCP",
        "name": "multiling-http",
        "port": 777
    },
    {
        "protocol": "TCP",
        "name": "wpgs",
        "port": 780
    },
    {
        "protocol": "TCP",
        "name": "mdbs_daemon",
        "port": 800
    },
    {
        "protocol": "TCP",
        "name": "device",
        "port": 801
    },
    {
        "protocol": "TCP",
        "name": "fcp-udp",
        "port": 810
    },
    {
        "protocol": "TCP",
        "name": "itm-mcell-s",
        "port": 828
    },
    {
        "protocol": "TCP",
        "name": "pkix-3-ca-ra",
        "port": 829
    },
    {
        "protocol": "TCP",
        "name": "dhcp-failover2",
        "port": 847
    },
    {
        "protocol": "TCP",
        "name": "gdoi",
        "port": 848
    },
    {
        "protocol": "TCP",
        "name": "iscsi",
        "port": 860
    },
    {
        "protocol": "TCP",
        "name": "rsync",
        "port": 873
    },
    {
        "protocol": "TCP",
        "name": "iclcnet-locate",
        "port": 886
    },
    {
        "protocol": "TCP",
        "name": "iclcnet_svinfo",
        "port": 887
    },
    {
        "protocol": "TCP",
        "name": "accessbuilder",
        "port": 888
    },
    {
        "protocol": "TCP",
        "name": "omginitialrefs",
        "port": 900
    },
    {
        "protocol": "TCP",
        "name": "smpnameres",
        "port": 901
    },
    {
        "protocol": "TCP",
        "name": "ideafarm-chat",
        "port": 902
    },
    {
        "protocol": "TCP",
        "name": "ideafarm-catch",
        "port": 903
    },
    {
        "protocol": "TCP",
        "name": "kink",
        "port": 910
    },
    {
        "protocol": "TCP",
        "name": "xact-backup",
        "port": 911
    },
    {
        "protocol": "TCP",
        "name": "apex-mesh",
        "port": 912
    },
    {
        "protocol": "TCP",
        "name": "apex-edge",
        "port": 913
    },
    {
        "protocol": "TCP",
        "name": "ftps-data",
        "port": 989
    },
    {
        "protocol": "TCP",
        "name": "ftps",
        "port": 990
    },
    {
        "protocol": "TCP",
        "name": "nas",
        "port": 991
    },
    {
        "protocol": "TCP",
        "name": "telnets",
        "port": 992
    },
    {
        "protocol": "TCP",
        "name": "imaps",
        "port": 993
    },
    {
        "protocol": "TCP",
        "name": "ircs",
        "port": 994
    },
    {
        "protocol": "TCP",
        "name": "pop3s",
        "port": 995
    },
    {
        "protocol": "TCP",
        "name": "vsinet",
        "port": 996
    },
    {
        "protocol": "TCP",
        "name": "maitrd",
        "port": 997
    },
    {
        "protocol": "TCP",
        "name": "busboy",
        "port": 998
    },
    {
        "protocol": "TCP",
        "name": "puprouter",
        "port": 999
    },
    {
        "protocol": "TCP",
        "name": "cadlock2",
        "port": 1000
    },
    {
        "protocol": "TCP",
        "name": "surf",
        "port": 1010
    },
    {
        "protocol": "TCP",
        "name": "blackjack",
        "port": 1025
    },
    {
        "protocol": "TCP",
        "name": "cap",
        "port": 1026
    },
    {
        "protocol": "TCP",
        "name": "solid-mux",
        "port": 1029
    },
    {
        "protocol": "TCP",
        "name": "iad1",
        "port": 1030
    },
    {
        "protocol": "TCP",
        "name": "iad2",
        "port": 1031
    },
    {
        "protocol": "TCP",
        "name": "iad3",
        "port": 1032
    },
    {
        "protocol": "TCP",
        "name": "netinfo-local",
        "port": 1033
    },
    {
        "protocol": "TCP",
        "name": "activesync",
        "port": 1034
    },
    {
        "protocol": "TCP",
        "name": "mxxrlogin",
        "port": 1035
    },
    {
        "protocol": "TCP",
        "name": "nsstp",
        "port": 1036
    },
    {
        "protocol": "TCP",
        "name": "ams",
        "port": 1037
    },
    {
        "protocol": "TCP",
        "name": "mtqp",
        "port": 1038
    },
    {
        "protocol": "TCP",
        "name": "sbl",
        "port": 1039
    },
    {
        "protocol": "TCP",
        "name": "netarx",
        "port": 1040
    },
    {
        "protocol": "TCP",
        "name": "danf-ak2",
        "port": 1041
    },
    {
        "protocol": "TCP",
        "name": "afrog",
        "port": 1042
    },
    {
        "protocol": "TCP",
        "name": "boinc-client",
        "port": 1043
    },
    {
        "protocol": "TCP",
        "name": "dcutility",
        "port": 1044
    },
    {
        "protocol": "TCP",
        "name": "fpitp",
        "port": 1045
    },
    {
        "protocol": "TCP",
        "name": "wfremotertm",
        "port": 1046
    },
    {
        "protocol": "TCP",
        "name": "neod1",
        "port": 1047
    },
    {
        "protocol": "TCP",
        "name": "neod2",
        "port": 1048
    },
    {
        "protocol": "TCP",
        "name": "td-postman",
        "port": 1049
    },
    {
        "protocol": "TCP",
        "name": "cma",
        "port": 1050
    },
    {
        "protocol": "TCP",
        "name": "optima-vnet",
        "port": 1051
    },
    {
        "protocol": "TCP",
        "name": "ddt",
        "port": 1052
    },
    {
        "protocol": "TCP",
        "name": "remote-as",
        "port": 1053
    },
    {
        "protocol": "TCP",
        "name": "brvread",
        "port": 1054
    },
    {
        "protocol": "TCP",
        "name": "ansyslmd",
        "port": 1055
    },
    {
        "protocol": "TCP",
        "name": "vfo",
        "port": 1056
    },
    {
        "protocol": "TCP",
        "name": "startron",
        "port": 1057
    },
    {
        "protocol": "TCP",
        "name": "nim",
        "port": 1058
    },
    {
        "protocol": "TCP",
        "name": "nimreg",
        "port": 1059
    },
    {
        "protocol": "TCP",
        "name": "polestar",
        "port": 1060
    },
    {
        "protocol": "TCP",
        "name": "kiosk",
        "port": 1061
    },
    {
        "protocol": "TCP",
        "name": "veracity",
        "port": 1062
    },
    {
        "protocol": "TCP",
        "name": "kyoceranetdev",
        "port": 1063
    },
    {
        "protocol": "TCP",
        "name": "jstel",
        "port": 1064
    },
    {
        "protocol": "TCP",
        "name": "syscomlan",
        "port": 1065
    },
    {
        "protocol": "TCP",
        "name": "fpo-fns",
        "port": 1066
    },
    {
        "protocol": "TCP",
        "name": "instl_boots",
        "port": 1067
    },
    {
        "protocol": "TCP",
        "name": "instl_bootc",
        "port": 1068
    },
    {
        "protocol": "TCP",
        "name": "cognex-insight",
        "port": 1069
    },
    {
        "protocol": "TCP",
        "name": "gmrupdateserv",
        "port": 1070
    },
    {
        "protocol": "TCP",
        "name": "bsquare-voip",
        "port": 1071
    },
    {
        "protocol": "TCP",
        "name": "cardax",
        "port": 1072
    },
    {
        "protocol": "TCP",
        "name": "bridgecontrol",
        "port": 1073
    },
    {
        "protocol": "TCP",
        "name": "fastechnologlm",
        "port": 1074
    },
    {
        "protocol": "TCP",
        "name": "rdrmshc",
        "port": 1075
    },
    {
        "protocol": "TCP",
        "name": "dab-sti-c",
        "port": 1076
    },
    {
        "protocol": "TCP",
        "name": "imgames",
        "port": 1077
    },
    {
        "protocol": "TCP",
        "name": "avocent-proxy",
        "port": 1078
    },
    {
        "protocol": "TCP",
        "name": "asprovatalk",
        "port": 1079
    },
    {
        "protocol": "TCP",
        "name": "socks",
        "port": 1080
    },
    {
        "protocol": "TCP",
        "name": "pvuniwien",
        "port": 1081
    },
    {
        "protocol": "TCP",
        "name": "amt-esd-prot",
        "port": 1082
    },
    {
        "protocol": "TCP",
        "name": "ansoft-lm-1",
        "port": 1083
    },
    {
        "protocol": "TCP",
        "name": "ansoft-lm-2",
        "port": 1084
    },
    {
        "protocol": "TCP",
        "name": "webobjects",
        "port": 1085
    },
    {
        "protocol": "TCP",
        "name": "cplscrambler-lg",
        "port": 1086
    },
    {
        "protocol": "TCP",
        "name": "cplscrambler-in",
        "port": 1087
    },
    {
        "protocol": "TCP",
        "name": "cplscrambler-al",
        "port": 1088
    },
    {
        "protocol": "TCP",
        "name": "ff-annunc",
        "port": 1089
    },
    {
        "protocol": "TCP",
        "name": "ff-fms",
        "port": 1090
    },
    {
        "protocol": "TCP",
        "name": "ff-sm",
        "port": 1091
    },
    {
        "protocol": "TCP",
        "name": "obrpd",
        "port": 1092
    },
    {
        "protocol": "TCP",
        "name": "proofd",
        "port": 1093
    },
    {
        "protocol": "TCP",
        "name": "rootd",
        "port": 1094
    },
    {
        "protocol": "TCP",
        "name": "nicelink",
        "port": 1095
    },
    {
        "protocol": "TCP",
        "name": "cnrprotocol",
        "port": 1096
    },
    {
        "protocol": "TCP",
        "name": "sunclustermgr",
        "port": 1097
    },
    {
        "protocol": "TCP",
        "name": "rmiactivation",
        "port": 1098
    },
    {
        "protocol": "TCP",
        "name": "rmiregistry",
        "port": 1099
    },
    {
        "protocol": "TCP",
        "name": "mctp",
        "port": 1100
    },
    {
        "protocol": "TCP",
        "name": "pt2-discover",
        "port": 1101
    },
    {
        "protocol": "TCP",
        "name": "adobeserver-1",
        "port": 1102
    },
    {
        "protocol": "TCP",
        "name": "adobeserver-2",
        "port": 1103
    },
    {
        "protocol": "TCP",
        "name": "xrl",
        "port": 1104
    },
    {
        "protocol": "TCP",
        "name": "ftranhc",
        "port": 1105
    },
    {
        "protocol": "TCP",
        "name": "isoipsigport-1",
        "port": 1106
    },
    {
        "protocol": "TCP",
        "name": "isoipsigport-2",
        "port": 1107
    },
    {
        "protocol": "TCP",
        "name": "ratio-adp",
        "port": 1108
    },
    {
        "protocol": "TCP",
        "name": "nfsd-status",
        "port": 1110
    },
    {
        "protocol": "TCP",
        "name": "lmsocialserver",
        "port": 1111
    },
    {
        "protocol": "TCP",
        "name": "icp",
        "port": 1112
    },
    {
        "protocol": "TCP",
        "name": "ltp-deepspace",
        "port": 1113
    },
    {
        "protocol": "TCP",
        "name": "mini-sql",
        "port": 1114
    },
    {
        "protocol": "TCP",
        "name": "ardus-trns",
        "port": 1115
    },
    {
        "protocol": "TCP",
        "name": "ardus-cntl",
        "port": 1116
    },
    {
        "protocol": "TCP",
        "name": "ardus-mtrns",
        "port": 1117
    },
    {
        "protocol": "TCP",
        "name": "sacred",
        "port": 1118
    },
    {
        "protocol": "TCP",
        "name": "bnetgame",
        "port": 1119
    },
    {
        "protocol": "TCP",
        "name": "bnetfile",
        "port": 1120
    },
    {
        "protocol": "TCP",
        "name": "rmpp",
        "port": 1121
    },
    {
        "protocol": "TCP",
        "name": "availant-mgr",
        "port": 1122
    },
    {
        "protocol": "TCP",
        "name": "murray",
        "port": 1123
    },
    {
        "protocol": "TCP",
        "name": "hpvmmcontrol",
        "port": 1124
    },
    {
        "protocol": "TCP",
        "name": "hpvmmagent",
        "port": 1125
    },
    {
        "protocol": "TCP",
        "name": "hpvmmdata",
        "port": 1126
    },
    {
        "protocol": "TCP",
        "name": "kwdb-commn",
        "port": 1127
    },
    {
        "protocol": "TCP",
        "name": "casp",
        "port": 1130
    },
    {
        "protocol": "TCP",
        "name": "caspssl",
        "port": 1131
    },
    {
        "protocol": "TCP",
        "name": "kvm-via-ip",
        "port": 1132
    },
    {
        "protocol": "TCP",
        "name": "dfn",
        "port": 1133
    },
    {
        "protocol": "TCP",
        "name": "aplx",
        "port": 1134
    },
    {
        "protocol": "TCP",
        "name": "omnivision",
        "port": 1135
    },
    {
        "protocol": "TCP",
        "name": "hhb-gateway",
        "port": 1136
    },
    {
        "protocol": "TCP",
        "name": "trim",
        "port": 1137
    },
    {
        "protocol": "TCP",
        "name": "autonoc",
        "port": 1140
    },
    {
        "protocol": "TCP",
        "name": "mxomss",
        "port": 1141
    },
    {
        "protocol": "TCP",
        "name": "edtools",
        "port": 1142
    },
    {
        "protocol": "TCP",
        "name": "fuscript",
        "port": 1144
    },
    {
        "protocol": "TCP",
        "name": "x9-icue",
        "port": 1145
    },
    {
        "protocol": "TCP",
        "name": "audit-transfer",
        "port": 1146
    },
    {
        "protocol": "TCP",
        "name": "capioverlan",
        "port": 1147
    },
    {
        "protocol": "TCP",
        "name": "elfiq-repl",
        "port": 1148
    },
    {
        "protocol": "TCP",
        "name": "bvtsonar",
        "port": 1149
    },
    {
        "protocol": "TCP",
        "name": "blaze",
        "port": 1150
    },
    {
        "protocol": "TCP",
        "name": "unizensus",
        "port": 1151
    },
    {
        "protocol": "TCP",
        "name": "winpoplanmess",
        "port": 1152
    },
    {
        "protocol": "TCP",
        "name": "c1222-acse",
        "port": 1153
    },
    {
        "protocol": "TCP",
        "name": "resacommunity",
        "port": 1154
    },
    {
        "protocol": "TCP",
        "name": "nfa",
        "port": 1155
    },
    {
        "protocol": "TCP",
        "name": "iascontrol-oms",
        "port": 1156
    },
    {
        "protocol": "TCP",
        "name": "iascontrol",
        "port": 1157
    },
    {
        "protocol": "TCP",
        "name": "dbcontrol-oms",
        "port": 1158
    },
    {
        "protocol": "TCP",
        "name": "oracle-oms",
        "port": 1159
    },
    {
        "protocol": "TCP",
        "name": "olsv",
        "port": 1160
    },
    {
        "protocol": "TCP",
        "name": "health-polling",
        "port": 1161
    },
    {
        "protocol": "TCP",
        "name": "health-trap",
        "port": 1162
    },
    {
        "protocol": "TCP",
        "name": "sddp",
        "port": 1163
    },
    {
        "protocol": "TCP",
        "name": "qsm-proxy",
        "port": 1164
    },
    {
        "protocol": "TCP",
        "name": "qsm-gui",
        "port": 1165
    },
    {
        "protocol": "TCP",
        "name": "qsm-remote",
        "port": 1166
    },
    {
        "protocol": "TCP",
        "name": "vchat",
        "port": 1168
    },
    {
        "protocol": "TCP",
        "name": "tripwire",
        "port": 1169
    },
    {
        "protocol": "TCP",
        "name": "atc-lm",
        "port": 1170
    },
    {
        "protocol": "TCP",
        "name": "atc-appserver",
        "port": 1171
    },
    {
        "protocol": "TCP",
        "name": "dnap",
        "port": 1172
    },
    {
        "protocol": "TCP",
        "name": "d-cinema-rrp",
        "port": 1173
    },
    {
        "protocol": "TCP",
        "name": "fnet-remote-ui",
        "port": 1174
    },
    {
        "protocol": "TCP",
        "name": "dossier",
        "port": 1175
    },
    {
        "protocol": "TCP",
        "name": "indigo-server",
        "port": 1176
    },
    {
        "protocol": "TCP",
        "name": "dkmessenger",
        "port": 1177
    },
    {
        "protocol": "TCP",
        "name": "sgi-storman",
        "port": 1178
    },
    {
        "protocol": "TCP",
        "name": "b2n",
        "port": 1179
    },
    {
        "protocol": "TCP",
        "name": "mc-client",
        "port": 1180
    },
    {
        "protocol": "TCP",
        "name": "3comnetman",
        "port": 1181
    },
    {
        "protocol": "TCP",
        "name": "accelenet",
        "port": 1182
    },
    {
        "protocol": "TCP",
        "name": "llsurfup-http",
        "port": 1183
    },
    {
        "protocol": "TCP",
        "name": "llsurfup-https",
        "port": 1184
    },
    {
        "protocol": "TCP",
        "name": "catchpole",
        "port": 1185
    },
    {
        "protocol": "TCP",
        "name": "mysql-cluster",
        "port": 1186
    },
    {
        "protocol": "TCP",
        "name": "alias",
        "port": 1187
    },
    {
        "protocol": "TCP",
        "name": "hp-webadmin",
        "port": 1188
    },
    {
        "protocol": "TCP",
        "name": "unet",
        "port": 1189
    },
    {
        "protocol": "TCP",
        "name": "commlinx-avl",
        "port": 1190
    },
    {
        "protocol": "TCP",
        "name": "gpfs",
        "port": 1191
    },
    {
        "protocol": "TCP",
        "name": "caids-sensor",
        "port": 1192
    },
    {
        "protocol": "TCP",
        "name": "fiveacross",
        "port": 1193
    },
    {
        "protocol": "TCP",
        "name": "openvpn",
        "port": 1194
    },
    {
        "protocol": "TCP",
        "name": "rsf-1",
        "port": 1195
    },
    {
        "protocol": "TCP",
        "name": "netmagic",
        "port": 1196
    },
    {
        "protocol": "TCP",
        "name": "carrius-rshell",
        "port": 1197
    },
    {
        "protocol": "TCP",
        "name": "cajo-discovery",
        "port": 1198
    },
    {
        "protocol": "TCP",
        "name": "dmidi",
        "port": 1199
    },
    {
        "protocol": "TCP",
        "name": "scol",
        "port": 1200
    },
    {
        "protocol": "TCP",
        "name": "nucleus-sand",
        "port": 1201
    },
    {
        "protocol": "TCP",
        "name": "caiccipc",
        "port": 1202
    },
    {
        "protocol": "TCP",
        "name": "ssslic-mgr",
        "port": 1203
    },
    {
        "protocol": "TCP",
        "name": "ssslog-mgr",
        "port": 1204
    },
    {
        "protocol": "TCP",
        "name": "accord-mgc",
        "port": 1205
    },
    {
        "protocol": "TCP",
        "name": "anthony-data",
        "port": 1206
    },
    {
        "protocol": "TCP",
        "name": "metasage",
        "port": 1207
    },
    {
        "protocol": "TCP",
        "name": "seagull-ais",
        "port": 1208
    },
    {
        "protocol": "TCP",
        "name": "ipcd3",
        "port": 1209
    },
    {
        "protocol": "TCP",
        "name": "eoss",
        "port": 1210
    },
    {
        "protocol": "TCP",
        "name": "groove-dpp",
        "port": 1211
    },
    {
        "protocol": "TCP",
        "name": "lupa",
        "port": 1212
    },
    {
        "protocol": "TCP",
        "name": "mpc-lifenet",
        "port": 1213
    },
    {
        "protocol": "TCP",
        "name": "kazaa",
        "port": 1214
    },
    {
        "protocol": "TCP",
        "name": "scanstat-1",
        "port": 1215
    },
    {
        "protocol": "TCP",
        "name": "etebac5",
        "port": 1216
    },
    {
        "protocol": "TCP",
        "name": "hpss-ndapi",
        "port": 1217
    },
    {
        "protocol": "TCP",
        "name": "aeroflight-ads",
        "port": 1218
    },
    {
        "protocol": "TCP",
        "name": "aeroflight-ret",
        "port": 1219
    },
    {
        "protocol": "TCP",
        "name": "qt-serveradmin",
        "port": 1220
    },
    {
        "protocol": "TCP",
        "name": "sweetware-apps",
        "port": 1221
    },
    {
        "protocol": "TCP",
        "name": "nerv",
        "port": 1222
    },
    {
        "protocol": "TCP",
        "name": "tgp",
        "port": 1223
    },
    {
        "protocol": "TCP",
        "name": "vpnz",
        "port": 1224
    },
    {
        "protocol": "TCP",
        "name": "slinkysearch",
        "port": 1225
    },
    {
        "protocol": "TCP",
        "name": "stgxfws",
        "port": 1226
    },
    {
        "protocol": "TCP",
        "name": "dns2go",
        "port": 1227
    },
    {
        "protocol": "TCP",
        "name": "florence",
        "port": 1228
    },
    {
        "protocol": "TCP",
        "name": "novell-zfs",
        "port": 1229
    },
    {
        "protocol": "TCP",
        "name": "periscope",
        "port": 1230
    },
    {
        "protocol": "TCP",
        "name": "menandmice-lpm",
        "port": 1231
    },
    {
        "protocol": "TCP",
        "name": "univ-appserver",
        "port": 1233
    },
    {
        "protocol": "TCP",
        "name": "search-agent",
        "port": 1234
    },
    {
        "protocol": "TCP",
        "name": "mosaicsyssvc1",
        "port": 1235
    },
    {
        "protocol": "TCP",
        "name": "bvcontrol",
        "port": 1236
    },
    {
        "protocol": "TCP",
        "name": "tsdos390",
        "port": 1237
    },
    {
        "protocol": "TCP",
        "name": "hacl-qs",
        "port": 1238
    },
    {
        "protocol": "TCP",
        "name": "nmsd",
        "port": 1239
    },
    {
        "protocol": "TCP",
        "name": "instantia",
        "port": 1240
    },
    {
        "protocol": "TCP",
        "name": "nessus",
        "port": 1241
    },
    {
        "protocol": "TCP",
        "name": "nmasoverip",
        "port": 1242
    },
    {
        "protocol": "TCP",
        "name": "serialgateway",
        "port": 1243
    },
    {
        "protocol": "TCP",
        "name": "isbconference1",
        "port": 1244
    },
    {
        "protocol": "TCP",
        "name": "isbconference2",
        "port": 1245
    },
    {
        "protocol": "TCP",
        "name": "payrouter",
        "port": 1246
    },
    {
        "protocol": "TCP",
        "name": "visionpyramid",
        "port": 1247
    },
    {
        "protocol": "TCP",
        "name": "hermes",
        "port": 1248
    },
    {
        "protocol": "TCP",
        "name": "mesavistaco",
        "port": 1249
    },
    {
        "protocol": "TCP",
        "name": "swldy-sias",
        "port": 1250
    },
    {
        "protocol": "TCP",
        "name": "servergraph",
        "port": 1251
    },
    {
        "protocol": "TCP",
        "name": "bspne-pcc",
        "port": 1252
    },
    {
        "protocol": "TCP",
        "name": "q55-pcc",
        "port": 1253
    },
    {
        "protocol": "TCP",
        "name": "de-noc",
        "port": 1254
    },
    {
        "protocol": "TCP",
        "name": "de-cache-query",
        "port": 1255
    },
    {
        "protocol": "TCP",
        "name": "de-server",
        "port": 1256
    },
    {
        "protocol": "TCP",
        "name": "shockwave2",
        "port": 1257
    },
    {
        "protocol": "TCP",
        "name": "opennl",
        "port": 1258
    },
    {
        "protocol": "TCP",
        "name": "opennl-voice",
        "port": 1259
    },
    {
        "protocol": "TCP",
        "name": "ibm-ssd",
        "port": 1260
    },
    {
        "protocol": "TCP",
        "name": "mpshrsv",
        "port": 1261
    },
    {
        "protocol": "TCP",
        "name": "qnts-orb",
        "port": 1262
    },
    {
        "protocol": "TCP",
        "name": "dka",
        "port": 1263
    },
    {
        "protocol": "TCP",
        "name": "prat",
        "port": 1264
    },
    {
        "protocol": "TCP",
        "name": "dssiapi",
        "port": 1265
    },
    {
        "protocol": "TCP",
        "name": "dellpwrappks",
        "port": 1266
    },
    {
        "protocol": "TCP",
        "name": "epc",
        "port": 1267
    },
    {
        "protocol": "TCP",
        "name": "propel-msgsys",
        "port": 1268
    },
    {
        "protocol": "TCP",
        "name": "watilapp",
        "port": 1269
    },
    {
        "protocol": "TCP",
        "name": "opsmgr",
        "port": 1270
    },
    {
        "protocol": "TCP",
        "name": "excw",
        "port": 1271
    },
    {
        "protocol": "TCP",
        "name": "cspmlockmgr",
        "port": 1272
    },
    {
        "protocol": "TCP",
        "name": "emc-gateway",
        "port": 1273
    },
    {
        "protocol": "TCP",
        "name": "t1distproc",
        "port": 1274
    },
    {
        "protocol": "TCP",
        "name": "ivcollector",
        "port": 1275
    },
    {
        "protocol": "TCP",
        "name": "ivmanager",
        "port": 1276
    },
    {
        "protocol": "TCP",
        "name": "miva-mqs",
        "port": 1277
    },
    {
        "protocol": "TCP",
        "name": "dellwebadmin-1",
        "port": 1278
    },
    {
        "protocol": "TCP",
        "name": "dellwebadmin-2",
        "port": 1279
    },
    {
        "protocol": "TCP",
        "name": "pictrography",
        "port": 1280
    },
    {
        "protocol": "TCP",
        "name": "healthd",
        "port": 1281
    },
    {
        "protocol": "TCP",
        "name": "emperion",
        "port": 1282
    },
    {
        "protocol": "TCP",
        "name": "productinfo",
        "port": 1283
    },
    {
        "protocol": "TCP",
        "name": "iee-qfx",
        "port": 1284
    },
    {
        "protocol": "TCP",
        "name": "neoiface",
        "port": 1285
    },
    {
        "protocol": "TCP",
        "name": "netuitive",
        "port": 1286
    },
    {
        "protocol": "TCP",
        "name": "routematch",
        "port": 1287
    },
    {
        "protocol": "TCP",
        "name": "navbuddy",
        "port": 1288
    },
    {
        "protocol": "TCP",
        "name": "jwalkserver",
        "port": 1289
    },
    {
        "protocol": "TCP",
        "name": "winjaserver",
        "port": 1290
    },
    {
        "protocol": "TCP",
        "name": "seagulllms",
        "port": 1291
    },
    {
        "protocol": "TCP",
        "name": "dsdn",
        "port": 1292
    },
    {
        "protocol": "TCP",
        "name": "pkt-krb-ipsec",
        "port": 1293
    },
    {
        "protocol": "TCP",
        "name": "cmmdriver",
        "port": 1294
    },
    {
        "protocol": "TCP",
        "name": "ehtp",
        "port": 1295
    },
    {
        "protocol": "TCP",
        "name": "dproxy",
        "port": 1296
    },
    {
        "protocol": "TCP",
        "name": "sdproxy",
        "port": 1297
    },
    {
        "protocol": "TCP",
        "name": "lpcp",
        "port": 1298
    },
    {
        "protocol": "TCP",
        "name": "hp-sci",
        "port": 1299
    },
    {
        "protocol": "TCP",
        "name": "h323hostcallsc",
        "port": 1300
    },
    {
        "protocol": "TCP",
        "name": "ci3-software-1",
        "port": 1301
    },
    {
        "protocol": "TCP",
        "name": "ci3-software-2",
        "port": 1302
    },
    {
        "protocol": "TCP",
        "name": "sftsrv",
        "port": 1303
    },
    {
        "protocol": "TCP",
        "name": "boomerang",
        "port": 1304
    },
    {
        "protocol": "TCP",
        "name": "pe-mike",
        "port": 1305
    },
    {
        "protocol": "TCP",
        "name": "re-conn-proto",
        "port": 1306
    },
    {
        "protocol": "TCP",
        "name": "pacmand",
        "port": 1307
    },
    {
        "protocol": "TCP",
        "name": "odsi",
        "port": 1308
    },
    {
        "protocol": "TCP",
        "name": "jtag-server",
        "port": 1309
    },
    {
        "protocol": "TCP",
        "name": "husky",
        "port": 1310
    },
    {
        "protocol": "TCP",
        "name": "rxmon",
        "port": 1311
    },
    {
        "protocol": "TCP",
        "name": "sti-envision",
        "port": 1312
    },
    {
        "protocol": "TCP",
        "name": "bmc_patroldb",
        "port": 1313
    },
    {
        "protocol": "TCP",
        "name": "pdps",
        "port": 1314
    },
    {
        "protocol": "TCP",
        "name": "els",
        "port": 1315
    },
    {
        "protocol": "TCP",
        "name": "exbit-escp",
        "port": 1316
    },
    {
        "protocol": "TCP",
        "name": "vrts-ipcserver",
        "port": 1317
    },
    {
        "protocol": "TCP",
        "name": "krb5gatekeeper",
        "port": 1318
    },
    {
        "protocol": "TCP",
        "name": "panja-icsp",
        "port": 1319
    },
    {
        "protocol": "TCP",
        "name": "panja-axbnet",
        "port": 1320
    },
    {
        "protocol": "TCP",
        "name": "pip",
        "port": 1321
    },
    {
        "protocol": "TCP",
        "name": "novation",
        "port": 1322
    },
    {
        "protocol": "TCP",
        "name": "brcd",
        "port": 1323
    },
    {
        "protocol": "TCP",
        "name": "delta-mcp",
        "port": 1324
    },
    {
        "protocol": "TCP",
        "name": "dx-instrument",
        "port": 1325
    },
    {
        "protocol": "TCP",
        "name": "wimsic",
        "port": 1326
    },
    {
        "protocol": "TCP",
        "name": "ultrex",
        "port": 1327
    },
    {
        "protocol": "TCP",
        "name": "ewall",
        "port": 1328
    },
    {
        "protocol": "TCP",
        "name": "netdb-export",
        "port": 1329
    },
    {
        "protocol": "TCP",
        "name": "streetperfect",
        "port": 1330
    },
    {
        "protocol": "TCP",
        "name": "intersan",
        "port": 1331
    },
    {
        "protocol": "TCP",
        "name": "pcia-rxp-b",
        "port": 1332
    },
    {
        "protocol": "TCP",
        "name": "passwrd-policy",
        "port": 1333
    },
    {
        "protocol": "TCP",
        "name": "writesrv",
        "port": 1334
    },
    {
        "protocol": "TCP",
        "name": "digital-notary",
        "port": 1335
    },
    {
        "protocol": "TCP",
        "name": "ischat",
        "port": 1336
    },
    {
        "protocol": "TCP",
        "name": "menandmice-dns",
        "port": 1337
    },
    {
        "protocol": "TCP",
        "name": "wmc-log-svc",
        "port": 1338
    },
    {
        "protocol": "TCP",
        "name": "kjtsiteserver",
        "port": 1339
    },
    {
        "protocol": "TCP",
        "name": "naap",
        "port": 1340
    },
    {
        "protocol": "TCP",
        "name": "qubes",
        "port": 1341
    },
    {
        "protocol": "TCP",
        "name": "esbroker",
        "port": 1342
    },
    {
        "protocol": "TCP",
        "name": "re101",
        "port": 1343
    },
    {
        "protocol": "TCP",
        "name": "icap",
        "port": 1344
    },
    {
        "protocol": "TCP",
        "name": "vpjp",
        "port": 1345
    },
    {
        "protocol": "TCP",
        "name": "alta-ana-lm",
        "port": 1346
    },
    {
        "protocol": "TCP",
        "name": "bbn-mmc",
        "port": 1347
    },
    {
        "protocol": "TCP",
        "name": "bbn-mmx",
        "port": 1348
    },
    {
        "protocol": "TCP",
        "name": "sbook",
        "port": 1349
    },
    {
        "protocol": "TCP",
        "name": "editbench",
        "port": 1350
    },
    {
        "protocol": "TCP",
        "name": "equationbuilder",
        "port": 1351
    },
    {
        "protocol": "TCP",
        "name": "lotusnote",
        "port": 1352
    },
    {
        "protocol": "TCP",
        "name": "relief",
        "port": 1353
    },
    {
        "protocol": "TCP",
        "name": "XSIP-network",
        "port": 1354
    },
    {
        "protocol": "TCP",
        "name": "intuitive-edge",
        "port": 1355
    },
    {
        "protocol": "TCP",
        "name": "pegboard",
        "port": 1357
    },
    {
        "protocol": "TCP",
        "name": "connlcli",
        "port": 1358
    },
    {
        "protocol": "TCP",
        "name": "ftsrv",
        "port": 1359
    },
    {
        "protocol": "TCP",
        "name": "mimer",
        "port": 1360
    },
    {
        "protocol": "TCP",
        "name": "linx",
        "port": 1361
    },
    {
        "protocol": "TCP",
        "name": "timeflies",
        "port": 1362
    },
    {
        "protocol": "TCP",
        "name": "ndm-requester",
        "port": 1363
    },
    {
        "protocol": "TCP",
        "name": "ndm-server",
        "port": 1364
    },
    {
        "protocol": "TCP",
        "name": "adapt-sna",
        "port": 1365
    },
    {
        "protocol": "TCP",
        "name": "netware-csp",
        "port": 1366
    },
    {
        "protocol": "TCP",
        "name": "dcs",
        "port": 1367
    },
    {
        "protocol": "TCP",
        "name": "screencast",
        "port": 1368
    },
    {
        "protocol": "TCP",
        "name": "gv-us",
        "port": 1369
    },
    {
        "protocol": "TCP",
        "name": "us-gv",
        "port": 1370
    },
    {
        "protocol": "TCP",
        "name": "fc-cli",
        "port": 1371
    },
    {
        "protocol": "TCP",
        "name": "fc-ser",
        "port": 1372
    },
    {
        "protocol": "TCP",
        "name": "chromagrafx",
        "port": 1373
    },
    {
        "protocol": "TCP",
        "name": "molly",
        "port": 1374
    },
    {
        "protocol": "TCP",
        "name": "bytex",
        "port": 1375
    },
    {
        "protocol": "TCP",
        "name": "ibm-pps",
        "port": 1376
    },
    {
        "protocol": "TCP",
        "name": "cichlid",
        "port": 1377
    },
    {
        "protocol": "TCP",
        "name": "elan",
        "port": 1378
    },
    {
        "protocol": "TCP",
        "name": "dbreporter",
        "port": 1379
    },
    {
        "protocol": "TCP",
        "name": "telesis-licman",
        "port": 1380
    },
    {
        "protocol": "TCP",
        "name": "apple-licman",
        "port": 1381
    },
    {
        "protocol": "TCP",
        "name": "udt_os",
        "port": 1382
    },
    {
        "protocol": "TCP",
        "name": "gwha",
        "port": 1383
    },
    {
        "protocol": "TCP",
        "name": "os-licman",
        "port": 1384
    },
    {
        "protocol": "TCP",
        "name": "atex_elmd",
        "port": 1385
    },
    {
        "protocol": "TCP",
        "name": "checksum",
        "port": 1386
    },
    {
        "protocol": "TCP",
        "name": "cadsi-lm",
        "port": 1387
    },
    {
        "protocol": "TCP",
        "name": "objective-dbc",
        "port": 1388
    },
    {
        "protocol": "TCP",
        "name": "iclpv-dm",
        "port": 1389
    },
    {
        "protocol": "TCP",
        "name": "iclpv-sc",
        "port": 1390
    },
    {
        "protocol": "TCP",
        "name": "iclpv-sas",
        "port": 1391
    },
    {
        "protocol": "TCP",
        "name": "iclpv-pm",
        "port": 1392
    },
    {
        "protocol": "TCP",
        "name": "iclpv-nls",
        "port": 1393
    },
    {
        "protocol": "TCP",
        "name": "iclpv-nlc",
        "port": 1394
    },
    {
        "protocol": "TCP",
        "name": "iclpv-wsm",
        "port": 1395
    },
    {
        "protocol": "TCP",
        "name": "dvl-activemail",
        "port": 1396
    },
    {
        "protocol": "TCP",
        "name": "audio-activmail",
        "port": 1397
    },
    {
        "protocol": "TCP",
        "name": "video-activmail",
        "port": 1398
    },
    {
        "protocol": "TCP",
        "name": "cadkey-licman",
        "port": 1399
    },
    {
        "protocol": "TCP",
        "name": "cadkey-tablet",
        "port": 1400
    },
    {
        "protocol": "TCP",
        "name": "goldleaf-licman",
        "port": 1401
    },
    {
        "protocol": "TCP",
        "name": "prm-sm-np",
        "port": 1402
    },
    {
        "protocol": "TCP",
        "name": "prm-nm-np",
        "port": 1403
    },
    {
        "protocol": "TCP",
        "name": "igi-lm",
        "port": 1404
    },
    {
        "protocol": "TCP",
        "name": "ibm-res",
        "port": 1405
    },
    {
        "protocol": "TCP",
        "name": "netlabs-lm",
        "port": 1406
    },
    {
        "protocol": "TCP",
        "name": "dbsa-lm",
        "port": 1407
    },
    {
        "protocol": "TCP",
        "name": "sophia-lm",
        "port": 1408
    },
    {
        "protocol": "TCP",
        "name": "here-lm",
        "port": 1409
    },
    {
        "protocol": "TCP",
        "name": "hiq",
        "port": 1410
    },
    {
        "protocol": "TCP",
        "name": "af",
        "port": 1411
    },
    {
        "protocol": "TCP",
        "name": "innosys",
        "port": 1412
    },
    {
        "protocol": "TCP",
        "name": "innosys-acl",
        "port": 1413
    },
    {
        "protocol": "TCP",
        "name": "ibm-mqseries",
        "port": 1414
    },
    {
        "protocol": "TCP",
        "name": "dbstar",
        "port": 1415
    },
    {
        "protocol": "TCP",
        "name": "novell-lu6.2",
        "port": 1416
    },
    {
        "protocol": "TCP",
        "name": "timbuktu-srv1",
        "port": 1417
    },
    {
        "protocol": "TCP",
        "name": "timbuktu-srv2",
        "port": 1418
    },
    {
        "protocol": "TCP",
        "name": "timbuktu-srv3",
        "port": 1419
    },
    {
        "protocol": "TCP",
        "name": "timbuktu-srv4",
        "port": 1420
    },
    {
        "protocol": "TCP",
        "name": "gandalf-lm",
        "port": 1421
    },
    {
        "protocol": "TCP",
        "name": "autodesk-lm",
        "port": 1422
    },
    {
        "protocol": "TCP",
        "name": "essbase",
        "port": 1423
    },
    {
        "protocol": "TCP",
        "name": "hybrid",
        "port": 1424
    },
    {
        "protocol": "TCP",
        "name": "zion-lm",
        "port": 1425
    },
    {
        "protocol": "TCP",
        "name": "sais",
        "port": 1426
    },
    {
        "protocol": "TCP",
        "name": "mloadd",
        "port": 1427
    },
    {
        "protocol": "TCP",
        "name": "informatik-lm",
        "port": 1428
    },
    {
        "protocol": "TCP",
        "name": "nms",
        "port": 1429
    },
    {
        "protocol": "TCP",
        "name": "tpdu",
        "port": 1430
    },
    {
        "protocol": "TCP",
        "name": "rgtp",
        "port": 1431
    },
    {
        "protocol": "TCP",
        "name": "blueberry-lm",
        "port": 1432
    },
    {
        "protocol": "TCP",
        "name": "ms-sql-s",
        "port": 1433
    },
    {
        "protocol": "TCP",
        "name": "ms-sql-m",
        "port": 1434
    },
    {
        "protocol": "TCP",
        "name": "ibm-cics",
        "port": 1435
    },
    {
        "protocol": "TCP",
        "name": "saism",
        "port": 1436
    },
    {
        "protocol": "TCP",
        "name": "tabula",
        "port": 1437
    },
    {
        "protocol": "TCP",
        "name": "eicon-server",
        "port": 1438
    },
    {
        "protocol": "TCP",
        "name": "eicon-x25",
        "port": 1439
    },
    {
        "protocol": "TCP",
        "name": "eicon-slp",
        "port": 1440
    },
    {
        "protocol": "TCP",
        "name": "cadis-1",
        "port": 1441
    },
    {
        "protocol": "TCP",
        "name": "cadis-2",
        "port": 1442
    },
    {
        "protocol": "TCP",
        "name": "ies-lm",
        "port": 1443
    },
    {
        "protocol": "TCP",
        "name": "marcam-lm",
        "port": 1444
    },
    {
        "protocol": "TCP",
        "name": "proxima-lm",
        "port": 1445
    },
    {
        "protocol": "TCP",
        "name": "ora-lm",
        "port": 1446
    },
    {
        "protocol": "TCP",
        "name": "apri-lm",
        "port": 1447
    },
    {
        "protocol": "TCP",
        "name": "oc-lm",
        "port": 1448
    },
    {
        "protocol": "TCP",
        "name": "peport",
        "port": 1449
    },
    {
        "protocol": "TCP",
        "name": "dwf",
        "port": 1450
    },
    {
        "protocol": "TCP",
        "name": "infoman",
        "port": 1451
    },
    {
        "protocol": "TCP",
        "name": "gtegsc-lm",
        "port": 1452
    },
    {
        "protocol": "TCP",
        "name": "genie-lm",
        "port": 1453
    },
    {
        "protocol": "TCP",
        "name": "interhdl_elmd",
        "port": 1454
    },
    {
        "protocol": "TCP",
        "name": "esl-lm",
        "port": 1455
    },
    {
        "protocol": "TCP",
        "name": "dca",
        "port": 1456
    },
    {
        "protocol": "TCP",
        "name": "valisys-lm",
        "port": 1457
    },
    {
        "protocol": "TCP",
        "name": "nrcabq-lm",
        "port": 1458
    },
    {
        "protocol": "TCP",
        "name": "proshare1",
        "port": 1459
    },
    {
        "protocol": "TCP",
        "name": "proshare2",
        "port": 1460
    },
    {
        "protocol": "TCP",
        "name": "ibm_wrless_lan",
        "port": 1461
    },
    {
        "protocol": "TCP",
        "name": "world-lm",
        "port": 1462
    },
    {
        "protocol": "TCP",
        "name": "nucleus",
        "port": 1463
    },
    {
        "protocol": "TCP",
        "name": "msl_lmd",
        "port": 1464
    },
    {
        "protocol": "TCP",
        "name": "pipes",
        "port": 1465
    },
    {
        "protocol": "TCP",
        "name": "oceansoft-lm",
        "port": 1466
    },
    {
        "protocol": "TCP",
        "name": "csdmbase",
        "port": 1467
    },
    {
        "protocol": "TCP",
        "name": "csdm",
        "port": 1468
    },
    {
        "protocol": "TCP",
        "name": "aal-lm",
        "port": 1469
    },
    {
        "protocol": "TCP",
        "name": "uaiact",
        "port": 1470
    },
    {
        "protocol": "TCP",
        "name": "csdmbase",
        "port": 1471
    },
    {
        "protocol": "TCP",
        "name": "csdm",
        "port": 1472
    },
    {
        "protocol": "TCP",
        "name": "openmath",
        "port": 1473
    },
    {
        "protocol": "TCP",
        "name": "telefinder",
        "port": 1474
    },
    {
        "protocol": "TCP",
        "name": "taligent-lm",
        "port": 1475
    },
    {
        "protocol": "TCP",
        "name": "clvm-cfg",
        "port": 1476
    },
    {
        "protocol": "TCP",
        "name": "ms-sna-server",
        "port": 1477
    },
    {
        "protocol": "TCP",
        "name": "ms-sna-base",
        "port": 1478
    },
    {
        "protocol": "TCP",
        "name": "dberegister",
        "port": 1479
    },
    {
        "protocol": "TCP",
        "name": "pacerforum",
        "port": 1480
    },
    {
        "protocol": "TCP",
        "name": "airs",
        "port": 1481
    },
    {
        "protocol": "TCP",
        "name": "miteksys-lm",
        "port": 1482
    },
    {
        "protocol": "TCP",
        "name": "afs",
        "port": 1483
    },
    {
        "protocol": "TCP",
        "name": "confluent",
        "port": 1484
    },
    {
        "protocol": "TCP",
        "name": "lansource",
        "port": 1485
    },
    {
        "protocol": "TCP",
        "name": "nms_topo_serv",
        "port": 1486
    },
    {
        "protocol": "TCP",
        "name": "localinfosrvr",
        "port": 1487
    },
    {
        "protocol": "TCP",
        "name": "docstor",
        "port": 1488
    },
    {
        "protocol": "TCP",
        "name": "dmdocbroker",
        "port": 1489
    },
    {
        "protocol": "TCP",
        "name": "insitu-conf",
        "port": 1490
    },
    {
        "protocol": "TCP",
        "name": "anynetgateway",
        "port": 1491
    },
    {
        "protocol": "TCP",
        "name": "stone-design-1",
        "port": 1492
    },
    {
        "protocol": "TCP",
        "name": "netmap_lm",
        "port": 1493
    },
    {
        "protocol": "TCP",
        "name": "ica",
        "port": 1494
    },
    {
        "protocol": "TCP",
        "name": "cvc",
        "port": 1495
    },
    {
        "protocol": "TCP",
        "name": "liberty-lm",
        "port": 1496
    },
    {
        "protocol": "TCP",
        "name": "rfx-lm",
        "port": 1497
    },
    {
        "protocol": "TCP",
        "name": "sybase-sqlany",
        "port": 1498
    },
    {
        "protocol": "TCP",
        "name": "fhc",
        "port": 1499
    },
    {
        "protocol": "TCP",
        "name": "vlsi-lm",
        "port": 1500
    },
    {
        "protocol": "TCP",
        "name": "saiscm",
        "port": 1501
    },
    {
        "protocol": "TCP",
        "name": "shivadiscovery",
        "port": 1502
    },
    {
        "protocol": "TCP",
        "name": "imtc-mcs",
        "port": 1503
    },
    {
        "protocol": "TCP",
        "name": "evb-elm",
        "port": 1504
    },
    {
        "protocol": "TCP",
        "name": "funkproxy",
        "port": 1505
    },
    {
        "protocol": "TCP",
        "name": "utcd",
        "port": 1506
    },
    {
        "protocol": "TCP",
        "name": "symplex",
        "port": 1507
    },
    {
        "protocol": "TCP",
        "name": "diagmond",
        "port": 1508
    },
    {
        "protocol": "TCP",
        "name": "robcad-lm",
        "port": 1509
    },
    {
        "protocol": "TCP",
        "name": "mvx-lm",
        "port": 1510
    },
    {
        "protocol": "TCP",
        "name": "3l-l1",
        "port": 1511
    },
    {
        "protocol": "TCP",
        "name": "wins",
        "port": 1512
    },
    {
        "protocol": "TCP",
        "name": "fujitsu-dtc",
        "port": 1513
    },
    {
        "protocol": "TCP",
        "name": "fujitsu-dtcns",
        "port": 1514
    },
    {
        "protocol": "TCP",
        "name": "ifor-protocol",
        "port": 1515
    },
    {
        "protocol": "TCP",
        "name": "vpad",
        "port": 1516
    },
    {
        "protocol": "TCP",
        "name": "vpac",
        "port": 1517
    },
    {
        "protocol": "TCP",
        "name": "vpvd",
        "port": 1518
    },
    {
        "protocol": "TCP",
        "name": "vpvc",
        "port": 1519
    },
    {
        "protocol": "TCP",
        "name": "atm-zip-office",
        "port": 1520
    },
    {
        "protocol": "TCP",
        "name": "ncube-lm",
        "port": 1521
    },
    {
        "protocol": "TCP",
        "name": "ricardo-lm",
        "port": 1522
    },
    {
        "protocol": "TCP",
        "name": "cichild-lm",
        "port": 1523
    },
    {
        "protocol": "TCP",
        "name": "ingreslock",
        "port": 1524
    },
    {
        "protocol": "TCP",
        "name": "prospero-np",
        "port": 1525
    },
    {
        "protocol": "TCP",
        "name": "pdap-np",
        "port": 1526
    },
    {
        "protocol": "TCP",
        "name": "tlisrv",
        "port": 1527
    },
    {
        "protocol": "TCP",
        "name": "mciautoreg",
        "port": 1528
    },
    {
        "protocol": "TCP",
        "name": "coauthor",
        "port": 1529
    },
    {
        "protocol": "TCP",
        "name": "rap-service",
        "port": 1530
    },
    {
        "protocol": "TCP",
        "name": "rap-listen",
        "port": 1531
    },
    {
        "protocol": "TCP",
        "name": "miroconnect",
        "port": 1532
    },
    {
        "protocol": "TCP",
        "name": "virtual-places",
        "port": 1533
    },
    {
        "protocol": "TCP",
        "name": "micromuse-lm",
        "port": 1534
    },
    {
        "protocol": "TCP",
        "name": "ampr-info",
        "port": 1535
    },
    {
        "protocol": "TCP",
        "name": "ampr-inter",
        "port": 1536
    },
    {
        "protocol": "TCP",
        "name": "sdsc-lm",
        "port": 1537
    },
    {
        "protocol": "TCP",
        "name": "3ds-lm",
        "port": 1538
    },
    {
        "protocol": "TCP",
        "name": "intellistor-lm",
        "port": 1539
    },
    {
        "protocol": "TCP",
        "name": "rds",
        "port": 1540
    },
    {
        "protocol": "TCP",
        "name": "rds2",
        "port": 1541
    },
    {
        "protocol": "TCP",
        "name": "gridgen-elmd",
        "port": 1542
    },
    {
        "protocol": "TCP",
        "name": "simba-cs",
        "port": 1543
    },
    {
        "protocol": "TCP",
        "name": "aspeclmd",
        "port": 1544
    },
    {
        "protocol": "TCP",
        "name": "vistium-share",
        "port": 1545
    },
    {
        "protocol": "TCP",
        "name": "laplink",
        "port": 1547
    },
    {
        "protocol": "TCP",
        "name": "axon-lm",
        "port": 1548
    },
    {
        "protocol": "TCP",
        "name": "shivahose",
        "port": 1549
    },
    {
        "protocol": "TCP",
        "name": "3m-image-lm",
        "port": 1550
    },
    {
        "protocol": "TCP",
        "name": "hecmtl-db",
        "port": 1551
    },
    {
        "protocol": "TCP",
        "name": "pciarray",
        "port": 1552
    },
    {
        "protocol": "TCP",
        "name": "sna-cs",
        "port": 1553
    },
    {
        "protocol": "TCP",
        "name": "caci-lm",
        "port": 1554
    },
    {
        "protocol": "TCP",
        "name": "livelan",
        "port": 1555
    },
    {
        "protocol": "TCP",
        "name": "veritas_pbx",
        "port": 1556
    },
    {
        "protocol": "TCP",
        "name": "arbortext-lm",
        "port": 1557
    },
    {
        "protocol": "TCP",
        "name": "xingmpeg",
        "port": 1558
    },
    {
        "protocol": "TCP",
        "name": "web2host",
        "port": 1559
    },
    {
        "protocol": "TCP",
        "name": "asci-val",
        "port": 1560
    },
    {
        "protocol": "TCP",
        "name": "facilityview",
        "port": 1561
    },
    {
        "protocol": "TCP",
        "name": "pconnectmgr",
        "port": 1562
    },
    {
        "protocol": "TCP",
        "name": "cadabra-lm",
        "port": 1563
    },
    {
        "protocol": "TCP",
        "name": "pay-per-view",
        "port": 1564
    },
    {
        "protocol": "TCP",
        "name": "winddlb",
        "port": 1565
    },
    {
        "protocol": "TCP",
        "name": "corelvideo",
        "port": 1566
    },
    {
        "protocol": "TCP",
        "name": "jlicelmd",
        "port": 1567
    },
    {
        "protocol": "TCP",
        "name": "tsspmap",
        "port": 1568
    },
    {
        "protocol": "TCP",
        "name": "ets",
        "port": 1569
    },
    {
        "protocol": "TCP",
        "name": "orbixd",
        "port": 1570
    },
    {
        "protocol": "TCP",
        "name": "rdb-dbs-disp",
        "port": 1571
    },
    {
        "protocol": "TCP",
        "name": "chip-lm",
        "port": 1572
    },
    {
        "protocol": "TCP",
        "name": "itscomm-ns",
        "port": 1573
    },
    {
        "protocol": "TCP",
        "name": "mvel-lm",
        "port": 1574
    },
    {
        "protocol": "TCP",
        "name": "oraclenames",
        "port": 1575
    },
    {
        "protocol": "TCP",
        "name": "moldflow-lm",
        "port": 1576
    },
    {
        "protocol": "TCP",
        "name": "hypercube-lm",
        "port": 1577
    },
    {
        "protocol": "TCP",
        "name": "jacobus-lm",
        "port": 1578
    },
    {
        "protocol": "TCP",
        "name": "ioc-sea-lm",
        "port": 1579
    },
    {
        "protocol": "TCP",
        "name": "tn-tl-r1",
        "port": 1580
    },
    {
        "protocol": "TCP",
        "name": "mil-2045-47001",
        "port": 1581
    },
    {
        "protocol": "TCP",
        "name": "msims",
        "port": 1582
    },
    {
        "protocol": "TCP",
        "name": "simbaexpress",
        "port": 1583
    },
    {
        "protocol": "TCP",
        "name": "tn-tl-fd2",
        "port": 1584
    },
    {
        "protocol": "TCP",
        "name": "intv",
        "port": 1585
    },
    {
        "protocol": "TCP",
        "name": "ibm-abtact",
        "port": 1586
    },
    {
        "protocol": "TCP",
        "name": "pra_elmd",
        "port": 1587
    },
    {
        "protocol": "TCP",
        "name": "triquest-lm",
        "port": 1588
    },
    {
        "protocol": "TCP",
        "name": "vqp",
        "port": 1589
    },
    {
        "protocol": "TCP",
        "name": "gemini-lm",
        "port": 1590
    },
    {
        "protocol": "TCP",
        "name": "ncpm-pm",
        "port": 1591
    },
    {
        "protocol": "TCP",
        "name": "commonspace",
        "port": 1592
    },
    {
        "protocol": "TCP",
        "name": "mainsoft-lm",
        "port": 1593
    },
    {
        "protocol": "TCP",
        "name": "sixtrak",
        "port": 1594
    },
    {
        "protocol": "TCP",
        "name": "radio",
        "port": 1595
    },
    {
        "protocol": "TCP",
        "name": "radio-sm",
        "port": 1596
    },
    {
        "protocol": "TCP",
        "name": "orbplus-iiop",
        "port": 1597
    },
    {
        "protocol": "TCP",
        "name": "picknfs",
        "port": 1598
    },
    {
        "protocol": "TCP",
        "name": "simbaservices",
        "port": 1599
    },
    {
        "protocol": "TCP",
        "name": "issd",
        "port": 1600
    },
    {
        "protocol": "TCP",
        "name": "aas",
        "port": 1601
    },
    {
        "protocol": "TCP",
        "name": "inspect",
        "port": 1602
    },
    {
        "protocol": "TCP",
        "name": "picodbc",
        "port": 1603
    },
    {
        "protocol": "TCP",
        "name": "icabrowser",
        "port": 1604
    },
    {
        "protocol": "TCP",
        "name": "slp",
        "port": 1605
    },
    {
        "protocol": "TCP",
        "name": "slm-api",
        "port": 1606
    },
    {
        "protocol": "TCP",
        "name": "stt",
        "port": 1607
    },
    {
        "protocol": "TCP",
        "name": "smart-lm",
        "port": 1608
    },
    {
        "protocol": "TCP",
        "name": "isysg-lm",
        "port": 1609
    },
    {
        "protocol": "TCP",
        "name": "taurus-wh",
        "port": 1610
    },
    {
        "protocol": "TCP",
        "name": "ill",
        "port": 1611
    },
    {
        "protocol": "TCP",
        "name": "netbill-trans",
        "port": 1612
    },
    {
        "protocol": "TCP",
        "name": "netbill-keyrep",
        "port": 1613
    },
    {
        "protocol": "TCP",
        "name": "netbill-cred",
        "port": 1614
    },
    {
        "protocol": "TCP",
        "name": "netbill-auth",
        "port": 1615
    },
    {
        "protocol": "TCP",
        "name": "netbill-prod",
        "port": 1616
    },
    {
        "protocol": "TCP",
        "name": "nimrod-agent",
        "port": 1617
    },
    {
        "protocol": "TCP",
        "name": "skytelnet",
        "port": 1618
    },
    {
        "protocol": "TCP",
        "name": "xs-openstorage",
        "port": 1619
    },
    {
        "protocol": "TCP",
        "name": "faxportwinport",
        "port": 1620
    },
    {
        "protocol": "TCP",
        "name": "softdataphone",
        "port": 1621
    },
    {
        "protocol": "TCP",
        "name": "ontime",
        "port": 1622
    },
    {
        "protocol": "TCP",
        "name": "jaleosnd",
        "port": 1623
    },
    {
        "protocol": "TCP",
        "name": "udp-sr-port",
        "port": 1624
    },
    {
        "protocol": "TCP",
        "name": "svs-omagent",
        "port": 1625
    },
    {
        "protocol": "TCP",
        "name": "shockwave",
        "port": 1626
    },
    {
        "protocol": "TCP",
        "name": "t128-gateway",
        "port": 1627
    },
    {
        "protocol": "TCP",
        "name": "lontalk-norm",
        "port": 1628
    },
    {
        "protocol": "TCP",
        "name": "lontalk-urgnt",
        "port": 1629
    },
    {
        "protocol": "TCP",
        "name": "oraclenet8cman",
        "port": 1630
    },
    {
        "protocol": "TCP",
        "name": "visitview",
        "port": 1631
    },
    {
        "protocol": "TCP",
        "name": "pammratc",
        "port": 1632
    },
    {
        "protocol": "TCP",
        "name": "pammrpc",
        "port": 1633
    },
    {
        "protocol": "TCP",
        "name": "loaprobe",
        "port": 1634
    },
    {
        "protocol": "TCP",
        "name": "edb-server1",
        "port": 1635
    },
    {
        "protocol": "TCP",
        "name": "cncp",
        "port": 1636
    },
    {
        "protocol": "TCP",
        "name": "cnap",
        "port": 1637
    },
    {
        "protocol": "TCP",
        "name": "cnip",
        "port": 1638
    },
    {
        "protocol": "TCP",
        "name": "cert-initiator",
        "port": 1639
    },
    {
        "protocol": "TCP",
        "name": "cert-responder",
        "port": 1640
    },
    {
        "protocol": "TCP",
        "name": "invision",
        "port": 1641
    },
    {
        "protocol": "TCP",
        "name": "isis-am",
        "port": 1642
    },
    {
        "protocol": "TCP",
        "name": "isis-ambc",
        "port": 1643
    },
    {
        "protocol": "TCP",
        "name": "saiseh",
        "port": 1644
    },
    {
        "protocol": "TCP",
        "name": "sightline",
        "port": 1645
    },
    {
        "protocol": "TCP",
        "name": "sa-msg-port",
        "port": 1646
    },
    {
        "protocol": "TCP",
        "name": "rsap",
        "port": 1647
    },
    {
        "protocol": "TCP",
        "name": "concurrent-lm",
        "port": 1648
    },
    {
        "protocol": "TCP",
        "name": "kermit",
        "port": 1649
    },
    {
        "protocol": "TCP",
        "name": "nkd",
        "port": 1650
    },
    {
        "protocol": "TCP",
        "name": "shiva_confsrvr",
        "port": 1651
    },
    {
        "protocol": "TCP",
        "name": "xnmp",
        "port": 1652
    },
    {
        "protocol": "TCP",
        "name": "alphatech-lm",
        "port": 1653
    },
    {
        "protocol": "TCP",
        "name": "stargatealerts",
        "port": 1654
    },
    {
        "protocol": "TCP",
        "name": "dec-mbadmin",
        "port": 1655
    },
    {
        "protocol": "TCP",
        "name": "dec-mbadmin-h",
        "port": 1656
    },
    {
        "protocol": "TCP",
        "name": "fujitsu-mmpdc",
        "port": 1657
    },
    {
        "protocol": "TCP",
        "name": "sixnetudr",
        "port": 1658
    },
    {
        "protocol": "TCP",
        "name": "sg-lm",
        "port": 1659
    },
    {
        "protocol": "TCP",
        "name": "skip-mc-gikreq",
        "port": 1660
    },
    {
        "protocol": "TCP",
        "name": "netview-aix-1",
        "port": 1661
    },
    {
        "protocol": "TCP",
        "name": "netview-aix-2",
        "port": 1662
    },
    {
        "protocol": "TCP",
        "name": "netview-aix-3",
        "port": 1663
    },
    {
        "protocol": "TCP",
        "name": "netview-aix-4",
        "port": 1664
    },
    {
        "protocol": "TCP",
        "name": "netview-aix-5",
        "port": 1665
    },
    {
        "protocol": "TCP",
        "name": "netview-aix-6",
        "port": 1666
    },
    {
        "protocol": "TCP",
        "name": "netview-aix-7",
        "port": 1667
    },
    {
        "protocol": "TCP",
        "name": "netview-aix-8",
        "port": 1668
    },
    {
        "protocol": "TCP",
        "name": "netview-aix-9",
        "port": 1669
    },
    {
        "protocol": "TCP",
        "name": "netview-aix-10",
        "port": 1670
    },
    {
        "protocol": "TCP",
        "name": "netview-aix-11",
        "port": 1671
    },
    {
        "protocol": "TCP",
        "name": "netview-aix-12",
        "port": 1672
    },
    {
        "protocol": "TCP",
        "name": "proshare-mc-1",
        "port": 1673
    },
    {
        "protocol": "TCP",
        "name": "proshare-mc-2",
        "port": 1674
    },
    {
        "protocol": "TCP",
        "name": "pdp",
        "port": 1675
    },
    {
        "protocol": "TCP",
        "name": "netcomm1",
        "port": 1676
    },
    {
        "protocol": "TCP",
        "name": "groupwise",
        "port": 1677
    },
    {
        "protocol": "TCP",
        "name": "prolink",
        "port": 1678
    },
    {
        "protocol": "TCP",
        "name": "darcorp-lm",
        "port": 1679
    },
    {
        "protocol": "TCP",
        "name": "microcom-sbp",
        "port": 1680
    },
    {
        "protocol": "TCP",
        "name": "sd-elmd",
        "port": 1681
    },
    {
        "protocol": "TCP",
        "name": "lanyon-lantern",
        "port": 1682
    },
    {
        "protocol": "TCP",
        "name": "ncpm-hip",
        "port": 1683
    },
    {
        "protocol": "TCP",
        "name": "snaresecure",
        "port": 1684
    },
    {
        "protocol": "TCP",
        "name": "n2nremote",
        "port": 1685
    },
    {
        "protocol": "TCP",
        "name": "cvmon",
        "port": 1686
    },
    {
        "protocol": "TCP",
        "name": "nsjtp-ctrl",
        "port": 1687
    },
    {
        "protocol": "TCP",
        "name": "nsjtp-data",
        "port": 1688
    },
    {
        "protocol": "TCP",
        "name": "firefox",
        "port": 1689
    },
    {
        "protocol": "TCP",
        "name": "ng-umds",
        "port": 1690
    },
    {
        "protocol": "TCP",
        "name": "empire-empuma",
        "port": 1691
    },
    {
        "protocol": "TCP",
        "name": "sstsys-lm",
        "port": 1692
    },
    {
        "protocol": "TCP",
        "name": "rrirtr",
        "port": 1693
    },
    {
        "protocol": "TCP",
        "name": "rrimwm",
        "port": 1694
    },
    {
        "protocol": "TCP",
        "name": "rrilwm",
        "port": 1695
    },
    {
        "protocol": "TCP",
        "name": "rrifmm",
        "port": 1696
    },
    {
        "protocol": "TCP",
        "name": "rrisat",
        "port": 1697
    },
    {
        "protocol": "TCP",
        "name": "rsvp-encap-1",
        "port": 1698
    },
    {
        "protocol": "TCP",
        "name": "rsvp-encap-2",
        "port": 1699
    },
    {
        "protocol": "TCP",
        "name": "mps-raft",
        "port": 1700
    },
    {
        "protocol": "TCP",
        "name": "l2tp",
        "port": 1701
    },
    {
        "protocol": "TCP",
        "name": "deskshare",
        "port": 1702
    },
    {
        "protocol": "TCP",
        "name": "hb-engine",
        "port": 1703
    },
    {
        "protocol": "TCP",
        "name": "bcs-broker",
        "port": 1704
    },
    {
        "protocol": "TCP",
        "name": "slingshot",
        "port": 1705
    },
    {
        "protocol": "TCP",
        "name": "jetform",
        "port": 1706
    },
    {
        "protocol": "TCP",
        "name": "vdmplay",
        "port": 1707
    },
    {
        "protocol": "TCP",
        "name": "gat-lmd",
        "port": 1708
    },
    {
        "protocol": "TCP",
        "name": "centra",
        "port": 1709
    },
    {
        "protocol": "TCP",
        "name": "impera",
        "port": 1710
    },
    {
        "protocol": "TCP",
        "name": "pptconference",
        "port": 1711
    },
    {
        "protocol": "TCP",
        "name": "registrar",
        "port": 1712
    },
    {
        "protocol": "TCP",
        "name": "conferencetalk",
        "port": 1713
    },
    {
        "protocol": "TCP",
        "name": "sesi-lm",
        "port": 1714
    },
    {
        "protocol": "TCP",
        "name": "houdini-lm",
        "port": 1715
    },
    {
        "protocol": "TCP",
        "name": "xmsg",
        "port": 1716
    },
    {
        "protocol": "TCP",
        "name": "fj-hdnet",
        "port": 1717
    },
    {
        "protocol": "TCP",
        "name": "h323gatedisc",
        "port": 1718
    },
    {
        "protocol": "TCP",
        "name": "h323gatestat",
        "port": 1719
    },
    {
        "protocol": "TCP",
        "name": "h323hostcall",
        "port": 1720
    },
    {
        "protocol": "TCP",
        "name": "caicci",
        "port": 1721
    },
    {
        "protocol": "TCP",
        "name": "hks-lm",
        "port": 1722
    },
    {
        "protocol": "TCP",
        "name": "pptp",
        "port": 1723
    },
    {
        "protocol": "TCP",
        "name": "csbphonemaster",
        "port": 1724
    },
    {
        "protocol": "TCP",
        "name": "iden-ralp",
        "port": 1725
    },
    {
        "protocol": "TCP",
        "name": "iberiagames",
        "port": 1726
    },
    {
        "protocol": "TCP",
        "name": "winddx",
        "port": 1727
    },
    {
        "protocol": "TCP",
        "name": "telindus",
        "port": 1728
    },
    {
        "protocol": "TCP",
        "name": "citynl",
        "port": 1729
    },
    {
        "protocol": "TCP",
        "name": "roketz",
        "port": 1730
    },
    {
        "protocol": "TCP",
        "name": "msiccp",
        "port": 1731
    },
    {
        "protocol": "TCP",
        "name": "proxim",
        "port": 1732
    },
    {
        "protocol": "TCP",
        "name": "siipat",
        "port": 1733
    },
    {
        "protocol": "TCP",
        "name": "cambertx-lm",
        "port": 1734
    },
    {
        "protocol": "TCP",
        "name": "privatechat",
        "port": 1735
    },
    {
        "protocol": "TCP",
        "name": "street-stream",
        "port": 1736
    },
    {
        "protocol": "TCP",
        "name": "ultimad",
        "port": 1737
    },
    {
        "protocol": "TCP",
        "name": "gamegen1",
        "port": 1738
    },
    {
        "protocol": "TCP",
        "name": "webaccess",
        "port": 1739
    },
    {
        "protocol": "TCP",
        "name": "encore",
        "port": 1740
    },
    {
        "protocol": "TCP",
        "name": "cisco-net-mgmt",
        "port": 1741
    },
    {
        "protocol": "TCP",
        "name": "3Com-nsd",
        "port": 1742
    },
    {
        "protocol": "TCP",
        "name": "cinegrfx-lm",
        "port": 1743
    },
    {
        "protocol": "TCP",
        "name": "ncpm-ft",
        "port": 1744
    },
    {
        "protocol": "TCP",
        "name": "remote-winsock",
        "port": 1745
    },
    {
        "protocol": "TCP",
        "name": "ftrapid-1",
        "port": 1746
    },
    {
        "protocol": "TCP",
        "name": "ftrapid-2",
        "port": 1747
    },
    {
        "protocol": "TCP",
        "name": "oracle-em1",
        "port": 1748
    },
    {
        "protocol": "TCP",
        "name": "aspen-services",
        "port": 1749
    },
    {
        "protocol": "TCP",
        "name": "sslp",
        "port": 1750
    },
    {
        "protocol": "TCP",
        "name": "swiftnet",
        "port": 1751
    },
    {
        "protocol": "TCP",
        "name": "lofr-lm",
        "port": 1752
    },
    {
        "protocol": "TCP",
        "name": "oracle-em2",
        "port": 1754
    },
    {
        "protocol": "TCP",
        "name": "ms-streaming",
        "port": 1755
    },
    {
        "protocol": "TCP",
        "name": "capfast-lmd",
        "port": 1756
    },
    {
        "protocol": "TCP",
        "name": "cnhrp",
        "port": 1757
    },
    {
        "protocol": "TCP",
        "name": "tftp-mcast",
        "port": 1758
    },
    {
        "protocol": "TCP",
        "name": "spss-lm",
        "port": 1759
    },
    {
        "protocol": "TCP",
        "name": "www-ldap-gw",
        "port": 1760
    },
    {
        "protocol": "TCP",
        "name": "cft-0",
        "port": 1761
    },
    {
        "protocol": "TCP",
        "name": "cft-1",
        "port": 1762
    },
    {
        "protocol": "TCP",
        "name": "cft-2",
        "port": 1763
    },
    {
        "protocol": "TCP",
        "name": "cft-3",
        "port": 1764
    },
    {
        "protocol": "TCP",
        "name": "cft-4",
        "port": 1765
    },
    {
        "protocol": "TCP",
        "name": "cft-5",
        "port": 1766
    },
    {
        "protocol": "TCP",
        "name": "cft-6",
        "port": 1767
    },
    {
        "protocol": "TCP",
        "name": "cft-7",
        "port": 1768
    },
    {
        "protocol": "TCP",
        "name": "bmc-net-adm",
        "port": 1769
    },
    {
        "protocol": "TCP",
        "name": "bmc-net-svc",
        "port": 1770
    },
    {
        "protocol": "TCP",
        "name": "vaultbase",
        "port": 1771
    },
    {
        "protocol": "TCP",
        "name": "essweb-gw",
        "port": 1772
    },
    {
        "protocol": "TCP",
        "name": "kmscontrol",
        "port": 1773
    },
    {
        "protocol": "TCP",
        "name": "global-dtserv",
        "port": 1774
    },
    {
        "protocol": "TCP",
        "name": "femis",
        "port": 1776
    },
    {
        "protocol": "TCP",
        "name": "powerguardian",
        "port": 1777
    },
    {
        "protocol": "TCP",
        "name": "prodigy-intrnet",
        "port": 1778
    },
    {
        "protocol": "TCP",
        "name": "pharmasoft",
        "port": 1779
    },
    {
        "protocol": "TCP",
        "name": "dpkeyserv",
        "port": 1780
    },
    {
        "protocol": "TCP",
        "name": "answersoft-lm",
        "port": 1781
    },
    {
        "protocol": "TCP",
        "name": "hp-hcip",
        "port": 1782
    },
    {
        "protocol": "TCP",
        "name": "finle-lm",
        "port": 1784
    },
    {
        "protocol": "TCP",
        "name": "windlm",
        "port": 1785
    },
    {
        "protocol": "TCP",
        "name": "funk-logger",
        "port": 1786
    },
    {
        "protocol": "TCP",
        "name": "funk-license",
        "port": 1787
    },
    {
        "protocol": "TCP",
        "name": "psmond",
        "port": 1788
    },
    {
        "protocol": "TCP",
        "name": "hello",
        "port": 1789
    },
    {
        "protocol": "TCP",
        "name": "nmsp",
        "port": 1790
    },
    {
        "protocol": "TCP",
        "name": "ea1",
        "port": 1791
    },
    {
        "protocol": "TCP",
        "name": "ibm-dt-2",
        "port": 1792
    },
    {
        "protocol": "TCP",
        "name": "rsc-robot",
        "port": 1793
    },
    {
        "protocol": "TCP",
        "name": "cera-bcm",
        "port": 1794
    },
    {
        "protocol": "TCP",
        "name": "dpi-proxy",
        "port": 1795
    },
    {
        "protocol": "TCP",
        "name": "vocaltec-admin",
        "port": 1796
    },
    {
        "protocol": "TCP",
        "name": "uma",
        "port": 1797
    },
    {
        "protocol": "TCP",
        "name": "etp",
        "port": 1798
    },
    {
        "protocol": "TCP",
        "name": "netrisk",
        "port": 1799
    },
    {
        "protocol": "TCP",
        "name": "ansys-lm",
        "port": 1800
    },
    {
        "protocol": "TCP",
        "name": "msmq",
        "port": 1801
    },
    {
        "protocol": "TCP",
        "name": "concomp1",
        "port": 1802
    },
    {
        "protocol": "TCP",
        "name": "hp-hcip-gwy",
        "port": 1803
    },
    {
        "protocol": "TCP",
        "name": "enl",
        "port": 1804
    },
    {
        "protocol": "TCP",
        "name": "enl-name",
        "port": 1805
    },
    {
        "protocol": "TCP",
        "name": "musiconline",
        "port": 1806
    },
    {
        "protocol": "TCP",
        "name": "fhsp",
        "port": 1807
    },
    {
        "protocol": "TCP",
        "name": "oracle-vp2",
        "port": 1808
    },
    {
        "protocol": "TCP",
        "name": "oracle-vp1",
        "port": 1809
    },
    {
        "protocol": "TCP",
        "name": "jerand-lm",
        "port": 1810
    },
    {
        "protocol": "TCP",
        "name": "scientia-sdb",
        "port": 1811
    },
    {
        "protocol": "TCP",
        "name": "radius",
        "port": 1812
    },
    {
        "protocol": "TCP",
        "name": "radius-acct",
        "port": 1813
    },
    {
        "protocol": "TCP",
        "name": "tdp-suite",
        "port": 1814
    },
    {
        "protocol": "TCP",
        "name": "mmpft",
        "port": 1815
    },
    {
        "protocol": "TCP",
        "name": "harp",
        "port": 1816
    },
    {
        "protocol": "TCP",
        "name": "rkb-oscs",
        "port": 1817
    },
    {
        "protocol": "TCP",
        "name": "etftp",
        "port": 1818
    },
    {
        "protocol": "TCP",
        "name": "plato-lm",
        "port": 1819
    },
    {
        "protocol": "TCP",
        "name": "mcagent",
        "port": 1820
    },
    {
        "protocol": "TCP",
        "name": "donnyworld",
        "port": 1821
    },
    {
        "protocol": "TCP",
        "name": "es-elmd",
        "port": 1822
    },
    {
        "protocol": "TCP",
        "name": "unisys-lm",
        "port": 1823
    },
    {
        "protocol": "TCP",
        "name": "metrics-pas",
        "port": 1824
    },
    {
        "protocol": "TCP",
        "name": "direcpc-video",
        "port": 1825
    },
    {
        "protocol": "TCP",
        "name": "ardt",
        "port": 1826
    },
    {
        "protocol": "TCP",
        "name": "asi",
        "port": 1827
    },
    {
        "protocol": "TCP",
        "name": "itm-mcell-u",
        "port": 1828
    },
    {
        "protocol": "TCP",
        "name": "optika-emedia",
        "port": 1829
    },
    {
        "protocol": "TCP",
        "name": "net8-cman",
        "port": 1830
    },
    {
        "protocol": "TCP",
        "name": "myrtle",
        "port": 1831
    },
    {
        "protocol": "TCP",
        "name": "tht-treasure",
        "port": 1832
    },
    {
        "protocol": "TCP",
        "name": "udpradio",
        "port": 1833
    },
    {
        "protocol": "TCP",
        "name": "ardusuni",
        "port": 1834
    },
    {
        "protocol": "TCP",
        "name": "ardusmul",
        "port": 1835
    },
    {
        "protocol": "TCP",
        "name": "ste-smsc",
        "port": 1836
    },
    {
        "protocol": "TCP",
        "name": "csoft1",
        "port": 1837
    },
    {
        "protocol": "TCP",
        "name": "talnet",
        "port": 1838
    },
    {
        "protocol": "TCP",
        "name": "netopia-vo1",
        "port": 1839
    },
    {
        "protocol": "TCP",
        "name": "netopia-vo2",
        "port": 1840
    },
    {
        "protocol": "TCP",
        "name": "netopia-vo3",
        "port": 1841
    },
    {
        "protocol": "TCP",
        "name": "netopia-vo4",
        "port": 1842
    },
    {
        "protocol": "TCP",
        "name": "netopia-vo5",
        "port": 1843
    },
    {
        "protocol": "TCP",
        "name": "direcpc-dll",
        "port": 1844
    },
    {
        "protocol": "TCP",
        "name": "altalink",
        "port": 1845
    },
    {
        "protocol": "TCP",
        "name": "tunstall-pnc",
        "port": 1846
    },
    {
        "protocol": "TCP",
        "name": "slp-notify",
        "port": 1847
    },
    {
        "protocol": "TCP",
        "name": "fjdocdist",
        "port": 1848
    },
    {
        "protocol": "TCP",
        "name": "alpha-sms",
        "port": 1849
    },
    {
        "protocol": "TCP",
        "name": "gsi",
        "port": 1850
    },
    {
        "protocol": "TCP",
        "name": "ctcd",
        "port": 1851
    },
    {
        "protocol": "TCP",
        "name": "virtual-time",
        "port": 1852
    },
    {
        "protocol": "TCP",
        "name": "vids-avtp",
        "port": 1853
    },
    {
        "protocol": "TCP",
        "name": "buddy-draw",
        "port": 1854
    },
    {
        "protocol": "TCP",
        "name": "fiorano-rtrsvc",
        "port": 1855
    },
    {
        "protocol": "TCP",
        "name": "fiorano-msgsvc",
        "port": 1856
    },
    {
        "protocol": "TCP",
        "name": "datacaptor",
        "port": 1857
    },
    {
        "protocol": "TCP",
        "name": "privateark",
        "port": 1858
    },
    {
        "protocol": "TCP",
        "name": "gammafetchsvr",
        "port": 1859
    },
    {
        "protocol": "TCP",
        "name": "sunscalar-svc",
        "port": 1860
    },
    {
        "protocol": "TCP",
        "name": "lecroy-vicp",
        "port": 1861
    },
    {
        "protocol": "TCP",
        "name": "techra-server",
        "port": 1862
    },
    {
        "protocol": "TCP",
        "name": "msnp",
        "port": 1863
    },
    {
        "protocol": "TCP",
        "name": "paradym-31port",
        "port": 1864
    },
    {
        "protocol": "TCP",
        "name": "entp",
        "port": 1865
    },
    {
        "protocol": "TCP",
        "name": "swrmi",
        "port": 1866
    },
    {
        "protocol": "TCP",
        "name": "udrive",
        "port": 1867
    },
    {
        "protocol": "TCP",
        "name": "viziblebrowser",
        "port": 1868
    },
    {
        "protocol": "TCP",
        "name": "yestrader",
        "port": 1869
    },
    {
        "protocol": "TCP",
        "name": "sunscalar-dns",
        "port": 1870
    },
    {
        "protocol": "TCP",
        "name": "canocentral0",
        "port": 1871
    },
    {
        "protocol": "TCP",
        "name": "canocentral1",
        "port": 1872
    },
    {
        "protocol": "TCP",
        "name": "fjmpjps",
        "port": 1873
    },
    {
        "protocol": "TCP",
        "name": "fjswapsnp",
        "port": 1874
    },
    {
        "protocol": "TCP",
        "name": "westell-stats",
        "port": 1875
    },
    {
        "protocol": "TCP",
        "name": "ewcappsrv",
        "port": 1876
    },
    {
        "protocol": "TCP",
        "name": "hp-webqosdb",
        "port": 1877
    },
    {
        "protocol": "TCP",
        "name": "drmsmc",
        "port": 1878
    },
    {
        "protocol": "TCP",
        "name": "nettgain-nms",
        "port": 1879
    },
    {
        "protocol": "TCP",
        "name": "vsat-control",
        "port": 1880
    },
    {
        "protocol": "TCP",
        "name": "ibm-mqseries2",
        "port": 1881
    },
    {
        "protocol": "TCP",
        "name": "ecsqdmn",
        "port": 1882
    },
    {
        "protocol": "TCP",
        "name": "ibm-mqisdp",
        "port": 1883
    },
    {
        "protocol": "TCP",
        "name": "idmaps",
        "port": 1884
    },
    {
        "protocol": "TCP",
        "name": "vrtstrapserver",
        "port": 1885
    },
    {
        "protocol": "TCP",
        "name": "leoip",
        "port": 1886
    },
    {
        "protocol": "TCP",
        "name": "filex-lport",
        "port": 1887
    },
    {
        "protocol": "TCP",
        "name": "ncconfig",
        "port": 1888
    },
    {
        "protocol": "TCP",
        "name": "unify-adapter",
        "port": 1889
    },
    {
        "protocol": "TCP",
        "name": "wilkenlistener",
        "port": 1890
    },
    {
        "protocol": "TCP",
        "name": "childkey-notif",
        "port": 1891
    },
    {
        "protocol": "TCP",
        "name": "childkey-ctrl",
        "port": 1892
    },
    {
        "protocol": "TCP",
        "name": "elad",
        "port": 1893
    },
    {
        "protocol": "TCP",
        "name": "o2server-port",
        "port": 1894
    },
    {
        "protocol": "TCP",
        "name": "b-novative-ls",
        "port": 1896
    },
    {
        "protocol": "TCP",
        "name": "metaagent",
        "port": 1897
    },
    {
        "protocol": "TCP",
        "name": "cymtec-port",
        "port": 1898
    },
    {
        "protocol": "TCP",
        "name": "mc2studios",
        "port": 1899
    },
    {
        "protocol": "TCP",
        "name": "ssdp",
        "port": 1900
    },
    {
        "protocol": "TCP",
        "name": "fjicl-tep-a",
        "port": 1901
    },
    {
        "protocol": "TCP",
        "name": "fjicl-tep-b",
        "port": 1902
    },
    {
        "protocol": "TCP",
        "name": "linkname",
        "port": 1903
    },
    {
        "protocol": "TCP",
        "name": "fjicl-tep-c",
        "port": 1904
    },
    {
        "protocol": "TCP",
        "name": "sugp",
        "port": 1905
    },
    {
        "protocol": "TCP",
        "name": "tpmd",
        "port": 1906
    },
    {
        "protocol": "TCP",
        "name": "intrastar",
        "port": 1907
    },
    {
        "protocol": "TCP",
        "name": "dawn",
        "port": 1908
    },
    {
        "protocol": "TCP",
        "name": "global-wlink",
        "port": 1909
    },
    {
        "protocol": "TCP",
        "name": "ultrabac",
        "port": 1910
    },
    {
        "protocol": "TCP",
        "name": "mtp",
        "port": 1911
    },
    {
        "protocol": "TCP",
        "name": "rhp-iibp",
        "port": 1912
    },
    {
        "protocol": "TCP",
        "name": "armadp",
        "port": 1913
    },
    {
        "protocol": "TCP",
        "name": "elm-momentum",
        "port": 1914
    },
    {
        "protocol": "TCP",
        "name": "facelink",
        "port": 1915
    },
    {
        "protocol": "TCP",
        "name": "persona",
        "port": 1916
    },
    {
        "protocol": "TCP",
        "name": "noagent",
        "port": 1917
    },
    {
        "protocol": "TCP",
        "name": "can-nds",
        "port": 1918
    },
    {
        "protocol": "TCP",
        "name": "can-dch",
        "port": 1919
    },
    {
        "protocol": "TCP",
        "name": "can-ferret",
        "port": 1920
    },
    {
        "protocol": "TCP",
        "name": "noadmin",
        "port": 1921
    },
    {
        "protocol": "TCP",
        "name": "tapestry",
        "port": 1922
    },
    {
        "protocol": "TCP",
        "name": "spice",
        "port": 1923
    },
    {
        "protocol": "TCP",
        "name": "xiip",
        "port": 1924
    },
    {
        "protocol": "TCP",
        "name": "discovery-port",
        "port": 1925
    },
    {
        "protocol": "TCP",
        "name": "egs",
        "port": 1926
    },
    {
        "protocol": "TCP",
        "name": "videte-cipc",
        "port": 1927
    },
    {
        "protocol": "TCP",
        "name": "emsd-port",
        "port": 1928
    },
    {
        "protocol": "TCP",
        "name": "bandwiz-system",
        "port": 1929
    },
    {
        "protocol": "TCP",
        "name": "driveappserver",
        "port": 1930
    },
    {
        "protocol": "TCP",
        "name": "amdsched",
        "port": 1931
    },
    {
        "protocol": "TCP",
        "name": "ctt-broker",
        "port": 1932
    },
    {
        "protocol": "TCP",
        "name": "xmapi",
        "port": 1933
    },
    {
        "protocol": "TCP",
        "name": "xaapi",
        "port": 1934
    },
    {
        "protocol": "TCP",
        "name": "macromedia-fcs",
        "port": 1935
    },
    {
        "protocol": "TCP",
        "name": "jetcmeserver",
        "port": 1936
    },
    {
        "protocol": "TCP",
        "name": "jwserver",
        "port": 1937
    },
    {
        "protocol": "TCP",
        "name": "jwclient",
        "port": 1938
    },
    {
        "protocol": "TCP",
        "name": "jvserver",
        "port": 1939
    },
    {
        "protocol": "TCP",
        "name": "jvclient",
        "port": 1940
    },
    {
        "protocol": "TCP",
        "name": "dic-aida",
        "port": 1941
    },
    {
        "protocol": "TCP",
        "name": "res",
        "port": 1942
    },
    {
        "protocol": "TCP",
        "name": "beeyond-media",
        "port": 1943
    },
    {
        "protocol": "TCP",
        "name": "close-combat",
        "port": 1944
    },
    {
        "protocol": "TCP",
        "name": "dialogic-elmd",
        "port": 1945
    },
    {
        "protocol": "TCP",
        "name": "tekpls",
        "port": 1946
    },
    {
        "protocol": "TCP",
        "name": "hlserver",
        "port": 1947
    },
    {
        "protocol": "TCP",
        "name": "eye2eye",
        "port": 1948
    },
    {
        "protocol": "TCP",
        "name": "ismaeasdaqlive",
        "port": 1949
    },
    {
        "protocol": "TCP",
        "name": "ismaeasdaqtest",
        "port": 1950
    },
    {
        "protocol": "TCP",
        "name": "bcs-lmserver",
        "port": 1951
    },
    {
        "protocol": "TCP",
        "name": "mpnjsc",
        "port": 1952
    },
    {
        "protocol": "TCP",
        "name": "rapidbase",
        "port": 1953
    },
    {
        "protocol": "TCP",
        "name": "abr-basic",
        "port": 1954
    },
    {
        "protocol": "TCP",
        "name": "abr-secure",
        "port": 1955
    },
    {
        "protocol": "TCP",
        "name": "vrtl-vmf-ds",
        "port": 1956
    },
    {
        "protocol": "TCP",
        "name": "unix-status",
        "port": 1957
    },
    {
        "protocol": "TCP",
        "name": "dxadmind",
        "port": 1958
    },
    {
        "protocol": "TCP",
        "name": "simp-all",
        "port": 1959
    },
    {
        "protocol": "TCP",
        "name": "nasmanager",
        "port": 1960
    },
    {
        "protocol": "TCP",
        "name": "bts-appserver",
        "port": 1961
    },
    {
        "protocol": "TCP",
        "name": "biap-mp",
        "port": 1962
    },
    {
        "protocol": "TCP",
        "name": "webmachine",
        "port": 1963
    },
    {
        "protocol": "TCP",
        "name": "solid-e-engine",
        "port": 1964
    },
    {
        "protocol": "TCP",
        "name": "tivoli-npm",
        "port": 1965
    },
    {
        "protocol": "TCP",
        "name": "slush",
        "port": 1966
    },
    {
        "protocol": "TCP",
        "name": "sns-quote",
        "port": 1967
    },
    {
        "protocol": "TCP",
        "name": "lipsinc",
        "port": 1968
    },
    {
        "protocol": "TCP",
        "name": "lipsinc1",
        "port": 1969
    },
    {
        "protocol": "TCP",
        "name": "netop-rc",
        "port": 1970
    },
    {
        "protocol": "TCP",
        "name": "netop-school",
        "port": 1971
    },
    {
        "protocol": "TCP",
        "name": "intersys-cache",
        "port": 1972
    },
    {
        "protocol": "TCP",
        "name": "dlsrap",
        "port": 1973
    },
    {
        "protocol": "TCP",
        "name": "drp",
        "port": 1974
    },
    {
        "protocol": "TCP",
        "name": "tcoflashagent",
        "port": 1975
    },
    {
        "protocol": "TCP",
        "name": "tcoregagent",
        "port": 1976
    },
    {
        "protocol": "TCP",
        "name": "tcoaddressbook",
        "port": 1977
    },
    {
        "protocol": "TCP",
        "name": "unisql",
        "port": 1978
    },
    {
        "protocol": "TCP",
        "name": "unisql-java",
        "port": 1979
    },
    {
        "protocol": "TCP",
        "name": "pearldoc-xact",
        "port": 1980
    },
    {
        "protocol": "TCP",
        "name": "p2pq",
        "port": 1981
    },
    {
        "protocol": "TCP",
        "name": "estamp",
        "port": 1982
    },
    {
        "protocol": "TCP",
        "name": "lhtp",
        "port": 1983
    },
    {
        "protocol": "TCP",
        "name": "bb",
        "port": 1984
    },
    {
        "protocol": "TCP",
        "name": "hsrp",
        "port": 1985
    },
    {
        "protocol": "TCP",
        "name": "licensedaemon",
        "port": 1986
    },
    {
        "protocol": "TCP",
        "name": "tr-rsrb-p1",
        "port": 1987
    },
    {
        "protocol": "TCP",
        "name": "tr-rsrb-p2",
        "port": 1988
    },
    {
        "protocol": "TCP",
        "name": "tr-rsrb-p3",
        "port": 1989
    },
    {
        "protocol": "TCP",
        "name": "stun-p1",
        "port": 1990
    },
    {
        "protocol": "TCP",
        "name": "stun-p2",
        "port": 1991
    },
    {
        "protocol": "TCP",
        "name": "stun-p3",
        "port": 1992
    },
    {
        "protocol": "TCP",
        "name": "snmp-TCP-port",
        "port": 1993
    },
    {
        "protocol": "TCP",
        "name": "stun-port",
        "port": 1994
    },
    {
        "protocol": "TCP",
        "name": "perf-port",
        "port": 1995
    },
    {
        "protocol": "TCP",
        "name": "tr-rsrb-port",
        "port": 1996
    },
    {
        "protocol": "TCP",
        "name": "gdp-port",
        "port": 1997
    },
    {
        "protocol": "TCP",
        "name": "x25-svc-port",
        "port": 1998
    },
    {
        "protocol": "TCP",
        "name": "TCP-id-port",
        "port": 1999
    },
    {
        "protocol": "TCP",
        "name": "cisco-sccp",
        "port": 2000
    },
    {
        "protocol": "TCP",
        "name": "dc",
        "port": 2001
    },
    {
        "protocol": "TCP",
        "name": "globe",
        "port": 2002
    },
    {
        "protocol": "TCP",
        "name": "mailbox",
        "port": 2004
    },
    {
        "protocol": "TCP",
        "name": "berknet",
        "port": 2005
    },
    {
        "protocol": "TCP",
        "name": "invokator",
        "port": 2006
    },
    {
        "protocol": "TCP",
        "name": "dectalk",
        "port": 2007
    },
    {
        "protocol": "TCP",
        "name": "conf",
        "port": 2008
    },
    {
        "protocol": "TCP",
        "name": "news",
        "port": 2009
    },
    {
        "protocol": "TCP",
        "name": "search",
        "port": 2010
    },
    {
        "protocol": "TCP",
        "name": "raid-cc",
        "port": 2011
    },
    {
        "protocol": "TCP",
        "name": "ttyinfo",
        "port": 2012
    },
    {
        "protocol": "TCP",
        "name": "raid-am",
        "port": 2013
    },
    {
        "protocol": "TCP",
        "name": "troff",
        "port": 2014
    },
    {
        "protocol": "TCP",
        "name": "cypress",
        "port": 2015
    },
    {
        "protocol": "TCP",
        "name": "bootserver",
        "port": 2016
    },
    {
        "protocol": "TCP",
        "name": "cypress-stat",
        "port": 2017
    },
    {
        "protocol": "TCP",
        "name": "terminaldb",
        "port": 2018
    },
    {
        "protocol": "TCP",
        "name": "whosockami",
        "port": 2019
    },
    {
        "protocol": "TCP",
        "name": "xinupageserver",
        "port": 2020
    },
    {
        "protocol": "TCP",
        "name": "servexec",
        "port": 2021
    },
    {
        "protocol": "TCP",
        "name": "down",
        "port": 2022
    },
    {
        "protocol": "TCP",
        "name": "xinuexpansion3",
        "port": 2023
    },
    {
        "protocol": "TCP",
        "name": "xinuexpansion4",
        "port": 2024
    },
    {
        "protocol": "TCP",
        "name": "ellpack",
        "port": 2025
    },
    {
        "protocol": "TCP",
        "name": "scrabble",
        "port": 2026
    },
    {
        "protocol": "TCP",
        "name": "shadowserver",
        "port": 2027
    },
    {
        "protocol": "TCP",
        "name": "submitserver",
        "port": 2028
    },
    {
        "protocol": "TCP",
        "name": "hsrpv6",
        "port": 2029
    },
    {
        "protocol": "TCP",
        "name": "device2",
        "port": 2030
    },
    {
        "protocol": "TCP",
        "name": "mobrien-chat",
        "port": 2031
    },
    {
        "protocol": "TCP",
        "name": "blackboard",
        "port": 2032
    },
    {
        "protocol": "TCP",
        "name": "glogger",
        "port": 2033
    },
    {
        "protocol": "TCP",
        "name": "scoremgr",
        "port": 2034
    },
    {
        "protocol": "TCP",
        "name": "imsldoc",
        "port": 2035
    },
    {
        "protocol": "TCP",
        "name": "e-dpnet",
        "port": 2036
    },
    {
        "protocol": "TCP",
        "name": "p2plus",
        "port": 2037
    },
    {
        "protocol": "TCP",
        "name": "objectmanager",
        "port": 2038
    },
    {
        "protocol": "TCP",
        "name": "prizma",
        "port": 2039
    },
    {
        "protocol": "TCP",
        "name": "lam",
        "port": 2040
    },
    {
        "protocol": "TCP",
        "name": "interbase",
        "port": 2041
    },
    {
        "protocol": "TCP",
        "name": "isis",
        "port": 2042
    },
    {
        "protocol": "TCP",
        "name": "isis-bcast",
        "port": 2043
    },
    {
        "protocol": "TCP",
        "name": "rimsl",
        "port": 2044
    },
    {
        "protocol": "TCP",
        "name": "cdfunc",
        "port": 2045
    },
    {
        "protocol": "TCP",
        "name": "sdfunc",
        "port": 2046
    },
    {
        "protocol": "TCP",
        "name": "dls",
        "port": 2047
    },
    {
        "protocol": "TCP",
        "name": "dls-monitor",
        "port": 2048
    },
    {
        "protocol": "TCP",
        "name": "nfs",
        "port": 2049
    },
    {
        "protocol": "TCP",
        "name": "av-emb-config",
        "port": 2050
    },
    {
        "protocol": "TCP",
        "name": "epnsdp",
        "port": 2051
    },
    {
        "protocol": "TCP",
        "name": "clearvisn",
        "port": 2052
    },
    {
        "protocol": "TCP",
        "name": "lot105-ds-upd",
        "port": 2053
    },
    {
        "protocol": "TCP",
        "name": "weblogin",
        "port": 2054
    },
    {
        "protocol": "TCP",
        "name": "iop",
        "port": 2055
    },
    {
        "protocol": "TCP",
        "name": "omnisky",
        "port": 2056
    },
    {
        "protocol": "TCP",
        "name": "rich-cp",
        "port": 2057
    },
    {
        "protocol": "TCP",
        "name": "newwavesearch",
        "port": 2058
    },
    {
        "protocol": "TCP",
        "name": "bmc-messaging",
        "port": 2059
    },
    {
        "protocol": "TCP",
        "name": "teleniumdaemon",
        "port": 2060
    },
    {
        "protocol": "TCP",
        "name": "netmount",
        "port": 2061
    },
    {
        "protocol": "TCP",
        "name": "icg-swp",
        "port": 2062
    },
    {
        "protocol": "TCP",
        "name": "icg-bridge",
        "port": 2063
    },
    {
        "protocol": "TCP",
        "name": "icg-iprelay",
        "port": 2064
    },
    {
        "protocol": "TCP",
        "name": "dlsrpn",
        "port": 2065
    },
    {
        "protocol": "TCP",
        "name": "dlswpn",
        "port": 2067
    },
    {
        "protocol": "TCP",
        "name": "avauthsrvprtcl",
        "port": 2068
    },
    {
        "protocol": "TCP",
        "name": "event-port",
        "port": 2069
    },
    {
        "protocol": "TCP",
        "name": "ah-esp-encap",
        "port": 2070
    },
    {
        "protocol": "TCP",
        "name": "acp-port",
        "port": 2071
    },
    {
        "protocol": "TCP",
        "name": "msync",
        "port": 2072
    },
    {
        "protocol": "TCP",
        "name": "gxs-data-port",
        "port": 2073
    },
    {
        "protocol": "TCP",
        "name": "vrtl-vmf-sa",
        "port": 2074
    },
    {
        "protocol": "TCP",
        "name": "newlixengine",
        "port": 2075
    },
    {
        "protocol": "TCP",
        "name": "newlixconfig",
        "port": 2076
    },
    {
        "protocol": "TCP",
        "name": "trellisagt",
        "port": 2077
    },
    {
        "protocol": "TCP",
        "name": "trellissvr",
        "port": 2078
    },
    {
        "protocol": "TCP",
        "name": "idware-router",
        "port": 2079
    },
    {
        "protocol": "TCP",
        "name": "autodesk-nlm",
        "port": 2080
    },
    {
        "protocol": "TCP",
        "name": "kme-trap-port",
        "port": 2081
    },
    {
        "protocol": "TCP",
        "name": "infowave",
        "port": 2082
    },
    {
        "protocol": "TCP",
        "name": "radsec",
        "port": 2083
    },
    {
        "protocol": "TCP",
        "name": "sunclustergeo",
        "port": 2084
    },
    {
        "protocol": "TCP",
        "name": "ada-cip",
        "port": 2085
    },
    {
        "protocol": "TCP",
        "name": "gnunet",
        "port": 2086
    },
    {
        "protocol": "TCP",
        "name": "eli",
        "port": 2087
    },
    {
        "protocol": "TCP",
        "name": "ip-blf",
        "port": 2088
    },
    {
        "protocol": "TCP",
        "name": "sep",
        "port": 2089
    },
    {
        "protocol": "TCP",
        "name": "lrp",
        "port": 2090
    },
    {
        "protocol": "TCP",
        "name": "prp",
        "port": 2091
    },
    {
        "protocol": "TCP",
        "name": "descent3",
        "port": 2092
    },
    {
        "protocol": "TCP",
        "name": "nbx-cc",
        "port": 2093
    },
    {
        "protocol": "TCP",
        "name": "nbx-au",
        "port": 2094
    },
    {
        "protocol": "TCP",
        "name": "nbx-ser",
        "port": 2095
    },
    {
        "protocol": "TCP",
        "name": "nbx-dir",
        "port": 2096
    },
    {
        "protocol": "TCP",
        "name": "jetformpreview",
        "port": 2097
    },
    {
        "protocol": "TCP",
        "name": "dialog-port",
        "port": 2098
    },
    {
        "protocol": "TCP",
        "name": "h2250-annex-g",
        "port": 2099
    },
    {
        "protocol": "TCP",
        "name": "amiganetfs",
        "port": 2100
    },
    {
        "protocol": "TCP",
        "name": "rtcm-sc104",
        "port": 2101
    },
    {
        "protocol": "TCP",
        "name": "zephyr-srv",
        "port": 2102
    },
    {
        "protocol": "TCP",
        "name": "zephyr-clt",
        "port": 2103
    },
    {
        "protocol": "TCP",
        "name": "zephyr-hm",
        "port": 2104
    },
    {
        "protocol": "TCP",
        "name": "minipay",
        "port": 2105
    },
    {
        "protocol": "TCP",
        "name": "mzap",
        "port": 2106
    },
    {
        "protocol": "TCP",
        "name": "bintec-admin",
        "port": 2107
    },
    {
        "protocol": "TCP",
        "name": "comcam",
        "port": 2108
    },
    {
        "protocol": "TCP",
        "name": "ergolight",
        "port": 2109
    },
    {
        "protocol": "TCP",
        "name": "umsp",
        "port": 2110
    },
    {
        "protocol": "TCP",
        "name": "dsatp",
        "port": 2111
    },
    {
        "protocol": "TCP",
        "name": "idonix-metanet",
        "port": 2112
    },
    {
        "protocol": "TCP",
        "name": "hsl-storm",
        "port": 2113
    },
    {
        "protocol": "TCP",
        "name": "newheights",
        "port": 2114
    },
    {
        "protocol": "TCP",
        "name": "kdm",
        "port": 2115
    },
    {
        "protocol": "TCP",
        "name": "ccowcmr",
        "port": 2116
    },
    {
        "protocol": "TCP",
        "name": "mentaclient",
        "port": 2117
    },
    {
        "protocol": "TCP",
        "name": "mentaserver",
        "port": 2118
    },
    {
        "protocol": "TCP",
        "name": "gsigatekeeper",
        "port": 2119
    },
    {
        "protocol": "TCP",
        "name": "qencp",
        "port": 2120
    },
    {
        "protocol": "TCP",
        "name": "scientia-ssdb",
        "port": 2121
    },
    {
        "protocol": "TCP",
        "name": "caupc-remote",
        "port": 2122
    },
    {
        "protocol": "TCP",
        "name": "gtp-control",
        "port": 2123
    },
    {
        "protocol": "TCP",
        "name": "elatelink",
        "port": 2124
    },
    {
        "protocol": "TCP",
        "name": "lockstep",
        "port": 2125
    },
    {
        "protocol": "TCP",
        "name": "pktcable-cops",
        "port": 2126
    },
    {
        "protocol": "TCP",
        "name": "index-pc-wb",
        "port": 2127
    },
    {
        "protocol": "TCP",
        "name": "net-steward",
        "port": 2128
    },
    {
        "protocol": "TCP",
        "name": "cs-live",
        "port": 2129
    },
    {
        "protocol": "TCP",
        "name": "swc-xds",
        "port": 2130
    },
    {
        "protocol": "TCP",
        "name": "avantageb2b",
        "port": 2131
    },
    {
        "protocol": "TCP",
        "name": "avail-epmap",
        "port": 2132
    },
    {
        "protocol": "TCP",
        "name": "zymed-zpp",
        "port": 2133
    },
    {
        "protocol": "TCP",
        "name": "avenue",
        "port": 2134
    },
    {
        "protocol": "TCP",
        "name": "gris",
        "port": 2135
    },
    {
        "protocol": "TCP",
        "name": "appworxsrv",
        "port": 2136
    },
    {
        "protocol": "TCP",
        "name": "connect",
        "port": 2137
    },
    {
        "protocol": "TCP",
        "name": "unbind-cluster",
        "port": 2138
    },
    {
        "protocol": "TCP",
        "name": "ias-auth",
        "port": 2139
    },
    {
        "protocol": "TCP",
        "name": "ias-reg",
        "port": 2140
    },
    {
        "protocol": "TCP",
        "name": "ias-admind",
        "port": 2141
    },
    {
        "protocol": "TCP",
        "name": "tdm-over-ip",
        "port": 2142
    },
    {
        "protocol": "TCP",
        "name": "lv-jc",
        "port": 2143
    },
    {
        "protocol": "TCP",
        "name": "lv-ffx",
        "port": 2144
    },
    {
        "protocol": "TCP",
        "name": "lv-pici",
        "port": 2145
    },
    {
        "protocol": "TCP",
        "name": "lv-not",
        "port": 2146
    },
    {
        "protocol": "TCP",
        "name": "lv-auth",
        "port": 2147
    },
    {
        "protocol": "TCP",
        "name": "veritas-ucl",
        "port": 2148
    },
    {
        "protocol": "TCP",
        "name": "acptsys",
        "port": 2149
    },
    {
        "protocol": "TCP",
        "name": "dynamic3d",
        "port": 2150
    },
    {
        "protocol": "TCP",
        "name": "docent",
        "port": 2151
    },
    {
        "protocol": "TCP",
        "name": "gtp-user",
        "port": 2152
    },
    {
        "protocol": "TCP",
        "name": "gdbremote",
        "port": 2159
    },
    {
        "protocol": "TCP",
        "name": "apc-2160",
        "port": 2160
    },
    {
        "protocol": "TCP",
        "name": "apc-2161",
        "port": 2161
    },
    {
        "protocol": "TCP",
        "name": "navisphere",
        "port": 2162
    },
    {
        "protocol": "TCP",
        "name": "navisphere-sec",
        "port": 2163
    },
    {
        "protocol": "TCP",
        "name": "ddns-v3",
        "port": 2164
    },
    {
        "protocol": "TCP",
        "name": "x-bone-api",
        "port": 2165
    },
    {
        "protocol": "TCP",
        "name": "iwserver",
        "port": 2166
    },
    {
        "protocol": "TCP",
        "name": "raw-serial",
        "port": 2167
    },
    {
        "protocol": "TCP",
        "name": "easy-soft-mux",
        "port": 2168
    },
    {
        "protocol": "TCP",
        "name": "archisfcp",
        "port": 2169
    },
    {
        "protocol": "TCP",
        "name": "eyetv",
        "port": 2170
    },
    {
        "protocol": "TCP",
        "name": "msfw-storage",
        "port": 2171
    },
    {
        "protocol": "TCP",
        "name": "msfw-s-storage",
        "port": 2172
    },
    {
        "protocol": "TCP",
        "name": "msfw-replica",
        "port": 2173
    },
    {
        "protocol": "TCP",
        "name": "msfw-array",
        "port": 2174
    },
    {
        "protocol": "TCP",
        "name": "airsync",
        "port": 2175
    },
    {
        "protocol": "TCP",
        "name": "rapi",
        "port": 2176
    },
    {
        "protocol": "TCP",
        "name": "qwave",
        "port": 2177
    },
    {
        "protocol": "TCP",
        "name": "bitspeer",
        "port": 2178
    },
    {
        "protocol": "TCP",
        "name": "mc-gt-srv",
        "port": 2180
    },
    {
        "protocol": "TCP",
        "name": "eforward",
        "port": 2181
    },
    {
        "protocol": "TCP",
        "name": "cgn-stat",
        "port": 2182
    },
    {
        "protocol": "TCP",
        "name": "cgn-config",
        "port": 2183
    },
    {
        "protocol": "TCP",
        "name": "nvd",
        "port": 2184
    },
    {
        "protocol": "TCP",
        "name": "onbase-dds",
        "port": 2185
    },
    {
        "protocol": "TCP",
        "name": "tivoconnect",
        "port": 2190
    },
    {
        "protocol": "TCP",
        "name": "tvbus",
        "port": 2191
    },
    {
        "protocol": "TCP",
        "name": "asdis",
        "port": 2192
    },
    {
        "protocol": "TCP",
        "name": "mnp-exchange",
        "port": 2197
    },
    {
        "protocol": "TCP",
        "name": "onehome-remote",
        "port": 2198
    },
    {
        "protocol": "TCP",
        "name": "onehome-help",
        "port": 2199
    },
    {
        "protocol": "TCP",
        "name": "ici",
        "port": 2200
    },
    {
        "protocol": "TCP",
        "name": "ats",
        "port": 2201
    },
    {
        "protocol": "TCP",
        "name": "imtc-map",
        "port": 2202
    },
    {
        "protocol": "TCP",
        "name": "b2-runtime",
        "port": 2203
    },
    {
        "protocol": "TCP",
        "name": "b2-license",
        "port": 2204
    },
    {
        "protocol": "TCP",
        "name": "jps",
        "port": 2205
    },
    {
        "protocol": "TCP",
        "name": "hpocbus",
        "port": 2206
    },
    {
        "protocol": "TCP",
        "name": "kali",
        "port": 2213
    },
    {
        "protocol": "TCP",
        "name": "rpi",
        "port": 2214
    },
    {
        "protocol": "TCP",
        "name": "ipcore",
        "port": 2215
    },
    {
        "protocol": "TCP",
        "name": "vtu-comms",
        "port": 2216
    },
    {
        "protocol": "TCP",
        "name": "gotodevice",
        "port": 2217
    },
    {
        "protocol": "TCP",
        "name": "bounzza",
        "port": 2218
    },
    {
        "protocol": "TCP",
        "name": "netiq-ncap",
        "port": 2219
    },
    {
        "protocol": "TCP",
        "name": "netiq",
        "port": 2220
    },
    {
        "protocol": "TCP",
        "name": "rockwell-csp1",
        "port": 2221
    },
    {
        "protocol": "TCP",
        "name": "rockwell-csp2",
        "port": 2222
    },
    {
        "protocol": "TCP",
        "name": "rockwell-csp3",
        "port": 2223
    },
    {
        "protocol": "TCP",
        "name": "di-drm",
        "port": 2226
    },
    {
        "protocol": "TCP",
        "name": "di-msg",
        "port": 2227
    },
    {
        "protocol": "TCP",
        "name": "ehome-ms",
        "port": 2228
    },
    {
        "protocol": "TCP",
        "name": "datalens",
        "port": 2229
    },
    {
        "protocol": "TCP",
        "name": "ivs-video",
        "port": 2232
    },
    {
        "protocol": "TCP",
        "name": "infocrypt",
        "port": 2233
    },
    {
        "protocol": "TCP",
        "name": "directplay",
        "port": 2234
    },
    {
        "protocol": "TCP",
        "name": "sercomm-wlink",
        "port": 2235
    },
    {
        "protocol": "TCP",
        "name": "nani",
        "port": 2236
    },
    {
        "protocol": "TCP",
        "name": "optech-port1-lm",
        "port": 2237
    },
    {
        "protocol": "TCP",
        "name": "aviva-sna",
        "port": 2238
    },
    {
        "protocol": "TCP",
        "name": "imagequery",
        "port": 2239
    },
    {
        "protocol": "TCP",
        "name": "recipe",
        "port": 2240
    },
    {
        "protocol": "TCP",
        "name": "ivsd",
        "port": 2241
    },
    {
        "protocol": "TCP",
        "name": "foliocorp",
        "port": 2242
    },
    {
        "protocol": "TCP",
        "name": "magicom",
        "port": 2243
    },
    {
        "protocol": "TCP",
        "name": "nmsserver",
        "port": 2244
    },
    {
        "protocol": "TCP",
        "name": "hao",
        "port": 2245
    },
    {
        "protocol": "TCP",
        "name": "pc-mta-addrmap",
        "port": 2246
    },
    {
        "protocol": "TCP",
        "name": "antidotemgrsvr",
        "port": 2247
    },
    {
        "protocol": "TCP",
        "name": "ums",
        "port": 2248
    },
    {
        "protocol": "TCP",
        "name": "rfmp",
        "port": 2249
    },
    {
        "protocol": "TCP",
        "name": "remote-collab",
        "port": 2250
    },
    {
        "protocol": "TCP",
        "name": "dif-port",
        "port": 2251
    },
    {
        "protocol": "TCP",
        "name": "njenet-ssl",
        "port": 2252
    },
    {
        "protocol": "TCP",
        "name": "dtv-chan-req",
        "port": 2253
    },
    {
        "protocol": "TCP",
        "name": "seispoc",
        "port": 2254
    },
    {
        "protocol": "TCP",
        "name": "vrtp",
        "port": 2255
    },
    {
        "protocol": "TCP",
        "name": "pcc-mfp",
        "port": 2256
    },
    {
        "protocol": "TCP",
        "name": "apc-2260",
        "port": 2260
    },
    {
        "protocol": "TCP",
        "name": "comotionmaster",
        "port": 2261
    },
    {
        "protocol": "TCP",
        "name": "comotionback",
        "port": 2262
    },
    {
        "protocol": "TCP",
        "name": "mfserver",
        "port": 2266
    },
    {
        "protocol": "TCP",
        "name": "ontobroker",
        "port": 2267
    },
    {
        "protocol": "TCP",
        "name": "amt",
        "port": 2268
    },
    {
        "protocol": "TCP",
        "name": "mikey",
        "port": 2269
    },
    {
        "protocol": "TCP",
        "name": "starschool",
        "port": 2270
    },
    {
        "protocol": "TCP",
        "name": "mmcals",
        "port": 2271
    },
    {
        "protocol": "TCP",
        "name": "mmcal",
        "port": 2272
    },
    {
        "protocol": "TCP",
        "name": "mysql-im",
        "port": 2273
    },
    {
        "protocol": "TCP",
        "name": "pcttunnell",
        "port": 2274
    },
    {
        "protocol": "TCP",
        "name": "ibridge-data",
        "port": 2275
    },
    {
        "protocol": "TCP",
        "name": "ibridge-mgmt",
        "port": 2276
    },
    {
        "protocol": "TCP",
        "name": "bluectrlproxy",
        "port": 2277
    },
    {
        "protocol": "TCP",
        "name": "xmquery",
        "port": 2279
    },
    {
        "protocol": "TCP",
        "name": "lnvpoller",
        "port": 2280
    },
    {
        "protocol": "TCP",
        "name": "lnvconsole",
        "port": 2281
    },
    {
        "protocol": "TCP",
        "name": "lnvalarm",
        "port": 2282
    },
    {
        "protocol": "TCP",
        "name": "lnvstatus",
        "port": 2283
    },
    {
        "protocol": "TCP",
        "name": "lnvmaps",
        "port": 2284
    },
    {
        "protocol": "TCP",
        "name": "lnvmailmon",
        "port": 2285
    },
    {
        "protocol": "TCP",
        "name": "nas-metering",
        "port": 2286
    },
    {
        "protocol": "TCP",
        "name": "dna",
        "port": 2287
    },
    {
        "protocol": "TCP",
        "name": "netml",
        "port": 2288
    },
    {
        "protocol": "TCP",
        "name": "dict-lookup",
        "port": 2289
    },
    {
        "protocol": "TCP",
        "name": "sonus-logging",
        "port": 2290
    },
    {
        "protocol": "TCP",
        "name": "konshus-lm",
        "port": 2294
    },
    {
        "protocol": "TCP",
        "name": "advant-lm",
        "port": 2295
    },
    {
        "protocol": "TCP",
        "name": "theta-lm",
        "port": 2296
    },
    {
        "protocol": "TCP",
        "name": "d2k-datamover1",
        "port": 2297
    },
    {
        "protocol": "TCP",
        "name": "d2k-datamover2",
        "port": 2298
    },
    {
        "protocol": "TCP",
        "name": "pc-telecommute",
        "port": 2299
    },
    {
        "protocol": "TCP",
        "name": "cvmmon",
        "port": 2300
    },
    {
        "protocol": "TCP",
        "name": "cpq-wbem",
        "port": 2301
    },
    {
        "protocol": "TCP",
        "name": "binderysupport",
        "port": 2302
    },
    {
        "protocol": "TCP",
        "name": "proxy-gateway",
        "port": 2303
    },
    {
        "protocol": "TCP",
        "name": "attachmate-uts",
        "port": 2304
    },
    {
        "protocol": "TCP",
        "name": "mt-scaleserver",
        "port": 2305
    },
    {
        "protocol": "TCP",
        "name": "tappi-boxnet",
        "port": 2306
    },
    {
        "protocol": "TCP",
        "name": "pehelp",
        "port": 2307
    },
    {
        "protocol": "TCP",
        "name": "sdhelp",
        "port": 2308
    },
    {
        "protocol": "TCP",
        "name": "sdserver",
        "port": 2309
    },
    {
        "protocol": "TCP",
        "name": "sdclient",
        "port": 2310
    },
    {
        "protocol": "TCP",
        "name": "messageservice",
        "port": 2311
    },
    {
        "protocol": "TCP",
        "name": "iapp",
        "port": 2313
    },
    {
        "protocol": "TCP",
        "name": "cr-websystems",
        "port": 2314
    },
    {
        "protocol": "TCP",
        "name": "precise-sft",
        "port": 2315
    },
    {
        "protocol": "TCP",
        "name": "sent-lm",
        "port": 2316
    },
    {
        "protocol": "TCP",
        "name": "attachmate-g32",
        "port": 2317
    },
    {
        "protocol": "TCP",
        "name": "cadencecontrol",
        "port": 2318
    },
    {
        "protocol": "TCP",
        "name": "infolibria",
        "port": 2319
    },
    {
        "protocol": "TCP",
        "name": "siebel-ns",
        "port": 2320
    },
    {
        "protocol": "TCP",
        "name": "rdlap",
        "port": 2321
    },
    {
        "protocol": "TCP",
        "name": "ofsd",
        "port": 2322
    },
    {
        "protocol": "TCP",
        "name": "3d-nfsd",
        "port": 2323
    },
    {
        "protocol": "TCP",
        "name": "cosmocall",
        "port": 2324
    },
    {
        "protocol": "TCP",
        "name": "designspace-lm",
        "port": 2325
    },
    {
        "protocol": "TCP",
        "name": "idcp",
        "port": 2326
    },
    {
        "protocol": "TCP",
        "name": "xingcsm",
        "port": 2327
    },
    {
        "protocol": "TCP",
        "name": "netrix-sftm",
        "port": 2328
    },
    {
        "protocol": "TCP",
        "name": "nvd",
        "port": 2329
    },
    {
        "protocol": "TCP",
        "name": "tscchat",
        "port": 2330
    },
    {
        "protocol": "TCP",
        "name": "agentview",
        "port": 2331
    },
    {
        "protocol": "TCP",
        "name": "rcc-host",
        "port": 2332
    },
    {
        "protocol": "TCP",
        "name": "snapp",
        "port": 2333
    },
    {
        "protocol": "TCP",
        "name": "ace-client",
        "port": 2334
    },
    {
        "protocol": "TCP",
        "name": "ace-proxy",
        "port": 2335
    },
    {
        "protocol": "TCP",
        "name": "appleugcontrol",
        "port": 2336
    },
    {
        "protocol": "TCP",
        "name": "ideesrv",
        "port": 2337
    },
    {
        "protocol": "TCP",
        "name": "norton-lambert",
        "port": 2338
    },
    {
        "protocol": "TCP",
        "name": "3com-webview",
        "port": 2339
    },
    {
        "protocol": "TCP",
        "name": "wrs_registry",
        "port": 2340
    },
    {
        "protocol": "TCP",
        "name": "xiostatus",
        "port": 2341
    },
    {
        "protocol": "TCP",
        "name": "manage-exec",
        "port": 2342
    },
    {
        "protocol": "TCP",
        "name": "nati-logos",
        "port": 2343
    },
    {
        "protocol": "TCP",
        "name": "fcmsys",
        "port": 2344
    },
    {
        "protocol": "TCP",
        "name": "dbm",
        "port": 2345
    },
    {
        "protocol": "TCP",
        "name": "redstorm_join",
        "port": 2346
    },
    {
        "protocol": "TCP",
        "name": "redstorm_find",
        "port": 2347
    },
    {
        "protocol": "TCP",
        "name": "redstorm_info",
        "port": 2348
    },
    {
        "protocol": "TCP",
        "name": "redstorm_diag",
        "port": 2349
    },
    {
        "protocol": "TCP",
        "name": "psbserver",
        "port": 2350
    },
    {
        "protocol": "TCP",
        "name": "psrserver",
        "port": 2351
    },
    {
        "protocol": "TCP",
        "name": "pslserver",
        "port": 2352
    },
    {
        "protocol": "TCP",
        "name": "pspserver",
        "port": 2353
    },
    {
        "protocol": "TCP",
        "name": "psprserver",
        "port": 2354
    },
    {
        "protocol": "TCP",
        "name": "psdbserver",
        "port": 2355
    },
    {
        "protocol": "TCP",
        "name": "gxtelmd",
        "port": 2356
    },
    {
        "protocol": "TCP",
        "name": "unihub-server",
        "port": 2357
    },
    {
        "protocol": "TCP",
        "name": "futrix",
        "port": 2358
    },
    {
        "protocol": "TCP",
        "name": "flukeserver",
        "port": 2359
    },
    {
        "protocol": "TCP",
        "name": "nexstorindltd",
        "port": 2360
    },
    {
        "protocol": "TCP",
        "name": "tl1",
        "port": 2361
    },
    {
        "protocol": "TCP",
        "name": "digiman",
        "port": 2362
    },
    {
        "protocol": "TCP",
        "name": "mediacntrlnfsd",
        "port": 2363
    },
    {
        "protocol": "TCP",
        "name": "oi-2000",
        "port": 2364
    },
    {
        "protocol": "TCP",
        "name": "dbref",
        "port": 2365
    },
    {
        "protocol": "TCP",
        "name": "qip-login",
        "port": 2366
    },
    {
        "protocol": "TCP",
        "name": "service-ctrl",
        "port": 2367
    },
    {
        "protocol": "TCP",
        "name": "opentable",
        "port": 2368
    },
    {
        "protocol": "TCP",
        "name": "acs2000-dsp",
        "port": 2369
    },
    {
        "protocol": "TCP",
        "name": "l3-hbmon",
        "port": 2370
    },
    {
        "protocol": "TCP",
        "name": "worldwire",
        "port": 2371
    },
    {
        "protocol": "TCP",
        "name": "compaq-https",
        "port": 2381
    },
    {
        "protocol": "TCP",
        "name": "ms-olap3",
        "port": 2382
    },
    {
        "protocol": "TCP",
        "name": "ms-olap4",
        "port": 2383
    },
    {
        "protocol": "TCP",
        "name": "sd-request",
        "port": 2384
    },
    {
        "protocol": "TCP",
        "name": "sd-data",
        "port": 2385
    },
    {
        "protocol": "TCP",
        "name": "virtualtape",
        "port": 2386
    },
    {
        "protocol": "TCP",
        "name": "vsamredirector",
        "port": 2387
    },
    {
        "protocol": "TCP",
        "name": "mynahautostart",
        "port": 2388
    },
    {
        "protocol": "TCP",
        "name": "ovsessionmgr",
        "port": 2389
    },
    {
        "protocol": "TCP",
        "name": "rsmtp",
        "port": 2390
    },
    {
        "protocol": "TCP",
        "name": "3com-net-mgmt",
        "port": 2391
    },
    {
        "protocol": "TCP",
        "name": "tacticalauth",
        "port": 2392
    },
    {
        "protocol": "TCP",
        "name": "ms-olap1",
        "port": 2393
    },
    {
        "protocol": "TCP",
        "name": "ms-olap2",
        "port": 2394
    },
    {
        "protocol": "TCP",
        "name": "lan900_remote",
        "port": 2395
    },
    {
        "protocol": "TCP",
        "name": "wusage",
        "port": 2396
    },
    {
        "protocol": "TCP",
        "name": "ncl",
        "port": 2397
    },
    {
        "protocol": "TCP",
        "name": "orbiter",
        "port": 2398
    },
    {
        "protocol": "TCP",
        "name": "fmpro-fdal",
        "port": 2399
    },
    {
        "protocol": "TCP",
        "name": "opequus-server",
        "port": 2400
    },
    {
        "protocol": "TCP",
        "name": "cvspserver",
        "port": 2401
    },
    {
        "protocol": "TCP",
        "name": "taskmaster2000",
        "port": 2402
    },
    {
        "protocol": "TCP",
        "name": "taskmaster2000",
        "port": 2403
    },
    {
        "protocol": "TCP",
        "name": "iec-104",
        "port": 2404
    },
    {
        "protocol": "TCP",
        "name": "trc-netpoll",
        "port": 2405
    },
    {
        "protocol": "TCP",
        "name": "jediserver",
        "port": 2406
    },
    {
        "protocol": "TCP",
        "name": "orion",
        "port": 2407
    },
    {
        "protocol": "TCP",
        "name": "optimanet",
        "port": 2408
    },
    {
        "protocol": "TCP",
        "name": "sns-protocol",
        "port": 2409
    },
    {
        "protocol": "TCP",
        "name": "vrts-registry",
        "port": 2410
    },
    {
        "protocol": "TCP",
        "name": "netwave-ap-mgmt",
        "port": 2411
    },
    {
        "protocol": "TCP",
        "name": "cdn",
        "port": 2412
    },
    {
        "protocol": "TCP",
        "name": "orion-rmi-reg",
        "port": 2413
    },
    {
        "protocol": "TCP",
        "name": "beeyond",
        "port": 2414
    },
    {
        "protocol": "TCP",
        "name": "codima-rtp",
        "port": 2415
    },
    {
        "protocol": "TCP",
        "name": "rmtserver",
        "port": 2416
    },
    {
        "protocol": "TCP",
        "name": "composit-server",
        "port": 2417
    },
    {
        "protocol": "TCP",
        "name": "cas",
        "port": 2418
    },
    {
        "protocol": "TCP",
        "name": "attachmate-s2s",
        "port": 2419
    },
    {
        "protocol": "TCP",
        "name": "dslremote-mgmt",
        "port": 2420
    },
    {
        "protocol": "TCP",
        "name": "g-talk",
        "port": 2421
    },
    {
        "protocol": "TCP",
        "name": "crmsbits",
        "port": 2422
    },
    {
        "protocol": "TCP",
        "name": "rnrp",
        "port": 2423
    },
    {
        "protocol": "TCP",
        "name": "kofax-svr",
        "port": 2424
    },
    {
        "protocol": "TCP",
        "name": "fjitsuappmgr",
        "port": 2425
    },
    {
        "protocol": "TCP",
        "name": "mgcp-gateway",
        "port": 2427
    },
    {
        "protocol": "TCP",
        "name": "ott",
        "port": 2428
    },
    {
        "protocol": "TCP",
        "name": "ft-role",
        "port": 2429
    },
    {
        "protocol": "TCP",
        "name": "venus",
        "port": 2430
    },
    {
        "protocol": "TCP",
        "name": "venus-se",
        "port": 2431
    },
    {
        "protocol": "TCP",
        "name": "codasrv",
        "port": 2432
    },
    {
        "protocol": "TCP",
        "name": "codasrv-se",
        "port": 2433
    },
    {
        "protocol": "TCP",
        "name": "pxc-epmap",
        "port": 2434
    },
    {
        "protocol": "TCP",
        "name": "optilogic",
        "port": 2435
    },
    {
        "protocol": "TCP",
        "name": "topx",
        "port": 2436
    },
    {
        "protocol": "TCP",
        "name": "unicontrol",
        "port": 2437
    },
    {
        "protocol": "TCP",
        "name": "msp",
        "port": 2438
    },
    {
        "protocol": "TCP",
        "name": "sybasedbsynch",
        "port": 2439
    },
    {
        "protocol": "TCP",
        "name": "spearway",
        "port": 2440
    },
    {
        "protocol": "TCP",
        "name": "pvsw-inet",
        "port": 2441
    },
    {
        "protocol": "TCP",
        "name": "netangel",
        "port": 2442
    },
    {
        "protocol": "TCP",
        "name": "powerclientcsf",
        "port": 2443
    },
    {
        "protocol": "TCP",
        "name": "btpp2sectrans",
        "port": 2444
    },
    {
        "protocol": "TCP",
        "name": "dtn1",
        "port": 2445
    },
    {
        "protocol": "TCP",
        "name": "bues_service",
        "port": 2446
    },
    {
        "protocol": "TCP",
        "name": "ovwdb",
        "port": 2447
    },
    {
        "protocol": "TCP",
        "name": "hpppssvr",
        "port": 2448
    },
    {
        "protocol": "TCP",
        "name": "ratl",
        "port": 2449
    },
    {
        "protocol": "TCP",
        "name": "netadmin",
        "port": 2450
    },
    {
        "protocol": "TCP",
        "name": "netchat",
        "port": 2451
    },
    {
        "protocol": "TCP",
        "name": "snifferclient",
        "port": 2452
    },
    {
        "protocol": "TCP",
        "name": "madge-ltd",
        "port": 2453
    },
    {
        "protocol": "TCP",
        "name": "indx-dds",
        "port": 2454
    },
    {
        "protocol": "TCP",
        "name": "wago-io-system",
        "port": 2455
    },
    {
        "protocol": "TCP",
        "name": "altav-remmgt",
        "port": 2456
    },
    {
        "protocol": "TCP",
        "name": "rapido-ip",
        "port": 2457
    },
    {
        "protocol": "TCP",
        "name": "griffin",
        "port": 2458
    },
    {
        "protocol": "TCP",
        "name": "community",
        "port": 2459
    },
    {
        "protocol": "TCP",
        "name": "ms-theater",
        "port": 2460
    },
    {
        "protocol": "TCP",
        "name": "qadmifoper",
        "port": 2461
    },
    {
        "protocol": "TCP",
        "name": "qadmifevent",
        "port": 2462
    },
    {
        "protocol": "TCP",
        "name": "symbios-raid",
        "port": 2463
    },
    {
        "protocol": "TCP",
        "name": "direcpc-si",
        "port": 2464
    },
    {
        "protocol": "TCP",
        "name": "lbm",
        "port": 2465
    },
    {
        "protocol": "TCP",
        "name": "lbf",
        "port": 2466
    },
    {
        "protocol": "TCP",
        "name": "high-criteria",
        "port": 2467
    },
    {
        "protocol": "TCP",
        "name": "qip-msgd",
        "port": 2468
    },
    {
        "protocol": "TCP",
        "name": "mti-tcs-comm",
        "port": 2469
    },
    {
        "protocol": "TCP",
        "name": "taskman-port",
        "port": 2470
    },
    {
        "protocol": "TCP",
        "name": "seaodbc",
        "port": 2471
    },
    {
        "protocol": "TCP",
        "name": "c3",
        "port": 2472
    },
    {
        "protocol": "TCP",
        "name": "aker-cdp",
        "port": 2473
    },
    {
        "protocol": "TCP",
        "name": "vitalanalysis",
        "port": 2474
    },
    {
        "protocol": "TCP",
        "name": "ace-server",
        "port": 2475
    },
    {
        "protocol": "TCP",
        "name": "ace-svr-prop",
        "port": 2476
    },
    {
        "protocol": "TCP",
        "name": "ssm-cvs",
        "port": 2477
    },
    {
        "protocol": "TCP",
        "name": "ssm-cssps",
        "port": 2478
    },
    {
        "protocol": "TCP",
        "name": "ssm-els",
        "port": 2479
    },
    {
        "protocol": "TCP",
        "name": "lingwood",
        "port": 2480
    },
    {
        "protocol": "TCP",
        "name": "giop",
        "port": 2481
    },
    {
        "protocol": "TCP",
        "name": "giop-ssl",
        "port": 2482
    },
    {
        "protocol": "TCP",
        "name": "ttc",
        "port": 2483
    },
    {
        "protocol": "TCP",
        "name": "ttc-ssl",
        "port": 2484
    },
    {
        "protocol": "TCP",
        "name": "netobjects1",
        "port": 2485
    },
    {
        "protocol": "TCP",
        "name": "netobjects2",
        "port": 2486
    },
    {
        "protocol": "TCP",
        "name": "pns",
        "port": 2487
    },
    {
        "protocol": "TCP",
        "name": "moy-corp",
        "port": 2488
    },
    {
        "protocol": "TCP",
        "name": "tsilb",
        "port": 2489
    },
    {
        "protocol": "TCP",
        "name": "qip-qdhcp",
        "port": 2490
    },
    {
        "protocol": "TCP",
        "name": "conclave-cpp",
        "port": 2491
    },
    {
        "protocol": "TCP",
        "name": "groove",
        "port": 2492
    },
    {
        "protocol": "TCP",
        "name": "talarian-mqs",
        "port": 2493
    },
    {
        "protocol": "TCP",
        "name": "bmc-ar",
        "port": 2494
    },
    {
        "protocol": "TCP",
        "name": "fast-rem-serv",
        "port": 2495
    },
    {
        "protocol": "TCP",
        "name": "dirgis",
        "port": 2496
    },
    {
        "protocol": "TCP",
        "name": "quaddb",
        "port": 2497
    },
    {
        "protocol": "TCP",
        "name": "odn-castraq",
        "port": 2498
    },
    {
        "protocol": "TCP",
        "name": "unicontrol",
        "port": 2499
    },
    {
        "protocol": "TCP",
        "name": "rtsserv",
        "port": 2500
    },
    {
        "protocol": "TCP",
        "name": "rtsclient",
        "port": 2501
    },
    {
        "protocol": "TCP",
        "name": "kentrox-prot",
        "port": 2502
    },
    {
        "protocol": "TCP",
        "name": "nms-dpnss",
        "port": 2503
    },
    {
        "protocol": "TCP",
        "name": "wlbs",
        "port": 2504
    },
    {
        "protocol": "TCP",
        "name": "ppcontrol",
        "port": 2505
    },
    {
        "protocol": "TCP",
        "name": "jbroker",
        "port": 2506
    },
    {
        "protocol": "TCP",
        "name": "spock",
        "port": 2507
    },
    {
        "protocol": "TCP",
        "name": "jdatastore",
        "port": 2508
    },
    {
        "protocol": "TCP",
        "name": "fjmpss",
        "port": 2509
    },
    {
        "protocol": "TCP",
        "name": "fjappmgrbulk",
        "port": 2510
    },
    {
        "protocol": "TCP",
        "name": "metastorm",
        "port": 2511
    },
    {
        "protocol": "TCP",
        "name": "citrixima",
        "port": 2512
    },
    {
        "protocol": "TCP",
        "name": "citrixadmin",
        "port": 2513
    },
    {
        "protocol": "TCP",
        "name": "facsys-ntp",
        "port": 2514
    },
    {
        "protocol": "TCP",
        "name": "facsys-router",
        "port": 2515
    },
    {
        "protocol": "TCP",
        "name": "maincontrol",
        "port": 2516
    },
    {
        "protocol": "TCP",
        "name": "call-sig-trans",
        "port": 2517
    },
    {
        "protocol": "TCP",
        "name": "willy",
        "port": 2518
    },
    {
        "protocol": "TCP",
        "name": "globmsgsvc",
        "port": 2519
    },
    {
        "protocol": "TCP",
        "name": "pvsw",
        "port": 2520
    },
    {
        "protocol": "TCP",
        "name": "adaptecmgr",
        "port": 2521
    },
    {
        "protocol": "TCP",
        "name": "windb",
        "port": 2522
    },
    {
        "protocol": "TCP",
        "name": "qke-llc-v3",
        "port": 2523
    },
    {
        "protocol": "TCP",
        "name": "optiwave-lm",
        "port": 2524
    },
    {
        "protocol": "TCP",
        "name": "ms-v-worlds",
        "port": 2525
    },
    {
        "protocol": "TCP",
        "name": "ema-sent-lm",
        "port": 2526
    },
    {
        "protocol": "TCP",
        "name": "iqserver",
        "port": 2527
    },
    {
        "protocol": "TCP",
        "name": "ncr_ccl",
        "port": 2528
    },
    {
        "protocol": "TCP",
        "name": "utsftp",
        "port": 2529
    },
    {
        "protocol": "TCP",
        "name": "vrcommerce",
        "port": 2530
    },
    {
        "protocol": "TCP",
        "name": "ito-e-gui",
        "port": 2531
    },
    {
        "protocol": "TCP",
        "name": "ovtopmd",
        "port": 2532
    },
    {
        "protocol": "TCP",
        "name": "snifferserver",
        "port": 2533
    },
    {
        "protocol": "TCP",
        "name": "combox-web-acc",
        "port": 2534
    },
    {
        "protocol": "TCP",
        "name": "madcap",
        "port": 2535
    },
    {
        "protocol": "TCP",
        "name": "btpp2audctr1",
        "port": 2536
    },
    {
        "protocol": "TCP",
        "name": "upgrade",
        "port": 2537
    },
    {
        "protocol": "TCP",
        "name": "vnwk-prapi",
        "port": 2538
    },
    {
        "protocol": "TCP",
        "name": "vsiadmin",
        "port": 2539
    },
    {
        "protocol": "TCP",
        "name": "lonworks",
        "port": 2540
    },
    {
        "protocol": "TCP",
        "name": "lonworks2",
        "port": 2541
    },
    {
        "protocol": "TCP",
        "name": "davinci",
        "port": 2542
    },
    {
        "protocol": "TCP",
        "name": "reftek",
        "port": 2543
    },
    {
        "protocol": "TCP",
        "name": "novell-zen",
        "port": 2544
    },
    {
        "protocol": "TCP",
        "name": "sis-emt",
        "port": 2545
    },
    {
        "protocol": "TCP",
        "name": "vytalvaultbrtp",
        "port": 2546
    },
    {
        "protocol": "TCP",
        "name": "vytalvaultvsmp",
        "port": 2547
    },
    {
        "protocol": "TCP",
        "name": "vytalvaultpipe",
        "port": 2548
    },
    {
        "protocol": "TCP",
        "name": "ipass",
        "port": 2549
    },
    {
        "protocol": "TCP",
        "name": "ads",
        "port": 2550
    },
    {
        "protocol": "TCP",
        "name": "isg-uda-server",
        "port": 2551
    },
    {
        "protocol": "TCP",
        "name": "call-logging",
        "port": 2552
    },
    {
        "protocol": "TCP",
        "name": "efidiningport",
        "port": 2553
    },
    {
        "protocol": "TCP",
        "name": "vcnet-link-v10",
        "port": 2554
    },
    {
        "protocol": "TCP",
        "name": "compaq-wcp",
        "port": 2555
    },
    {
        "protocol": "TCP",
        "name": "nicetec-nmsvc",
        "port": 2556
    },
    {
        "protocol": "TCP",
        "name": "nicetec-mgmt",
        "port": 2557
    },
    {
        "protocol": "TCP",
        "name": "pclemultimedia",
        "port": 2558
    },
    {
        "protocol": "TCP",
        "name": "lstp",
        "port": 2559
    },
    {
        "protocol": "TCP",
        "name": "labrat",
        "port": 2560
    },
    {
        "protocol": "TCP",
        "name": "mosaixcc",
        "port": 2561
    },
    {
        "protocol": "TCP",
        "name": "delibo",
        "port": 2562
    },
    {
        "protocol": "TCP",
        "name": "cti-redwood",
        "port": 2563
    },
    {
        "protocol": "TCP",
        "name": "hp-3000-telnet",
        "port": 2564
    },
    {
        "protocol": "TCP",
        "name": "coord-svr",
        "port": 2565
    },
    {
        "protocol": "TCP",
        "name": "pcs-pcw",
        "port": 2566
    },
    {
        "protocol": "TCP",
        "name": "clp",
        "port": 2567
    },
    {
        "protocol": "TCP",
        "name": "spamtrap",
        "port": 2568
    },
    {
        "protocol": "TCP",
        "name": "sonuscallsig",
        "port": 2569
    },
    {
        "protocol": "TCP",
        "name": "hs-port",
        "port": 2570
    },
    {
        "protocol": "TCP",
        "name": "cecsvc",
        "port": 2571
    },
    {
        "protocol": "TCP",
        "name": "ibp",
        "port": 2572
    },
    {
        "protocol": "TCP",
        "name": "trustestablish",
        "port": 2573
    },
    {
        "protocol": "TCP",
        "name": "blockade-bpsp",
        "port": 2574
    },
    {
        "protocol": "TCP",
        "name": "hl7",
        "port": 2575
    },
    {
        "protocol": "TCP",
        "name": "tclprodebugger",
        "port": 2576
    },
    {
        "protocol": "TCP",
        "name": "scipticslsrvr",
        "port": 2577
    },
    {
        "protocol": "TCP",
        "name": "rvs-isdn-dcp",
        "port": 2578
    },
    {
        "protocol": "TCP",
        "name": "mpfoncl",
        "port": 2579
    },
    {
        "protocol": "TCP",
        "name": "tributary",
        "port": 2580
    },
    {
        "protocol": "TCP",
        "name": "argis-te",
        "port": 2581
    },
    {
        "protocol": "TCP",
        "name": "argis-ds",
        "port": 2582
    },
    {
        "protocol": "TCP",
        "name": "mon",
        "port": 2583
    },
    {
        "protocol": "TCP",
        "name": "cyaserv",
        "port": 2584
    },
    {
        "protocol": "TCP",
        "name": "netx-server",
        "port": 2585
    },
    {
        "protocol": "TCP",
        "name": "netx-agent",
        "port": 2586
    },
    {
        "protocol": "TCP",
        "name": "masc",
        "port": 2587
    },
    {
        "protocol": "TCP",
        "name": "privilege",
        "port": 2588
    },
    {
        "protocol": "TCP",
        "name": "quartus-tcl",
        "port": 2589
    },
    {
        "protocol": "TCP",
        "name": "idotdist",
        "port": 2590
    },
    {
        "protocol": "TCP",
        "name": "maytagshuffle",
        "port": 2591
    },
    {
        "protocol": "TCP",
        "name": "netrek",
        "port": 2592
    },
    {
        "protocol": "TCP",
        "name": "mns-mail",
        "port": 2593
    },
    {
        "protocol": "TCP",
        "name": "dts",
        "port": 2594
    },
    {
        "protocol": "TCP",
        "name": "worldfusion1",
        "port": 2595
    },
    {
        "protocol": "TCP",
        "name": "worldfusion2",
        "port": 2596
    },
    {
        "protocol": "TCP",
        "name": "homesteadglory",
        "port": 2597
    },
    {
        "protocol": "TCP",
        "name": "citriximaclient",
        "port": 2598
    },
    {
        "protocol": "TCP",
        "name": "snapd",
        "port": 2599
    },
    {
        "protocol": "TCP",
        "name": "hpstgmgr",
        "port": 2600
    },
    {
        "protocol": "TCP",
        "name": "discp-client",
        "port": 2601
    },
    {
        "protocol": "TCP",
        "name": "discp-server",
        "port": 2602
    },
    {
        "protocol": "TCP",
        "name": "servicemeter",
        "port": 2603
    },
    {
        "protocol": "TCP",
        "name": "nsc-ccs",
        "port": 2604
    },
    {
        "protocol": "TCP",
        "name": "nsc-posa",
        "port": 2605
    },
    {
        "protocol": "TCP",
        "name": "netmon",
        "port": 2606
    },
    {
        "protocol": "TCP",
        "name": "connection",
        "port": 2607
    },
    {
        "protocol": "TCP",
        "name": "wag-service",
        "port": 2608
    },
    {
        "protocol": "TCP",
        "name": "system-monitor",
        "port": 2609
    },
    {
        "protocol": "TCP",
        "name": "versa-tek",
        "port": 2610
    },
    {
        "protocol": "TCP",
        "name": "lionhead",
        "port": 2611
    },
    {
        "protocol": "TCP",
        "name": "qpasa-agent",
        "port": 2612
    },
    {
        "protocol": "TCP",
        "name": "smntubootstrap",
        "port": 2613
    },
    {
        "protocol": "TCP",
        "name": "neveroffline",
        "port": 2614
    },
    {
        "protocol": "TCP",
        "name": "firepower",
        "port": 2615
    },
    {
        "protocol": "TCP",
        "name": "appswitch-emp",
        "port": 2616
    },
    {
        "protocol": "TCP",
        "name": "cmadmin",
        "port": 2617
    },
    {
        "protocol": "TCP",
        "name": "priority-e-com",
        "port": 2618
    },
    {
        "protocol": "TCP",
        "name": "bruce",
        "port": 2619
    },
    {
        "protocol": "TCP",
        "name": "lpsrecommender",
        "port": 2620
    },
    {
        "protocol": "TCP",
        "name": "miles-apart",
        "port": 2621
    },
    {
        "protocol": "TCP",
        "name": "metricadbc",
        "port": 2622
    },
    {
        "protocol": "TCP",
        "name": "lmdp",
        "port": 2623
    },
    {
        "protocol": "TCP",
        "name": "aria",
        "port": 2624
    },
    {
        "protocol": "TCP",
        "name": "blwnkl-port",
        "port": 2625
    },
    {
        "protocol": "TCP",
        "name": "gbjd816",
        "port": 2626
    },
    {
        "protocol": "TCP",
        "name": "moshebeeri",
        "port": 2627
    },
    {
        "protocol": "TCP",
        "name": "dict",
        "port": 2628
    },
    {
        "protocol": "TCP",
        "name": "sitaraserver",
        "port": 2629
    },
    {
        "protocol": "TCP",
        "name": "sitaramgmt",
        "port": 2630
    },
    {
        "protocol": "TCP",
        "name": "sitaradir",
        "port": 2631
    },
    {
        "protocol": "TCP",
        "name": "irdg-post",
        "port": 2632
    },
    {
        "protocol": "TCP",
        "name": "interintelli",
        "port": 2633
    },
    {
        "protocol": "TCP",
        "name": "pk-electronics",
        "port": 2634
    },
    {
        "protocol": "TCP",
        "name": "backburner",
        "port": 2635
    },
    {
        "protocol": "TCP",
        "name": "solve",
        "port": 2636
    },
    {
        "protocol": "TCP",
        "name": "imdocsvc",
        "port": 2637
    },
    {
        "protocol": "TCP",
        "name": "sybaseanywhere",
        "port": 2638
    },
    {
        "protocol": "TCP",
        "name": "aminet",
        "port": 2639
    },
    {
        "protocol": "TCP",
        "name": "sai_sentlm",
        "port": 2640
    },
    {
        "protocol": "TCP",
        "name": "hdl-srv",
        "port": 2641
    },
    {
        "protocol": "TCP",
        "name": "tragic",
        "port": 2642
    },
    {
        "protocol": "TCP",
        "name": "gte-samp",
        "port": 2643
    },
    {
        "protocol": "TCP",
        "name": "travsoft-ipx-t",
        "port": 2644
    },
    {
        "protocol": "TCP",
        "name": "novell-ipx-cmd",
        "port": 2645
    },
    {
        "protocol": "TCP",
        "name": "and-lm",
        "port": 2646
    },
    {
        "protocol": "TCP",
        "name": "syncserver",
        "port": 2647
    },
    {
        "protocol": "TCP",
        "name": "upsnotifyprot",
        "port": 2648
    },
    {
        "protocol": "TCP",
        "name": "vpsipport",
        "port": 2649
    },
    {
        "protocol": "TCP",
        "name": "eristwoguns",
        "port": 2650
    },
    {
        "protocol": "TCP",
        "name": "ebinsite",
        "port": 2651
    },
    {
        "protocol": "TCP",
        "name": "interpathpanel",
        "port": 2652
    },
    {
        "protocol": "TCP",
        "name": "sonus",
        "port": 2653
    },
    {
        "protocol": "TCP",
        "name": "corel_vncadmin",
        "port": 2654
    },
    {
        "protocol": "TCP",
        "name": "unglue",
        "port": 2655
    },
    {
        "protocol": "TCP",
        "name": "kana",
        "port": 2656
    },
    {
        "protocol": "TCP",
        "name": "sns-dispatcher",
        "port": 2657
    },
    {
        "protocol": "TCP",
        "name": "sns-admin",
        "port": 2658
    },
    {
        "protocol": "TCP",
        "name": "sns-query",
        "port": 2659
    },
    {
        "protocol": "TCP",
        "name": "gcmonitor",
        "port": 2660
    },
    {
        "protocol": "TCP",
        "name": "olhost",
        "port": 2661
    },
    {
        "protocol": "TCP",
        "name": "bintec-capi",
        "port": 2662
    },
    {
        "protocol": "TCP",
        "name": "bintec-tapi",
        "port": 2663
    },
    {
        "protocol": "TCP",
        "name": "patrol-mq-gm",
        "port": 2664
    },
    {
        "protocol": "TCP",
        "name": "patrol-mq-nm",
        "port": 2665
    },
    {
        "protocol": "TCP",
        "name": "extensis",
        "port": 2666
    },
    {
        "protocol": "TCP",
        "name": "alarm-clock-s",
        "port": 2667
    },
    {
        "protocol": "TCP",
        "name": "alarm-clock-c",
        "port": 2668
    },
    {
        "protocol": "TCP",
        "name": "toad",
        "port": 2669
    },
    {
        "protocol": "TCP",
        "name": "tve-announce",
        "port": 2670
    },
    {
        "protocol": "TCP",
        "name": "newlixreg",
        "port": 2671
    },
    {
        "protocol": "TCP",
        "name": "nhserver",
        "port": 2672
    },
    {
        "protocol": "TCP",
        "name": "firstcall42",
        "port": 2673
    },
    {
        "protocol": "TCP",
        "name": "ewnn",
        "port": 2674
    },
    {
        "protocol": "TCP",
        "name": "ttc-etap",
        "port": 2675
    },
    {
        "protocol": "TCP",
        "name": "simslink",
        "port": 2676
    },
    {
        "protocol": "TCP",
        "name": "gadgetgate1way",
        "port": 2677
    },
    {
        "protocol": "TCP",
        "name": "gadgetgate2way",
        "port": 2678
    },
    {
        "protocol": "TCP",
        "name": "syncserverssl",
        "port": 2679
    },
    {
        "protocol": "TCP",
        "name": "pxc-sapxom",
        "port": 2680
    },
    {
        "protocol": "TCP",
        "name": "mpnjsomb",
        "port": 2681
    },
    {
        "protocol": "TCP",
        "name": "ncdloadbalance",
        "port": 2683
    },
    {
        "protocol": "TCP",
        "name": "mpnjsosv",
        "port": 2684
    },
    {
        "protocol": "TCP",
        "name": "mpnjsocl",
        "port": 2685
    },
    {
        "protocol": "TCP",
        "name": "mpnjsomg",
        "port": 2686
    },
    {
        "protocol": "TCP",
        "name": "pq-lic-mgmt",
        "port": 2687
    },
    {
        "protocol": "TCP",
        "name": "md-cg-http",
        "port": 2688
    },
    {
        "protocol": "TCP",
        "name": "fastlynx",
        "port": 2689
    },
    {
        "protocol": "TCP",
        "name": "hp-nnm-data",
        "port": 2690
    },
    {
        "protocol": "TCP",
        "name": "itinternet",
        "port": 2691
    },
    {
        "protocol": "TCP",
        "name": "admins-lms",
        "port": 2692
    },
    {
        "protocol": "TCP",
        "name": "pwrsevent",
        "port": 2694
    },
    {
        "protocol": "TCP",
        "name": "vspread",
        "port": 2695
    },
    {
        "protocol": "TCP",
        "name": "unifyadmin",
        "port": 2696
    },
    {
        "protocol": "TCP",
        "name": "oce-snmp-trap",
        "port": 2697
    },
    {
        "protocol": "TCP",
        "name": "mck-ivpip",
        "port": 2698
    },
    {
        "protocol": "TCP",
        "name": "csoft-plusclnt",
        "port": 2699
    },
    {
        "protocol": "TCP",
        "name": "tqdata",
        "port": 2700
    },
    {
        "protocol": "TCP",
        "name": "sms-rcinfo",
        "port": 2701
    },
    {
        "protocol": "TCP",
        "name": "sms-xfer",
        "port": 2702
    },
    {
        "protocol": "TCP",
        "name": "sms-chat",
        "port": 2703
    },
    {
        "protocol": "TCP",
        "name": "sms-remctrl",
        "port": 2704
    },
    {
        "protocol": "TCP",
        "name": "sds-admin",
        "port": 2705
    },
    {
        "protocol": "TCP",
        "name": "ncdmirroring",
        "port": 2706
    },
    {
        "protocol": "TCP",
        "name": "emcsymapiport",
        "port": 2707
    },
    {
        "protocol": "TCP",
        "name": "banyan-net",
        "port": 2708
    },
    {
        "protocol": "TCP",
        "name": "supermon",
        "port": 2709
    },
    {
        "protocol": "TCP",
        "name": "sso-service",
        "port": 2710
    },
    {
        "protocol": "TCP",
        "name": "sso-control",
        "port": 2711
    },
    {
        "protocol": "TCP",
        "name": "aocp",
        "port": 2712
    },
    {
        "protocol": "TCP",
        "name": "raven1",
        "port": 2713
    },
    {
        "protocol": "TCP",
        "name": "raven2",
        "port": 2714
    },
    {
        "protocol": "TCP",
        "name": "hpstgmgr2",
        "port": 2715
    },
    {
        "protocol": "TCP",
        "name": "inova-ip-disco",
        "port": 2716
    },
    {
        "protocol": "TCP",
        "name": "pn-requester",
        "port": 2717
    },
    {
        "protocol": "TCP",
        "name": "pn-requester2",
        "port": 2718
    },
    {
        "protocol": "TCP",
        "name": "scan-change",
        "port": 2719
    },
    {
        "protocol": "TCP",
        "name": "wkars",
        "port": 2720
    },
    {
        "protocol": "TCP",
        "name": "smart-diagnose",
        "port": 2721
    },
    {
        "protocol": "TCP",
        "name": "proactivesrvr",
        "port": 2722
    },
    {
        "protocol": "TCP",
        "name": "watchdognt",
        "port": 2723
    },
    {
        "protocol": "TCP",
        "name": "qotps",
        "port": 2724
    },
    {
        "protocol": "TCP",
        "name": "msolap-ptp2",
        "port": 2725
    },
    {
        "protocol": "TCP",
        "name": "tams",
        "port": 2726
    },
    {
        "protocol": "TCP",
        "name": "mgcp-callagent",
        "port": 2727
    },
    {
        "protocol": "TCP",
        "name": "sqdr",
        "port": 2728
    },
    {
        "protocol": "TCP",
        "name": "tcim-control",
        "port": 2729
    },
    {
        "protocol": "TCP",
        "name": "nec-raidplus",
        "port": 2730
    },
    {
        "protocol": "TCP",
        "name": "fyre-messanger",
        "port": 2731
    },
    {
        "protocol": "TCP",
        "name": "g5m",
        "port": 2732
    },
    {
        "protocol": "TCP",
        "name": "signet-ctf",
        "port": 2733
    },
    {
        "protocol": "TCP",
        "name": "ccs-software",
        "port": 2734
    },
    {
        "protocol": "TCP",
        "name": "netiq-mc",
        "port": 2735
    },
    {
        "protocol": "TCP",
        "name": "radwiz-nms-srv",
        "port": 2736
    },
    {
        "protocol": "TCP",
        "name": "srp-feedback",
        "port": 2737
    },
    {
        "protocol": "TCP",
        "name": "ndl-TCP-ois-gw",
        "port": 2738
    },
    {
        "protocol": "TCP",
        "name": "tn-timing",
        "port": 2739
    },
    {
        "protocol": "TCP",
        "name": "alarm",
        "port": 2740
    },
    {
        "protocol": "TCP",
        "name": "tsb",
        "port": 2741
    },
    {
        "protocol": "TCP",
        "name": "tsb2",
        "port": 2742
    },
    {
        "protocol": "TCP",
        "name": "murx",
        "port": 2743
    },
    {
        "protocol": "TCP",
        "name": "honyaku",
        "port": 2744
    },
    {
        "protocol": "TCP",
        "name": "urbisnet",
        "port": 2745
    },
    {
        "protocol": "TCP",
        "name": "cpudpencap",
        "port": 2746
    },
    {
        "protocol": "TCP",
        "name": "fjippol-swrly",
        "port": 2747
    },
    {
        "protocol": "TCP",
        "name": "fjippol-polsvr",
        "port": 2748
    },
    {
        "protocol": "TCP",
        "name": "fjippol-cnsl",
        "port": 2749
    },
    {
        "protocol": "TCP",
        "name": "fjippol-port1",
        "port": 2750
    },
    {
        "protocol": "TCP",
        "name": "fjippol-port2",
        "port": 2751
    },
    {
        "protocol": "TCP",
        "name": "rsisysaccess",
        "port": 2752
    },
    {
        "protocol": "TCP",
        "name": "de-spot",
        "port": 2753
    },
    {
        "protocol": "TCP",
        "name": "apollo-cc",
        "port": 2754
    },
    {
        "protocol": "TCP",
        "name": "expresspay",
        "port": 2755
    },
    {
        "protocol": "TCP",
        "name": "simplement-tie",
        "port": 2756
    },
    {
        "protocol": "TCP",
        "name": "cnrp",
        "port": 2757
    },
    {
        "protocol": "TCP",
        "name": "apollo-status",
        "port": 2758
    },
    {
        "protocol": "TCP",
        "name": "apollo-gms",
        "port": 2759
    },
    {
        "protocol": "TCP",
        "name": "sabams",
        "port": 2760
    },
    {
        "protocol": "TCP",
        "name": "dicom-iscl",
        "port": 2761
    },
    {
        "protocol": "TCP",
        "name": "dicom-tls",
        "port": 2762
    },
    {
        "protocol": "TCP",
        "name": "desktop-dna",
        "port": 2763
    },
    {
        "protocol": "TCP",
        "name": "data-insurance",
        "port": 2764
    },
    {
        "protocol": "TCP",
        "name": "qip-audup",
        "port": 2765
    },
    {
        "protocol": "TCP",
        "name": "compaq-scp",
        "port": 2766
    },
    {
        "protocol": "TCP",
        "name": "uadtc",
        "port": 2767
    },
    {
        "protocol": "TCP",
        "name": "uacs",
        "port": 2768
    },
    {
        "protocol": "TCP",
        "name": "exce",
        "port": 2769
    },
    {
        "protocol": "TCP",
        "name": "veronica",
        "port": 2770
    },
    {
        "protocol": "TCP",
        "name": "vergencecm",
        "port": 2771
    },
    {
        "protocol": "TCP",
        "name": "auris",
        "port": 2772
    },
    {
        "protocol": "TCP",
        "name": "rbakcup1",
        "port": 2773
    },
    {
        "protocol": "TCP",
        "name": "rbakcup2",
        "port": 2774
    },
    {
        "protocol": "TCP",
        "name": "smpp",
        "port": 2775
    },
    {
        "protocol": "TCP",
        "name": "ridgeway1",
        "port": 2776
    },
    {
        "protocol": "TCP",
        "name": "ridgeway2",
        "port": 2777
    },
    {
        "protocol": "TCP",
        "name": "gwen-sonya",
        "port": 2778
    },
    {
        "protocol": "TCP",
        "name": "lbc-sync",
        "port": 2779
    },
    {
        "protocol": "TCP",
        "name": "lbc-control",
        "port": 2780
    },
    {
        "protocol": "TCP",
        "name": "whosells",
        "port": 2781
    },
    {
        "protocol": "TCP",
        "name": "everydayrc",
        "port": 2782
    },
    {
        "protocol": "TCP",
        "name": "aises",
        "port": 2783
    },
    {
        "protocol": "TCP",
        "name": "www-dev",
        "port": 2784
    },
    {
        "protocol": "TCP",
        "name": "aic-np",
        "port": 2785
    },
    {
        "protocol": "TCP",
        "name": "aic-oncrpc",
        "port": 2786
    },
    {
        "protocol": "TCP",
        "name": "piccolo",
        "port": 2787
    },
    {
        "protocol": "TCP",
        "name": "fryeserv",
        "port": 2788
    },
    {
        "protocol": "TCP",
        "name": "media-agent",
        "port": 2789
    },
    {
        "protocol": "TCP",
        "name": "plgproxy",
        "port": 2790
    },
    {
        "protocol": "TCP",
        "name": "mtport-regist",
        "port": 2791
    },
    {
        "protocol": "TCP",
        "name": "f5-globalsite",
        "port": 2792
    },
    {
        "protocol": "TCP",
        "name": "initlsmsad",
        "port": 2793
    },
    {
        "protocol": "TCP",
        "name": "aaftp",
        "port": 2794
    },
    {
        "protocol": "TCP",
        "name": "livestats",
        "port": 2795
    },
    {
        "protocol": "TCP",
        "name": "ac-tech",
        "port": 2796
    },
    {
        "protocol": "TCP",
        "name": "esp-encap",
        "port": 2797
    },
    {
        "protocol": "TCP",
        "name": "tmesis-upshot",
        "port": 2798
    },
    {
        "protocol": "TCP",
        "name": "icon-discover",
        "port": 2799
    },
    {
        "protocol": "TCP",
        "name": "acc-raid",
        "port": 2800
    },
    {
        "protocol": "TCP",
        "name": "igcp",
        "port": 2801
    },
    {
        "protocol": "TCP",
        "name": "veritas-TCP1",
        "port": 2802
    },
    {
        "protocol": "TCP",
        "name": "btprjctrl",
        "port": 2803
    },
    {
        "protocol": "TCP",
        "name": "dvr-esm",
        "port": 2804
    },
    {
        "protocol": "TCP",
        "name": "wta-wsp-s",
        "port": 2805
    },
    {
        "protocol": "TCP",
        "name": "cspuni",
        "port": 2806
    },
    {
        "protocol": "TCP",
        "name": "cspmulti",
        "port": 2807
    },
    {
        "protocol": "TCP",
        "name": "j-lan-p",
        "port": 2808
    },
    {
        "protocol": "TCP",
        "name": "corbaloc",
        "port": 2809
    },
    {
        "protocol": "TCP",
        "name": "netsteward",
        "port": 2810
    },
    {
        "protocol": "TCP",
        "name": "gsiftp",
        "port": 2811
    },
    {
        "protocol": "TCP",
        "name": "atmTCP",
        "port": 2812
    },
    {
        "protocol": "TCP",
        "name": "llm-pass",
        "port": 2813
    },
    {
        "protocol": "TCP",
        "name": "llm-csv",
        "port": 2814
    },
    {
        "protocol": "TCP",
        "name": "lbc-measure",
        "port": 2815
    },
    {
        "protocol": "TCP",
        "name": "lbc-watchdog",
        "port": 2816
    },
    {
        "protocol": "TCP",
        "name": "nmsigport",
        "port": 2817
    },
    {
        "protocol": "TCP",
        "name": "rmlnk",
        "port": 2818
    },
    {
        "protocol": "TCP",
        "name": "fc-faultnotify",
        "port": 2819
    },
    {
        "protocol": "TCP",
        "name": "univision",
        "port": 2820
    },
    {
        "protocol": "TCP",
        "name": "vrts-at-port",
        "port": 2821
    },
    {
        "protocol": "TCP",
        "name": "ka0wuc",
        "port": 2822
    },
    {
        "protocol": "TCP",
        "name": "cqg-netlan",
        "port": 2823
    },
    {
        "protocol": "TCP",
        "name": "cqg-netlan-1",
        "port": 2824
    },
    {
        "protocol": "TCP",
        "name": "slc-systemlog",
        "port": 2826
    },
    {
        "protocol": "TCP",
        "name": "slc-ctrlrloops",
        "port": 2827
    },
    {
        "protocol": "TCP",
        "name": "itm-lm",
        "port": 2828
    },
    {
        "protocol": "TCP",
        "name": "silkp1",
        "port": 2829
    },
    {
        "protocol": "TCP",
        "name": "silkp2",
        "port": 2830
    },
    {
        "protocol": "TCP",
        "name": "silkp3",
        "port": 2831
    },
    {
        "protocol": "TCP",
        "name": "silkp4",
        "port": 2832
    },
    {
        "protocol": "TCP",
        "name": "glishd",
        "port": 2833
    },
    {
        "protocol": "TCP",
        "name": "evtp",
        "port": 2834
    },
    {
        "protocol": "TCP",
        "name": "evtp-data",
        "port": 2835
    },
    {
        "protocol": "TCP",
        "name": "catalyst",
        "port": 2836
    },
    {
        "protocol": "TCP",
        "name": "repliweb",
        "port": 2837
    },
    {
        "protocol": "TCP",
        "name": "starbot",
        "port": 2838
    },
    {
        "protocol": "TCP",
        "name": "nmsigport",
        "port": 2839
    },
    {
        "protocol": "TCP",
        "name": "l3-exprt",
        "port": 2840
    },
    {
        "protocol": "TCP",
        "name": "l3-ranger",
        "port": 2841
    },
    {
        "protocol": "TCP",
        "name": "l3-hawk",
        "port": 2842
    },
    {
        "protocol": "TCP",
        "name": "pdnet",
        "port": 2843
    },
    {
        "protocol": "TCP",
        "name": "bpcp-poll",
        "port": 2844
    },
    {
        "protocol": "TCP",
        "name": "bpcp-trap",
        "port": 2845
    },
    {
        "protocol": "TCP",
        "name": "aimpp-hello",
        "port": 2846
    },
    {
        "protocol": "TCP",
        "name": "aimpp-port-req",
        "port": 2847
    },
    {
        "protocol": "TCP",
        "name": "amt-blc-port",
        "port": 2848
    },
    {
        "protocol": "TCP",
        "name": "fxp",
        "port": 2849
    },
    {
        "protocol": "TCP",
        "name": "metaconsole",
        "port": 2850
    },
    {
        "protocol": "TCP",
        "name": "webemshttp",
        "port": 2851
    },
    {
        "protocol": "TCP",
        "name": "bears-01",
        "port": 2852
    },
    {
        "protocol": "TCP",
        "name": "ispipes",
        "port": 2853
    },
    {
        "protocol": "TCP",
        "name": "infomover",
        "port": 2854
    },
    {
        "protocol": "TCP",
        "name": "cesdinv",
        "port": 2856
    },
    {
        "protocol": "TCP",
        "name": "simctlp",
        "port": 2857
    },
    {
        "protocol": "TCP",
        "name": "ecnp",
        "port": 2858
    },
    {
        "protocol": "TCP",
        "name": "activememory",
        "port": 2859
    },
    {
        "protocol": "TCP",
        "name": "dialpad-voice1",
        "port": 2860
    },
    {
        "protocol": "TCP",
        "name": "dialpad-voice2",
        "port": 2861
    },
    {
        "protocol": "TCP",
        "name": "ttg-protocol",
        "port": 2862
    },
    {
        "protocol": "TCP",
        "name": "sonardata",
        "port": 2863
    },
    {
        "protocol": "TCP",
        "name": "astromed-main",
        "port": 2864
    },
    {
        "protocol": "TCP",
        "name": "pit-vpn",
        "port": 2865
    },
    {
        "protocol": "TCP",
        "name": "iwlistener",
        "port": 2866
    },
    {
        "protocol": "TCP",
        "name": "esps-portal",
        "port": 2867
    },
    {
        "protocol": "TCP",
        "name": "npep-messaging",
        "port": 2868
    },
    {
        "protocol": "TCP",
        "name": "icslap",
        "port": 2869
    },
    {
        "protocol": "TCP",
        "name": "daishi",
        "port": 2870
    },
    {
        "protocol": "TCP",
        "name": "msi-selectplay",
        "port": 2871
    },
    {
        "protocol": "TCP",
        "name": "radix",
        "port": 2872
    },
    {
        "protocol": "TCP",
        "name": "dxmessagebase1",
        "port": 2874
    },
    {
        "protocol": "TCP",
        "name": "dxmessagebase2",
        "port": 2875
    },
    {
        "protocol": "TCP",
        "name": "sps-tunnel",
        "port": 2876
    },
    {
        "protocol": "TCP",
        "name": "bluelance",
        "port": 2877
    },
    {
        "protocol": "TCP",
        "name": "aap",
        "port": 2878
    },
    {
        "protocol": "TCP",
        "name": "ucentric-ds",
        "port": 2879
    },
    {
        "protocol": "TCP",
        "name": "synapse",
        "port": 2880
    },
    {
        "protocol": "TCP",
        "name": "ndsp",
        "port": 2881
    },
    {
        "protocol": "TCP",
        "name": "ndtp",
        "port": 2882
    },
    {
        "protocol": "TCP",
        "name": "ndnp",
        "port": 2883
    },
    {
        "protocol": "TCP",
        "name": "flashmsg",
        "port": 2884
    },
    {
        "protocol": "TCP",
        "name": "topflow",
        "port": 2885
    },
    {
        "protocol": "TCP",
        "name": "responselogic",
        "port": 2886
    },
    {
        "protocol": "TCP",
        "name": "aironetddp",
        "port": 2887
    },
    {
        "protocol": "TCP",
        "name": "spcsdlobby",
        "port": 2888
    },
    {
        "protocol": "TCP",
        "name": "rsom",
        "port": 2889
    },
    {
        "protocol": "TCP",
        "name": "cspclmulti",
        "port": 2890
    },
    {
        "protocol": "TCP",
        "name": "cinegrfx-elmd",
        "port": 2891
    },
    {
        "protocol": "TCP",
        "name": "snifferdata",
        "port": 2892
    },
    {
        "protocol": "TCP",
        "name": "vseconnector",
        "port": 2893
    },
    {
        "protocol": "TCP",
        "name": "abacus-remote",
        "port": 2894
    },
    {
        "protocol": "TCP",
        "name": "natuslink",
        "port": 2895
    },
    {
        "protocol": "TCP",
        "name": "ecovisiong6-1",
        "port": 2896
    },
    {
        "protocol": "TCP",
        "name": "citrix-rtmp",
        "port": 2897
    },
    {
        "protocol": "TCP",
        "name": "appliance-cfg",
        "port": 2898
    },
    {
        "protocol": "TCP",
        "name": "powergemplus",
        "port": 2899
    },
    {
        "protocol": "TCP",
        "name": "quicksuite",
        "port": 2900
    },
    {
        "protocol": "TCP",
        "name": "allstorcns",
        "port": 2901
    },
    {
        "protocol": "TCP",
        "name": "netaspi",
        "port": 2902
    },
    {
        "protocol": "TCP",
        "name": "suitcase",
        "port": 2903
    },
    {
        "protocol": "TCP",
        "name": "m2ua",
        "port": 2904
    },
    {
        "protocol": "TCP",
        "name": "m3ua",
        "port": 2905
    },
    {
        "protocol": "TCP",
        "name": "caller9",
        "port": 2906
    },
    {
        "protocol": "TCP",
        "name": "webmethods-b2b",
        "port": 2907
    },
    {
        "protocol": "TCP",
        "name": "mao",
        "port": 2908
    },
    {
        "protocol": "TCP",
        "name": "funk-dialout",
        "port": 2909
    },
    {
        "protocol": "TCP",
        "name": "tdaccess",
        "port": 2910
    },
    {
        "protocol": "TCP",
        "name": "blockade",
        "port": 2911
    },
    {
        "protocol": "TCP",
        "name": "epicon",
        "port": 2912
    },
    {
        "protocol": "TCP",
        "name": "boosterware",
        "port": 2913
    },
    {
        "protocol": "TCP",
        "name": "gamelobby",
        "port": 2914
    },
    {
        "protocol": "TCP",
        "name": "tksocket",
        "port": 2915
    },
    {
        "protocol": "TCP",
        "name": "elvin_server",
        "port": 2916
    },
    {
        "protocol": "TCP",
        "name": "elvin_client",
        "port": 2917
    },
    {
        "protocol": "TCP",
        "name": "kastenchasepad",
        "port": 2918
    },
    {
        "protocol": "TCP",
        "name": "roboer",
        "port": 2919
    },
    {
        "protocol": "TCP",
        "name": "roboeda",
        "port": 2920
    },
    {
        "protocol": "TCP",
        "name": "cesdcdman",
        "port": 2921
    },
    {
        "protocol": "TCP",
        "name": "cesdcdtrn",
        "port": 2922
    },
    {
        "protocol": "TCP",
        "name": "wta-wsp-wtp-s",
        "port": 2923
    },
    {
        "protocol": "TCP",
        "name": "precise-vip",
        "port": 2924
    },
    {
        "protocol": "TCP",
        "name": "mobile-file-dl",
        "port": 2926
    },
    {
        "protocol": "TCP",
        "name": "unimobilectrl",
        "port": 2927
    },
    {
        "protocol": "TCP",
        "name": "redstone-cpss",
        "port": 2928
    },
    {
        "protocol": "TCP",
        "name": "amx-webadmin",
        "port": 2929
    },
    {
        "protocol": "TCP",
        "name": "amx-weblinx",
        "port": 2930
    },
    {
        "protocol": "TCP",
        "name": "circle-x",
        "port": 2931
    },
    {
        "protocol": "TCP",
        "name": "incp",
        "port": 2932
    },
    {
        "protocol": "TCP",
        "name": "4-tieropmgw",
        "port": 2933
    },
    {
        "protocol": "TCP",
        "name": "4-tieropmcli",
        "port": 2934
    },
    {
        "protocol": "TCP",
        "name": "qtp",
        "port": 2935
    },
    {
        "protocol": "TCP",
        "name": "otpatch",
        "port": 2936
    },
    {
        "protocol": "TCP",
        "name": "pnaconsult-lm",
        "port": 2937
    },
    {
        "protocol": "TCP",
        "name": "sm-pas-1",
        "port": 2938
    },
    {
        "protocol": "TCP",
        "name": "sm-pas-2",
        "port": 2939
    },
    {
        "protocol": "TCP",
        "name": "sm-pas-3",
        "port": 2940
    },
    {
        "protocol": "TCP",
        "name": "sm-pas-4",
        "port": 2941
    },
    {
        "protocol": "TCP",
        "name": "sm-pas-5",
        "port": 2942
    },
    {
        "protocol": "TCP",
        "name": "ttnrepository",
        "port": 2943
    },
    {
        "protocol": "TCP",
        "name": "megaco-h248",
        "port": 2944
    },
    {
        "protocol": "TCP",
        "name": "h248-binary",
        "port": 2945
    },
    {
        "protocol": "TCP",
        "name": "fjsvmpor",
        "port": 2946
    },
    {
        "protocol": "TCP",
        "name": "gpsd",
        "port": 2947
    },
    {
        "protocol": "TCP",
        "name": "wap-push",
        "port": 2948
    },
    {
        "protocol": "TCP",
        "name": "wap-pushsecure",
        "port": 2949
    },
    {
        "protocol": "TCP",
        "name": "esip",
        "port": 2950
    },
    {
        "protocol": "TCP",
        "name": "ottp",
        "port": 2951
    },
    {
        "protocol": "TCP",
        "name": "mpfwsas",
        "port": 2952
    },
    {
        "protocol": "TCP",
        "name": "ovalarmsrv",
        "port": 2953
    },
    {
        "protocol": "TCP",
        "name": "ovalarmsrv-cmd",
        "port": 2954
    },
    {
        "protocol": "TCP",
        "name": "csnotify",
        "port": 2955
    },
    {
        "protocol": "TCP",
        "name": "ovrimosdbman",
        "port": 2956
    },
    {
        "protocol": "TCP",
        "name": "jmact5",
        "port": 2957
    },
    {
        "protocol": "TCP",
        "name": "jmact6",
        "port": 2958
    },
    {
        "protocol": "TCP",
        "name": "rmopagt",
        "port": 2959
    },
    {
        "protocol": "TCP",
        "name": "dfoxserver",
        "port": 2960
    },
    {
        "protocol": "TCP",
        "name": "boldsoft-lm",
        "port": 2961
    },
    {
        "protocol": "TCP",
        "name": "iph-policy-cli",
        "port": 2962
    },
    {
        "protocol": "TCP",
        "name": "iph-policy-adm",
        "port": 2963
    },
    {
        "protocol": "TCP",
        "name": "bullant-srap",
        "port": 2964
    },
    {
        "protocol": "TCP",
        "name": "bullant-rap",
        "port": 2965
    },
    {
        "protocol": "TCP",
        "name": "idp-infotrieve",
        "port": 2966
    },
    {
        "protocol": "TCP",
        "name": "ssc-agent",
        "port": 2967
    },
    {
        "protocol": "TCP",
        "name": "enpp",
        "port": 2968
    },
    {
        "protocol": "TCP",
        "name": "essp",
        "port": 2969
    },
    {
        "protocol": "TCP",
        "name": "index-net",
        "port": 2970
    },
    {
        "protocol": "TCP",
        "name": "netclip",
        "port": 2971
    },
    {
        "protocol": "TCP",
        "name": "pmsm-webrctl",
        "port": 2972
    },
    {
        "protocol": "TCP",
        "name": "svnetworks",
        "port": 2973
    },
    {
        "protocol": "TCP",
        "name": "signal",
        "port": 2974
    },
    {
        "protocol": "TCP",
        "name": "fjmpcm",
        "port": 2975
    },
    {
        "protocol": "TCP",
        "name": "cns-srv-port",
        "port": 2976
    },
    {
        "protocol": "TCP",
        "name": "ttc-etap-ns",
        "port": 2977
    },
    {
        "protocol": "TCP",
        "name": "ttc-etap-ds",
        "port": 2978
    },
    {
        "protocol": "TCP",
        "name": "h263-video",
        "port": 2979
    },
    {
        "protocol": "TCP",
        "name": "wimd",
        "port": 2980
    },
    {
        "protocol": "TCP",
        "name": "mylxamport",
        "port": 2981
    },
    {
        "protocol": "TCP",
        "name": "iwb-whiteboard",
        "port": 2982
    },
    {
        "protocol": "TCP",
        "name": "netplan",
        "port": 2983
    },
    {
        "protocol": "TCP",
        "name": "hpidsadmin",
        "port": 2984
    },
    {
        "protocol": "TCP",
        "name": "hpidsagent",
        "port": 2985
    },
    {
        "protocol": "TCP",
        "name": "stonefalls",
        "port": 2986
    },
    {
        "protocol": "TCP",
        "name": "identify",
        "port": 2987
    },
    {
        "protocol": "TCP",
        "name": "hippad",
        "port": 2988
    },
    {
        "protocol": "TCP",
        "name": "zarkov",
        "port": 2989
    },
    {
        "protocol": "TCP",
        "name": "boscap",
        "port": 2990
    },
    {
        "protocol": "TCP",
        "name": "wkstn-mon",
        "port": 2991
    },
    {
        "protocol": "TCP",
        "name": "itb301",
        "port": 2992
    },
    {
        "protocol": "TCP",
        "name": "veritas-vis1",
        "port": 2993
    },
    {
        "protocol": "TCP",
        "name": "veritas-vis2",
        "port": 2994
    },
    {
        "protocol": "TCP",
        "name": "idrs",
        "port": 2995
    },
    {
        "protocol": "TCP",
        "name": "vsixml",
        "port": 2996
    },
    {
        "protocol": "TCP",
        "name": "rebol",
        "port": 2997
    },
    {
        "protocol": "TCP",
        "name": "realsecure",
        "port": 2998
    },
    {
        "protocol": "TCP",
        "name": "remoteware-un",
        "port": 2999
    },
    {
        "protocol": "TCP",
        "name": "remoteware-cl",
        "port": 3000
    },
    {
        "protocol": "TCP",
        "name": "redwood-broker",
        "port": 3001
    },
    {
        "protocol": "TCP",
        "name": "remoteware-srv",
        "port": 3002
    },
    {
        "protocol": "TCP",
        "name": "cgms",
        "port": 3003
    },
    {
        "protocol": "TCP",
        "name": "csoftragent",
        "port": 3004
    },
    {
        "protocol": "TCP",
        "name": "geniuslm",
        "port": 3005
    },
    {
        "protocol": "TCP",
        "name": "ii-admin",
        "port": 3006
    },
    {
        "protocol": "TCP",
        "name": "lotusmtap",
        "port": 3007
    },
    {
        "protocol": "TCP",
        "name": "midnight-tech",
        "port": 3008
    },
    {
        "protocol": "TCP",
        "name": "pxc-ntfy",
        "port": 3009
    },
    {
        "protocol": "TCP",
        "name": "gw",
        "port": 3010
    },
    {
        "protocol": "TCP",
        "name": "trusted-web",
        "port": 3011
    },
    {
        "protocol": "TCP",
        "name": "twsdss",
        "port": 3012
    },
    {
        "protocol": "TCP",
        "name": "gilatskysurfer",
        "port": 3013
    },
    {
        "protocol": "TCP",
        "name": "broker_service",
        "port": 3014
    },
    {
        "protocol": "TCP",
        "name": "nati-dstp",
        "port": 3015
    },
    {
        "protocol": "TCP",
        "name": "notify_srvr",
        "port": 3016
    },
    {
        "protocol": "TCP",
        "name": "event_listener",
        "port": 3017
    },
    {
        "protocol": "TCP",
        "name": "srvc_registry",
        "port": 3018
    },
    {
        "protocol": "TCP",
        "name": "resource_mgr",
        "port": 3019
    },
    {
        "protocol": "TCP",
        "name": "cifs",
        "port": 3020
    },
    {
        "protocol": "TCP",
        "name": "agriserver",
        "port": 3021
    },
    {
        "protocol": "TCP",
        "name": "csregagent",
        "port": 3022
    },
    {
        "protocol": "TCP",
        "name": "magicnotes",
        "port": 3023
    },
    {
        "protocol": "TCP",
        "name": "nds_sso",
        "port": 3024
    },
    {
        "protocol": "TCP",
        "name": "arepa-raft",
        "port": 3025
    },
    {
        "protocol": "TCP",
        "name": "agri-gateway",
        "port": 3026
    },
    {
        "protocol": "TCP",
        "name": "LiebDevMgmt_C",
        "port": 3027
    },
    {
        "protocol": "TCP",
        "name": "LiebDevMgmt_DM",
        "port": 3028
    },
    {
        "protocol": "TCP",
        "name": "LiebDevMgmt_A",
        "port": 3029
    },
    {
        "protocol": "TCP",
        "name": "arepa-cas",
        "port": 3030
    },
    {
        "protocol": "TCP",
        "name": "eppc",
        "port": 3031
    },
    {
        "protocol": "TCP",
        "name": "redwood-chat",
        "port": 3032
    },
    {
        "protocol": "TCP",
        "name": "pdb",
        "port": 3033
    },
    {
        "protocol": "TCP",
        "name": "osmosis-aeea",
        "port": 3034
    },
    {
        "protocol": "TCP",
        "name": "fjsv-gssagt",
        "port": 3035
    },
    {
        "protocol": "TCP",
        "name": "hagel-dump",
        "port": 3036
    },
    {
        "protocol": "TCP",
        "name": "hp-san-mgmt",
        "port": 3037
    },
    {
        "protocol": "TCP",
        "name": "santak-ups",
        "port": 3038
    },
    {
        "protocol": "TCP",
        "name": "cogitate",
        "port": 3039
    },
    {
        "protocol": "TCP",
        "name": "tomato-springs",
        "port": 3040
    },
    {
        "protocol": "TCP",
        "name": "di-traceware",
        "port": 3041
    },
    {
        "protocol": "TCP",
        "name": "journee",
        "port": 3042
    },
    {
        "protocol": "TCP",
        "name": "brp",
        "port": 3043
    },
    {
        "protocol": "TCP",
        "name": "epp",
        "port": 3044
    },
    {
        "protocol": "TCP",
        "name": "responsenet",
        "port": 3045
    },
    {
        "protocol": "TCP",
        "name": "di-ase",
        "port": 3046
    },
    {
        "protocol": "TCP",
        "name": "hlserver",
        "port": 3047
    },
    {
        "protocol": "TCP",
        "name": "pctrader",
        "port": 3048
    },
    {
        "protocol": "TCP",
        "name": "nsws",
        "port": 3049
    },
    {
        "protocol": "TCP",
        "name": "gds_db",
        "port": 3050
    },
    {
        "protocol": "TCP",
        "name": "galaxy-server",
        "port": 3051
    },
    {
        "protocol": "TCP",
        "name": "apc-3052",
        "port": 3052
    },
    {
        "protocol": "TCP",
        "name": "dsom-server",
        "port": 3053
    },
    {
        "protocol": "TCP",
        "name": "amt-cnf-prot",
        "port": 3054
    },
    {
        "protocol": "TCP",
        "name": "policyserver",
        "port": 3055
    },
    {
        "protocol": "TCP",
        "name": "cdl-server",
        "port": 3056
    },
    {
        "protocol": "TCP",
        "name": "goahead-fldup",
        "port": 3057
    },
    {
        "protocol": "TCP",
        "name": "videobeans",
        "port": 3058
    },
    {
        "protocol": "TCP",
        "name": "qsoft",
        "port": 3059
    },
    {
        "protocol": "TCP",
        "name": "interserver",
        "port": 3060
    },
    {
        "protocol": "TCP",
        "name": "cauTCPd",
        "port": 3061
    },
    {
        "protocol": "TCP",
        "name": "ncacn-ip-TCP",
        "port": 3062
    },
    {
        "protocol": "TCP",
        "name": "ncadg-ip-udp",
        "port": 3063
    },
    {
        "protocol": "TCP",
        "name": "rprt",
        "port": 3064
    },
    {
        "protocol": "TCP",
        "name": "slinterbase",
        "port": 3065
    },
    {
        "protocol": "TCP",
        "name": "netattachsdmp",
        "port": 3066
    },
    {
        "protocol": "TCP",
        "name": "fjhpjp",
        "port": 3067
    },
    {
        "protocol": "TCP",
        "name": "ls3bcast",
        "port": 3068
    },
    {
        "protocol": "TCP",
        "name": "ls3",
        "port": 3069
    },
    {
        "protocol": "TCP",
        "name": "mgxswitch",
        "port": 3070
    },
    {
        "protocol": "TCP",
        "name": "csd-mgmt-port",
        "port": 3071
    },
    {
        "protocol": "TCP",
        "name": "csd-monitor",
        "port": 3072
    },
    {
        "protocol": "TCP",
        "name": "vcrp",
        "port": 3073
    },
    {
        "protocol": "TCP",
        "name": "xbox",
        "port": 3074
    },
    {
        "protocol": "TCP",
        "name": "orbix-locator",
        "port": 3075
    },
    {
        "protocol": "TCP",
        "name": "orbix-config",
        "port": 3076
    },
    {
        "protocol": "TCP",
        "name": "orbix-loc-ssl",
        "port": 3077
    },
    {
        "protocol": "TCP",
        "name": "orbix-cfg-ssl",
        "port": 3078
    },
    {
        "protocol": "TCP",
        "name": "lv-frontpanel",
        "port": 3079
    },
    {
        "protocol": "TCP",
        "name": "stm_pproc",
        "port": 3080
    },
    {
        "protocol": "TCP",
        "name": "tl1-lv",
        "port": 3081
    },
    {
        "protocol": "TCP",
        "name": "tl1-raw",
        "port": 3082
    },
    {
        "protocol": "TCP",
        "name": "tl1-telnet",
        "port": 3083
    },
    {
        "protocol": "TCP",
        "name": "itm-mccs",
        "port": 3084
    },
    {
        "protocol": "TCP",
        "name": "pcihreq",
        "port": 3085
    },
    {
        "protocol": "TCP",
        "name": "jdl-dbkitchen",
        "port": 3086
    },
    {
        "protocol": "TCP",
        "name": "asoki-sma",
        "port": 3087
    },
    {
        "protocol": "TCP",
        "name": "xdtp",
        "port": 3088
    },
    {
        "protocol": "TCP",
        "name": "ptk-alink",
        "port": 3089
    },
    {
        "protocol": "TCP",
        "name": "rtss",
        "port": 3090
    },
    {
        "protocol": "TCP",
        "name": "1ci-smcs",
        "port": 3091
    },
    {
        "protocol": "TCP",
        "name": "njfss",
        "port": 3092
    },
    {
        "protocol": "TCP",
        "name": "rapidmq-center",
        "port": 3093
    },
    {
        "protocol": "TCP",
        "name": "rapidmq-reg",
        "port": 3094
    },
    {
        "protocol": "TCP",
        "name": "panasas",
        "port": 3095
    },
    {
        "protocol": "TCP",
        "name": "ndl-aps",
        "port": 3096
    },
    {
        "protocol": "TCP",
        "name": "umm-port",
        "port": 3098
    },
    {
        "protocol": "TCP",
        "name": "chmd",
        "port": 3099
    },
    {
        "protocol": "TCP",
        "name": "opcon-xps",
        "port": 3100
    },
    {
        "protocol": "TCP",
        "name": "hp-pxpib",
        "port": 3101
    },
    {
        "protocol": "TCP",
        "name": "slslavemon",
        "port": 3102
    },
    {
        "protocol": "TCP",
        "name": "autocuesmi",
        "port": 3103
    },
    {
        "protocol": "TCP",
        "name": "autocuelog",
        "port": 3104
    },
    {
        "protocol": "TCP",
        "name": "cardbox",
        "port": 3105
    },
    {
        "protocol": "TCP",
        "name": "cardbox-http",
        "port": 3106
    },
    {
        "protocol": "TCP",
        "name": "business",
        "port": 3107
    },
    {
        "protocol": "TCP",
        "name": "geolocate",
        "port": 3108
    },
    {
        "protocol": "TCP",
        "name": "personnel",
        "port": 3109
    },
    {
        "protocol": "TCP",
        "name": "sim-control",
        "port": 3110
    },
    {
        "protocol": "TCP",
        "name": "wsynch",
        "port": 3111
    },
    {
        "protocol": "TCP",
        "name": "ksysguard",
        "port": 3112
    },
    {
        "protocol": "TCP",
        "name": "cs-auth-svr",
        "port": 3113
    },
    {
        "protocol": "TCP",
        "name": "ccmad",
        "port": 3114
    },
    {
        "protocol": "TCP",
        "name": "mctet-master",
        "port": 3115
    },
    {
        "protocol": "TCP",
        "name": "mctet-gateway",
        "port": 3116
    },
    {
        "protocol": "TCP",
        "name": "mctet-jserv",
        "port": 3117
    },
    {
        "protocol": "TCP",
        "name": "pkagent",
        "port": 3118
    },
    {
        "protocol": "TCP",
        "name": "d2000kernel",
        "port": 3119
    },
    {
        "protocol": "TCP",
        "name": "d2000webserver",
        "port": 3120
    },
    {
        "protocol": "TCP",
        "name": "vtr-emulator",
        "port": 3122
    },
    {
        "protocol": "TCP",
        "name": "edix",
        "port": 3123
    },
    {
        "protocol": "TCP",
        "name": "beacon-port",
        "port": 3124
    },
    {
        "protocol": "TCP",
        "name": "a13-an",
        "port": 3125
    },
    {
        "protocol": "TCP",
        "name": "ms-dotnetster",
        "port": 3126
    },
    {
        "protocol": "TCP",
        "name": "ctx-bridge",
        "port": 3127
    },
    {
        "protocol": "TCP",
        "name": "ndl-aas",
        "port": 3128
    },
    {
        "protocol": "TCP",
        "name": "netport-id",
        "port": 3129
    },
    {
        "protocol": "TCP",
        "name": "icpv2",
        "port": 3130
    },
    {
        "protocol": "TCP",
        "name": "netbookmark",
        "port": 3131
    },
    {
        "protocol": "TCP",
        "name": "ms-rule-engine",
        "port": 3132
    },
    {
        "protocol": "TCP",
        "name": "prism-deploy",
        "port": 3133
    },
    {
        "protocol": "TCP",
        "name": "ecp",
        "port": 3134
    },
    {
        "protocol": "TCP",
        "name": "peerbook-port",
        "port": 3135
    },
    {
        "protocol": "TCP",
        "name": "grubd",
        "port": 3136
    },
    {
        "protocol": "TCP",
        "name": "rtnt-1",
        "port": 3137
    },
    {
        "protocol": "TCP",
        "name": "rtnt-2",
        "port": 3138
    },
    {
        "protocol": "TCP",
        "name": "incognitorv",
        "port": 3139
    },
    {
        "protocol": "TCP",
        "name": "ariliamulti",
        "port": 3140
    },
    {
        "protocol": "TCP",
        "name": "vmodem",
        "port": 3141
    },
    {
        "protocol": "TCP",
        "name": "rdc-wh-eos",
        "port": 3142
    },
    {
        "protocol": "TCP",
        "name": "seaview",
        "port": 3143
    },
    {
        "protocol": "TCP",
        "name": "tarantella",
        "port": 3144
    },
    {
        "protocol": "TCP",
        "name": "csi-lfap",
        "port": 3145
    },
    {
        "protocol": "TCP",
        "name": "bears-02",
        "port": 3146
    },
    {
        "protocol": "TCP",
        "name": "rfio",
        "port": 3147
    },
    {
        "protocol": "TCP",
        "name": "nm-game-admin",
        "port": 3148
    },
    {
        "protocol": "TCP",
        "name": "nm-game-server",
        "port": 3149
    },
    {
        "protocol": "TCP",
        "name": "nm-asses-admin",
        "port": 3150
    },
    {
        "protocol": "TCP",
        "name": "nm-assessor",
        "port": 3151
    },
    {
        "protocol": "TCP",
        "name": "feitianrockey",
        "port": 3152
    },
    {
        "protocol": "TCP",
        "name": "s8-client-port",
        "port": 3153
    },
    {
        "protocol": "TCP",
        "name": "ccmrmi",
        "port": 3154
    },
    {
        "protocol": "TCP",
        "name": "jpegmpeg",
        "port": 3155
    },
    {
        "protocol": "TCP",
        "name": "indura",
        "port": 3156
    },
    {
        "protocol": "TCP",
        "name": "e3consultants",
        "port": 3157
    },
    {
        "protocol": "TCP",
        "name": "stvp",
        "port": 3158
    },
    {
        "protocol": "TCP",
        "name": "navegaweb-port",
        "port": 3159
    },
    {
        "protocol": "TCP",
        "name": "tip-app-server",
        "port": 3160
    },
    {
        "protocol": "TCP",
        "name": "doc1lm",
        "port": 3161
    },
    {
        "protocol": "TCP",
        "name": "sflm",
        "port": 3162
    },
    {
        "protocol": "TCP",
        "name": "res-sap",
        "port": 3163
    },
    {
        "protocol": "TCP",
        "name": "imprs",
        "port": 3164
    },
    {
        "protocol": "TCP",
        "name": "newgenpay",
        "port": 3165
    },
    {
        "protocol": "TCP",
        "name": "qrepos",
        "port": 3166
    },
    {
        "protocol": "TCP",
        "name": "poweroncontact",
        "port": 3167
    },
    {
        "protocol": "TCP",
        "name": "poweronnud",
        "port": 3168
    },
    {
        "protocol": "TCP",
        "name": "serverview-as",
        "port": 3169
    },
    {
        "protocol": "TCP",
        "name": "serverview-asn",
        "port": 3170
    },
    {
        "protocol": "TCP",
        "name": "serverview-gf",
        "port": 3171
    },
    {
        "protocol": "TCP",
        "name": "serverview-rm",
        "port": 3172
    },
    {
        "protocol": "TCP",
        "name": "serverview-icc",
        "port": 3173
    },
    {
        "protocol": "TCP",
        "name": "armi-server",
        "port": 3174
    },
    {
        "protocol": "TCP",
        "name": "t1-e1-over-ip",
        "port": 3175
    },
    {
        "protocol": "TCP",
        "name": "ars-master",
        "port": 3176
    },
    {
        "protocol": "TCP",
        "name": "phonex-port",
        "port": 3177
    },
    {
        "protocol": "TCP",
        "name": "radclientport",
        "port": 3178
    },
    {
        "protocol": "TCP",
        "name": "h2gf-w-2m",
        "port": 3179
    },
    {
        "protocol": "TCP",
        "name": "mc-brk-srv",
        "port": 3180
    },
    {
        "protocol": "TCP",
        "name": "bmcpatrolagent",
        "port": 3181
    },
    {
        "protocol": "TCP",
        "name": "bmcpatrolrnvu",
        "port": 3182
    },
    {
        "protocol": "TCP",
        "name": "cops-tls",
        "port": 3183
    },
    {
        "protocol": "TCP",
        "name": "apogeex-port",
        "port": 3184
    },
    {
        "protocol": "TCP",
        "name": "smpppd",
        "port": 3185
    },
    {
        "protocol": "TCP",
        "name": "iiw-port",
        "port": 3186
    },
    {
        "protocol": "TCP",
        "name": "odi-port",
        "port": 3187
    },
    {
        "protocol": "TCP",
        "name": "brcm-comm-port",
        "port": 3188
    },
    {
        "protocol": "TCP",
        "name": "pcle-infex",
        "port": 3189
    },
    {
        "protocol": "TCP",
        "name": "csvr-proxy",
        "port": 3190
    },
    {
        "protocol": "TCP",
        "name": "csvr-sslproxy",
        "port": 3191
    },
    {
        "protocol": "TCP",
        "name": "firemonrcc",
        "port": 3192
    },
    {
        "protocol": "TCP",
        "name": "spandataport",
        "port": 3193
    },
    {
        "protocol": "TCP",
        "name": "magbind",
        "port": 3194
    },
    {
        "protocol": "TCP",
        "name": "ncu-1",
        "port": 3195
    },
    {
        "protocol": "TCP",
        "name": "ncu-2",
        "port": 3196
    },
    {
        "protocol": "TCP",
        "name": "embrace-dp-s",
        "port": 3197
    },
    {
        "protocol": "TCP",
        "name": "embrace-dp-c",
        "port": 3198
    },
    {
        "protocol": "TCP",
        "name": "dmod-workspace",
        "port": 3199
    },
    {
        "protocol": "TCP",
        "name": "tick-port",
        "port": 3200
    },
    {
        "protocol": "TCP",
        "name": "cpq-tasksmart",
        "port": 3201
    },
    {
        "protocol": "TCP",
        "name": "intraintra",
        "port": 3202
    },
    {
        "protocol": "TCP",
        "name": "netwatcher-mon",
        "port": 3203
    },
    {
        "protocol": "TCP",
        "name": "netwatcher-db",
        "port": 3204
    },
    {
        "protocol": "TCP",
        "name": "isns",
        "port": 3205
    },
    {
        "protocol": "TCP",
        "name": "ironmail",
        "port": 3206
    },
    {
        "protocol": "TCP",
        "name": "vx-auth-port",
        "port": 3207
    },
    {
        "protocol": "TCP",
        "name": "pfu-prcallback",
        "port": 3208
    },
    {
        "protocol": "TCP",
        "name": "netwkpathengine",
        "port": 3209
    },
    {
        "protocol": "TCP",
        "name": "flamenco-proxy",
        "port": 3210
    },
    {
        "protocol": "TCP",
        "name": "avsecuremgmt",
        "port": 3211
    },
    {
        "protocol": "TCP",
        "name": "surveyinst",
        "port": 3212
    },
    {
        "protocol": "TCP",
        "name": "neon24x7",
        "port": 3213
    },
    {
        "protocol": "TCP",
        "name": "jmq-daemon-1",
        "port": 3214
    },
    {
        "protocol": "TCP",
        "name": "jmq-daemon-2",
        "port": 3215
    },
    {
        "protocol": "TCP",
        "name": "ferrari-foam",
        "port": 3216
    },
    {
        "protocol": "TCP",
        "name": "unite",
        "port": 3217
    },
    {
        "protocol": "TCP",
        "name": "smartpackets",
        "port": 3218
    },
    {
        "protocol": "TCP",
        "name": "wms-messenger",
        "port": 3219
    },
    {
        "protocol": "TCP",
        "name": "xnm-ssl",
        "port": 3220
    },
    {
        "protocol": "TCP",
        "name": "xnm-clear-text",
        "port": 3221
    },
    {
        "protocol": "TCP",
        "name": "glbp",
        "port": 3222
    },
    {
        "protocol": "TCP",
        "name": "digivote",
        "port": 3223
    },
    {
        "protocol": "TCP",
        "name": "aes-discovery",
        "port": 3224
    },
    {
        "protocol": "TCP",
        "name": "fcip-port",
        "port": 3225
    },
    {
        "protocol": "TCP",
        "name": "isi-irp",
        "port": 3226
    },
    {
        "protocol": "TCP",
        "name": "dwnmshttp",
        "port": 3227
    },
    {
        "protocol": "TCP",
        "name": "dwmsgserver",
        "port": 3228
    },
    {
        "protocol": "TCP",
        "name": "global-cd-port",
        "port": 3229
    },
    {
        "protocol": "TCP",
        "name": "sftdst-port",
        "port": 3230
    },
    {
        "protocol": "TCP",
        "name": "dsnl",
        "port": 3231
    },
    {
        "protocol": "TCP",
        "name": "mdtp",
        "port": 3232
    },
    {
        "protocol": "TCP",
        "name": "whisker",
        "port": 3233
    },
    {
        "protocol": "TCP",
        "name": "alchemy",
        "port": 3234
    },
    {
        "protocol": "TCP",
        "name": "mdap-port",
        "port": 3235
    },
    {
        "protocol": "TCP",
        "name": "apparenet-ts",
        "port": 3236
    },
    {
        "protocol": "TCP",
        "name": "apparenet-tps",
        "port": 3237
    },
    {
        "protocol": "TCP",
        "name": "apparenet-as",
        "port": 3238
    },
    {
        "protocol": "TCP",
        "name": "apparenet-ui",
        "port": 3239
    },
    {
        "protocol": "TCP",
        "name": "triomotion",
        "port": 3240
    },
    {
        "protocol": "TCP",
        "name": "sysorb",
        "port": 3241
    },
    {
        "protocol": "TCP",
        "name": "sdp-id-port",
        "port": 3242
    },
    {
        "protocol": "TCP",
        "name": "timelot",
        "port": 3243
    },
    {
        "protocol": "TCP",
        "name": "onesaf",
        "port": 3244
    },
    {
        "protocol": "TCP",
        "name": "vieo-fe",
        "port": 3245
    },
    {
        "protocol": "TCP",
        "name": "dvt-system",
        "port": 3246
    },
    {
        "protocol": "TCP",
        "name": "dvt-data",
        "port": 3247
    },
    {
        "protocol": "TCP",
        "name": "procos-lm",
        "port": 3248
    },
    {
        "protocol": "TCP",
        "name": "ssp",
        "port": 3249
    },
    {
        "protocol": "TCP",
        "name": "hicp",
        "port": 3250
    },
    {
        "protocol": "TCP",
        "name": "sysscanner",
        "port": 3251
    },
    {
        "protocol": "TCP",
        "name": "dhe",
        "port": 3252
    },
    {
        "protocol": "TCP",
        "name": "pda-data",
        "port": 3253
    },
    {
        "protocol": "TCP",
        "name": "pda-sys",
        "port": 3254
    },
    {
        "protocol": "TCP",
        "name": "semaphore",
        "port": 3255
    },
    {
        "protocol": "TCP",
        "name": "cpqrpm-agent",
        "port": 3256
    },
    {
        "protocol": "TCP",
        "name": "cpqrpm-server",
        "port": 3257
    },
    {
        "protocol": "TCP",
        "name": "ivecon-port",
        "port": 3258
    },
    {
        "protocol": "TCP",
        "name": "epncdp2",
        "port": 3259
    },
    {
        "protocol": "TCP",
        "name": "iscsi-target",
        "port": 3260
    },
    {
        "protocol": "TCP",
        "name": "winshadow",
        "port": 3261
    },
    {
        "protocol": "TCP",
        "name": "necp",
        "port": 3262
    },
    {
        "protocol": "TCP",
        "name": "ecolor-imager",
        "port": 3263
    },
    {
        "protocol": "TCP",
        "name": "ccmail",
        "port": 3264
    },
    {
        "protocol": "TCP",
        "name": "altav-tunnel",
        "port": 3265
    },
    {
        "protocol": "TCP",
        "name": "ns-cfg-server",
        "port": 3266
    },
    {
        "protocol": "TCP",
        "name": "ibm-dial-out",
        "port": 3267
    },
    {
        "protocol": "TCP",
        "name": "msft-gc",
        "port": 3268
    },
    {
        "protocol": "TCP",
        "name": "msft-gc-ssl",
        "port": 3269
    },
    {
        "protocol": "TCP",
        "name": "verismart",
        "port": 3270
    },
    {
        "protocol": "TCP",
        "name": "csoft-prev",
        "port": 3271
    },
    {
        "protocol": "TCP",
        "name": "user-manager",
        "port": 3272
    },
    {
        "protocol": "TCP",
        "name": "sxmp",
        "port": 3273
    },
    {
        "protocol": "TCP",
        "name": "ordinox-server",
        "port": 3274
    },
    {
        "protocol": "TCP",
        "name": "samd",
        "port": 3275
    },
    {
        "protocol": "TCP",
        "name": "maxim-asics",
        "port": 3276
    },
    {
        "protocol": "TCP",
        "name": "awg-proxy",
        "port": 3277
    },
    {
        "protocol": "TCP",
        "name": "lkcmserver",
        "port": 3278
    },
    {
        "protocol": "TCP",
        "name": "admind",
        "port": 3279
    },
    {
        "protocol": "TCP",
        "name": "vs-server",
        "port": 3280
    },
    {
        "protocol": "TCP",
        "name": "sysopt",
        "port": 3281
    },
    {
        "protocol": "TCP",
        "name": "datusorb",
        "port": 3282
    },
    {
        "protocol": "TCP",
        "name": "net-assistant",
        "port": 3283
    },
    {
        "protocol": "TCP",
        "name": "4talk",
        "port": 3284
    },
    {
        "protocol": "TCP",
        "name": "plato",
        "port": 3285
    },
    {
        "protocol": "TCP",
        "name": "e-net",
        "port": 3286
    },
    {
        "protocol": "TCP",
        "name": "directvdata",
        "port": 3287
    },
    {
        "protocol": "TCP",
        "name": "cops",
        "port": 3288
    },
    {
        "protocol": "TCP",
        "name": "enpc",
        "port": 3289
    },
    {
        "protocol": "TCP",
        "name": "caps-lm",
        "port": 3290
    },
    {
        "protocol": "TCP",
        "name": "sah-lm",
        "port": 3291
    },
    {
        "protocol": "TCP",
        "name": "cart-o-rama",
        "port": 3292
    },
    {
        "protocol": "TCP",
        "name": "fg-fps",
        "port": 3293
    },
    {
        "protocol": "TCP",
        "name": "fg-gip",
        "port": 3294
    },
    {
        "protocol": "TCP",
        "name": "dyniplookup",
        "port": 3295
    },
    {
        "protocol": "TCP",
        "name": "rib-slm",
        "port": 3296
    },
    {
        "protocol": "TCP",
        "name": "cytel-lm",
        "port": 3297
    },
    {
        "protocol": "TCP",
        "name": "deskview",
        "port": 3298
    },
    {
        "protocol": "TCP",
        "name": "pdrncs",
        "port": 3299
    },
    {
        "protocol": "TCP",
        "name": "mcs-fastmail",
        "port": 3302
    },
    {
        "protocol": "TCP",
        "name": "opsession-clnt",
        "port": 3303
    },
    {
        "protocol": "TCP",
        "name": "opsession-srvr",
        "port": 3304
    },
    {
        "protocol": "TCP",
        "name": "odette-ftp",
        "port": 3305
    },
    {
        "protocol": "TCP",
        "name": "mysql",
        "port": 3306
    },
    {
        "protocol": "TCP",
        "name": "opsession-prxy",
        "port": 3307
    },
    {
        "protocol": "TCP",
        "name": "tns-server",
        "port": 3308
    },
    {
        "protocol": "TCP",
        "name": "tns-adv",
        "port": 3309
    },
    {
        "protocol": "TCP",
        "name": "dyna-access",
        "port": 3310
    },
    {
        "protocol": "TCP",
        "name": "mcns-tel-ret",
        "port": 3311
    },
    {
        "protocol": "TCP",
        "name": "appman-server",
        "port": 3312
    },
    {
        "protocol": "TCP",
        "name": "uorb",
        "port": 3313
    },
    {
        "protocol": "TCP",
        "name": "uohost",
        "port": 3314
    },
    {
        "protocol": "TCP",
        "name": "cdid",
        "port": 3315
    },
    {
        "protocol": "TCP",
        "name": "aicc-cmi",
        "port": 3316
    },
    {
        "protocol": "TCP",
        "name": "vsaiport",
        "port": 3317
    },
    {
        "protocol": "TCP",
        "name": "ssrip",
        "port": 3318
    },
    {
        "protocol": "TCP",
        "name": "sdt-lmd",
        "port": 3319
    },
    {
        "protocol": "TCP",
        "name": "officelink2000",
        "port": 3320
    },
    {
        "protocol": "TCP",
        "name": "vnsstr",
        "port": 3321
    },
    {
        "protocol": "TCP",
        "name": "active-net",
        "port": 3322
    },
    {
        "protocol": "TCP",
        "name": "sftu",
        "port": 3326
    },
    {
        "protocol": "TCP",
        "name": "bbars",
        "port": 3327
    },
    {
        "protocol": "TCP",
        "name": "egptlm",
        "port": 3328
    },
    {
        "protocol": "TCP",
        "name": "hp-device-disc",
        "port": 3329
    },
    {
        "protocol": "TCP",
        "name": "mcs-calypsoicf",
        "port": 3330
    },
    {
        "protocol": "TCP",
        "name": "mcs-messaging",
        "port": 3331
    },
    {
        "protocol": "TCP",
        "name": "mcs-mailsvr",
        "port": 3332
    },
    {
        "protocol": "TCP",
        "name": "dec-notes",
        "port": 3333
    },
    {
        "protocol": "TCP",
        "name": "directv-web",
        "port": 3334
    },
    {
        "protocol": "TCP",
        "name": "directv-soft",
        "port": 3335
    },
    {
        "protocol": "TCP",
        "name": "directv-tick",
        "port": 3336
    },
    {
        "protocol": "TCP",
        "name": "directv-catlg",
        "port": 3337
    },
    {
        "protocol": "TCP",
        "name": "anet-b",
        "port": 3338
    },
    {
        "protocol": "TCP",
        "name": "anet-l",
        "port": 3339
    },
    {
        "protocol": "TCP",
        "name": "anet-m",
        "port": 3340
    },
    {
        "protocol": "TCP",
        "name": "anet-h",
        "port": 3341
    },
    {
        "protocol": "TCP",
        "name": "webtie",
        "port": 3342
    },
    {
        "protocol": "TCP",
        "name": "ms-cluster-net",
        "port": 3343
    },
    {
        "protocol": "TCP",
        "name": "bnt-manager",
        "port": 3344
    },
    {
        "protocol": "TCP",
        "name": "influence",
        "port": 3345
    },
    {
        "protocol": "TCP",
        "name": "trnsprntproxy",
        "port": 3346
    },
    {
        "protocol": "TCP",
        "name": "phoenix-rpc",
        "port": 3347
    },
    {
        "protocol": "TCP",
        "name": "pangolin-laser",
        "port": 3348
    },
    {
        "protocol": "TCP",
        "name": "chevinservices",
        "port": 3349
    },
    {
        "protocol": "TCP",
        "name": "findviatv",
        "port": 3350
    },
    {
        "protocol": "TCP",
        "name": "btrieve",
        "port": 3351
    },
    {
        "protocol": "TCP",
        "name": "ssql",
        "port": 3352
    },
    {
        "protocol": "TCP",
        "name": "fatpipe",
        "port": 3353
    },
    {
        "protocol": "TCP",
        "name": "suitjd",
        "port": 3354
    },
    {
        "protocol": "TCP",
        "name": "ordinox-dbase",
        "port": 3355
    },
    {
        "protocol": "TCP",
        "name": "upnotifyps",
        "port": 3356
    },
    {
        "protocol": "TCP",
        "name": "adtech-test",
        "port": 3357
    },
    {
        "protocol": "TCP",
        "name": "mpsysrmsvr",
        "port": 3358
    },
    {
        "protocol": "TCP",
        "name": "wg-netforce",
        "port": 3359
    },
    {
        "protocol": "TCP",
        "name": "kv-server",
        "port": 3360
    },
    {
        "protocol": "TCP",
        "name": "kv-agent",
        "port": 3361
    },
    {
        "protocol": "TCP",
        "name": "dj-ilm",
        "port": 3362
    },
    {
        "protocol": "TCP",
        "name": "nati-vi-server",
        "port": 3363
    },
    {
        "protocol": "TCP",
        "name": "creativeserver",
        "port": 3364
    },
    {
        "protocol": "TCP",
        "name": "contentserver",
        "port": 3365
    },
    {
        "protocol": "TCP",
        "name": "creativepartnr",
        "port": 3366
    },
    {
        "protocol": "TCP",
        "name": "satvid-datalnk",
        "port": 3367
    },
    {
        "protocol": "TCP",
        "name": "satvid-datalnk",
        "port": 3368
    },
    {
        "protocol": "TCP",
        "name": "satvid-datalnk",
        "port": 3369
    },
    {
        "protocol": "TCP",
        "name": "satvid-datalnk",
        "port": 3370
    },
    {
        "protocol": "TCP",
        "name": "satvid-datalnk",
        "port": 3371
    },
    {
        "protocol": "TCP",
        "name": "tip2",
        "port": 3372
    },
    {
        "protocol": "TCP",
        "name": "lavenir-lm",
        "port": 3373
    },
    {
        "protocol": "TCP",
        "name": "cluster-disc",
        "port": 3374
    },
    {
        "protocol": "TCP",
        "name": "vsnm-agent",
        "port": 3375
    },
    {
        "protocol": "TCP",
        "name": "cdbroker",
        "port": 3376
    },
    {
        "protocol": "TCP",
        "name": "cogsys-lm",
        "port": 3377
    },
    {
        "protocol": "TCP",
        "name": "wsicopy",
        "port": 3378
    },
    {
        "protocol": "TCP",
        "name": "socorfs",
        "port": 3379
    },
    {
        "protocol": "TCP",
        "name": "sns-channels",
        "port": 3380
    },
    {
        "protocol": "TCP",
        "name": "geneous",
        "port": 3381
    },
    {
        "protocol": "TCP",
        "name": "fujitsu-neat",
        "port": 3382
    },
    {
        "protocol": "TCP",
        "name": "esp-lm",
        "port": 3383
    },
    {
        "protocol": "TCP",
        "name": "hp-clic",
        "port": 3384
    },
    {
        "protocol": "TCP",
        "name": "qnxnetman",
        "port": 3385
    },
    {
        "protocol": "TCP",
        "name": "gprs-data",
        "port": 3386
    },
    {
        "protocol": "TCP",
        "name": "backroomnet",
        "port": 3387
    },
    {
        "protocol": "TCP",
        "name": "cbserver",
        "port": 3388
    },
    {
        "protocol": "TCP",
        "name": "ms-wbt-server",
        "port": 3389
    },
    {
        "protocol": "TCP",
        "name": "dsc",
        "port": 3390
    },
    {
        "protocol": "TCP",
        "name": "savant",
        "port": 3391
    },
    {
        "protocol": "TCP",
        "name": "efi-lm",
        "port": 3392
    },
    {
        "protocol": "TCP",
        "name": "d2k-tapestry1",
        "port": 3393
    },
    {
        "protocol": "TCP",
        "name": "d2k-tapestry2",
        "port": 3394
    },
    {
        "protocol": "TCP",
        "name": "dyna-lm",
        "port": 3395
    },
    {
        "protocol": "TCP",
        "name": "printer_agent",
        "port": 3396
    },
    {
        "protocol": "TCP",
        "name": "cloanto-lm",
        "port": 3397
    },
    {
        "protocol": "TCP",
        "name": "mercantile",
        "port": 3398
    },
    {
        "protocol": "TCP",
        "name": "csms",
        "port": 3399
    },
    {
        "protocol": "TCP",
        "name": "csms2",
        "port": 3400
    },
    {
        "protocol": "TCP",
        "name": "filecast",
        "port": 3401
    },
    {
        "protocol": "TCP",
        "name": "fxaengine-net",
        "port": 3402
    },
    {
        "protocol": "TCP",
        "name": "copysnap",
        "port": 3403
    },
    {
        "protocol": "TCP",
        "name": "nokia-ann-ch1",
        "port": 3405
    },
    {
        "protocol": "TCP",
        "name": "nokia-ann-ch2",
        "port": 3406
    },
    {
        "protocol": "TCP",
        "name": "ldap-admin",
        "port": 3407
    },
    {
        "protocol": "TCP",
        "name": "issapi",
        "port": 3408
    },
    {
        "protocol": "TCP",
        "name": "networklens",
        "port": 3409
    },
    {
        "protocol": "TCP",
        "name": "networklenss",
        "port": 3410
    },
    {
        "protocol": "TCP",
        "name": "biolink-auth",
        "port": 3411
    },
    {
        "protocol": "TCP",
        "name": "xmlblaster",
        "port": 3412
    },
    {
        "protocol": "TCP",
        "name": "svnet",
        "port": 3413
    },
    {
        "protocol": "TCP",
        "name": "wip-port",
        "port": 3414
    },
    {
        "protocol": "TCP",
        "name": "bcinameservice",
        "port": 3415
    },
    {
        "protocol": "TCP",
        "name": "commandport",
        "port": 3416
    },
    {
        "protocol": "TCP",
        "name": "csvr",
        "port": 3417
    },
    {
        "protocol": "TCP",
        "name": "rnmap",
        "port": 3418
    },
    {
        "protocol": "TCP",
        "name": "softaudit",
        "port": 3419
    },
    {
        "protocol": "TCP",
        "name": "ifcp-port",
        "port": 3420
    },
    {
        "protocol": "TCP",
        "name": "bmap",
        "port": 3421
    },
    {
        "protocol": "TCP",
        "name": "rusb-sys-port",
        "port": 3422
    },
    {
        "protocol": "TCP",
        "name": "xtrm",
        "port": 3423
    },
    {
        "protocol": "TCP",
        "name": "xtrms",
        "port": 3424
    },
    {
        "protocol": "TCP",
        "name": "agps-port",
        "port": 3425
    },
    {
        "protocol": "TCP",
        "name": "arkivio",
        "port": 3426
    },
    {
        "protocol": "TCP",
        "name": "websphere-snmp",
        "port": 3427
    },
    {
        "protocol": "TCP",
        "name": "twcss",
        "port": 3428
    },
    {
        "protocol": "TCP",
        "name": "gcsp",
        "port": 3429
    },
    {
        "protocol": "TCP",
        "name": "ssdispatch",
        "port": 3430
    },
    {
        "protocol": "TCP",
        "name": "ndl-als",
        "port": 3431
    },
    {
        "protocol": "TCP",
        "name": "osdcp",
        "port": 3432
    },
    {
        "protocol": "TCP",
        "name": "alta-smp",
        "port": 3433
    },
    {
        "protocol": "TCP",
        "name": "opencm",
        "port": 3434
    },
    {
        "protocol": "TCP",
        "name": "pacom",
        "port": 3435
    },
    {
        "protocol": "TCP",
        "name": "gc-config",
        "port": 3436
    },
    {
        "protocol": "TCP",
        "name": "autocueds",
        "port": 3437
    },
    {
        "protocol": "TCP",
        "name": "spiral-admin",
        "port": 3438
    },
    {
        "protocol": "TCP",
        "name": "hri-port",
        "port": 3439
    },
    {
        "protocol": "TCP",
        "name": "ans-console",
        "port": 3440
    },
    {
        "protocol": "TCP",
        "name": "connect-client",
        "port": 3441
    },
    {
        "protocol": "TCP",
        "name": "connect-server",
        "port": 3442
    },
    {
        "protocol": "TCP",
        "name": "ov-nnm-websrv",
        "port": 3443
    },
    {
        "protocol": "TCP",
        "name": "denali-server",
        "port": 3444
    },
    {
        "protocol": "TCP",
        "name": "monp",
        "port": 3445
    },
    {
        "protocol": "TCP",
        "name": "3comfaxrpc",
        "port": 3446
    },
    {
        "protocol": "TCP",
        "name": "cddn",
        "port": 3447
    },
    {
        "protocol": "TCP",
        "name": "dnc-port",
        "port": 3448
    },
    {
        "protocol": "TCP",
        "name": "hotu-chat",
        "port": 3449
    },
    {
        "protocol": "TCP",
        "name": "castorproxy",
        "port": 3450
    },
    {
        "protocol": "TCP",
        "name": "asam",
        "port": 3451
    },
    {
        "protocol": "TCP",
        "name": "sabp-signal",
        "port": 3452
    },
    {
        "protocol": "TCP",
        "name": "pscupd",
        "port": 3453
    },
    {
        "protocol": "TCP",
        "name": "mira",
        "port": 3454
    },
    {
        "protocol": "TCP",
        "name": "prsvp",
        "port": 3455
    },
    {
        "protocol": "TCP",
        "name": "vat",
        "port": 3456
    },
    {
        "protocol": "TCP",
        "name": "vat-control",
        "port": 3457
    },
    {
        "protocol": "TCP",
        "name": "d3winosfi",
        "port": 3458
    },
    {
        "protocol": "TCP",
        "name": "integral",
        "port": 3459
    },
    {
        "protocol": "TCP",
        "name": "edm-manager",
        "port": 3460
    },
    {
        "protocol": "TCP",
        "name": "edm-stager",
        "port": 3461
    },
    {
        "protocol": "TCP",
        "name": "edm-std-notify",
        "port": 3462
    },
    {
        "protocol": "TCP",
        "name": "edm-adm-notify",
        "port": 3463
    },
    {
        "protocol": "TCP",
        "name": "edm-mgr-sync",
        "port": 3464
    },
    {
        "protocol": "TCP",
        "name": "edm-mgr-cntrl",
        "port": 3465
    },
    {
        "protocol": "TCP",
        "name": "workflow",
        "port": 3466
    },
    {
        "protocol": "TCP",
        "name": "rcst",
        "port": 3467
    },
    {
        "protocol": "TCP",
        "name": "ttcmremotectrl",
        "port": 3468
    },
    {
        "protocol": "TCP",
        "name": "pluribus",
        "port": 3469
    },
    {
        "protocol": "TCP",
        "name": "jt400",
        "port": 3470
    },
    {
        "protocol": "TCP",
        "name": "jt400-ssl",
        "port": 3471
    },
    {
        "protocol": "TCP",
        "name": "jaugsremotec-1",
        "port": 3472
    },
    {
        "protocol": "TCP",
        "name": "jaugsremotec-2",
        "port": 3473
    },
    {
        "protocol": "TCP",
        "name": "ttntspauto",
        "port": 3474
    },
    {
        "protocol": "TCP",
        "name": "genisar-port",
        "port": 3475
    },
    {
        "protocol": "TCP",
        "name": "nppmp",
        "port": 3476
    },
    {
        "protocol": "TCP",
        "name": "ecomm",
        "port": 3477
    },
    {
        "protocol": "TCP",
        "name": "nat-stun-port",
        "port": 3478
    },
    {
        "protocol": "TCP",
        "name": "twrpc",
        "port": 3479
    },
    {
        "protocol": "TCP",
        "name": "plethora",
        "port": 3480
    },
    {
        "protocol": "TCP",
        "name": "cleanerliverc",
        "port": 3481
    },
    {
        "protocol": "TCP",
        "name": "vulture",
        "port": 3482
    },
    {
        "protocol": "TCP",
        "name": "slim-devices",
        "port": 3483
    },
    {
        "protocol": "TCP",
        "name": "gbs-stp",
        "port": 3484
    },
    {
        "protocol": "TCP",
        "name": "celatalk",
        "port": 3485
    },
    {
        "protocol": "TCP",
        "name": "ifsf-hb-port",
        "port": 3486
    },
    {
        "protocol": "TCP",
        "name": "ltcTCP",
        "port": 3487
    },
    {
        "protocol": "TCP",
        "name": "fs-rh-srv",
        "port": 3488
    },
    {
        "protocol": "TCP",
        "name": "dtp-dia",
        "port": 3489
    },
    {
        "protocol": "TCP",
        "name": "colubris",
        "port": 3490
    },
    {
        "protocol": "TCP",
        "name": "swr-port",
        "port": 3491
    },
    {
        "protocol": "TCP",
        "name": "tvdumtray-port",
        "port": 3492
    },
    {
        "protocol": "TCP",
        "name": "nut",
        "port": 3493
    },
    {
        "protocol": "TCP",
        "name": "ibm3494",
        "port": 3494
    },
    {
        "protocol": "TCP",
        "name": "seclayer-TCP",
        "port": 3495
    },
    {
        "protocol": "TCP",
        "name": "seclayer-tls",
        "port": 3496
    },
    {
        "protocol": "TCP",
        "name": "ipether232port",
        "port": 3497
    },
    {
        "protocol": "TCP",
        "name": "dashpas-port",
        "port": 3498
    },
    {
        "protocol": "TCP",
        "name": "sccip-media",
        "port": 3499
    },
    {
        "protocol": "TCP",
        "name": "rtmp-port",
        "port": 3500
    },
    {
        "protocol": "TCP",
        "name": "isoft-p2p",
        "port": 3501
    },
    {
        "protocol": "TCP",
        "name": "avinstalldisc",
        "port": 3502
    },
    {
        "protocol": "TCP",
        "name": "lsp-ping",
        "port": 3503
    },
    {
        "protocol": "TCP",
        "name": "ironstorm",
        "port": 3504
    },
    {
        "protocol": "TCP",
        "name": "ccmcomm",
        "port": 3505
    },
    {
        "protocol": "TCP",
        "name": "apc-3506",
        "port": 3506
    },
    {
        "protocol": "TCP",
        "name": "nesh-broker",
        "port": 3507
    },
    {
        "protocol": "TCP",
        "name": "interactionweb",
        "port": 3508
    },
    {
        "protocol": "TCP",
        "name": "vt-ssl",
        "port": 3509
    },
    {
        "protocol": "TCP",
        "name": "xss-port",
        "port": 3510
    },
    {
        "protocol": "TCP",
        "name": "webmail-2",
        "port": 3511
    },
    {
        "protocol": "TCP",
        "name": "aztec",
        "port": 3512
    },
    {
        "protocol": "TCP",
        "name": "arcpd",
        "port": 3513
    },
    {
        "protocol": "TCP",
        "name": "must-p2p",
        "port": 3514
    },
    {
        "protocol": "TCP",
        "name": "must-backplane",
        "port": 3515
    },
    {
        "protocol": "TCP",
        "name": "smartcard-port",
        "port": 3516
    },
    {
        "protocol": "TCP",
        "name": "802-11-iapp",
        "port": 3517
    },
    {
        "protocol": "TCP",
        "name": "artifact-msg",
        "port": 3518
    },
    {
        "protocol": "TCP",
        "name": "nvmsgd",
        "port": 3519
    },
    {
        "protocol": "TCP",
        "name": "galileolog",
        "port": 3520
    },
    {
        "protocol": "TCP",
        "name": "mc3ss",
        "port": 3521
    },
    {
        "protocol": "TCP",
        "name": "nssocketport",
        "port": 3522
    },
    {
        "protocol": "TCP",
        "name": "odeumservlink",
        "port": 3523
    },
    {
        "protocol": "TCP",
        "name": "ecmport",
        "port": 3524
    },
    {
        "protocol": "TCP",
        "name": "eisport",
        "port": 3525
    },
    {
        "protocol": "TCP",
        "name": "starquiz-port",
        "port": 3526
    },
    {
        "protocol": "TCP",
        "name": "beserver-msg-q",
        "port": 3527
    },
    {
        "protocol": "TCP",
        "name": "jboss-iiop",
        "port": 3528
    },
    {
        "protocol": "TCP",
        "name": "jboss-iiop-ssl",
        "port": 3529
    },
    {
        "protocol": "TCP",
        "name": "gf",
        "port": 3530
    },
    {
        "protocol": "TCP",
        "name": "joltid",
        "port": 3531
    },
    {
        "protocol": "TCP",
        "name": "raven-rmp",
        "port": 3532
    },
    {
        "protocol": "TCP",
        "name": "urld-port",
        "port": 3533
    },
    {
        "protocol": "TCP",
        "name": "ms-la",
        "port": 3535
    },
    {
        "protocol": "TCP",
        "name": "snac",
        "port": 3536
    },
    {
        "protocol": "TCP",
        "name": "ni-visa-remote",
        "port": 3537
    },
    {
        "protocol": "TCP",
        "name": "ibm-diradm",
        "port": 3538
    },
    {
        "protocol": "TCP",
        "name": "ibm-diradm-ssl",
        "port": 3539
    },
    {
        "protocol": "TCP",
        "name": "pnrp-port",
        "port": 3540
    },
    {
        "protocol": "TCP",
        "name": "voispeed-port",
        "port": 3541
    },
    {
        "protocol": "TCP",
        "name": "hacl-monitor",
        "port": 3542
    },
    {
        "protocol": "TCP",
        "name": "qftest-lookup",
        "port": 3543
    },
    {
        "protocol": "TCP",
        "name": "teredo",
        "port": 3544
    },
    {
        "protocol": "TCP",
        "name": "camac",
        "port": 3545
    },
    {
        "protocol": "TCP",
        "name": "symantec-sim",
        "port": 3547
    },
    {
        "protocol": "TCP",
        "name": "interworld",
        "port": 3548
    },
    {
        "protocol": "TCP",
        "name": "tellumat-nms",
        "port": 3549
    },
    {
        "protocol": "TCP",
        "name": "ssmpp",
        "port": 3550
    },
    {
        "protocol": "TCP",
        "name": "apcupsd",
        "port": 3551
    },
    {
        "protocol": "TCP",
        "name": "taserver",
        "port": 3552
    },
    {
        "protocol": "TCP",
        "name": "rbr-discovery",
        "port": 3553
    },
    {
        "protocol": "TCP",
        "name": "questnotify",
        "port": 3554
    },
    {
        "protocol": "TCP",
        "name": "razor",
        "port": 3555
    },
    {
        "protocol": "TCP",
        "name": "sky-transport",
        "port": 3556
    },
    {
        "protocol": "TCP",
        "name": "personalos-001",
        "port": 3557
    },
    {
        "protocol": "TCP",
        "name": "mcp-port",
        "port": 3558
    },
    {
        "protocol": "TCP",
        "name": "cctv-port",
        "port": 3559
    },
    {
        "protocol": "TCP",
        "name": "iniserve-port",
        "port": 3560
    },
    {
        "protocol": "TCP",
        "name": "bmc-onekey",
        "port": 3561
    },
    {
        "protocol": "TCP",
        "name": "sdbproxy",
        "port": 3562
    },
    {
        "protocol": "TCP",
        "name": "watcomdebug",
        "port": 3563
    },
    {
        "protocol": "TCP",
        "name": "esimport",
        "port": 3564
    },
    {
        "protocol": "TCP",
        "name": "quest-launcher",
        "port": 3566
    },
    {
        "protocol": "TCP",
        "name": "emware-oft",
        "port": 3567
    },
    {
        "protocol": "TCP",
        "name": "emware-epss",
        "port": 3568
    },
    {
        "protocol": "TCP",
        "name": "mbg-ctrl",
        "port": 3569
    },
    {
        "protocol": "TCP",
        "name": "mccwebsvr-port",
        "port": 3570
    },
    {
        "protocol": "TCP",
        "name": "megardsvr-port",
        "port": 3571
    },
    {
        "protocol": "TCP",
        "name": "megaregsvrport",
        "port": 3572
    },
    {
        "protocol": "TCP",
        "name": "tag-ups-1",
        "port": 3573
    },
    {
        "protocol": "TCP",
        "name": "dmaf-server",
        "port": 3574
    },
    {
        "protocol": "TCP",
        "name": "ccm-port",
        "port": 3575
    },
    {
        "protocol": "TCP",
        "name": "cmc-port",
        "port": 3576
    },
    {
        "protocol": "TCP",
        "name": "config-port",
        "port": 3577
    },
    {
        "protocol": "TCP",
        "name": "data-port",
        "port": 3578
    },
    {
        "protocol": "TCP",
        "name": "ttat3lb",
        "port": 3579
    },
    {
        "protocol": "TCP",
        "name": "nati-svrloc",
        "port": 3580
    },
    {
        "protocol": "TCP",
        "name": "kfxaclicensing",
        "port": 3581
    },
    {
        "protocol": "TCP",
        "name": "press",
        "port": 3582
    },
    {
        "protocol": "TCP",
        "name": "canex-watch",
        "port": 3583
    },
    {
        "protocol": "TCP",
        "name": "u-dbap",
        "port": 3584
    },
    {
        "protocol": "TCP",
        "name": "emprise-lls",
        "port": 3585
    },
    {
        "protocol": "TCP",
        "name": "emprise-lsc",
        "port": 3586
    },
    {
        "protocol": "TCP",
        "name": "p2pgroup",
        "port": 3587
    },
    {
        "protocol": "TCP",
        "name": "sentinel",
        "port": 3588
    },
    {
        "protocol": "TCP",
        "name": "isomair",
        "port": 3589
    },
    {
        "protocol": "TCP",
        "name": "wv-csp-sms",
        "port": 3590
    },
    {
        "protocol": "TCP",
        "name": "gtrack-server",
        "port": 3591
    },
    {
        "protocol": "TCP",
        "name": "gtrack-ne",
        "port": 3592
    },
    {
        "protocol": "TCP",
        "name": "bpmd",
        "port": 3593
    },
    {
        "protocol": "TCP",
        "name": "mediaspace",
        "port": 3594
    },
    {
        "protocol": "TCP",
        "name": "shareapp",
        "port": 3595
    },
    {
        "protocol": "TCP",
        "name": "iw-mmogame",
        "port": 3596
    },
    {
        "protocol": "TCP",
        "name": "a14",
        "port": 3597
    },
    {
        "protocol": "TCP",
        "name": "a15",
        "port": 3598
    },
    {
        "protocol": "TCP",
        "name": "quasar-server",
        "port": 3599
    },
    {
        "protocol": "TCP",
        "name": "trap-daemon",
        "port": 3600
    },
    {
        "protocol": "TCP",
        "name": "visinet-gui",
        "port": 3601
    },
    {
        "protocol": "TCP",
        "name": "infiniswitchcl",
        "port": 3602
    },
    {
        "protocol": "TCP",
        "name": "int-rcv-cntrl",
        "port": 3603
    },
    {
        "protocol": "TCP",
        "name": "bmc-jmx-port",
        "port": 3604
    },
    {
        "protocol": "TCP",
        "name": "comcam-io",
        "port": 3605
    },
    {
        "protocol": "TCP",
        "name": "splitlock",
        "port": 3606
    },
    {
        "protocol": "TCP",
        "name": "precise-i3",
        "port": 3607
    },
    {
        "protocol": "TCP",
        "name": "trendchip-dcp",
        "port": 3608
    },
    {
        "protocol": "TCP",
        "name": "cpdi-pidas-cm",
        "port": 3609
    },
    {
        "protocol": "TCP",
        "name": "echonet",
        "port": 3610
    },
    {
        "protocol": "TCP",
        "name": "six-degrees",
        "port": 3611
    },
    {
        "protocol": "TCP",
        "name": "hp-dataprotect",
        "port": 3612
    },
    {
        "protocol": "TCP",
        "name": "alaris-disc",
        "port": 3613
    },
    {
        "protocol": "TCP",
        "name": "sigma-port",
        "port": 3614
    },
    {
        "protocol": "TCP",
        "name": "start-network",
        "port": 3615
    },
    {
        "protocol": "TCP",
        "name": "cd3o-protocol",
        "port": 3616
    },
    {
        "protocol": "TCP",
        "name": "sharp-server",
        "port": 3617
    },
    {
        "protocol": "TCP",
        "name": "aairnet-1",
        "port": 3618
    },
    {
        "protocol": "TCP",
        "name": "aairnet-2",
        "port": 3619
    },
    {
        "protocol": "TCP",
        "name": "ep-pcp",
        "port": 3620
    },
    {
        "protocol": "TCP",
        "name": "ep-nsp",
        "port": 3621
    },
    {
        "protocol": "TCP",
        "name": "ff-lr-port",
        "port": 3622
    },
    {
        "protocol": "TCP",
        "name": "haipe-discover",
        "port": 3623
    },
    {
        "protocol": "TCP",
        "name": "dist-upgrade",
        "port": 3624
    },
    {
        "protocol": "TCP",
        "name": "volley",
        "port": 3625
    },
    {
        "protocol": "TCP",
        "name": "bvcdaemon-port",
        "port": 3626
    },
    {
        "protocol": "TCP",
        "name": "jamserverport",
        "port": 3627
    },
    {
        "protocol": "TCP",
        "name": "ept-machine",
        "port": 3628
    },
    {
        "protocol": "TCP",
        "name": "escvpnet",
        "port": 3629
    },
    {
        "protocol": "TCP",
        "name": "cs-remote-db",
        "port": 3630
    },
    {
        "protocol": "TCP",
        "name": "cs-services",
        "port": 3631
    },
    {
        "protocol": "TCP",
        "name": "distcc",
        "port": 3632
    },
    {
        "protocol": "TCP",
        "name": "wacp",
        "port": 3633
    },
    {
        "protocol": "TCP",
        "name": "hlibmgr",
        "port": 3634
    },
    {
        "protocol": "TCP",
        "name": "sdo",
        "port": 3635
    },
    {
        "protocol": "TCP",
        "name": "opscenter",
        "port": 3636
    },
    {
        "protocol": "TCP",
        "name": "scservp",
        "port": 3637
    },
    {
        "protocol": "TCP",
        "name": "ehp-backup",
        "port": 3638
    },
    {
        "protocol": "TCP",
        "name": "xap-ha",
        "port": 3639
    },
    {
        "protocol": "TCP",
        "name": "netplay-port1",
        "port": 3640
    },
    {
        "protocol": "TCP",
        "name": "netplay-port2",
        "port": 3641
    },
    {
        "protocol": "TCP",
        "name": "juxml-port",
        "port": 3642
    },
    {
        "protocol": "TCP",
        "name": "audiojuggler",
        "port": 3643
    },
    {
        "protocol": "TCP",
        "name": "ssowatch",
        "port": 3644
    },
    {
        "protocol": "TCP",
        "name": "cyc",
        "port": 3645
    },
    {
        "protocol": "TCP",
        "name": "xss-srv-port",
        "port": 3646
    },
    {
        "protocol": "TCP",
        "name": "splitlock-gw",
        "port": 3647
    },
    {
        "protocol": "TCP",
        "name": "fjcp",
        "port": 3648
    },
    {
        "protocol": "TCP",
        "name": "nmmp",
        "port": 3649
    },
    {
        "protocol": "TCP",
        "name": "prismiq-plugin",
        "port": 3650
    },
    {
        "protocol": "TCP",
        "name": "xrpc-registry",
        "port": 3651
    },
    {
        "protocol": "TCP",
        "name": "vxcrnbuport",
        "port": 3652
    },
    {
        "protocol": "TCP",
        "name": "tsp",
        "port": 3653
    },
    {
        "protocol": "TCP",
        "name": "vaprtm",
        "port": 3654
    },
    {
        "protocol": "TCP",
        "name": "abatemgr",
        "port": 3655
    },
    {
        "protocol": "TCP",
        "name": "abatjss",
        "port": 3656
    },
    {
        "protocol": "TCP",
        "name": "immedianet-bcn",
        "port": 3657
    },
    {
        "protocol": "TCP",
        "name": "ps-ams",
        "port": 3658
    },
    {
        "protocol": "TCP",
        "name": "apple-sasl",
        "port": 3659
    },
    {
        "protocol": "TCP",
        "name": "can-nds-ssl",
        "port": 3660
    },
    {
        "protocol": "TCP",
        "name": "can-ferret-ssl",
        "port": 3661
    },
    {
        "protocol": "TCP",
        "name": "pserver",
        "port": 3662
    },
    {
        "protocol": "TCP",
        "name": "dtp",
        "port": 3663
    },
    {
        "protocol": "TCP",
        "name": "ups-engine",
        "port": 3664
    },
    {
        "protocol": "TCP",
        "name": "ent-engine",
        "port": 3665
    },
    {
        "protocol": "TCP",
        "name": "eserver-pap",
        "port": 3666
    },
    {
        "protocol": "TCP",
        "name": "infoexch",
        "port": 3667
    },
    {
        "protocol": "TCP",
        "name": "dell-rm-port",
        "port": 3668
    },
    {
        "protocol": "TCP",
        "name": "casanswmgmt",
        "port": 3669
    },
    {
        "protocol": "TCP",
        "name": "smile",
        "port": 3670
    },
    {
        "protocol": "TCP",
        "name": "efcp",
        "port": 3671
    },
    {
        "protocol": "TCP",
        "name": "lispworks-orb",
        "port": 3672
    },
    {
        "protocol": "TCP",
        "name": "mediavault-gui",
        "port": 3673
    },
    {
        "protocol": "TCP",
        "name": "wininstall-ipc",
        "port": 3674
    },
    {
        "protocol": "TCP",
        "name": "calltrax",
        "port": 3675
    },
    {
        "protocol": "TCP",
        "name": "va-pacbase",
        "port": 3676
    },
    {
        "protocol": "TCP",
        "name": "roverlog",
        "port": 3677
    },
    {
        "protocol": "TCP",
        "name": "ipr-dglt",
        "port": 3678
    },
    {
        "protocol": "TCP",
        "name": "newton-dock",
        "port": 3679
    },
    {
        "protocol": "TCP",
        "name": "npds-tracker",
        "port": 3680
    },
    {
        "protocol": "TCP",
        "name": "bts-x73",
        "port": 3681
    },
    {
        "protocol": "TCP",
        "name": "cas-mapi",
        "port": 3682
    },
    {
        "protocol": "TCP",
        "name": "bmc-ea",
        "port": 3683
    },
    {
        "protocol": "TCP",
        "name": "faxstfx-port",
        "port": 3684
    },
    {
        "protocol": "TCP",
        "name": "dsx-agent",
        "port": 3685
    },
    {
        "protocol": "TCP",
        "name": "tnmpv2",
        "port": 3686
    },
    {
        "protocol": "TCP",
        "name": "simple-push",
        "port": 3687
    },
    {
        "protocol": "TCP",
        "name": "simple-push-s",
        "port": 3688
    },
    {
        "protocol": "TCP",
        "name": "daap",
        "port": 3689
    },
    {
        "protocol": "TCP",
        "name": "svn",
        "port": 3690
    },
    {
        "protocol": "TCP",
        "name": "magaya-network",
        "port": 3691
    },
    {
        "protocol": "TCP",
        "name": "intelsync",
        "port": 3692
    },
    {
        "protocol": "TCP",
        "name": "gttp",
        "port": 3693
    },
    {
        "protocol": "TCP",
        "name": "vpntpp",
        "port": 3694
    },
    {
        "protocol": "TCP",
        "name": "bmc-data-coll",
        "port": 3695
    },
    {
        "protocol": "TCP",
        "name": "telneTCPcd",
        "port": 3696
    },
    {
        "protocol": "TCP",
        "name": "nw-license",
        "port": 3697
    },
    {
        "protocol": "TCP",
        "name": "sagectlpanel",
        "port": 3698
    },
    {
        "protocol": "TCP",
        "name": "kpn-icw",
        "port": 3699
    },
    {
        "protocol": "TCP",
        "name": "lrs-paging",
        "port": 3700
    },
    {
        "protocol": "TCP",
        "name": "netcelera",
        "port": 3701
    },
    {
        "protocol": "TCP",
        "name": "ws-discovery",
        "port": 3702
    },
    {
        "protocol": "TCP",
        "name": "adobeserver-3",
        "port": 3703
    },
    {
        "protocol": "TCP",
        "name": "adobeserver-4",
        "port": 3704
    },
    {
        "protocol": "TCP",
        "name": "adobeserver-5",
        "port": 3705
    },
    {
        "protocol": "TCP",
        "name": "rt-event",
        "port": 3706
    },
    {
        "protocol": "TCP",
        "name": "rt-event-s",
        "port": 3707
    },
    {
        "protocol": "TCP",
        "name": "sun-as-iiops",
        "port": 3708
    },
    {
        "protocol": "TCP",
        "name": "ca-idms",
        "port": 3709
    },
    {
        "protocol": "TCP",
        "name": "portgate-auth",
        "port": 3710
    },
    {
        "protocol": "TCP",
        "name": "edb-server2",
        "port": 3711
    },
    {
        "protocol": "TCP",
        "name": "sentinel-ent",
        "port": 3712
    },
    {
        "protocol": "TCP",
        "name": "tftps",
        "port": 3713
    },
    {
        "protocol": "TCP",
        "name": "delos-dms",
        "port": 3714
    },
    {
        "protocol": "TCP",
        "name": "anoto-rendezv",
        "port": 3715
    },
    {
        "protocol": "TCP",
        "name": "wv-csp-sms-cir",
        "port": 3716
    },
    {
        "protocol": "TCP",
        "name": "wv-csp-udp-cir",
        "port": 3717
    },
    {
        "protocol": "TCP",
        "name": "opus-services",
        "port": 3718
    },
    {
        "protocol": "TCP",
        "name": "itelserverport",
        "port": 3719
    },
    {
        "protocol": "TCP",
        "name": "ufastro-instr",
        "port": 3720
    },
    {
        "protocol": "TCP",
        "name": "xsync",
        "port": 3721
    },
    {
        "protocol": "TCP",
        "name": "xserveraid",
        "port": 3722
    },
    {
        "protocol": "TCP",
        "name": "sychrond",
        "port": 3723
    },
    {
        "protocol": "TCP",
        "name": "blizwow",
        "port": 3724
    },
    {
        "protocol": "TCP",
        "name": "na-er-tip",
        "port": 3725
    },
    {
        "protocol": "TCP",
        "name": "array-manager",
        "port": 3726
    },
    {
        "protocol": "TCP",
        "name": "e-mdu",
        "port": 3727
    },
    {
        "protocol": "TCP",
        "name": "e-woa",
        "port": 3728
    },
    {
        "protocol": "TCP",
        "name": "fksp-audit",
        "port": 3729
    },
    {
        "protocol": "TCP",
        "name": "client-ctrl",
        "port": 3730
    },
    {
        "protocol": "TCP",
        "name": "smap",
        "port": 3731
    },
    {
        "protocol": "TCP",
        "name": "m-wnn",
        "port": 3732
    },
    {
        "protocol": "TCP",
        "name": "multip-msg",
        "port": 3733
    },
    {
        "protocol": "TCP",
        "name": "synel-data",
        "port": 3734
    },
    {
        "protocol": "TCP",
        "name": "pwdis",
        "port": 3735
    },
    {
        "protocol": "TCP",
        "name": "rs-rmi",
        "port": 3736
    },
    {
        "protocol": "TCP",
        "name": "versatalk",
        "port": 3738
    },
    {
        "protocol": "TCP",
        "name": "launchbird-lm",
        "port": 3739
    },
    {
        "protocol": "TCP",
        "name": "heartbeat",
        "port": 3740
    },
    {
        "protocol": "TCP",
        "name": "wysdma",
        "port": 3741
    },
    {
        "protocol": "TCP",
        "name": "cst-port",
        "port": 3742
    },
    {
        "protocol": "TCP",
        "name": "ipcs-command",
        "port": 3743
    },
    {
        "protocol": "TCP",
        "name": "sasg",
        "port": 3744
    },
    {
        "protocol": "TCP",
        "name": "gw-call-port",
        "port": 3745
    },
    {
        "protocol": "TCP",
        "name": "linktest",
        "port": 3746
    },
    {
        "protocol": "TCP",
        "name": "linktest-s",
        "port": 3747
    },
    {
        "protocol": "TCP",
        "name": "webdata",
        "port": 3748
    },
    {
        "protocol": "TCP",
        "name": "cimtrak",
        "port": 3749
    },
    {
        "protocol": "TCP",
        "name": "cbos-ip-port",
        "port": 3750
    },
    {
        "protocol": "TCP",
        "name": "gprs-cube",
        "port": 3751
    },
    {
        "protocol": "TCP",
        "name": "vipremoteagent",
        "port": 3752
    },
    {
        "protocol": "TCP",
        "name": "nattyserver",
        "port": 3753
    },
    {
        "protocol": "TCP",
        "name": "timestenbroker",
        "port": 3754
    },
    {
        "protocol": "TCP",
        "name": "sas-remote-hlp",
        "port": 3755
    },
    {
        "protocol": "TCP",
        "name": "canon-capt",
        "port": 3756
    },
    {
        "protocol": "TCP",
        "name": "grf-port",
        "port": 3757
    },
    {
        "protocol": "TCP",
        "name": "apw-registry",
        "port": 3758
    },
    {
        "protocol": "TCP",
        "name": "exapt-lmgr",
        "port": 3759
    },
    {
        "protocol": "TCP",
        "name": "adtempusclient",
        "port": 3760
    },
    {
        "protocol": "TCP",
        "name": "gsakmp",
        "port": 3761
    },
    {
        "protocol": "TCP",
        "name": "gbs-smp",
        "port": 3762
    },
    {
        "protocol": "TCP",
        "name": "xo-wave",
        "port": 3763
    },
    {
        "protocol": "TCP",
        "name": "mni-prot-rout",
        "port": 3764
    },
    {
        "protocol": "TCP",
        "name": "rtraceroute",
        "port": 3765
    },
    {
        "protocol": "TCP",
        "name": "listmgr-port",
        "port": 3767
    },
    {
        "protocol": "TCP",
        "name": "rblcheckd",
        "port": 3768
    },
    {
        "protocol": "TCP",
        "name": "haipe-otnk",
        "port": 3769
    },
    {
        "protocol": "TCP",
        "name": "cindycollab",
        "port": 3770
    },
    {
        "protocol": "TCP",
        "name": "paging-port",
        "port": 3771
    },
    {
        "protocol": "TCP",
        "name": "ctp",
        "port": 3772
    },
    {
        "protocol": "TCP",
        "name": "ctdhercules",
        "port": 3773
    },
    {
        "protocol": "TCP",
        "name": "zicom",
        "port": 3774
    },
    {
        "protocol": "TCP",
        "name": "ispmmgr",
        "port": 3775
    },
    {
        "protocol": "TCP",
        "name": "dvcprov-port",
        "port": 3776
    },
    {
        "protocol": "TCP",
        "name": "jibe-eb",
        "port": 3777
    },
    {
        "protocol": "TCP",
        "name": "c-h-it-port",
        "port": 3778
    },
    {
        "protocol": "TCP",
        "name": "cognima",
        "port": 3779
    },
    {
        "protocol": "TCP",
        "name": "nnp",
        "port": 3780
    },
    {
        "protocol": "TCP",
        "name": "abcvoice-port",
        "port": 3781
    },
    {
        "protocol": "TCP",
        "name": "iso-tp0s",
        "port": 3782
    },
    {
        "protocol": "TCP",
        "name": "bim-pem",
        "port": 3783
    },
    {
        "protocol": "TCP",
        "name": "bfd-control",
        "port": 3784
    },
    {
        "protocol": "TCP",
        "name": "bfd-echo",
        "port": 3785
    },
    {
        "protocol": "TCP",
        "name": "upstriggervsw",
        "port": 3786
    },
    {
        "protocol": "TCP",
        "name": "fintrx",
        "port": 3787
    },
    {
        "protocol": "TCP",
        "name": "isrp-port",
        "port": 3788
    },
    {
        "protocol": "TCP",
        "name": "remotedeploy",
        "port": 3789
    },
    {
        "protocol": "TCP",
        "name": "quickbooksrds",
        "port": 3790
    },
    {
        "protocol": "TCP",
        "name": "tvnetworkvideo",
        "port": 3791
    },
    {
        "protocol": "TCP",
        "name": "sitewatch",
        "port": 3792
    },
    {
        "protocol": "TCP",
        "name": "dcsoftware",
        "port": 3793
    },
    {
        "protocol": "TCP",
        "name": "jaus",
        "port": 3794
    },
    {
        "protocol": "TCP",
        "name": "myblast",
        "port": 3795
    },
    {
        "protocol": "TCP",
        "name": "spw-dialer",
        "port": 3796
    },
    {
        "protocol": "TCP",
        "name": "idps",
        "port": 3797
    },
    {
        "protocol": "TCP",
        "name": "minilock",
        "port": 3798
    },
    {
        "protocol": "TCP",
        "name": "radius-dynauth",
        "port": 3799
    },
    {
        "protocol": "TCP",
        "name": "pwgpsi",
        "port": 3800
    },
    {
        "protocol": "TCP",
        "name": "vhd",
        "port": 3802
    },
    {
        "protocol": "TCP",
        "name": "soniqsync",
        "port": 3803
    },
    {
        "protocol": "TCP",
        "name": "iqnet-port",
        "port": 3804
    },
    {
        "protocol": "TCP",
        "name": "TCPdataserver",
        "port": 3805
    },
    {
        "protocol": "TCP",
        "name": "wsmlb",
        "port": 3806
    },
    {
        "protocol": "TCP",
        "name": "spugna",
        "port": 3807
    },
    {
        "protocol": "TCP",
        "name": "sun-as-iiops-ca",
        "port": 3808
    },
    {
        "protocol": "TCP",
        "name": "apocd",
        "port": 3809
    },
    {
        "protocol": "TCP",
        "name": "wlanauth",
        "port": 3810
    },
    {
        "protocol": "TCP",
        "name": "amp",
        "port": 3811
    },
    {
        "protocol": "TCP",
        "name": "neto-wol-server",
        "port": 3812
    },
    {
        "protocol": "TCP",
        "name": "rap-ip",
        "port": 3813
    },
    {
        "protocol": "TCP",
        "name": "neto-dcs",
        "port": 3814
    },
    {
        "protocol": "TCP",
        "name": "lansurveyorxml",
        "port": 3815
    },
    {
        "protocol": "TCP",
        "name": "sunlps-http",
        "port": 3816
    },
    {
        "protocol": "TCP",
        "name": "tapeware",
        "port": 3817
    },
    {
        "protocol": "TCP",
        "name": "crinis-hb",
        "port": 3818
    },
    {
        "protocol": "TCP",
        "name": "epl-slp",
        "port": 3819
    },
    {
        "protocol": "TCP",
        "name": "scp",
        "port": 3820
    },
    {
        "protocol": "TCP",
        "name": "pmcp",
        "port": 3821
    },
    {
        "protocol": "TCP",
        "name": "acp-discovery",
        "port": 3822
    },
    {
        "protocol": "TCP",
        "name": "acp-conduit",
        "port": 3823
    },
    {
        "protocol": "TCP",
        "name": "acp-policy",
        "port": 3824
    },
    {
        "protocol": "TCP",
        "name": "markem-dcp",
        "port": 3836
    },
    {
        "protocol": "TCP",
        "name": "mkm-discovery",
        "port": 3837
    },
    {
        "protocol": "TCP",
        "name": "sos",
        "port": 3838
    },
    {
        "protocol": "TCP",
        "name": "amx-rms",
        "port": 3839
    },
    {
        "protocol": "TCP",
        "name": "flirtmitmir",
        "port": 3840
    },
    {
        "protocol": "TCP",
        "name": "zfirm-shiprush3",
        "port": 3841
    },
    {
        "protocol": "TCP",
        "name": "nhci",
        "port": 3842
    },
    {
        "protocol": "TCP",
        "name": "quest-agent",
        "port": 3843
    },
    {
        "protocol": "TCP",
        "name": "rnm",
        "port": 3844
    },
    {
        "protocol": "TCP",
        "name": "v-one-spp",
        "port": 3845
    },
    {
        "protocol": "TCP",
        "name": "an-pcp",
        "port": 3846
    },
    {
        "protocol": "TCP",
        "name": "msfw-control",
        "port": 3847
    },
    {
        "protocol": "TCP",
        "name": "item",
        "port": 3848
    },
    {
        "protocol": "TCP",
        "name": "spw-dnspreload",
        "port": 3849
    },
    {
        "protocol": "TCP",
        "name": "qtms-bootstrap",
        "port": 3850
    },
    {
        "protocol": "TCP",
        "name": "spectraport",
        "port": 3851
    },
    {
        "protocol": "TCP",
        "name": "sse-app-config",
        "port": 3852
    },
    {
        "protocol": "TCP",
        "name": "sscan",
        "port": 3853
    },
    {
        "protocol": "TCP",
        "name": "stryker-com",
        "port": 3854
    },
    {
        "protocol": "TCP",
        "name": "opentrac",
        "port": 3855
    },
    {
        "protocol": "TCP",
        "name": "informer",
        "port": 3856
    },
    {
        "protocol": "TCP",
        "name": "trap-port",
        "port": 3857
    },
    {
        "protocol": "TCP",
        "name": "trap-port-mom",
        "port": 3858
    },
    {
        "protocol": "TCP",
        "name": "nav-port",
        "port": 3859
    },
    {
        "protocol": "TCP",
        "name": "ewlm",
        "port": 3860
    },
    {
        "protocol": "TCP",
        "name": "winshadow-hd",
        "port": 3861
    },
    {
        "protocol": "TCP",
        "name": "giga-pocket",
        "port": 3862
    },
    {
        "protocol": "TCP",
        "name": "asap-TCP",
        "port": 3863
    },
    {
        "protocol": "TCP",
        "name": "asap-TCP-tls",
        "port": 3864
    },
    {
        "protocol": "TCP",
        "name": "xpl",
        "port": 3865
    },
    {
        "protocol": "TCP",
        "name": "dzdaemon",
        "port": 3866
    },
    {
        "protocol": "TCP",
        "name": "dzoglserver",
        "port": 3867
    },
    {
        "protocol": "TCP",
        "name": "ovsam-mgmt",
        "port": 3869
    },
    {
        "protocol": "TCP",
        "name": "ovsam-d-agent",
        "port": 3870
    },
    {
        "protocol": "TCP",
        "name": "avocent-adsap",
        "port": 3871
    },
    {
        "protocol": "TCP",
        "name": "oem-agent",
        "port": 3872
    },
    {
        "protocol": "TCP",
        "name": "fagordnc",
        "port": 3873
    },
    {
        "protocol": "TCP",
        "name": "sixxsconfig",
        "port": 3874
    },
    {
        "protocol": "TCP",
        "name": "pnbscada",
        "port": 3875
    },
    {
        "protocol": "TCP",
        "name": "dl_agent",
        "port": 3876
    },
    {
        "protocol": "TCP",
        "name": "xmpcr-interface",
        "port": 3877
    },
    {
        "protocol": "TCP",
        "name": "fotogcad",
        "port": 3878
    },
    {
        "protocol": "TCP",
        "name": "appss-lm",
        "port": 3879
    },
    {
        "protocol": "TCP",
        "name": "microgrid",
        "port": 3880
    },
    {
        "protocol": "TCP",
        "name": "idac",
        "port": 3881
    },
    {
        "protocol": "TCP",
        "name": "msdts1",
        "port": 3882
    },
    {
        "protocol": "TCP",
        "name": "vrpn",
        "port": 3883
    },
    {
        "protocol": "TCP",
        "name": "softrack-meter",
        "port": 3884
    },
    {
        "protocol": "TCP",
        "name": "topflow-ssl",
        "port": 3885
    },
    {
        "protocol": "TCP",
        "name": "nei-management",
        "port": 3886
    },
    {
        "protocol": "TCP",
        "name": "ciphire-data",
        "port": 3887
    },
    {
        "protocol": "TCP",
        "name": "ciphire-serv",
        "port": 3888
    },
    {
        "protocol": "TCP",
        "name": "dandv-tester",
        "port": 3889
    },
    {
        "protocol": "TCP",
        "name": "ndsconnect",
        "port": 3890
    },
    {
        "protocol": "TCP",
        "name": "rtc-pm-port",
        "port": 3891
    },
    {
        "protocol": "TCP",
        "name": "pcc-image-port",
        "port": 3892
    },
    {
        "protocol": "TCP",
        "name": "cgi-starapi",
        "port": 3893
    },
    {
        "protocol": "TCP",
        "name": "syam-agent",
        "port": 3894
    },
    {
        "protocol": "TCP",
        "name": "syam-smc",
        "port": 3895
    },
    {
        "protocol": "TCP",
        "name": "sdo-tls",
        "port": 3896
    },
    {
        "protocol": "TCP",
        "name": "sdo-ssh",
        "port": 3897
    },
    {
        "protocol": "TCP",
        "name": "senip",
        "port": 3898
    },
    {
        "protocol": "TCP",
        "name": "itv-control",
        "port": 3899
    },
    {
        "protocol": "TCP",
        "name": "udt_os",
        "port": 3900
    },
    {
        "protocol": "TCP",
        "name": "nimsh",
        "port": 3901
    },
    {
        "protocol": "TCP",
        "name": "nimaux",
        "port": 3902
    },
    {
        "protocol": "TCP",
        "name": "charsetmgr",
        "port": 3903
    },
    {
        "protocol": "TCP",
        "name": "omnilink-port",
        "port": 3904
    },
    {
        "protocol": "TCP",
        "name": "mupdate",
        "port": 3905
    },
    {
        "protocol": "TCP",
        "name": "topovista-data",
        "port": 3906
    },
    {
        "protocol": "TCP",
        "name": "imoguia-port",
        "port": 3907
    },
    {
        "protocol": "TCP",
        "name": "hppronetman",
        "port": 3908
    },
    {
        "protocol": "TCP",
        "name": "surfcontrolcpa",
        "port": 3909
    },
    {
        "protocol": "TCP",
        "name": "prnrequest",
        "port": 3910
    },
    {
        "protocol": "TCP",
        "name": "prnstatus",
        "port": 3911
    },
    {
        "protocol": "TCP",
        "name": "gbmt-stars",
        "port": 3912
    },
    {
        "protocol": "TCP",
        "name": "listcrt-port",
        "port": 3913
    },
    {
        "protocol": "TCP",
        "name": "listcrt-port-2",
        "port": 3914
    },
    {
        "protocol": "TCP",
        "name": "agcat",
        "port": 3915
    },
    {
        "protocol": "TCP",
        "name": "wysdmc",
        "port": 3916
    },
    {
        "protocol": "TCP",
        "name": "aftmux",
        "port": 3917
    },
    {
        "protocol": "TCP",
        "name": "pktcablemmcops",
        "port": 3918
    },
    {
        "protocol": "TCP",
        "name": "hyperip",
        "port": 3919
    },
    {
        "protocol": "TCP",
        "name": "exasoftport1",
        "port": 3920
    },
    {
        "protocol": "TCP",
        "name": "herodotus-net",
        "port": 3921
    },
    {
        "protocol": "TCP",
        "name": "sor-update",
        "port": 3922
    },
    {
        "protocol": "TCP",
        "name": "symb-sb-port",
        "port": 3923
    },
    {
        "protocol": "TCP",
        "name": "mpl-gprs-port",
        "port": 3924
    },
    {
        "protocol": "TCP",
        "name": "zmp",
        "port": 3925
    },
    {
        "protocol": "TCP",
        "name": "winport",
        "port": 3926
    },
    {
        "protocol": "TCP",
        "name": "natdataservice",
        "port": 3927
    },
    {
        "protocol": "TCP",
        "name": "netboot-pxe",
        "port": 3928
    },
    {
        "protocol": "TCP",
        "name": "smauth-port",
        "port": 3929
    },
    {
        "protocol": "TCP",
        "name": "syam-webserver",
        "port": 3930
    },
    {
        "protocol": "TCP",
        "name": "msr-plugin-port",
        "port": 3931
    },
    {
        "protocol": "TCP",
        "name": "dyn-site",
        "port": 3932
    },
    {
        "protocol": "TCP",
        "name": "plbserve-port",
        "port": 3933
    },
    {
        "protocol": "TCP",
        "name": "sunfm-port",
        "port": 3934
    },
    {
        "protocol": "TCP",
        "name": "sdp-portmapper",
        "port": 3935
    },
    {
        "protocol": "TCP",
        "name": "mailprox",
        "port": 3936
    },
    {
        "protocol": "TCP",
        "name": "dvbservdscport",
        "port": 3937
    },
    {
        "protocol": "TCP",
        "name": "dbcontrol_agent",
        "port": 3938
    },
    {
        "protocol": "TCP",
        "name": "aamp",
        "port": 3939
    },
    {
        "protocol": "TCP",
        "name": "xecp-node",
        "port": 3940
    },
    {
        "protocol": "TCP",
        "name": "homeportal-web",
        "port": 3941
    },
    {
        "protocol": "TCP",
        "name": "srdp",
        "port": 3942
    },
    {
        "protocol": "TCP",
        "name": "tig",
        "port": 3943
    },
    {
        "protocol": "TCP",
        "name": "sops",
        "port": 3944
    },
    {
        "protocol": "TCP",
        "name": "emcads",
        "port": 3945
    },
    {
        "protocol": "TCP",
        "name": "backupedge",
        "port": 3946
    },
    {
        "protocol": "TCP",
        "name": "ccp",
        "port": 3947
    },
    {
        "protocol": "TCP",
        "name": "apdap",
        "port": 3948
    },
    {
        "protocol": "TCP",
        "name": "drip",
        "port": 3949
    },
    {
        "protocol": "TCP",
        "name": "namemunge",
        "port": 3950
    },
    {
        "protocol": "TCP",
        "name": "pwgippfax",
        "port": 3951
    },
    {
        "protocol": "TCP",
        "name": "i3-sessionmgr",
        "port": 3952
    },
    {
        "protocol": "TCP",
        "name": "xmlink-connect",
        "port": 3953
    },
    {
        "protocol": "TCP",
        "name": "adrep",
        "port": 3954
    },
    {
        "protocol": "TCP",
        "name": "p2pcommunity",
        "port": 3955
    },
    {
        "protocol": "TCP",
        "name": "gvcp",
        "port": 3956
    },
    {
        "protocol": "TCP",
        "name": "mqe-broker",
        "port": 3957
    },
    {
        "protocol": "TCP",
        "name": "mqe-agent",
        "port": 3958
    },
    {
        "protocol": "TCP",
        "name": "treehopper",
        "port": 3959
    },
    {
        "protocol": "TCP",
        "name": "bess",
        "port": 3960
    },
    {
        "protocol": "TCP",
        "name": "proaxess",
        "port": 3961
    },
    {
        "protocol": "TCP",
        "name": "sbi-agent",
        "port": 3962
    },
    {
        "protocol": "TCP",
        "name": "thrp",
        "port": 3963
    },
    {
        "protocol": "TCP",
        "name": "sasggprs",
        "port": 3964
    },
    {
        "protocol": "TCP",
        "name": "ati-ip-to-ncpe",
        "port": 3965
    },
    {
        "protocol": "TCP",
        "name": "bflckmgr",
        "port": 3966
    },
    {
        "protocol": "TCP",
        "name": "ppsms",
        "port": 3967
    },
    {
        "protocol": "TCP",
        "name": "ianywhere-dbns",
        "port": 3968
    },
    {
        "protocol": "TCP",
        "name": "landmarks",
        "port": 3969
    },
    {
        "protocol": "TCP",
        "name": "cobraclient",
        "port": 3970
    },
    {
        "protocol": "TCP",
        "name": "cobraserver",
        "port": 3971
    },
    {
        "protocol": "TCP",
        "name": "iconp",
        "port": 3972
    },
    {
        "protocol": "TCP",
        "name": "progistics",
        "port": 3973
    },
    {
        "protocol": "TCP",
        "name": "citysearch",
        "port": 3974
    },
    {
        "protocol": "TCP",
        "name": "airshot",
        "port": 3975
    },
    {
        "protocol": "TCP",
        "name": "mapper-nodemgr",
        "port": 3984
    },
    {
        "protocol": "TCP",
        "name": "mapper-mapethd",
        "port": 3985
    },
    {
        "protocol": "TCP",
        "name": "mapper-ws_ethd",
        "port": 3986
    },
    {
        "protocol": "TCP",
        "name": "centerline",
        "port": 3987
    },
    {
        "protocol": "TCP",
        "name": "dcs-config",
        "port": 3988
    },
    {
        "protocol": "TCP",
        "name": "bv-queryengine",
        "port": 3989
    },
    {
        "protocol": "TCP",
        "name": "bv-is",
        "port": 3990
    },
    {
        "protocol": "TCP",
        "name": "bv-smcsrv",
        "port": 3991
    },
    {
        "protocol": "TCP",
        "name": "bv-ds",
        "port": 3992
    },
    {
        "protocol": "TCP",
        "name": "bv-agent",
        "port": 3993
    },
    {
        "protocol": "TCP",
        "name": "iss-mgmt-ssl",
        "port": 3995
    },
    {
        "protocol": "TCP",
        "name": "abcsoftware",
        "port": 3996
    },
    {
        "protocol": "TCP",
        "name": "agentsease-db",
        "port": 3997
    },
    {
        "protocol": "TCP",
        "name": "terabase",
        "port": 4000
    },
    {
        "protocol": "TCP",
        "name": "newoak",
        "port": 4001
    },
    {
        "protocol": "TCP",
        "name": "pxc-spvr-ft",
        "port": 4002
    },
    {
        "protocol": "TCP",
        "name": "pxc-splr-ft",
        "port": 4003
    },
    {
        "protocol": "TCP",
        "name": "pxc-roid",
        "port": 4004
    },
    {
        "protocol": "TCP",
        "name": "pxc-pin",
        "port": 4005
    },
    {
        "protocol": "TCP",
        "name": "pxc-spvr",
        "port": 4006
    },
    {
        "protocol": "TCP",
        "name": "pxc-splr",
        "port": 4007
    },
    {
        "protocol": "TCP",
        "name": "netcheque",
        "port": 4008
    },
    {
        "protocol": "TCP",
        "name": "chimera-hwm",
        "port": 4009
    },
    {
        "protocol": "TCP",
        "name": "samsung-unidex",
        "port": 4010
    },
    {
        "protocol": "TCP",
        "name": "altserviceboot",
        "port": 4011
    },
    {
        "protocol": "TCP",
        "name": "pda-gate",
        "port": 4012
    },
    {
        "protocol": "TCP",
        "name": "acl-manager",
        "port": 4013
    },
    {
        "protocol": "TCP",
        "name": "taiclock",
        "port": 4014
    },
    {
        "protocol": "TCP",
        "name": "talarian-mcast1",
        "port": 4015
    },
    {
        "protocol": "TCP",
        "name": "talarian-mcast2",
        "port": 4016
    },
    {
        "protocol": "TCP",
        "name": "talarian-mcast3",
        "port": 4017
    },
    {
        "protocol": "TCP",
        "name": "talarian-mcast4",
        "port": 4018
    },
    {
        "protocol": "TCP",
        "name": "talarian-mcast5",
        "port": 4019
    },
    {
        "protocol": "TCP",
        "name": "trap",
        "port": 4020
    },
    {
        "protocol": "TCP",
        "name": "nexus-portal",
        "port": 4021
    },
    {
        "protocol": "TCP",
        "name": "dnox",
        "port": 4022
    },
    {
        "protocol": "TCP",
        "name": "esnm-zoning",
        "port": 4023
    },
    {
        "protocol": "TCP",
        "name": "tnp1-port",
        "port": 4024
    },
    {
        "protocol": "TCP",
        "name": "partimage",
        "port": 4025
    },
    {
        "protocol": "TCP",
        "name": "as-debug",
        "port": 4026
    },
    {
        "protocol": "TCP",
        "name": "bxp",
        "port": 4027
    },
    {
        "protocol": "TCP",
        "name": "dtserver-port",
        "port": 4028
    },
    {
        "protocol": "TCP",
        "name": "ip-qsig",
        "port": 4029
    },
    {
        "protocol": "TCP",
        "name": "jdmn-port",
        "port": 4030
    },
    {
        "protocol": "TCP",
        "name": "suucp",
        "port": 4031
    },
    {
        "protocol": "TCP",
        "name": "vrts-auth-port",
        "port": 4032
    },
    {
        "protocol": "TCP",
        "name": "sanavigator",
        "port": 4033
    },
    {
        "protocol": "TCP",
        "name": "ubxd",
        "port": 4034
    },
    {
        "protocol": "TCP",
        "name": "wap-push-http",
        "port": 4035
    },
    {
        "protocol": "TCP",
        "name": "wap-push-https",
        "port": 4036
    },
    {
        "protocol": "TCP",
        "name": "ravehd",
        "port": 4037
    },
    {
        "protocol": "TCP",
        "name": "yo-main",
        "port": 4040
    },
    {
        "protocol": "TCP",
        "name": "houston",
        "port": 4041
    },
    {
        "protocol": "TCP",
        "name": "ldxp",
        "port": 4042
    },
    {
        "protocol": "TCP",
        "name": "nirp",
        "port": 4043
    },
    {
        "protocol": "TCP",
        "name": "ltp",
        "port": 4044
    },
    {
        "protocol": "TCP",
        "name": "npp",
        "port": 4045
    },
    {
        "protocol": "TCP",
        "name": "acp-proto",
        "port": 4046
    },
    {
        "protocol": "TCP",
        "name": "ctp-state",
        "port": 4047
    },
    {
        "protocol": "TCP",
        "name": "wafs",
        "port": 4049
    },
    {
        "protocol": "TCP",
        "name": "cisco-wafs",
        "port": 4050
    },
    {
        "protocol": "TCP",
        "name": "bre",
        "port": 4096
    },
    {
        "protocol": "TCP",
        "name": "patrolview",
        "port": 4097
    },
    {
        "protocol": "TCP",
        "name": "drmsfsd",
        "port": 4098
    },
    {
        "protocol": "TCP",
        "name": "dpcp",
        "port": 4099
    },
    {
        "protocol": "TCP",
        "name": "igo-incognito",
        "port": 4100
    },
    {
        "protocol": "TCP",
        "name": "brlp-0",
        "port": 4101
    },
    {
        "protocol": "TCP",
        "name": "brlp-1",
        "port": 4102
    },
    {
        "protocol": "TCP",
        "name": "brlp-2",
        "port": 4103
    },
    {
        "protocol": "TCP",
        "name": "brlp-3",
        "port": 4104
    },
    {
        "protocol": "TCP",
        "name": "xgrid",
        "port": 4111
    },
    {
        "protocol": "TCP",
        "name": "jomamqmonitor",
        "port": 4114
    },
    {
        "protocol": "TCP",
        "name": "nuts_dem",
        "port": 4132
    },
    {
        "protocol": "TCP",
        "name": "nuts_bootp",
        "port": 4133
    },
    {
        "protocol": "TCP",
        "name": "nifty-hmi",
        "port": 4134
    },
    {
        "protocol": "TCP",
        "name": "nettest",
        "port": 4138
    },
    {
        "protocol": "TCP",
        "name": "thrtx",
        "port": 4139
    },
    {
        "protocol": "TCP",
        "name": "oirtgsvc",
        "port": 4141
    },
    {
        "protocol": "TCP",
        "name": "oidocsvc",
        "port": 4142
    },
    {
        "protocol": "TCP",
        "name": "oidsr",
        "port": 4143
    },
    {
        "protocol": "TCP",
        "name": "vvr-control",
        "port": 4145
    },
    {
        "protocol": "TCP",
        "name": "atlinks",
        "port": 4154
    },
    {
        "protocol": "TCP",
        "name": "jini-discovery",
        "port": 4160
    },
    {
        "protocol": "TCP",
        "name": "omscontact",
        "port": 4161
    },
    {
        "protocol": "TCP",
        "name": "omstopology",
        "port": 4162
    },
    {
        "protocol": "TCP",
        "name": "eims-admin",
        "port": 4199
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4200
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4201
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4202
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4203
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4204
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4205
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4206
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4207
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4208
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4209
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4210
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4211
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4212
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4213
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4214
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4215
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4216
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4217
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4218
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4219
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4220
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4221
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4222
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4223
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4224
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4225
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4226
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4227
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4228
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4229
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4230
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4231
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4232
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4233
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4234
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4235
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4236
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4237
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4238
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4239
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4240
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4241
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4242
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4243
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4244
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4245
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4246
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4247
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4248
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4249
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4250
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4251
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4252
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4253
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4254
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4255
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4256
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4257
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4258
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4259
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4260
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4261
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4262
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4263
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4264
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4265
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4266
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4267
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4268
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4269
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4270
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4271
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4272
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4273
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4274
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4275
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4276
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4277
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4278
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4279
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4280
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4281
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4282
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4283
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4284
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4285
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4286
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4287
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4288
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4289
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4290
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4291
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4292
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4293
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4294
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4295
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4296
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4297
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4298
    },
    {
        "protocol": "TCP",
        "name": "vrml-multi-use",
        "port": 4299
    },
    {
        "protocol": "TCP",
        "name": "corelccam",
        "port": 4300
    },
    {
        "protocol": "TCP",
        "name": "rwhois",
        "port": 4321
    },
    {
        "protocol": "TCP",
        "name": "unicall",
        "port": 4343
    },
    {
        "protocol": "TCP",
        "name": "vinainstall",
        "port": 4344
    },
    {
        "protocol": "TCP",
        "name": "m4-network-as",
        "port": 4345
    },
    {
        "protocol": "TCP",
        "name": "elanlm",
        "port": 4346
    },
    {
        "protocol": "TCP",
        "name": "lansurveyor",
        "port": 4347
    },
    {
        "protocol": "TCP",
        "name": "itose",
        "port": 4348
    },
    {
        "protocol": "TCP",
        "name": "fsportmap",
        "port": 4349
    },
    {
        "protocol": "TCP",
        "name": "net-device",
        "port": 4350
    },
    {
        "protocol": "TCP",
        "name": "plcy-net-svcs",
        "port": 4351
    },
    {
        "protocol": "TCP",
        "name": "pjlink",
        "port": 4352
    },
    {
        "protocol": "TCP",
        "name": "f5-iquery",
        "port": 4353
    },
    {
        "protocol": "TCP",
        "name": "qsnet-trans",
        "port": 4354
    },
    {
        "protocol": "TCP",
        "name": "qsnet-workst",
        "port": 4355
    },
    {
        "protocol": "TCP",
        "name": "qsnet-assist",
        "port": 4356
    },
    {
        "protocol": "TCP",
        "name": "qsnet-cond",
        "port": 4357
    },
    {
        "protocol": "TCP",
        "name": "qsnet-nucl",
        "port": 4358
    },
    {
        "protocol": "TCP",
        "name": "epmd",
        "port": 4369
    },
    {
        "protocol": "TCP",
        "name": "ds-srv",
        "port": 4400
    },
    {
        "protocol": "TCP",
        "name": "ds-srvr",
        "port": 4401
    },
    {
        "protocol": "TCP",
        "name": "ds-clnt",
        "port": 4402
    },
    {
        "protocol": "TCP",
        "name": "ds-user",
        "port": 4403
    },
    {
        "protocol": "TCP",
        "name": "ds-admin",
        "port": 4404
    },
    {
        "protocol": "TCP",
        "name": "ds-mail",
        "port": 4405
    },
    {
        "protocol": "TCP",
        "name": "ds-slp",
        "port": 4406
    },
    {
        "protocol": "TCP",
        "name": "beacon-port-2",
        "port": 4426
    },
    {
        "protocol": "TCP",
        "name": "saris",
        "port": 4442
    },
    {
        "protocol": "TCP",
        "name": "pharos",
        "port": 4443
    },
    {
        "protocol": "TCP",
        "name": "nv-video",
        "port": 4444
    },
    {
        "protocol": "TCP",
        "name": "upnotifyp",
        "port": 4445
    },
    {
        "protocol": "TCP",
        "name": "n1-fwp",
        "port": 4446
    },
    {
        "protocol": "TCP",
        "name": "n1-rmgmt",
        "port": 4447
    },
    {
        "protocol": "TCP",
        "name": "asc-slmd",
        "port": 4448
    },
    {
        "protocol": "TCP",
        "name": "privatewire",
        "port": 4449
    },
    {
        "protocol": "TCP",
        "name": "camp",
        "port": 4450
    },
    {
        "protocol": "TCP",
        "name": "ctisystemmsg",
        "port": 4451
    },
    {
        "protocol": "TCP",
        "name": "ctiprogramload",
        "port": 4452
    },
    {
        "protocol": "TCP",
        "name": "nssalertmgr",
        "port": 4453
    },
    {
        "protocol": "TCP",
        "name": "nssagentmgr",
        "port": 4454
    },
    {
        "protocol": "TCP",
        "name": "prchat-user",
        "port": 4455
    },
    {
        "protocol": "TCP",
        "name": "prchat-server",
        "port": 4456
    },
    {
        "protocol": "TCP",
        "name": "prRegister",
        "port": 4457
    },
    {
        "protocol": "TCP",
        "name": "hpssmgmt",
        "port": 4484
    },
    {
        "protocol": "TCP",
        "name": "ipsec-nat-t",
        "port": 4500
    },
    {
        "protocol": "TCP",
        "name": "ehs",
        "port": 4535
    },
    {
        "protocol": "TCP",
        "name": "ehs-ssl",
        "port": 4536
    },
    {
        "protocol": "TCP",
        "name": "wssauthsvc",
        "port": 4537
    },
    {
        "protocol": "TCP",
        "name": "worldscores",
        "port": 4545
    },
    {
        "protocol": "TCP",
        "name": "sf-lm",
        "port": 4546
    },
    {
        "protocol": "TCP",
        "name": "lanner-lm",
        "port": 4547
    },
    {
        "protocol": "TCP",
        "name": "synchromesh",
        "port": 4548
    },
    {
        "protocol": "TCP",
        "name": "aegate",
        "port": 4549
    },
    {
        "protocol": "TCP",
        "name": "rsip",
        "port": 4555
    },
    {
        "protocol": "TCP",
        "name": "hylafax",
        "port": 4559
    },
    {
        "protocol": "TCP",
        "name": "tram",
        "port": 4567
    },
    {
        "protocol": "TCP",
        "name": "bmc-reporting",
        "port": 4568
    },
    {
        "protocol": "TCP",
        "name": "iax",
        "port": 4569
    },
    {
        "protocol": "TCP",
        "name": "a21-an-1xbs",
        "port": 4597
    },
    {
        "protocol": "TCP",
        "name": "a16-an-an",
        "port": 4598
    },
    {
        "protocol": "TCP",
        "name": "a17-an-an",
        "port": 4599
    },
    {
        "protocol": "TCP",
        "name": "piranha1",
        "port": 4600
    },
    {
        "protocol": "TCP",
        "name": "piranha2",
        "port": 4601
    },
    {
        "protocol": "TCP",
        "name": "playsta2-app",
        "port": 4658
    },
    {
        "protocol": "TCP",
        "name": "playsta2-lob",
        "port": 4659
    },
    {
        "protocol": "TCP",
        "name": "smaclmgr",
        "port": 4660
    },
    {
        "protocol": "TCP",
        "name": "kar2ouche",
        "port": 4661
    },
    {
        "protocol": "TCP",
        "name": "oms",
        "port": 4662
    },
    {
        "protocol": "TCP",
        "name": "noteit",
        "port": 4663
    },
    {
        "protocol": "TCP",
        "name": "ems",
        "port": 4664
    },
    {
        "protocol": "TCP",
        "name": "contclientms",
        "port": 4665
    },
    {
        "protocol": "TCP",
        "name": "eportcomm",
        "port": 4666
    },
    {
        "protocol": "TCP",
        "name": "mmacomm",
        "port": 4667
    },
    {
        "protocol": "TCP",
        "name": "mmaeds",
        "port": 4668
    },
    {
        "protocol": "TCP",
        "name": "eportcommdata",
        "port": 4669
    },
    {
        "protocol": "TCP",
        "name": "acter",
        "port": 4671
    },
    {
        "protocol": "TCP",
        "name": "rfa",
        "port": 4672
    },
    {
        "protocol": "TCP",
        "name": "cxws",
        "port": 4673
    },
    {
        "protocol": "TCP",
        "name": "appiq-mgmt",
        "port": 4674
    },
    {
        "protocol": "TCP",
        "name": "dhct-status",
        "port": 4675
    },
    {
        "protocol": "TCP",
        "name": "dhct-alerts",
        "port": 4676
    },
    {
        "protocol": "TCP",
        "name": "bcs",
        "port": 4677
    },
    {
        "protocol": "TCP",
        "name": "traversal",
        "port": 4678
    },
    {
        "protocol": "TCP",
        "name": "mgesupervision",
        "port": 4679
    },
    {
        "protocol": "TCP",
        "name": "mgemanagement",
        "port": 4680
    },
    {
        "protocol": "TCP",
        "name": "parliant",
        "port": 4681
    },
    {
        "protocol": "TCP",
        "name": "finisar",
        "port": 4682
    },
    {
        "protocol": "TCP",
        "name": "spike",
        "port": 4683
    },
    {
        "protocol": "TCP",
        "name": "rfid-rp1",
        "port": 4684
    },
    {
        "protocol": "TCP",
        "name": "autopac",
        "port": 4685
    },
    {
        "protocol": "TCP",
        "name": "msp-os",
        "port": 4686
    },
    {
        "protocol": "TCP",
        "name": "nst",
        "port": 4687
    },
    {
        "protocol": "TCP",
        "name": "mobile-p2p",
        "port": 4688
    },
    {
        "protocol": "TCP",
        "name": "altovacentral",
        "port": 4689
    },
    {
        "protocol": "TCP",
        "name": "prelude",
        "port": 4690
    },
    {
        "protocol": "TCP",
        "name": "monotone",
        "port": 4691
    },
    {
        "protocol": "TCP",
        "name": "conspiracy",
        "port": 4692
    },
    {
        "protocol": "TCP",
        "name": "ipdr-sp",
        "port": 4737
    },
    {
        "protocol": "TCP",
        "name": "solera-lpn",
        "port": 4738
    },
    {
        "protocol": "TCP",
        "name": "ipfix",
        "port": 4739
    },
    {
        "protocol": "TCP",
        "name": "openhpid",
        "port": 4743
    },
    {
        "protocol": "TCP",
        "name": "ssad",
        "port": 4750
    },
    {
        "protocol": "TCP",
        "name": "spocp",
        "port": 4751
    },
    {
        "protocol": "TCP",
        "name": "snap",
        "port": 4752
    },
    {
        "protocol": "TCP",
        "name": "bfd-multi-ctl",
        "port": 4784
    },
    {
        "protocol": "TCP",
        "name": "iims",
        "port": 4800
    },
    {
        "protocol": "TCP",
        "name": "iwec",
        "port": 4801
    },
    {
        "protocol": "TCP",
        "name": "ilss",
        "port": 4802
    },
    {
        "protocol": "TCP",
        "name": "hTCP",
        "port": 4827
    },
    {
        "protocol": "TCP",
        "name": "varadero-0",
        "port": 4837
    },
    {
        "protocol": "TCP",
        "name": "varadero-1",
        "port": 4838
    },
    {
        "protocol": "TCP",
        "name": "varadero-2",
        "port": 4839
    },
    {
        "protocol": "TCP",
        "name": "appserv-http",
        "port": 4848
    },
    {
        "protocol": "TCP",
        "name": "appserv-https",
        "port": 4849
    },
    {
        "protocol": "TCP",
        "name": "sun-as-nodeagt",
        "port": 4850
    },
    {
        "protocol": "TCP",
        "name": "phrelay",
        "port": 4868
    },
    {
        "protocol": "TCP",
        "name": "phrelaydbg",
        "port": 4869
    },
    {
        "protocol": "TCP",
        "name": "cc-tracking",
        "port": 4870
    },
    {
        "protocol": "TCP",
        "name": "wired",
        "port": 4871
    },
    {
        "protocol": "TCP",
        "name": "abbs",
        "port": 4885
    },
    {
        "protocol": "TCP",
        "name": "lyskom",
        "port": 4894
    },
    {
        "protocol": "TCP",
        "name": "radmin-port",
        "port": 4899
    },
    {
        "protocol": "TCP",
        "name": "hfcs",
        "port": 4900
    },
    {
        "protocol": "TCP",
        "name": "munin",
        "port": 4949
    },
    {
        "protocol": "TCP",
        "name": "pwgwims",
        "port": 4951
    },
    {
        "protocol": "TCP",
        "name": "sagxtsds",
        "port": 4952
    },
    {
        "protocol": "TCP",
        "name": "ccss-qmm",
        "port": 4969
    },
    {
        "protocol": "TCP",
        "name": "ccss-qsm",
        "port": 4970
    },
    {
        "protocol": "TCP",
        "name": "smar-se-port1",
        "port": 4987
    },
    {
        "protocol": "TCP",
        "name": "smar-se-port2",
        "port": 4988
    },
    {
        "protocol": "TCP",
        "name": "parallel",
        "port": 4989
    },
    {
        "protocol": "TCP",
        "name": "hfcs-manager",
        "port": 4999
    },
    {
        "protocol": "TCP",
        "name": "commplex-main",
        "port": 5000
    },
    {
        "protocol": "TCP",
        "name": "commplex-link",
        "port": 5001
    },
    {
        "protocol": "TCP",
        "name": "rfe",
        "port": 5002
    },
    {
        "protocol": "TCP",
        "name": "fmpro-internal",
        "port": 5003
    },
    {
        "protocol": "TCP",
        "name": "avt-profile-1",
        "port": 5004
    },
    {
        "protocol": "TCP",
        "name": "avt-profile-2",
        "port": 5005
    },
    {
        "protocol": "TCP",
        "name": "wsm-server",
        "port": 5006
    },
    {
        "protocol": "TCP",
        "name": "wsm-server-ssl",
        "port": 5007
    },
    {
        "protocol": "TCP",
        "name": "synapsis-edge",
        "port": 5008
    },
    {
        "protocol": "TCP",
        "name": "winfs",
        "port": 5009
    },
    {
        "protocol": "TCP",
        "name": "telelpathstart",
        "port": 5010
    },
    {
        "protocol": "TCP",
        "name": "telelpathattack",
        "port": 5011
    },
    {
        "protocol": "TCP",
        "name": "zenginkyo-1",
        "port": 5020
    },
    {
        "protocol": "TCP",
        "name": "zenginkyo-2",
        "port": 5021
    },
    {
        "protocol": "TCP",
        "name": "mice",
        "port": 5022
    },
    {
        "protocol": "TCP",
        "name": "htuilsrv",
        "port": 5023
    },
    {
        "protocol": "TCP",
        "name": "scpi-telnet",
        "port": 5024
    },
    {
        "protocol": "TCP",
        "name": "scpi-raw",
        "port": 5025
    },
    {
        "protocol": "TCP",
        "name": "strexec-d",
        "port": 5026
    },
    {
        "protocol": "TCP",
        "name": "strexec-s",
        "port": 5027
    },
    {
        "protocol": "TCP",
        "name": "asnaacceler8db",
        "port": 5042
    },
    {
        "protocol": "TCP",
        "name": "swxadmin",
        "port": 5043
    },
    {
        "protocol": "TCP",
        "name": "lxi-evntsvc",
        "port": 5044
    },
    {
        "protocol": "TCP",
        "name": "mmcc",
        "port": 5050
    },
    {
        "protocol": "TCP",
        "name": "ita-agent",
        "port": 5051
    },
    {
        "protocol": "TCP",
        "name": "ita-manager",
        "port": 5052
    },
    {
        "protocol": "TCP",
        "name": "unot",
        "port": 5055
    },
    {
        "protocol": "TCP",
        "name": "intecom-ps1",
        "port": 5056
    },
    {
        "protocol": "TCP",
        "name": "intecom-ps2",
        "port": 5057
    },
    {
        "protocol": "TCP",
        "name": "sip",
        "port": 5060
    },
    {
        "protocol": "TCP",
        "name": "sip-tls",
        "port": 5061
    },
    {
        "protocol": "TCP",
        "name": "ca-1",
        "port": 5064
    },
    {
        "protocol": "TCP",
        "name": "ca-2",
        "port": 5065
    },
    {
        "protocol": "TCP",
        "name": "stanag-5066",
        "port": 5066
    },
    {
        "protocol": "TCP",
        "name": "authentx",
        "port": 5067
    },
    {
        "protocol": "TCP",
        "name": "i-net-2000-npr",
        "port": 5069
    },
    {
        "protocol": "TCP",
        "name": "vtsas",
        "port": 5070
    },
    {
        "protocol": "TCP",
        "name": "powerschool",
        "port": 5071
    },
    {
        "protocol": "TCP",
        "name": "ayiya",
        "port": 5072
    },
    {
        "protocol": "TCP",
        "name": "tag-pm",
        "port": 5073
    },
    {
        "protocol": "TCP",
        "name": "alesquery",
        "port": 5074
    },
    {
        "protocol": "TCP",
        "name": "sentinel-lm",
        "port": 5093
    },
    {
        "protocol": "TCP",
        "name": "sentlm-srv2srv",
        "port": 5099
    },
    {
        "protocol": "TCP",
        "name": "socalia",
        "port": 5100
    },
    {
        "protocol": "TCP",
        "name": "talarian-TCP",
        "port": 5101
    },
    {
        "protocol": "TCP",
        "name": "oms-nonsecure",
        "port": 5102
    },
    {
        "protocol": "TCP",
        "name": "pm-cmdsvr",
        "port": 5112
    },
    {
        "protocol": "TCP",
        "name": "nbt-pc",
        "port": 5133
    },
    {
        "protocol": "TCP",
        "name": "ctsd",
        "port": 5137
    },
    {
        "protocol": "TCP",
        "name": "rmonitor_secure",
        "port": 5145
    },
    {
        "protocol": "TCP",
        "name": "atmp",
        "port": 5150
    },
    {
        "protocol": "TCP",
        "name": "esri_sde",
        "port": 5151
    },
    {
        "protocol": "TCP",
        "name": "sde-discovery",
        "port": 5152
    },
    {
        "protocol": "TCP",
        "name": "bzflag",
        "port": 5154
    },
    {
        "protocol": "TCP",
        "name": "asctrl-agent",
        "port": 5155
    },
    {
        "protocol": "TCP",
        "name": "ife_icorp",
        "port": 5165
    },
    {
        "protocol": "TCP",
        "name": "winpcs",
        "port": 5166
    },
    {
        "protocol": "TCP",
        "name": "scte104",
        "port": 5167
    },
    {
        "protocol": "TCP",
        "name": "scte30",
        "port": 5168
    },
    {
        "protocol": "TCP",
        "name": "aol",
        "port": 5190
    },
    {
        "protocol": "TCP",
        "name": "aol-1",
        "port": 5191
    },
    {
        "protocol": "TCP",
        "name": "aol-2",
        "port": 5192
    },
    {
        "protocol": "TCP",
        "name": "aol-3",
        "port": 5193
    },
    {
        "protocol": "TCP",
        "name": "targus-getdata",
        "port": 5200
    },
    {
        "protocol": "TCP",
        "name": "targus-getdata1",
        "port": 5201
    },
    {
        "protocol": "TCP",
        "name": "targus-getdata2",
        "port": 5202
    },
    {
        "protocol": "TCP",
        "name": "targus-getdata3",
        "port": 5203
    },
    {
        "protocol": "TCP",
        "name": "xmpp-client",
        "port": 5222
    },
    {
        "protocol": "TCP",
        "name": "hp-server",
        "port": 5225
    },
    {
        "protocol": "TCP",
        "name": "hp-status",
        "port": 5226
    },
    {
        "protocol": "TCP",
        "name": "eenet",
        "port": 5234
    },
    {
        "protocol": "TCP",
        "name": "padl2sim",
        "port": 5236
    },
    {
        "protocol": "TCP",
        "name": "igateway",
        "port": 5250
    },
    {
        "protocol": "TCP",
        "name": "caevms",
        "port": 5251
    },
    {
        "protocol": "TCP",
        "name": "movaz-ssc",
        "port": 5252
    },
    {
        "protocol": "TCP",
        "name": "3com-njack-1",
        "port": 5264
    },
    {
        "protocol": "TCP",
        "name": "3com-njack-2",
        "port": 5265
    },
    {
        "protocol": "TCP",
        "name": "xmpp-server",
        "port": 5269
    },
    {
        "protocol": "TCP",
        "name": "pk",
        "port": 5272
    },
    {
        "protocol": "TCP",
        "name": "transmit-port",
        "port": 5282
    },
    {
        "protocol": "TCP",
        "name": "hacl-hb",
        "port": 5300
    },
    {
        "protocol": "TCP",
        "name": "hacl-gs",
        "port": 5301
    },
    {
        "protocol": "TCP",
        "name": "hacl-cfg",
        "port": 5302
    },
    {
        "protocol": "TCP",
        "name": "hacl-probe",
        "port": 5303
    },
    {
        "protocol": "TCP",
        "name": "hacl-local",
        "port": 5304
    },
    {
        "protocol": "TCP",
        "name": "hacl-test",
        "port": 5305
    },
    {
        "protocol": "TCP",
        "name": "sun-mc-grp",
        "port": 5306
    },
    {
        "protocol": "TCP",
        "name": "sco-aip",
        "port": 5307
    },
    {
        "protocol": "TCP",
        "name": "cfengine",
        "port": 5308
    },
    {
        "protocol": "TCP",
        "name": "jprinter",
        "port": 5309
    },
    {
        "protocol": "TCP",
        "name": "outlaws",
        "port": 5310
    },
    {
        "protocol": "TCP",
        "name": "permabit-cs",
        "port": 5312
    },
    {
        "protocol": "TCP",
        "name": "rrdp",
        "port": 5313
    },
    {
        "protocol": "TCP",
        "name": "opalis-rbt-ipc",
        "port": 5314
    },
    {
        "protocol": "TCP",
        "name": "hacl-poll",
        "port": 5315
    },
    {
        "protocol": "TCP",
        "name": "kfserver",
        "port": 5343
    },
    {
        "protocol": "TCP",
        "name": "xkotodrcp",
        "port": 5344
    },
    {
        "protocol": "TCP",
        "name": "nat-pmp",
        "port": 5351
    },
    {
        "protocol": "TCP",
        "name": "dns-llq",
        "port": 5352
    },
    {
        "protocol": "TCP",
        "name": "mdns",
        "port": 5353
    },
    {
        "protocol": "TCP",
        "name": "mdnsresponder",
        "port": 5354
    },
    {
        "protocol": "TCP",
        "name": "llmnr",
        "port": 5355
    },
    {
        "protocol": "TCP",
        "name": "ms-smlbiz",
        "port": 5356
    },
    {
        "protocol": "TCP",
        "name": "wsdapi",
        "port": 5357
    },
    {
        "protocol": "TCP",
        "name": "wsdapi-s",
        "port": 5358
    },
    {
        "protocol": "TCP",
        "name": "stresstester",
        "port": 5397
    },
    {
        "protocol": "TCP",
        "name": "elektron-admin",
        "port": 5398
    },
    {
        "protocol": "TCP",
        "name": "securitychase",
        "port": 5399
    },
    {
        "protocol": "TCP",
        "name": "excerpt",
        "port": 5400
    },
    {
        "protocol": "TCP",
        "name": "excerpts",
        "port": 5401
    },
    {
        "protocol": "TCP",
        "name": "mftp",
        "port": 5402
    },
    {
        "protocol": "TCP",
        "name": "hpoms-ci-lstn",
        "port": 5403
    },
    {
        "protocol": "TCP",
        "name": "hpoms-dps-lstn",
        "port": 5404
    },
    {
        "protocol": "TCP",
        "name": "netsupport",
        "port": 5405
    },
    {
        "protocol": "TCP",
        "name": "systemics-sox",
        "port": 5406
    },
    {
        "protocol": "TCP",
        "name": "foresyte-clear",
        "port": 5407
    },
    {
        "protocol": "TCP",
        "name": "foresyte-sec",
        "port": 5408
    },
    {
        "protocol": "TCP",
        "name": "salient-dtasrv",
        "port": 5409
    },
    {
        "protocol": "TCP",
        "name": "salient-usrmgr",
        "port": 5410
    },
    {
        "protocol": "TCP",
        "name": "actnet",
        "port": 5411
    },
    {
        "protocol": "TCP",
        "name": "continuus",
        "port": 5412
    },
    {
        "protocol": "TCP",
        "name": "wwiotalk",
        "port": 5413
    },
    {
        "protocol": "TCP",
        "name": "statusd",
        "port": 5414
    },
    {
        "protocol": "TCP",
        "name": "ns-server",
        "port": 5415
    },
    {
        "protocol": "TCP",
        "name": "sns-gateway",
        "port": 5416
    },
    {
        "protocol": "TCP",
        "name": "sns-agent",
        "port": 5417
    },
    {
        "protocol": "TCP",
        "name": "mcntp",
        "port": 5418
    },
    {
        "protocol": "TCP",
        "name": "dj-ice",
        "port": 5419
    },
    {
        "protocol": "TCP",
        "name": "cylink-c",
        "port": 5420
    },
    {
        "protocol": "TCP",
        "name": "netsupport2",
        "port": 5421
    },
    {
        "protocol": "TCP",
        "name": "salient-mux",
        "port": 5422
    },
    {
        "protocol": "TCP",
        "name": "virtualuser",
        "port": 5423
    },
    {
        "protocol": "TCP",
        "name": "beyond-remote",
        "port": 5424
    },
    {
        "protocol": "TCP",
        "name": "br-channel",
        "port": 5425
    },
    {
        "protocol": "TCP",
        "name": "devbasic",
        "port": 5426
    },
    {
        "protocol": "TCP",
        "name": "sco-peer-tta",
        "port": 5427
    },
    {
        "protocol": "TCP",
        "name": "telaconsole",
        "port": 5428
    },
    {
        "protocol": "TCP",
        "name": "base",
        "port": 5429
    },
    {
        "protocol": "TCP",
        "name": "radec-corp",
        "port": 5430
    },
    {
        "protocol": "TCP",
        "name": "park-agent",
        "port": 5431
    },
    {
        "protocol": "TCP",
        "name": "postgresql",
        "port": 5432
    },
    {
        "protocol": "TCP",
        "name": "pyrrho",
        "port": 5433
    },
    {
        "protocol": "TCP",
        "name": "sgi-arrayd",
        "port": 5434
    },
    {
        "protocol": "TCP",
        "name": "dttl",
        "port": 5435
    },
    {
        "protocol": "TCP",
        "name": "surebox",
        "port": 5453
    },
    {
        "protocol": "TCP",
        "name": "apc-5454",
        "port": 5454
    },
    {
        "protocol": "TCP",
        "name": "apc-5455",
        "port": 5455
    },
    {
        "protocol": "TCP",
        "name": "apc-5456",
        "port": 5456
    },
    {
        "protocol": "TCP",
        "name": "silkmeter",
        "port": 5461
    },
    {
        "protocol": "TCP",
        "name": "ttl-publisher",
        "port": 5462
    },
    {
        "protocol": "TCP",
        "name": "ttlpriceproxy",
        "port": 5463
    },
    {
        "protocol": "TCP",
        "name": "netops-broker",
        "port": 5465
    },
    {
        "protocol": "TCP",
        "name": "fcp-addr-srvr1",
        "port": 5500
    },
    {
        "protocol": "TCP",
        "name": "fcp-addr-srvr2",
        "port": 5501
    },
    {
        "protocol": "TCP",
        "name": "fcp-srvr-inst1",
        "port": 5502
    },
    {
        "protocol": "TCP",
        "name": "fcp-srvr-inst2",
        "port": 5503
    },
    {
        "protocol": "TCP",
        "name": "fcp-cics-gw1",
        "port": 5504
    },
    {
        "protocol": "TCP",
        "name": "sgi-eventmond",
        "port": 5553
    },
    {
        "protocol": "TCP",
        "name": "sgi-esphttp",
        "port": 5554
    },
    {
        "protocol": "TCP",
        "name": "personal-agent",
        "port": 5555
    },
    {
        "protocol": "TCP",
        "name": "freeciv",
        "port": 5556
    },
    {
        "protocol": "TCP",
        "name": "udpplus",
        "port": 5566
    },
    {
        "protocol": "TCP",
        "name": "emware-moap",
        "port": 5567
    },
    {
        "protocol": "TCP",
        "name": "bis-web",
        "port": 5584
    },
    {
        "protocol": "TCP",
        "name": "bis-sync",
        "port": 5585
    },
    {
        "protocol": "TCP",
        "name": "esinstall",
        "port": 5599
    },
    {
        "protocol": "TCP",
        "name": "esmmanager",
        "port": 5600
    },
    {
        "protocol": "TCP",
        "name": "esmagent",
        "port": 5601
    },
    {
        "protocol": "TCP",
        "name": "a1-msc",
        "port": 5602
    },
    {
        "protocol": "TCP",
        "name": "a1-bs",
        "port": 5603
    },
    {
        "protocol": "TCP",
        "name": "a3-sdunode",
        "port": 5604
    },
    {
        "protocol": "TCP",
        "name": "a4-sdunode",
        "port": 5605
    },
    {
        "protocol": "TCP",
        "name": "ninaf",
        "port": 5627
    },
    {
        "protocol": "TCP",
        "name": "pcanywheredata",
        "port": 5631
    },
    {
        "protocol": "TCP",
        "name": "pcanywherestat",
        "port": 5632
    },
    {
        "protocol": "TCP",
        "name": "beorl",
        "port": 5633
    },
    {
        "protocol": "TCP",
        "name": "amqp",
        "port": 5672
    },
    {
        "protocol": "TCP",
        "name": "jms",
        "port": 5673
    },
    {
        "protocol": "TCP",
        "name": "hyperscsi-port",
        "port": 5674
    },
    {
        "protocol": "TCP",
        "name": "raadmin",
        "port": 5676
    },
    {
        "protocol": "TCP",
        "name": "questdb2-lnchr",
        "port": 5677
    },
    {
        "protocol": "TCP",
        "name": "rrac",
        "port": 5678
    },
    {
        "protocol": "TCP",
        "name": "dccm",
        "port": 5679
    },
    {
        "protocol": "TCP",
        "name": "auriga-router",
        "port": 5680
    },
    {
        "protocol": "TCP",
        "name": "ggz",
        "port": 5688
    },
    {
        "protocol": "TCP",
        "name": "proshareaudio",
        "port": 5713
    },
    {
        "protocol": "TCP",
        "name": "prosharevideo",
        "port": 5714
    },
    {
        "protocol": "TCP",
        "name": "prosharedata",
        "port": 5715
    },
    {
        "protocol": "TCP",
        "name": "prosharerequest",
        "port": 5716
    },
    {
        "protocol": "TCP",
        "name": "prosharenotify",
        "port": 5717
    },
    {
        "protocol": "TCP",
        "name": "ms-licensing",
        "port": 5720
    },
    {
        "protocol": "TCP",
        "name": "dtpt",
        "port": 5721
    },
    {
        "protocol": "TCP",
        "name": "openmail",
        "port": 5729
    },
    {
        "protocol": "TCP",
        "name": "unieng",
        "port": 5730
    },
    {
        "protocol": "TCP",
        "name": "ida-discover1",
        "port": 5741
    },
    {
        "protocol": "TCP",
        "name": "ida-discover2",
        "port": 5742
    },
    {
        "protocol": "TCP",
        "name": "watchdoc-pod",
        "port": 5743
    },
    {
        "protocol": "TCP",
        "name": "watchdoc",
        "port": 5744
    },
    {
        "protocol": "TCP",
        "name": "fcopy-server",
        "port": 5745
    },
    {
        "protocol": "TCP",
        "name": "fcopys-server",
        "port": 5746
    },
    {
        "protocol": "TCP",
        "name": "tunatic",
        "port": 5747
    },
    {
        "protocol": "TCP",
        "name": "tunalyzer",
        "port": 5748
    },
    {
        "protocol": "TCP",
        "name": "openmailg",
        "port": 5755
    },
    {
        "protocol": "TCP",
        "name": "x500ms",
        "port": 5757
    },
    {
        "protocol": "TCP",
        "name": "openmailns",
        "port": 5766
    },
    {
        "protocol": "TCP",
        "name": "s-openmail",
        "port": 5767
    },
    {
        "protocol": "TCP",
        "name": "openmailpxy",
        "port": 5768
    },
    {
        "protocol": "TCP",
        "name": "spramsca",
        "port": 5769
    },
    {
        "protocol": "TCP",
        "name": "spramsd",
        "port": 5770
    },
    {
        "protocol": "TCP",
        "name": "netagent",
        "port": 5771
    },
    {
        "protocol": "TCP",
        "name": "dali-port",
        "port": 5777
    },
    {
        "protocol": "TCP",
        "name": "icmpd",
        "port": 5813
    },
    {
        "protocol": "TCP",
        "name": "spt-automation",
        "port": 5814
    },
    {
        "protocol": "TCP",
        "name": "icpp",
        "port": 5815
    },
    {
        "protocol": "TCP",
        "name": "wherehoo",
        "port": 5859
    },
    {
        "protocol": "TCP",
        "name": "ppsuitemsg",
        "port": 5863
    },
    {
        "protocol": "TCP",
        "name": "indy",
        "port": 5963
    },
    {
        "protocol": "TCP",
        "name": "mppolicy-v5",
        "port": 5968
    },
    {
        "protocol": "TCP",
        "name": "mppolicy-mgr",
        "port": 5969
    },
    {
        "protocol": "TCP",
        "name": "wbem-rmi",
        "port": 5987
    },
    {
        "protocol": "TCP",
        "name": "wbem-http",
        "port": 5988
    },
    {
        "protocol": "TCP",
        "name": "wbem-https",
        "port": 5989
    },
    {
        "protocol": "TCP",
        "name": "wbem-exp-https",
        "port": 5990
    },
    {
        "protocol": "TCP",
        "name": "nuxsl",
        "port": 5991
    },
    {
        "protocol": "TCP",
        "name": "consul-insight",
        "port": 5992
    },
    {
        "protocol": "TCP",
        "name": "cvsup",
        "port": 5999
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6000
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6001
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6002
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6003
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6004
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6005
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6006
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6007
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6008
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6009
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6010
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6011
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6012
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6013
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6014
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6015
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6016
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6017
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6018
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6019
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6020
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6021
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6022
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6023
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6024
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6025
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6026
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6027
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6028
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6029
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6030
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6031
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6032
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6033
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6034
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6035
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6036
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6037
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6038
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6039
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6040
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6041
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6042
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6043
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6044
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6045
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6046
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6047
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6048
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6049
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6050
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6051
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6052
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6053
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6054
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6055
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6056
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6057
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6058
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6059
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6060
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6061
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6062
    },
    {
        "protocol": "TCP",
        "name": "x11",
        "port": 6063
    },
    {
        "protocol": "TCP",
        "name": "ndl-ahp-svc",
        "port": 6064
    },
    {
        "protocol": "TCP",
        "name": "winpharaoh",
        "port": 6065
    },
    {
        "protocol": "TCP",
        "name": "ewctsp",
        "port": 6066
    },
    {
        "protocol": "TCP",
        "name": "srb",
        "port": 6067
    },
    {
        "protocol": "TCP",
        "name": "gsmp",
        "port": 6068
    },
    {
        "protocol": "TCP",
        "name": "trip",
        "port": 6069
    },
    {
        "protocol": "TCP",
        "name": "messageasap",
        "port": 6070
    },
    {
        "protocol": "TCP",
        "name": "ssdtp",
        "port": 6071
    },
    {
        "protocol": "TCP",
        "name": "diagnose-proc",
        "port": 6072
    },
    {
        "protocol": "TCP",
        "name": "directplay8",
        "port": 6073
    },
    {
        "protocol": "TCP",
        "name": "max",
        "port": 6074
    },
    {
        "protocol": "TCP",
        "name": "konspire2b",
        "port": 6085
    },
    {
        "protocol": "TCP",
        "name": "synchronet-db",
        "port": 6100
    },
    {
        "protocol": "TCP",
        "name": "synchronet-rtc",
        "port": 6101
    },
    {
        "protocol": "TCP",
        "name": "synchronet-upd",
        "port": 6102
    },
    {
        "protocol": "TCP",
        "name": "rets",
        "port": 6103
    },
    {
        "protocol": "TCP",
        "name": "dbdb",
        "port": 6104
    },
    {
        "protocol": "TCP",
        "name": "primaserver",
        "port": 6105
    },
    {
        "protocol": "TCP",
        "name": "mpsserver",
        "port": 6106
    },
    {
        "protocol": "TCP",
        "name": "etc-control",
        "port": 6107
    },
    {
        "protocol": "TCP",
        "name": "sercomm-scadmin",
        "port": 6108
    },
    {
        "protocol": "TCP",
        "name": "globecast-id",
        "port": 6109
    },
    {
        "protocol": "TCP",
        "name": "softcm",
        "port": 6110
    },
    {
        "protocol": "TCP",
        "name": "spc",
        "port": 6111
    },
    {
        "protocol": "TCP",
        "name": "dtspcd",
        "port": 6112
    },
    {
        "protocol": "TCP",
        "name": "bex-webadmin",
        "port": 6122
    },
    {
        "protocol": "TCP",
        "name": "backup-express",
        "port": 6123
    },
    {
        "protocol": "TCP",
        "name": "nbt-wol",
        "port": 6133
    },
    {
        "protocol": "TCP",
        "name": "meta-corp",
        "port": 6141
    },
    {
        "protocol": "TCP",
        "name": "aspentec-lm",
        "port": 6142
    },
    {
        "protocol": "TCP",
        "name": "watershed-lm",
        "port": 6143
    },
    {
        "protocol": "TCP",
        "name": "statsci1-lm",
        "port": 6144
    },
    {
        "protocol": "TCP",
        "name": "statsci2-lm",
        "port": 6145
    },
    {
        "protocol": "TCP",
        "name": "lonewolf-lm",
        "port": 6146
    },
    {
        "protocol": "TCP",
        "name": "montage-lm",
        "port": 6147
    },
    {
        "protocol": "TCP",
        "name": "ricardo-lm",
        "port": 6148
    },
    {
        "protocol": "TCP",
        "name": "tal-pod",
        "port": 6149
    },
    {
        "protocol": "TCP",
        "name": "patrol-ism",
        "port": 6161
    },
    {
        "protocol": "TCP",
        "name": "patrol-coll",
        "port": 6162
    },
    {
        "protocol": "TCP",
        "name": "pscribe",
        "port": 6163
    },
    {
        "protocol": "TCP",
        "name": "crip",
        "port": 6253
    },
    {
        "protocol": "TCP",
        "name": "bmc-grx",
        "port": 6300
    },
    {
        "protocol": "TCP",
        "name": "emp-server1",
        "port": 6321
    },
    {
        "protocol": "TCP",
        "name": "emp-server2",
        "port": 6322
    },
    {
        "protocol": "TCP",
        "name": "sflow",
        "port": 6343
    },
    {
        "protocol": "TCP",
        "name": "gnutella-svc",
        "port": 6346
    },
    {
        "protocol": "TCP",
        "name": "gnutella-rtr",
        "port": 6347
    },
    {
        "protocol": "TCP",
        "name": "metatude-mds",
        "port": 6382
    },
    {
        "protocol": "TCP",
        "name": "clariion-evr01",
        "port": 6389
    },
    {
        "protocol": "TCP",
        "name": "info-aps",
        "port": 6400
    },
    {
        "protocol": "TCP",
        "name": "info-was",
        "port": 6401
    },
    {
        "protocol": "TCP",
        "name": "info-eventsvr",
        "port": 6402
    },
    {
        "protocol": "TCP",
        "name": "info-cachesvr",
        "port": 6403
    },
    {
        "protocol": "TCP",
        "name": "info-filesvr",
        "port": 6404
    },
    {
        "protocol": "TCP",
        "name": "info-pagesvr",
        "port": 6405
    },
    {
        "protocol": "TCP",
        "name": "info-processvr",
        "port": 6406
    },
    {
        "protocol": "TCP",
        "name": "reserved1",
        "port": 6407
    },
    {
        "protocol": "TCP",
        "name": "reserved2",
        "port": 6408
    },
    {
        "protocol": "TCP",
        "name": "reserved3",
        "port": 6409
    },
    {
        "protocol": "TCP",
        "name": "reserved4",
        "port": 6410
    },
    {
        "protocol": "TCP",
        "name": "nim-vdrshell",
        "port": 6420
    },
    {
        "protocol": "TCP",
        "name": "nim-wan",
        "port": 6421
    },
    {
        "protocol": "TCP",
        "name": "skip-cert-recv",
        "port": 6455
    },
    {
        "protocol": "TCP",
        "name": "skip-cert-send",
        "port": 6456
    },
    {
        "protocol": "TCP",
        "name": "lvision-lm",
        "port": 6471
    },
    {
        "protocol": "TCP",
        "name": "boks",
        "port": 6500
    },
    {
        "protocol": "TCP",
        "name": "boks_servc",
        "port": 6501
    },
    {
        "protocol": "TCP",
        "name": "boks_servm",
        "port": 6502
    },
    {
        "protocol": "TCP",
        "name": "boks_clntd",
        "port": 6503
    },
    {
        "protocol": "TCP",
        "name": "badm_priv",
        "port": 6505
    },
    {
        "protocol": "TCP",
        "name": "badm_pub",
        "port": 6506
    },
    {
        "protocol": "TCP",
        "name": "bdir_priv",
        "port": 6507
    },
    {
        "protocol": "TCP",
        "name": "bdir_pub",
        "port": 6508
    },
    {
        "protocol": "TCP",
        "name": "mgcs-mfp-port",
        "port": 6509
    },
    {
        "protocol": "TCP",
        "name": "mcer-port",
        "port": 6510
    },
    {
        "protocol": "TCP",
        "name": "lds-distrib",
        "port": 6543
    },
    {
        "protocol": "TCP",
        "name": "lds-dump",
        "port": 6544
    },
    {
        "protocol": "TCP",
        "name": "apc-6547",
        "port": 6547
    },
    {
        "protocol": "TCP",
        "name": "apc-6548",
        "port": 6548
    },
    {
        "protocol": "TCP",
        "name": "apc-6549",
        "port": 6549
    },
    {
        "protocol": "TCP",
        "name": "fg-sysupdate",
        "port": 6550
    },
    {
        "protocol": "TCP",
        "name": "xdsxdm",
        "port": 6558
    },
    {
        "protocol": "TCP",
        "name": "sane-port",
        "port": 6566
    },
    {
        "protocol": "TCP",
        "name": "affiliate",
        "port": 6579
    },
    {
        "protocol": "TCP",
        "name": "parsec-master",
        "port": 6580
    },
    {
        "protocol": "TCP",
        "name": "parsec-peer",
        "port": 6581
    },
    {
        "protocol": "TCP",
        "name": "parsec-game",
        "port": 6582
    },
    {
        "protocol": "TCP",
        "name": "joaJewelSuite",
        "port": 6583
    },
    {
        "protocol": "TCP",
        "name": "odette-ftps",
        "port": 6619
    },
    {
        "protocol": "TCP",
        "name": "kftp-data",
        "port": 6620
    },
    {
        "protocol": "TCP",
        "name": "kftp",
        "port": 6621
    },
    {
        "protocol": "TCP",
        "name": "mcftp",
        "port": 6622
    },
    {
        "protocol": "TCP",
        "name": "ktelnet",
        "port": 6623
    },
    {
        "protocol": "TCP",
        "name": "nexgen",
        "port": 6627
    },
    {
        "protocol": "TCP",
        "name": "afesc-mc",
        "port": 6628
    },
    {
        "protocol": "TCP",
        "name": "ircu",
        "port": 6665
    },
    {
        "protocol": "TCP",
        "name": "ircu",
        "port": 6666
    },
    {
        "protocol": "TCP",
        "name": "ircu",
        "port": 6666
    },
    {
        "protocol": "TCP",
        "name": "ircu",
        "port": 6667
    },
    {
        "protocol": "TCP",
        "name": "ircu",
        "port": 6668
    },
    {
        "protocol": "TCP",
        "name": "ircu",
        "port": 6669
    },
    {
        "protocol": "TCP",
        "name": "vocaltec-gold",
        "port": 6670
    },
    {
        "protocol": "TCP",
        "name": "vision_server",
        "port": 6672
    },
    {
        "protocol": "TCP",
        "name": "vision_elmd",
        "port": 6673
    },
    {
        "protocol": "TCP",
        "name": "kti-icad-srvr",
        "port": 6701
    },
    {
        "protocol": "TCP",
        "name": "e-design-net",
        "port": 6702
    },
    {
        "protocol": "TCP",
        "name": "e-design-web",
        "port": 6703
    },
    {
        "protocol": "TCP",
        "name": "ibprotocol",
        "port": 6714
    },
    {
        "protocol": "TCP",
        "name": "fibotrader-com",
        "port": 6715
    },
    {
        "protocol": "TCP",
        "name": "bmc-perf-agent",
        "port": 6767
    },
    {
        "protocol": "TCP",
        "name": "bmc-perf-mgrd",
        "port": 6768
    },
    {
        "protocol": "TCP",
        "name": "adi-gxp-srvprt",
        "port": 6769
    },
    {
        "protocol": "TCP",
        "name": "plysrv-http",
        "port": 6770
    },
    {
        "protocol": "TCP",
        "name": "plysrv-https",
        "port": 6771
    },
    {
        "protocol": "TCP",
        "name": "hnmp",
        "port": 6776
    },
    {
        "protocol": "TCP",
        "name": "smc-jmx",
        "port": 6786
    },
    {
        "protocol": "TCP",
        "name": "smc-admin",
        "port": 6787
    },
    {
        "protocol": "TCP",
        "name": "smc-http",
        "port": 6788
    },
    {
        "protocol": "TCP",
        "name": "smc-https",
        "port": 6789
    },
    {
        "protocol": "TCP",
        "name": "hnmp",
        "port": 6790
    },
    {
        "protocol": "TCP",
        "name": "hnm",
        "port": 6791
    },
    {
        "protocol": "TCP",
        "name": "ambit-lm",
        "port": 6831
    },
    {
        "protocol": "TCP",
        "name": "netmo-default",
        "port": 6841
    },
    {
        "protocol": "TCP",
        "name": "netmo-http",
        "port": 6842
    },
    {
        "protocol": "TCP",
        "name": "iccrushmore",
        "port": 6850
    },
    {
        "protocol": "TCP",
        "name": "muse",
        "port": 6888
    },
    {
        "protocol": "TCP",
        "name": "bioserver",
        "port": 6946
    },
    {
        "protocol": "TCP",
        "name": "jmact3",
        "port": 6961
    },
    {
        "protocol": "TCP",
        "name": "jmevt2",
        "port": 6962
    },
    {
        "protocol": "TCP",
        "name": "swismgr1",
        "port": 6963
    },
    {
        "protocol": "TCP",
        "name": "swismgr2",
        "port": 6964
    },
    {
        "protocol": "TCP",
        "name": "swistrap",
        "port": 6965
    },
    {
        "protocol": "TCP",
        "name": "swispol",
        "port": 6966
    },
    {
        "protocol": "TCP",
        "name": "acmsoda",
        "port": 6969
    },
    {
        "protocol": "TCP",
        "name": "iatp-highpri",
        "port": 6998
    },
    {
        "protocol": "TCP",
        "name": "iatp-normalpri",
        "port": 6999
    },
    {
        "protocol": "TCP",
        "name": "afs3-fileserver",
        "port": 7000
    },
    {
        "protocol": "TCP",
        "name": "afs3-callback",
        "port": 7001
    },
    {
        "protocol": "TCP",
        "name": "afs3-prserver",
        "port": 7002
    },
    {
        "protocol": "TCP",
        "name": "afs3-vlserver",
        "port": 7003
    },
    {
        "protocol": "TCP",
        "name": "afs3-kaserver",
        "port": 7004
    },
    {
        "protocol": "TCP",
        "name": "afs3-volser",
        "port": 7005
    },
    {
        "protocol": "TCP",
        "name": "afs3-errors",
        "port": 7006
    },
    {
        "protocol": "TCP",
        "name": "afs3-bos",
        "port": 7007
    },
    {
        "protocol": "TCP",
        "name": "afs3-update",
        "port": 7008
    },
    {
        "protocol": "TCP",
        "name": "afs3-rmtsys",
        "port": 7009
    },
    {
        "protocol": "TCP",
        "name": "ups-onlinet",
        "port": 7010
    },
    {
        "protocol": "TCP",
        "name": "talon-disc",
        "port": 7011
    },
    {
        "protocol": "TCP",
        "name": "talon-engine",
        "port": 7012
    },
    {
        "protocol": "TCP",
        "name": "microtalon-dis",
        "port": 7013
    },
    {
        "protocol": "TCP",
        "name": "microtalon-com",
        "port": 7014
    },
    {
        "protocol": "TCP",
        "name": "talon-webserver",
        "port": 7015
    },
    {
        "protocol": "TCP",
        "name": "dpserve",
        "port": 7020
    },
    {
        "protocol": "TCP",
        "name": "dpserveadmin",
        "port": 7021
    },
    {
        "protocol": "TCP",
        "name": "ctdp",
        "port": 7022
    },
    {
        "protocol": "TCP",
        "name": "ct2nmcs",
        "port": 7023
    },
    {
        "protocol": "TCP",
        "name": "vmsvc",
        "port": 7024
    },
    {
        "protocol": "TCP",
        "name": "vmsvc-2",
        "port": 7025
    },
    {
        "protocol": "TCP",
        "name": "op-probe",
        "port": 7030
    },
    {
        "protocol": "TCP",
        "name": "arcp",
        "port": 7070
    },
    {
        "protocol": "TCP",
        "name": "lazy-ptop",
        "port": 7099
    },
    {
        "protocol": "TCP",
        "name": "font-service",
        "port": 7100
    },
    {
        "protocol": "TCP",
        "name": "virprot-lm",
        "port": 7121
    },
    {
        "protocol": "TCP",
        "name": "scenidm",
        "port": 7128
    },
    {
        "protocol": "TCP",
        "name": "cabsm-comm",
        "port": 7161
    },
    {
        "protocol": "TCP",
        "name": "caistoragemgr",
        "port": 7162
    },
    {
        "protocol": "TCP",
        "name": "cacsambroker",
        "port": 7163
    },
    {
        "protocol": "TCP",
        "name": "clutild",
        "port": 7174
    },
    {
        "protocol": "TCP",
        "name": "fodms",
        "port": 7200
    },
    {
        "protocol": "TCP",
        "name": "dlip",
        "port": 7201
    },
    {
        "protocol": "TCP",
        "name": "ramp",
        "port": 7227
    },
    {
        "protocol": "TCP",
        "name": "watchme-7272",
        "port": 7272
    },
    {
        "protocol": "TCP",
        "name": "oma-rlp",
        "port": 7273
    },
    {
        "protocol": "TCP",
        "name": "oma-rlp-s",
        "port": 7274
    },
    {
        "protocol": "TCP",
        "name": "oma-ulp",
        "port": 7275
    },
    {
        "protocol": "TCP",
        "name": "itactionserver1",
        "port": 7280
    },
    {
        "protocol": "TCP",
        "name": "itactionserver2",
        "port": 7281
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7300
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7301
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7302
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7303
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7304
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7305
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7306
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7307
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7308
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7309
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7310
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7311
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7312
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7313
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7314
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7315
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7316
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7317
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7318
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7319
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7320
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7321
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7322
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7323
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7324
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7325
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7326
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7327
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7328
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7329
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7330
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7331
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7332
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7333
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7334
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7335
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7336
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7337
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7338
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7339
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7340
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7341
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7342
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7343
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7344
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7345
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7346
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7347
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7348
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7349
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7350
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7351
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7352
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7353
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7354
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7355
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7356
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7357
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7358
    },
    {
        "protocol": "TCP",
        "name": "swx",
        "port": 7359
    },
    {
        "protocol": "TCP",
        "name": "lcm-server",
        "port": 7365
    },
    {
        "protocol": "TCP",
        "name": "mindfilesys",
        "port": 7391
    },
    {
        "protocol": "TCP",
        "name": "mrssrendezvous",
        "port": 7392
    },
    {
        "protocol": "TCP",
        "name": "nfoldman",
        "port": 7393
    },
    {
        "protocol": "TCP",
        "name": "winqedit",
        "port": 7395
    },
    {
        "protocol": "TCP",
        "name": "hexarc",
        "port": 7397
    },
    {
        "protocol": "TCP",
        "name": "rtps-discovery",
        "port": 7400
    },
    {
        "protocol": "TCP",
        "name": "rtps-dd-ut",
        "port": 7401
    },
    {
        "protocol": "TCP",
        "name": "rtps-dd-mt",
        "port": 7402
    },
    {
        "protocol": "TCP",
        "name": "mtportmon",
        "port": 7421
    },
    {
        "protocol": "TCP",
        "name": "pmdmgr",
        "port": 7426
    },
    {
        "protocol": "TCP",
        "name": "oveadmgr",
        "port": 7427
    },
    {
        "protocol": "TCP",
        "name": "ovladmgr",
        "port": 7428
    },
    {
        "protocol": "TCP",
        "name": "opi-sock",
        "port": 7429
    },
    {
        "protocol": "TCP",
        "name": "xmpv7",
        "port": 7430
    },
    {
        "protocol": "TCP",
        "name": "pmd",
        "port": 7431
    },
    {
        "protocol": "TCP",
        "name": "faximum",
        "port": 7437
    },
    {
        "protocol": "TCP",
        "name": "telops-lmd",
        "port": 7491
    },
    {
        "protocol": "TCP",
        "name": "silhouette",
        "port": 7500
    },
    {
        "protocol": "TCP",
        "name": "ovbus",
        "port": 7501
    },
    {
        "protocol": "TCP",
        "name": "ovhpas",
        "port": 7510
    },
    {
        "protocol": "TCP",
        "name": "pafec-lm",
        "port": 7511
    },
    {
        "protocol": "TCP",
        "name": "atul",
        "port": 7543
    },
    {
        "protocol": "TCP",
        "name": "nta-ds",
        "port": 7544
    },
    {
        "protocol": "TCP",
        "name": "nta-us",
        "port": 7545
    },
    {
        "protocol": "TCP",
        "name": "cfs",
        "port": 7546
    },
    {
        "protocol": "TCP",
        "name": "cwmp",
        "port": 7547
    },
    {
        "protocol": "TCP",
        "name": "tidp",
        "port": 7548
    },
    {
        "protocol": "TCP",
        "name": "sncp",
        "port": 7560
    },
    {
        "protocol": "TCP",
        "name": "vsi-omega",
        "port": 7566
    },
    {
        "protocol": "TCP",
        "name": "aries-kfinder",
        "port": 7570
    },
    {
        "protocol": "TCP",
        "name": "sun-lm",
        "port": 7588
    },
    {
        "protocol": "TCP",
        "name": "indi",
        "port": 7624
    },
    {
        "protocol": "TCP",
        "name": "simco",
        "port": 7626
    },
    {
        "protocol": "TCP",
        "name": "soap-http",
        "port": 7627
    },
    {
        "protocol": "TCP",
        "name": "pmdfmgt",
        "port": 7633
    },
    {
        "protocol": "TCP",
        "name": "imqtunnels",
        "port": 7674
    },
    {
        "protocol": "TCP",
        "name": "imqtunnel",
        "port": 7675
    },
    {
        "protocol": "TCP",
        "name": "imqbrokerd",
        "port": 7676
    },
    {
        "protocol": "TCP",
        "name": "sun-user-https",
        "port": 7677
    },
    {
        "protocol": "TCP",
        "name": "klio",
        "port": 7697
    },
    {
        "protocol": "TCP",
        "name": "sync-em7",
        "port": 7707
    },
    {
        "protocol": "TCP",
        "name": "scinet",
        "port": 7708
    },
    {
        "protocol": "TCP",
        "name": "medimageportal",
        "port": 7720
    },
    {
        "protocol": "TCP",
        "name": "nitrogen",
        "port": 7725
    },
    {
        "protocol": "TCP",
        "name": "freezexservice",
        "port": 7726
    },
    {
        "protocol": "TCP",
        "name": "trident-data",
        "port": 7727
    },
    {
        "protocol": "TCP",
        "name": "aiagent",
        "port": 7738
    },
    {
        "protocol": "TCP",
        "name": "sstp-1",
        "port": 7743
    },
    {
        "protocol": "TCP",
        "name": "cbt",
        "port": 7777
    },
    {
        "protocol": "TCP",
        "name": "interwise",
        "port": 7778
    },
    {
        "protocol": "TCP",
        "name": "vstat",
        "port": 7779
    },
    {
        "protocol": "TCP",
        "name": "accu-lmgr",
        "port": 7781
    },
    {
        "protocol": "TCP",
        "name": "minivend",
        "port": 7786
    },
    {
        "protocol": "TCP",
        "name": "popup-reminders",
        "port": 7787
    },
    {
        "protocol": "TCP",
        "name": "office-tools",
        "port": 7789
    },
    {
        "protocol": "TCP",
        "name": "q3ade",
        "port": 7794
    },
    {
        "protocol": "TCP",
        "name": "pnet-conn",
        "port": 7797
    },
    {
        "protocol": "TCP",
        "name": "pnet-enc",
        "port": 7798
    },
    {
        "protocol": "TCP",
        "name": "asr",
        "port": 7800
    },
    {
        "protocol": "TCP",
        "name": "apc-7845",
        "port": 7845
    },
    {
        "protocol": "TCP",
        "name": "apc-7846",
        "port": 7846
    },
    {
        "protocol": "TCP",
        "name": "ubroker",
        "port": 7887
    },
    {
        "protocol": "TCP",
        "name": "tnos-sp",
        "port": 7901
    },
    {
        "protocol": "TCP",
        "name": "tnos-dp",
        "port": 7902
    },
    {
        "protocol": "TCP",
        "name": "tnos-dps",
        "port": 7903
    },
    {
        "protocol": "TCP",
        "name": "qo-secure",
        "port": 7913
    },
    {
        "protocol": "TCP",
        "name": "t2-drm",
        "port": 7932
    },
    {
        "protocol": "TCP",
        "name": "t2-brm",
        "port": 7933
    },
    {
        "protocol": "TCP",
        "name": "supercell",
        "port": 7967
    },
    {
        "protocol": "TCP",
        "name": "micromuse-ncps",
        "port": 7979
    },
    {
        "protocol": "TCP",
        "name": "quest-vista",
        "port": 7980
    },
    {
        "protocol": "TCP",
        "name": "irdmi2",
        "port": 7999
    },
    {
        "protocol": "TCP",
        "name": "irdmi",
        "port": 8000
    },
    {
        "protocol": "TCP",
        "name": "vcom-tunnel",
        "port": 8001
    },
    {
        "protocol": "TCP",
        "name": "teradataordbms",
        "port": 8002
    },
    {
        "protocol": "TCP",
        "name": "http-alt",
        "port": 8008
    },
    {
        "protocol": "TCP",
        "name": "intu-ec-svcdisc",
        "port": 8020
    },
    {
        "protocol": "TCP",
        "name": "intu-ec-client",
        "port": 8021
    },
    {
        "protocol": "TCP",
        "name": "oa-system",
        "port": 8022
    },
    {
        "protocol": "TCP",
        "name": "pro-ed",
        "port": 8032
    },
    {
        "protocol": "TCP",
        "name": "mindprint",
        "port": 8033
    },
    {
        "protocol": "TCP",
        "name": "http-alt",
        "port": 8080
    },
    {
        "protocol": "TCP",
        "name": "sunproxyadmin",
        "port": 8081
    },
    {
        "protocol": "TCP",
        "name": "us-cli",
        "port": 8082
    },
    {
        "protocol": "TCP",
        "name": "us-srv",
        "port": 8083
    },
    {
        "protocol": "TCP",
        "name": "radan-http",
        "port": 8088
    },
    {
        "protocol": "TCP",
        "name": "xprint-server",
        "port": 8100
    },
    {
        "protocol": "TCP",
        "name": "mtl8000-matrix",
        "port": 8115
    },
    {
        "protocol": "TCP",
        "name": "cp-cluster",
        "port": 8116
    },
    {
        "protocol": "TCP",
        "name": "privoxy",
        "port": 8118
    },
    {
        "protocol": "TCP",
        "name": "apollo-data",
        "port": 8121
    },
    {
        "protocol": "TCP",
        "name": "apollo-admin",
        "port": 8122
    },
    {
        "protocol": "TCP",
        "name": "paycash-online",
        "port": 8128
    },
    {
        "protocol": "TCP",
        "name": "paycash-wbp",
        "port": 8129
    },
    {
        "protocol": "TCP",
        "name": "indigo-vrmi",
        "port": 8130
    },
    {
        "protocol": "TCP",
        "name": "indigo-vbcp",
        "port": 8131
    },
    {
        "protocol": "TCP",
        "name": "dbabble",
        "port": 8132
    },
    {
        "protocol": "TCP",
        "name": "isdd",
        "port": 8148
    },
    {
        "protocol": "TCP",
        "name": "patrol",
        "port": 8160
    },
    {
        "protocol": "TCP",
        "name": "patrol-snmp",
        "port": 8161
    },
    {
        "protocol": "TCP",
        "name": "vvr-data",
        "port": 8199
    },
    {
        "protocol": "TCP",
        "name": "trivnet1",
        "port": 8200
    },
    {
        "protocol": "TCP",
        "name": "trivnet2",
        "port": 8201
    },
    {
        "protocol": "TCP",
        "name": "lm-perfworks",
        "port": 8204
    },
    {
        "protocol": "TCP",
        "name": "lm-instmgr",
        "port": 8205
    },
    {
        "protocol": "TCP",
        "name": "lm-dta",
        "port": 8206
    },
    {
        "protocol": "TCP",
        "name": "lm-sserver",
        "port": 8207
    },
    {
        "protocol": "TCP",
        "name": "lm-webwatcher",
        "port": 8208
    },
    {
        "protocol": "TCP",
        "name": "rexecj",
        "port": 8230
    },
    {
        "protocol": "TCP",
        "name": "server-find",
        "port": 8351
    },
    {
        "protocol": "TCP",
        "name": "cruise-enum",
        "port": 8376
    },
    {
        "protocol": "TCP",
        "name": "cruise-swroute",
        "port": 8377
    },
    {
        "protocol": "TCP",
        "name": "cruise-config",
        "port": 8378
    },
    {
        "protocol": "TCP",
        "name": "cruise-diags",
        "port": 8379
    },
    {
        "protocol": "TCP",
        "name": "cruise-update",
        "port": 8380
    },
    {
        "protocol": "TCP",
        "name": "m2mservices",
        "port": 8383
    },
    {
        "protocol": "TCP",
        "name": "cvd",
        "port": 8400
    },
    {
        "protocol": "TCP",
        "name": "sabarsd",
        "port": 8401
    },
    {
        "protocol": "TCP",
        "name": "abarsd",
        "port": 8402
    },
    {
        "protocol": "TCP",
        "name": "admind",
        "port": 8403
    },
    {
        "protocol": "TCP",
        "name": "espeech",
        "port": 8416
    },
    {
        "protocol": "TCP",
        "name": "espeech-rtp",
        "port": 8417
    },
    {
        "protocol": "TCP",
        "name": "pcsync-https",
        "port": 8443
    },
    {
        "protocol": "TCP",
        "name": "pcsync-http",
        "port": 8444
    },
    {
        "protocol": "TCP",
        "name": "npmp",
        "port": 8450
    },
    {
        "protocol": "TCP",
        "name": "vp2p",
        "port": 8473
    },
    {
        "protocol": "TCP",
        "name": "noteshare",
        "port": 8474
    },
    {
        "protocol": "TCP",
        "name": "fde",
        "port": 8500
    },
    {
        "protocol": "TCP",
        "name": "rtsp-alt",
        "port": 8554
    },
    {
        "protocol": "TCP",
        "name": "d-fence",
        "port": 8555
    },
    {
        "protocol": "TCP",
        "name": "emware-admin",
        "port": 8567
    },
    {
        "protocol": "TCP",
        "name": "asterix",
        "port": 8600
    },
    {
        "protocol": "TCP",
        "name": "canon-bjnp1",
        "port": 8611
    },
    {
        "protocol": "TCP",
        "name": "canon-bjnp2",
        "port": 8612
    },
    {
        "protocol": "TCP",
        "name": "canon-bjnp3",
        "port": 8613
    },
    {
        "protocol": "TCP",
        "name": "canon-bjnp4",
        "port": 8614
    },
    {
        "protocol": "TCP",
        "name": "sun-as-jmxrmi",
        "port": 8686
    },
    {
        "protocol": "TCP",
        "name": "vnyx",
        "port": 8699
    },
    {
        "protocol": "TCP",
        "name": "ibus",
        "port": 8733
    },
    {
        "protocol": "TCP",
        "name": "mc-appserver",
        "port": 8763
    },
    {
        "protocol": "TCP",
        "name": "openqueue",
        "port": 8764
    },
    {
        "protocol": "TCP",
        "name": "ultraseek-http",
        "port": 8765
    },
    {
        "protocol": "TCP",
        "name": "dpap",
        "port": 8770
    },
    {
        "protocol": "TCP",
        "name": "msgclnt",
        "port": 8786
    },
    {
        "protocol": "TCP",
        "name": "msgsrvr",
        "port": 8787
    },
    {
        "protocol": "TCP",
        "name": "sunwebadmin",
        "port": 8800
    },
    {
        "protocol": "TCP",
        "name": "truecm",
        "port": 8804
    },
    {
        "protocol": "TCP",
        "name": "dxspider",
        "port": 8873
    },
    {
        "protocol": "TCP",
        "name": "cddbp-alt",
        "port": 8880
    },
    {
        "protocol": "TCP",
        "name": "ddi-TCP-1",
        "port": 8888
    },
    {
        "protocol": "TCP",
        "name": "ddi-TCP-2",
        "port": 8889
    },
    {
        "protocol": "TCP",
        "name": "ddi-TCP-3",
        "port": 8890
    },
    {
        "protocol": "TCP",
        "name": "ddi-TCP-4",
        "port": 8891
    },
    {
        "protocol": "TCP",
        "name": "ddi-TCP-5",
        "port": 8892
    },
    {
        "protocol": "TCP",
        "name": "ddi-TCP-6",
        "port": 8893
    },
    {
        "protocol": "TCP",
        "name": "ddi-TCP-7",
        "port": 8894
    },
    {
        "protocol": "TCP",
        "name": "jmb-cds1",
        "port": 8900
    },
    {
        "protocol": "TCP",
        "name": "jmb-cds2",
        "port": 8901
    },
    {
        "protocol": "TCP",
        "name": "manyone-http",
        "port": 8910
    },
    {
        "protocol": "TCP",
        "name": "manyone-xml",
        "port": 8911
    },
    {
        "protocol": "TCP",
        "name": "wcbackup",
        "port": 8912
    },
    {
        "protocol": "TCP",
        "name": "dragonfly",
        "port": 8913
    },
    {
        "protocol": "TCP",
        "name": "cumulus-admin",
        "port": 8954
    },
    {
        "protocol": "TCP",
        "name": "sunwebadmins",
        "port": 8989
    },
    {
        "protocol": "TCP",
        "name": "bctp",
        "port": 8999
    },
    {
        "protocol": "TCP",
        "name": "cslistener",
        "port": 9000
    },
    {
        "protocol": "TCP",
        "name": "etlservicemgr",
        "port": 9001
    },
    {
        "protocol": "TCP",
        "name": "dynamid",
        "port": 9002
    },
    {
        "protocol": "TCP",
        "name": "pichat",
        "port": 9009
    },
    {
        "protocol": "TCP",
        "name": "tambora",
        "port": 9020
    },
    {
        "protocol": "TCP",
        "name": "panagolin-ident",
        "port": 9021
    },
    {
        "protocol": "TCP",
        "name": "paragent",
        "port": 9022
    },
    {
        "protocol": "TCP",
        "name": "swa-1",
        "port": 9023
    },
    {
        "protocol": "TCP",
        "name": "swa-2",
        "port": 9024
    },
    {
        "protocol": "TCP",
        "name": "swa-3",
        "port": 9025
    },
    {
        "protocol": "TCP",
        "name": "swa-4",
        "port": 9026
    },
    {
        "protocol": "TCP",
        "name": "glrpc",
        "port": 9080
    },
    {
        "protocol": "TCP",
        "name": "sqlexec",
        "port": 9088
    },
    {
        "protocol": "TCP",
        "name": "sqlexec-ssl",
        "port": 9089
    },
    {
        "protocol": "TCP",
        "name": "websm",
        "port": 9090
    },
    {
        "protocol": "TCP",
        "name": "xmltec-xmlmail",
        "port": 9091
    },
    {
        "protocol": "TCP",
        "name": "XmlIpcRegSvc",
        "port": 9092
    },
    {
        "protocol": "TCP",
        "name": "hp-pdl-datastr",
        "port": 9100
    },
    {
        "protocol": "TCP",
        "name": "bacula-dir",
        "port": 9101
    },
    {
        "protocol": "TCP",
        "name": "bacula-fd",
        "port": 9102
    },
    {
        "protocol": "TCP",
        "name": "bacula-sd",
        "port": 9103
    },
    {
        "protocol": "TCP",
        "name": "peerwire",
        "port": 9104
    },
    {
        "protocol": "TCP",
        "name": "mxit",
        "port": 9119
    },
    {
        "protocol": "TCP",
        "name": "dddp",
        "port": 9131
    },
    {
        "protocol": "TCP",
        "name": "netlock1",
        "port": 9160
    },
    {
        "protocol": "TCP",
        "name": "netlock2",
        "port": 9161
    },
    {
        "protocol": "TCP",
        "name": "netlock3",
        "port": 9162
    },
    {
        "protocol": "TCP",
        "name": "netlock4",
        "port": 9163
    },
    {
        "protocol": "TCP",
        "name": "netlock5",
        "port": 9164
    },
    {
        "protocol": "TCP",
        "name": "sun-as-jpda",
        "port": 9191
    },
    {
        "protocol": "TCP",
        "name": "wap-wsp",
        "port": 9200
    },
    {
        "protocol": "TCP",
        "name": "wap-wsp-wtp",
        "port": 9201
    },
    {
        "protocol": "TCP",
        "name": "wap-wsp-s",
        "port": 9202
    },
    {
        "protocol": "TCP",
        "name": "wap-wsp-wtp-s",
        "port": 9203
    },
    {
        "protocol": "TCP",
        "name": "wap-vcard",
        "port": 9204
    },
    {
        "protocol": "TCP",
        "name": "wap-vcal",
        "port": 9205
    },
    {
        "protocol": "TCP",
        "name": "wap-vcard-s",
        "port": 9206
    },
    {
        "protocol": "TCP",
        "name": "wap-vcal-s",
        "port": 9207
    },
    {
        "protocol": "TCP",
        "name": "rjcdb-vcards",
        "port": 9208
    },
    {
        "protocol": "TCP",
        "name": "almobile-system",
        "port": 9209
    },
    {
        "protocol": "TCP",
        "name": "oma-mlp",
        "port": 9210
    },
    {
        "protocol": "TCP",
        "name": "oma-mlp-s",
        "port": 9211
    },
    {
        "protocol": "TCP",
        "name": "serverviewdbms",
        "port": 9212
    },
    {
        "protocol": "TCP",
        "name": "serverstart",
        "port": 9213
    },
    {
        "protocol": "TCP",
        "name": "ipdcesgbs",
        "port": 9214
    },
    {
        "protocol": "TCP",
        "name": "insis",
        "port": 9215
    },
    {
        "protocol": "TCP",
        "name": "fsc-port",
        "port": 9217
    },
    {
        "protocol": "TCP",
        "name": "teamcoherence",
        "port": 9222
    },
    {
        "protocol": "TCP",
        "name": "swtp-port1",
        "port": 9281
    },
    {
        "protocol": "TCP",
        "name": "swtp-port2",
        "port": 9282
    },
    {
        "protocol": "TCP",
        "name": "callwaveiam",
        "port": 9283
    },
    {
        "protocol": "TCP",
        "name": "visd",
        "port": 9284
    },
    {
        "protocol": "TCP",
        "name": "n2h2server",
        "port": 9285
    },
    {
        "protocol": "TCP",
        "name": "cumulus",
        "port": 9287
    },
    {
        "protocol": "TCP",
        "name": "armtechdaemon",
        "port": 9292
    },
    {
        "protocol": "TCP",
        "name": "secure-ts",
        "port": 9318
    },
    {
        "protocol": "TCP",
        "name": "guibase",
        "port": 9321
    },
    {
        "protocol": "TCP",
        "name": "mpidcmgr",
        "port": 9343
    },
    {
        "protocol": "TCP",
        "name": "mphlpdmc",
        "port": 9344
    },
    {
        "protocol": "TCP",
        "name": "ctechlicensing",
        "port": 9346
    },
    {
        "protocol": "TCP",
        "name": "fjdmimgr",
        "port": 9374
    },
    {
        "protocol": "TCP",
        "name": "fjinvmgr",
        "port": 9396
    },
    {
        "protocol": "TCP",
        "name": "mpidcagt",
        "port": 9397
    },
    {
        "protocol": "TCP",
        "name": "git",
        "port": 9418
    },
    {
        "protocol": "TCP",
        "name": "ismserver",
        "port": 9500
    },
    {
        "protocol": "TCP",
        "name": "mngsuite",
        "port": 9535
    },
    {
        "protocol": "TCP",
        "name": "trispen-sra",
        "port": 9555
    },
    {
        "protocol": "TCP",
        "name": "ldgateway",
        "port": 9592
    },
    {
        "protocol": "TCP",
        "name": "cba8",
        "port": 9593
    },
    {
        "protocol": "TCP",
        "name": "msgsys",
        "port": 9594
    },
    {
        "protocol": "TCP",
        "name": "pds",
        "port": 9595
    },
    {
        "protocol": "TCP",
        "name": "mercury-disc",
        "port": 9596
    },
    {
        "protocol": "TCP",
        "name": "pd-admin",
        "port": 9597
    },
    {
        "protocol": "TCP",
        "name": "vscp",
        "port": 9598
    },
    {
        "protocol": "TCP",
        "name": "robix",
        "port": 9599
    },
    {
        "protocol": "TCP",
        "name": "micromuse-ncpw",
        "port": 9600
    },
    {
        "protocol": "TCP",
        "name": "streamcomm-ds",
        "port": 9612
    },
    {
        "protocol": "TCP",
        "name": "board-roar",
        "port": 9700
    },
    {
        "protocol": "TCP",
        "name": "l5nas-parchan",
        "port": 9747
    },
    {
        "protocol": "TCP",
        "name": "board-voip",
        "port": 9750
    },
    {
        "protocol": "TCP",
        "name": "rasadv",
        "port": 9753
    },
    {
        "protocol": "TCP",
        "name": "davsrc",
        "port": 9800
    },
    {
        "protocol": "TCP",
        "name": "sstp-2",
        "port": 9801
    },
    {
        "protocol": "TCP",
        "name": "davsrcs",
        "port": 9802
    },
    {
        "protocol": "TCP",
        "name": "sapv1",
        "port": 9875
    },
    {
        "protocol": "TCP",
        "name": "sd",
        "port": 9876
    },
    {
        "protocol": "TCP",
        "name": "cyborg-systems",
        "port": 9888
    },
    {
        "protocol": "TCP",
        "name": "monkeycom",
        "port": 9898
    },
    {
        "protocol": "TCP",
        "name": "sctp-tunneling",
        "port": 9899
    },
    {
        "protocol": "TCP",
        "name": "iua",
        "port": 9900
    },
    {
        "protocol": "TCP",
        "name": "domaintime",
        "port": 9909
    },
    {
        "protocol": "TCP",
        "name": "sype-transport",
        "port": 9911
    },
    {
        "protocol": "TCP",
        "name": "apc-9950",
        "port": 9950
    },
    {
        "protocol": "TCP",
        "name": "apc-9951",
        "port": 9951
    },
    {
        "protocol": "TCP",
        "name": "apc-9952",
        "port": 9952
    },
    {
        "protocol": "TCP",
        "name": "acis",
        "port": 9953
    },
    {
        "protocol": "TCP",
        "name": "osm-appsrvr",
        "port": 9990
    },
    {
        "protocol": "TCP",
        "name": "osm-oev",
        "port": 9991
    },
    {
        "protocol": "TCP",
        "name": "palace-1",
        "port": 9992
    },
    {
        "protocol": "TCP",
        "name": "palace-2",
        "port": 9993
    },
    {
        "protocol": "TCP",
        "name": "palace-3",
        "port": 9994
    },
    {
        "protocol": "TCP",
        "name": "palace-4",
        "port": 9995
    },
    {
        "protocol": "TCP",
        "name": "palace-5",
        "port": 9996
    },
    {
        "protocol": "TCP",
        "name": "palace-6",
        "port": 9997
    },
    {
        "protocol": "TCP",
        "name": "distinct32",
        "port": 9998
    },
    {
        "protocol": "TCP",
        "name": "distinct",
        "port": 9999
    },
    {
        "protocol": "TCP",
        "name": "ndmp",
        "port": 10000
    },
    {
        "protocol": "TCP",
        "name": "scp-config",
        "port": 10001
    },
    {
        "protocol": "TCP",
        "name": "mvs-capacity",
        "port": 10007
    },
    {
        "protocol": "TCP",
        "name": "octopus",
        "port": 10008
    },
    {
        "protocol": "TCP",
        "name": "amanda",
        "port": 10080
    },
    {
        "protocol": "TCP",
        "name": "famdc",
        "port": 10081
    },
    {
        "protocol": "TCP",
        "name": "zabbix-agent",
        "port": 10050
    },
    {
        "protocol": "TCP",
        "name": "zabbix-trapper",
        "port": 10051
    },
    {
        "protocol": "TCP",
        "name": "itap-ddtp",
        "port": 10100
    },
    {
        "protocol": "TCP",
        "name": "ezmeeting-2",
        "port": 10101
    },
    {
        "protocol": "TCP",
        "name": "ezproxy-2",
        "port": 10102
    },
    {
        "protocol": "TCP",
        "name": "ezrelay",
        "port": 10103
    },
    {
        "protocol": "TCP",
        "name": "bctp-server",
        "port": 10107
    },
    {
        "protocol": "TCP",
        "name": "netiq-endpoint",
        "port": 10113
    },
    {
        "protocol": "TCP",
        "name": "netiq-qcheck",
        "port": 10114
    },
    {
        "protocol": "TCP",
        "name": "netiq-endpt",
        "port": 10115
    },
    {
        "protocol": "TCP",
        "name": "netiq-voipa",
        "port": 10116
    },
    {
        "protocol": "TCP",
        "name": "bmc-perf-sd",
        "port": 10128
    },
    {
        "protocol": "TCP",
        "name": "qb-db-server",
        "port": 10160
    },
    {
        "protocol": "TCP",
        "name": "apollo-relay",
        "port": 10252
    },
    {
        "protocol": "TCP",
        "name": "axis-wimp-port",
        "port": 10260
    },
    {
        "protocol": "TCP",
        "name": "blocks",
        "port": 10288
    },
    {
        "protocol": "TCP",
        "name": "lpdg",
        "port": 10805
    },
    {
        "protocol": "TCP",
        "name": "rmiaux",
        "port": 10990
    },
    {
        "protocol": "TCP",
        "name": "irisa",
        "port": 11000
    },
    {
        "protocol": "TCP",
        "name": "metasys",
        "port": 11001
    },
    {
        "protocol": "TCP",
        "name": "vce",
        "port": 11111
    },
    {
        "protocol": "TCP",
        "name": "dicom",
        "port": 11112
    },
    {
        "protocol": "TCP",
        "name": "suncacao-snmp",
        "port": 11161
    },
    {
        "protocol": "TCP",
        "name": "suncacao-jmxmp",
        "port": 11162
    },
    {
        "protocol": "TCP",
        "name": "suncacao-rmi",
        "port": 11163
    },
    {
        "protocol": "TCP",
        "name": "suncacao-csa",
        "port": 11164
    },
    {
        "protocol": "TCP",
        "name": "suncacao-websvc",
        "port": 11165
    },
    {
        "protocol": "TCP",
        "name": "smsqp",
        "port": 11201
    },
    {
        "protocol": "TCP",
        "name": "imip",
        "port": 11319
    },
    {
        "protocol": "TCP",
        "name": "imip-channels",
        "port": 11320
    },
    {
        "protocol": "TCP",
        "name": "arena-server",
        "port": 11321
    },
    {
        "protocol": "TCP",
        "name": "atm-uhas",
        "port": 11367
    },
    {
        "protocol": "TCP",
        "name": "hkp",
        "port": 11371
    },
    {
        "protocol": "TCP",
        "name": "tempest-port",
        "port": 11600
    },
    {
        "protocol": "TCP",
        "name": "h323callsigalt",
        "port": 11720
    },
    {
        "protocol": "TCP",
        "name": "intrepid-ssl",
        "port": 11751
    },
    {
        "protocol": "TCP",
        "name": "sysinfo-sp",
        "port": 11967
    },
    {
        "protocol": "TCP",
        "name": "entextxid",
        "port": 12000
    },
    {
        "protocol": "TCP",
        "name": "entextnetwk",
        "port": 12001
    },
    {
        "protocol": "TCP",
        "name": "entexthigh",
        "port": 12002
    },
    {
        "protocol": "TCP",
        "name": "entextmed",
        "port": 12003
    },
    {
        "protocol": "TCP",
        "name": "entextlow",
        "port": 12004
    },
    {
        "protocol": "TCP",
        "name": "dbisamserver1",
        "port": 12005
    },
    {
        "protocol": "TCP",
        "name": "dbisamserver2",
        "port": 12006
    },
    {
        "protocol": "TCP",
        "name": "accuracer",
        "port": 12007
    },
    {
        "protocol": "TCP",
        "name": "accuracer-dbms",
        "port": 12008
    },
    {
        "protocol": "TCP",
        "name": "vipera",
        "port": 12012
    },
    {
        "protocol": "TCP",
        "name": "rets-ssl",
        "port": 12109
    },
    {
        "protocol": "TCP",
        "name": "nupaper-ss",
        "port": 12121
    },
    {
        "protocol": "TCP",
        "name": "cawas",
        "port": 12168
    },
    {
        "protocol": "TCP",
        "name": "hivep",
        "port": 12172
    },
    {
        "protocol": "TCP",
        "name": "linogridengine",
        "port": 12300
    },
    {
        "protocol": "TCP",
        "name": "warehouse-sss",
        "port": 12321
    },
    {
        "protocol": "TCP",
        "name": "warehouse",
        "port": 12322
    },
    {
        "protocol": "TCP",
        "name": "italk",
        "port": 12345
    },
    {
        "protocol": "TCP",
        "name": "tsaf",
        "port": 12753
    },
    {
        "protocol": "TCP",
        "name": "i-zipqd",
        "port": 13160
    },
    {
        "protocol": "TCP",
        "name": "powwow-client",
        "port": 13223
    },
    {
        "protocol": "TCP",
        "name": "powwow-server",
        "port": 13224
    },
    {
        "protocol": "TCP",
        "name": "bprd",
        "port": 13720
    },
    {
        "protocol": "TCP",
        "name": "bpdbm",
        "port": 13721
    },
    {
        "protocol": "TCP",
        "name": "bpjava-msvc",
        "port": 13722
    },
    {
        "protocol": "TCP",
        "name": "vnetd",
        "port": 13724
    },
    {
        "protocol": "TCP",
        "name": "bpcd",
        "port": 13782
    },
    {
        "protocol": "TCP",
        "name": "vopied",
        "port": 13783
    },
    {
        "protocol": "TCP",
        "name": "nbdb",
        "port": 13785
    },
    {
        "protocol": "TCP",
        "name": "nomdb",
        "port": 13786
    },
    {
        "protocol": "TCP",
        "name": "dsmcc-config",
        "port": 13818
    },
    {
        "protocol": "TCP",
        "name": "dsmcc-session",
        "port": 13819
    },
    {
        "protocol": "TCP",
        "name": "dsmcc-passthru",
        "port": 13820
    },
    {
        "protocol": "TCP",
        "name": "dsmcc-download",
        "port": 13821
    },
    {
        "protocol": "TCP",
        "name": "dsmcc-ccp",
        "port": 13822
    },
    {
        "protocol": "TCP",
        "name": "sua",
        "port": 14001
    },
    {
        "protocol": "TCP",
        "name": "sage-best-com1",
        "port": 14033
    },
    {
        "protocol": "TCP",
        "name": "sage-best-com2",
        "port": 14034
    },
    {
        "protocol": "TCP",
        "name": "vcs-app",
        "port": 14141
    },
    {
        "protocol": "TCP",
        "name": "gcm-app",
        "port": 14145
    },
    {
        "protocol": "TCP",
        "name": "vrts-tdd",
        "port": 14149
    },
    {
        "protocol": "TCP",
        "name": "vad",
        "port": 14154
    },
    {
        "protocol": "TCP",
        "name": "hde-lcesrvr-1",
        "port": 14936
    },
    {
        "protocol": "TCP",
        "name": "hde-lcesrvr-2",
        "port": 14937
    },
    {
        "protocol": "TCP",
        "name": "hydap",
        "port": 15000
    },
    {
        "protocol": "TCP",
        "name": "xpilot",
        "port": 15345
    },
    {
        "protocol": "TCP",
        "name": "ptp",
        "port": 15740
    },
    {
        "protocol": "TCP",
        "name": "3link",
        "port": 15363
    },
    {
        "protocol": "TCP",
        "name": "sun-sea-port",
        "port": 16161
    },
    {
        "protocol": "TCP",
        "name": "etb4j",
        "port": 16309
    },
    {
        "protocol": "TCP",
        "name": "netserialext1",
        "port": 16360
    },
    {
        "protocol": "TCP",
        "name": "netserialext2",
        "port": 16361
    },
    {
        "protocol": "TCP",
        "name": "netserialext3",
        "port": 16367
    },
    {
        "protocol": "TCP",
        "name": "netserialext4",
        "port": 16368
    },
    {
        "protocol": "TCP",
        "name": "connected",
        "port": 16384
    },
    {
        "protocol": "TCP",
        "name": "intel-rci-mp",
        "port": 16991
    },
    {
        "protocol": "TCP",
        "name": "amt-soap-http",
        "port": 16992
    },
    {
        "protocol": "TCP",
        "name": "amt-soap-https",
        "port": 16993
    },
    {
        "protocol": "TCP",
        "name": "amt-redir-TCP",
        "port": 16994
    },
    {
        "protocol": "TCP",
        "name": "amt-redir-tls",
        "port": 16995
    },
    {
        "protocol": "TCP",
        "name": "isode-dua",
        "port": 17007
    },
    {
        "protocol": "TCP",
        "name": "soundsvirtual",
        "port": 17185
    },
    {
        "protocol": "TCP",
        "name": "chipper",
        "port": 17219
    },
    {
        "protocol": "TCP",
        "name": "ssh-mgmt",
        "port": 17235
    },
    {
        "protocol": "TCP",
        "name": "ea",
        "port": 17729
    },
    {
        "protocol": "TCP",
        "name": "zep",
        "port": 17754
    },
    {
        "protocol": "TCP",
        "name": "biimenu",
        "port": 18000
    },
    {
        "protocol": "TCP",
        "name": "opsec-cvp",
        "port": 18181
    },
    {
        "protocol": "TCP",
        "name": "opsec-ufp",
        "port": 18182
    },
    {
        "protocol": "TCP",
        "name": "opsec-sam",
        "port": 18183
    },
    {
        "protocol": "TCP",
        "name": "opsec-lea",
        "port": 18184
    },
    {
        "protocol": "TCP",
        "name": "opsec-omi",
        "port": 18185
    },
    {
        "protocol": "TCP",
        "name": "ohsc",
        "port": 18186
    },
    {
        "protocol": "TCP",
        "name": "opsec-ela",
        "port": 18187
    },
    {
        "protocol": "TCP",
        "name": "checkpoint-rtm",
        "port": 18241
    },
    {
        "protocol": "TCP",
        "name": "ac-cluster",
        "port": 18463
    },
    {
        "protocol": "TCP",
        "name": "ique",
        "port": 18769
    },
    {
        "protocol": "TCP",
        "name": "infotos",
        "port": 18881
    },
    {
        "protocol": "TCP",
        "name": "apc-necmp",
        "port": 18888
    },
    {
        "protocol": "TCP",
        "name": "igrid",
        "port": 19000
    },
    {
        "protocol": "TCP",
        "name": "opsec-uaa",
        "port": 19191
    },
    {
        "protocol": "TCP",
        "name": "ua-secureagent",
        "port": 19194
    },
    {
        "protocol": "TCP",
        "name": "keysrvr",
        "port": 19283
    },
    {
        "protocol": "TCP",
        "name": "keyshadow",
        "port": 19315
    },
    {
        "protocol": "TCP",
        "name": "mtrgtrans",
        "port": 19398
    },
    {
        "protocol": "TCP",
        "name": "hp-sco",
        "port": 19410
    },
    {
        "protocol": "TCP",
        "name": "hp-sca",
        "port": 19411
    },
    {
        "protocol": "TCP",
        "name": "hp-sessmon",
        "port": 19412
    },
    {
        "protocol": "TCP",
        "name": "fxuptp",
        "port": 19539
    },
    {
        "protocol": "TCP",
        "name": "sxuptp",
        "port": 19540
    },
    {
        "protocol": "TCP",
        "name": "jcp",
        "port": 19541
    },
    {
        "protocol": "TCP",
        "name": "dnp",
        "port": 20000
    },
    {
        "protocol": "TCP",
        "name": "microsan",
        "port": 20001
    },
    {
        "protocol": "TCP",
        "name": "commtact-http",
        "port": 20002
    },
    {
        "protocol": "TCP",
        "name": "commtact-https",
        "port": 20003
    },
    {
        "protocol": "TCP",
        "name": "opendeploy",
        "port": 20014
    },
    {
        "protocol": "TCP",
        "name": "nburn_id",
        "port": 20034
    },
    {
        "protocol": "TCP",
        "name": "ipdtp-port",
        "port": 20202
    },
    {
        "protocol": "TCP",
        "name": "ipulse-ics",
        "port": 20222
    },
    {
        "protocol": "TCP",
        "name": "track",
        "port": 20670
    },
    {
        "protocol": "TCP",
        "name": "athand-mmp",
        "port": 20999
    },
    {
        "protocol": "TCP",
        "name": "irtrans",
        "port": 21000
    },
    {
        "protocol": "TCP",
        "name": "vofr-gateway",
        "port": 21590
    },
    {
        "protocol": "TCP",
        "name": "tvpm",
        "port": 21800
    },
    {
        "protocol": "TCP",
        "name": "webphone",
        "port": 21845
    },
    {
        "protocol": "TCP",
        "name": "netspeak-is",
        "port": 21846
    },
    {
        "protocol": "TCP",
        "name": "netspeak-cs",
        "port": 21847
    },
    {
        "protocol": "TCP",
        "name": "netspeak-acd",
        "port": 21848
    },
    {
        "protocol": "TCP",
        "name": "netspeak-cps",
        "port": 21849
    },
    {
        "protocol": "TCP",
        "name": "snapenetio",
        "port": 22000
    },
    {
        "protocol": "TCP",
        "name": "optocontrol",
        "port": 22001
    },
    {
        "protocol": "TCP",
        "name": "wnn6",
        "port": 22273
    },
    {
        "protocol": "TCP",
        "name": "vocaltec-wconf",
        "port": 22555
    },
    {
        "protocol": "TCP",
        "name": "vocaltec-wconf",
        "port": 22793
    },
    {
        "protocol": "TCP",
        "name": "aws-brf",
        "port": 22800
    },
    {
        "protocol": "TCP",
        "name": "brf-gw",
        "port": 22951
    },
    {
        "protocol": "TCP",
        "name": "novar-dbase",
        "port": 23400
    },
    {
        "protocol": "TCP",
        "name": "novar-alarm",
        "port": 23401
    },
    {
        "protocol": "TCP",
        "name": "novar-global",
        "port": 23402
    },
    {
        "protocol": "TCP",
        "name": "med-ltp",
        "port": 24000
    },
    {
        "protocol": "TCP",
        "name": "med-fsp-rx",
        "port": 24001
    },
    {
        "protocol": "TCP",
        "name": "med-fsp-tx",
        "port": 24002
    },
    {
        "protocol": "TCP",
        "name": "med-supp",
        "port": 24003
    },
    {
        "protocol": "TCP",
        "name": "med-ovw",
        "port": 24004
    },
    {
        "protocol": "TCP",
        "name": "med-ci",
        "port": 24005
    },
    {
        "protocol": "TCP",
        "name": "med-net-svc",
        "port": 24006
    },
    {
        "protocol": "TCP",
        "name": "filesphere",
        "port": 24242
    },
    {
        "protocol": "TCP",
        "name": "vista-4gl",
        "port": 24249
    },
    {
        "protocol": "TCP",
        "name": "ild",
        "port": 24321
    },
    {
        "protocol": "TCP",
        "name": "intel_rci",
        "port": 24386
    },
    {
        "protocol": "TCP",
        "name": "binkp",
        "port": 24554
    },
    {
        "protocol": "TCP",
        "name": "flashfiler",
        "port": 24677
    },
    {
        "protocol": "TCP",
        "name": "proactivate",
        "port": 24678
    },
    {
        "protocol": "TCP",
        "name": "snip",
        "port": 24922
    },
    {
        "protocol": "TCP",
        "name": "icl-twobase1",
        "port": 25000
    },
    {
        "protocol": "TCP",
        "name": "icl-twobase2",
        "port": 25001
    },
    {
        "protocol": "TCP",
        "name": "icl-twobase3",
        "port": 25002
    },
    {
        "protocol": "TCP",
        "name": "icl-twobase4",
        "port": 25003
    },
    {
        "protocol": "TCP",
        "name": "icl-twobase5",
        "port": 25004
    },
    {
        "protocol": "TCP",
        "name": "icl-twobase6",
        "port": 25005
    },
    {
        "protocol": "TCP",
        "name": "icl-twobase7",
        "port": 25006
    },
    {
        "protocol": "TCP",
        "name": "icl-twobase8",
        "port": 25007
    },
    {
        "protocol": "TCP",
        "name": "icl-twobase9",
        "port": 25008
    },
    {
        "protocol": "TCP",
        "name": "icl-twobase10",
        "port": 25009
    },
    {
        "protocol": "TCP",
        "name": "vocaltec-hos",
        "port": 25793
    },
    {
        "protocol": "TCP",
        "name": "tasp-net",
        "port": 25900
    },
    {
        "protocol": "TCP",
        "name": "niobserver",
        "port": 25901
    },
    {
        "protocol": "TCP",
        "name": "niprobe",
        "port": 25903
    },
    {
        "protocol": "TCP",
        "name": "quake",
        "port": 26000
    },
    {
        "protocol": "TCP",
        "name": "wnn6-ds",
        "port": 26208
    },
    {
        "protocol": "TCP",
        "name": "ezproxy",
        "port": 26260
    },
    {
        "protocol": "TCP",
        "name": "ezmeeting",
        "port": 26261
    },
    {
        "protocol": "TCP",
        "name": "k3software-svr",
        "port": 26262
    },
    {
        "protocol": "TCP",
        "name": "k3software-cli",
        "port": 26263
    },
    {
        "protocol": "TCP",
        "name": "gserver",
        "port": 26264
    },
    {
        "protocol": "TCP",
        "name": "flex-lm",
        "port": 27000
    },
    {
        "protocol": "TCP",
        "name": "flex-lm",
        "port": 27001
    },
    {
        "protocol": "TCP",
        "name": "flex-lm",
        "port": 27002
    },
    {
        "protocol": "TCP",
        "name": "flex-lm",
        "port": 27003
    },
    {
        "protocol": "TCP",
        "name": "flex-lm",
        "port": 27004
    },
    {
        "protocol": "TCP",
        "name": "flex-lm",
        "port": 27005
    },
    {
        "protocol": "TCP",
        "name": "flex-lm",
        "port": 27006
    },
    {
        "protocol": "TCP",
        "name": "flex-lm",
        "port": 27007
    },
    {
        "protocol": "TCP",
        "name": "flex-lm",
        "port": 27008
    },
    {
        "protocol": "TCP",
        "name": "flex-lm",
        "port": 27009
    },
    {
        "protocol": "TCP",
        "name": "imagepump",
        "port": 27345
    },
    {
        "protocol": "TCP",
        "name": "kopek-httphead",
        "port": 27504
    },
    {
        "protocol": "TCP",
        "name": "ars-vista",
        "port": 27782
    },
    {
        "protocol": "TCP",
        "name": "tw-auth-key",
        "port": 27999
    },
    {
        "protocol": "TCP",
        "name": "nxlmd",
        "port": 28000
    },
    {
        "protocol": "TCP",
        "name": "siemensgsm",
        "port": 28240
    },
    {
        "protocol": "TCP",
        "name": "otmp",
        "port": 29167
    },
    {
        "protocol": "TCP",
        "name": "pago-services1",
        "port": 30001
    },
    {
        "protocol": "TCP",
        "name": "pago-services2",
        "port": 30002
    },
    {
        "protocol": "TCP",
        "name": "xqosd",
        "port": 31416
    },
    {
        "protocol": "TCP",
        "name": "tetrinet",
        "port": 31457
    },
    {
        "protocol": "TCP",
        "name": "lm-mon",
        "port": 31620
    },
    {
        "protocol": "TCP",
        "name": "gamesmith-port",
        "port": 31765
    },
    {
        "protocol": "TCP",
        "name": "t1distproc60",
        "port": 32249
    },
    {
        "protocol": "TCP",
        "name": "apm-link",
        "port": 32483
    },
    {
        "protocol": "TCP",
        "name": "sec-ntb-clnt",
        "port": 32635
    },
    {
        "protocol": "TCP",
        "name": "filenet-tms",
        "port": 32768
    },
    {
        "protocol": "TCP",
        "name": "filenet-rpc",
        "port": 32769
    },
    {
        "protocol": "TCP",
        "name": "filenet-nch",
        "port": 32770
    },
    {
        "protocol": "TCP",
        "name": "filenet-rmi",
        "port": 32771
    },
    {
        "protocol": "TCP",
        "name": "filenet-pa",
        "port": 32772
    },
    {
        "protocol": "TCP",
        "name": "filenet-cm",
        "port": 32773
    },
    {
        "protocol": "TCP",
        "name": "filenet-re",
        "port": 32774
    },
    {
        "protocol": "TCP",
        "name": "filenet-pch",
        "port": 32775
    },
    {
        "protocol": "TCP",
        "name": "idmgratm",
        "port": 32896
    },
    {
        "protocol": "TCP",
        "name": "diamondport",
        "port": 33331
    },
    {
        "protocol": "TCP",
        "name": "traceroute",
        "port": 33434
    },
    {
        "protocol": "TCP",
        "name": "snip-slave",
        "port": 33656
    },
    {
        "protocol": "TCP",
        "name": "turbonote-2",
        "port": 34249
    },
    {
        "protocol": "TCP",
        "name": "p-net-local",
        "port": 34378
    },
    {
        "protocol": "TCP",
        "name": "p-net-remote",
        "port": 34379
    },
    {
        "protocol": "TCP",
        "name": "profinet-rt",
        "port": 34962
    },
    {
        "protocol": "TCP",
        "name": "profinet-rtm",
        "port": 34963
    },
    {
        "protocol": "TCP",
        "name": "profinet-cm",
        "port": 34964
    },
    {
        "protocol": "TCP",
        "name": "ethercat",
        "port": 34980
    },
    {
        "protocol": "TCP",
        "name": "kastenxpipe",
        "port": 36865
    },
    {
        "protocol": "TCP",
        "name": "neckar",
        "port": 37475
    },
    {
        "protocol": "TCP",
        "name": "unisys-eportal",
        "port": 37654
    },
    {
        "protocol": "TCP",
        "name": "galaxy7-data",
        "port": 38201
    },
    {
        "protocol": "TCP",
        "name": "fairview",
        "port": 38202
    },
    {
        "protocol": "TCP",
        "name": "agpolicy",
        "port": 38203
    },
    {
        "protocol": "TCP",
        "name": "turbonote-1",
        "port": 39681
    },
    {
        "protocol": "TCP",
        "name": "cscp",
        "port": 40841
    },
    {
        "protocol": "TCP",
        "name": "csccredir",
        "port": 40842
    },
    {
        "protocol": "TCP",
        "name": "csccfirewall",
        "port": 40843
    },
    {
        "protocol": "TCP",
        "name": "fs-qos",
        "port": 41111
    },
    {
        "protocol": "TCP",
        "name": "crestron-cip",
        "port": 41794
    },
    {
        "protocol": "TCP",
        "name": "crestron-ctp",
        "port": 41795
    },
    {
        "protocol": "TCP",
        "name": "candp",
        "port": 42508
    },
    {
        "protocol": "TCP",
        "name": "candrp",
        "port": 42509
    },
    {
        "protocol": "TCP",
        "name": "caerpc",
        "port": 42510
    },
    {
        "protocol": "TCP",
        "name": "reachout",
        "port": 43188
    },
    {
        "protocol": "TCP",
        "name": "ndm-agent-port",
        "port": 43189
    },
    {
        "protocol": "TCP",
        "name": "ip-provision",
        "port": 43190
    },
    {
        "protocol": "TCP",
        "name": "ciscocsdb",
        "port": 43441
    },
    {
        "protocol": "TCP",
        "name": "pmcd",
        "port": 44321
    },
    {
        "protocol": "TCP",
        "name": "pmcdproxy",
        "port": 44322
    },
    {
        "protocol": "TCP",
        "name": "rbr-debug",
        "port": 44553
    },
    {
        "protocol": "TCP",
        "name": "rockwell-encap",
        "port": 44818
    },
    {
        "protocol": "TCP",
        "name": "invision-ag",
        "port": 45054
    },
    {
        "protocol": "TCP",
        "name": "eba",
        "port": 45678
    },
    {
        "protocol": "TCP",
        "name": "ssr-servermgr",
        "port": 45966
    },
    {
        "protocol": "TCP",
        "name": "mediabox",
        "port": 46999
    },
    {
        "protocol": "TCP",
        "name": "mbus",
        "port": 47000
    },
    {
        "protocol": "TCP",
        "name": "dbbrowse",
        "port": 47557
    },
    {
        "protocol": "TCP",
        "name": "directplaysrvr",
        "port": 47624
    },
    {
        "protocol": "TCP",
        "name": "ap",
        "port": 47806
    },
    {
        "protocol": "TCP",
        "name": "bacnet",
        "port": 47808
    },
    {
        "protocol": "TCP",
        "name": "nimcontroller",
        "port": 48000
    },
    {
        "protocol": "TCP",
        "name": "nimspooler",
        "port": 48001
    },
    {
        "protocol": "TCP",
        "name": "nimhub",
        "port": 48002
    },
    {
        "protocol": "TCP",
        "name": "nimgtw",
        "port": 48003
    },
    {
        "protocol": "TCP",
        "name": "com-bardac-dw",
        "port": 48556
    },
    {
        "protocol": "TCP",
        "name": "iqobject",
        "port": 48619
    }
]