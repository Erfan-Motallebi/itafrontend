export let protocolChartOptions = {
    plotOptions: {
        pie: {
            expandOnClick: false
        }
    },
    series: [44, 55, 13, 33],
    chart: {
        width: 380,
        grid: {
            padding: {
                bottom: -20,
                left: -10,
            }
        }
    },
    stroke: {
        show: false,
    },
    labels: ['tcp', 'udp', 'icmp'], // this will change on map data
    colors: ['var(--success)', 'var(--info)', 'var(--purple)'],
    dataLabels: {
        enabled: false
    },
    fill: {
        type: 'gradient',
        gradient: {
            shade: 'diagonal2',
            shadeIntensity: 1,
            inverseColors: true,
            opacityFrom: 1,
            opacityTo: 1,
            gradientToColors: ['var(--success-dark-20)', 'var(--info-dark-20)', 'var(--purple-dark-20)'],
            stops: [0, 25, 50, 75, 100]
        },
    },
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                show: false
            }
        }
    }],
    legend: {
        showForSingleSeries: true,
        markers: {
            // width: 7,
            // height: 7,
        },
        // fontSize: "10px",
        enables: true,
        position: 'bottom',
        offsetY: -10,
        formatter: (seriesName: string, opts: any) => {
            return `${seriesName.toUpperCase()}<br>${(opts.w.globals.series[opts.seriesIndex] > 100 ? 100 : opts.w.globals.series[opts.seriesIndex] - 2).toFixed(2)} % `
        }
    },
    tooltip: {
        theme: 'dark',
        fillSeriesColor: false,
        y: {
            formatter: (value: number) => {
                return `${(value > 100 ? 100 : value - 2).toFixed(2)} %`
            }
        }
        // custom: function ({ series, seriesIndex, dataPointIndex, w }: any) {
        //   console.log(series)
        //   console.log(seriesIndex)
        //   console.log(dataPointIndex)
        //   console.log(w)
        //   return `${series[seriesIndex][dataPointIndex]}%`
        //   // return `${(seriesName == 'ICMP') ? (opts.w.globals.series[opts.seriesIndex] - 2).toFixed(2) : opts.w.globals.series[opts.seriesIndex]} % `
        // }
    }
}

export let radialChartOptions = {
    chart: {
        // offsetY: -20,
        type: 'radialBar',
        toolbar: {
            show: false,
        },
        sparkline: {
            enabled: true,
        }
    },
    grid: {
        show: false,
        padding: {
            // bottom: 50,
        }
    },
    colors: [
        'var(--danger)',
        'var(--info)',
        'var(--warning)',
        'var(--primary)',
        'var(--purple)',
    ],
    labels: ["60\<x\<128", "128\<x\<256", "256\<x\<512", "512\<x\<1024", "1024<"],
    legend: {
        show: true,
        floating: false,
        fontSize: '11px',
        position: 'bottom',
        // offsetY: -50,
        padding: '4px',
        labels: {
            useSeriesColors: true,
        },
        markers: {
            width: 8,
            height: 3,
        },
        itemMargin: {
            vertical: 0
        },
        formatter: (r: string, opt: any) => `${r.replaceAll('<', '&lt')} ${opt.w.globals.series[opt.seriesIndex]}%`
    },
    plotOptions: {
        radialBar: {
            dataLabels: {
                name: {
                    show: true,
                    offsetY: 0,
                },
                value: {
                    show: true,
                    offsetY: 10,
                    fontSize: '14px',
                },
                total: {
                    show: true,
                    fontSize: '10px',
                    color: 'var(--text-color)',
                    label: 'Hover Over',
                    formatter: (s: number) => '%'
                },
            },
            startAngle: -110,
            endAngle: 110,
            hollow: {
                margin: 2,
                size: '25%',
                position: 'front',
                dropShadow: {
                    enabled: true,
                    top: 3,
                    left: 0,
                    blur: 4,
                    opacity: 0.24
                }
            },
            track: {
                opacity: 0.5,
                strokeWidth: '50%',
                margin: 5, // margin is in pixels
                // dropShadow: {
                //   enabled: true,
                //   top: -3,
                //   left: 0,
                //   blur: 4,
                //   opacity: 0.35,
                //   inset: true,
                // }
            },
        },
    },
    fill: {
        type: 'gradient',
        gradient: {
            shade: 'diagonal1',
            shadeIntensity: 5,
            inverseColors: false,
            opacityFrom: 1,
            opacityTo: 1,
            gradientToColors: [
                'var(--danger-dark-20)',
                'var(--info-dark-20)',
                'var(--warning-dark-20)',
                'var(--primary-dark-20)',
                'var(--purple-dark-10)',
            ],
            stops: [0, 900]
        },
    },
    stroke: {
        lineCap: 'round'
    },
    // labels: [],
}
export let barChartOptions = {
    chart: {
        height: 200,
        type: 'bar',
        toolbar: {
            show: false
        }
    },
    grid: {
        show: false,
        padding: {
            bottom: -20,
            top: -30,
            left: 0,
        }
    },
    plotOptions: {
        bar: {
            barHeight: '40%',
            horizontal: true,
            borderRadius: 5,
            dataLabels: {
                position: 'top', // top, center, bottom
            },
        }
    },
    colors: ['var(--secondary)'],
    dataLabels: {
        enabled: true,
        formatter: function (val: number) {
            return `${val}% `
        },
        offsetX: -20,
        style: {
            fontSize: '12px',
            color: '#3e3e3e'
        }
    },
    xaxis: {
        categories: [],
        axisBorder: {
            show: false
        },
        axisTicks: {
            show: false,
        },
        labels: {
            show: false,
        }
    },
    fill: {
        type: 'gradient',
        gradient: {
            shade: 'diagonal1',
            shadeIntensity: 5,
            inverseColors: false,
            opacityFrom: 1,
            opacityTo: 1,
            gradientToColors: [
                'var(--secondary-dark-20)',
            ],
            stops: [0, 900]
        },
    },
    yaxis: {
        position: 'left',
        offsetX: -20,
        axisBorder: {
            show: false
        },
        axisTicks: {
            show: false
        },
        tooltip: {
            enabled: false,
        }
    },
    tooltip: {
        fixed: {
            enabled: true,
            position: 'topRight',
            // offsetX: 40,
            // offsetY: 40,
        },
        custom: function ({ series, seriesIndex, dataPointIndex, w }: any) {
            let categories = ["less than 60", "60≤128", "128≤256", "256≤512", "512≤1024", "greater than 1024"]
            return `<div class="tooltipBox p-5">
  ${series[seriesIndex][dataPointIndex]}% of requests are from ${w.globals.labels[dataPointIndex]}
</div>`
        },
    },
}

export let statisticMapChart = {
    chart: {
        type: 'radialBar',
        toolbar: {
            show: false,
        },
        sparkline: {
            enabled: true
        }
    },
    grid: {
        show: false,
        padding: {
            bottom: 60,
        }
    },

    // labels: ["60\<x\<128", "128\<x\<256", "256\<x\<512", "512\<x\<1024", "1024<"],
    legend: {
        show: true,
        floating: true,
        // fontSize: '13px',
        position: 'bottom',
        // offsetY: 5,
        labels: {
            useSeriesColors: true,
        },
        markers: {
            width: 7,
            height: 7,
        },
        itemMargin: {
            vertical: 0
        },
        formatter: (r: string) => r.replaceAll('<', '&lt')
    },
    plotOptions: {
        radialBar: {
            dataLabels: {
                name: {
                    show: true,
                    offsetY: 0,
                },
                value: {
                    show: true,
                    offsetY: 10,
                    fontSize: '15px',
                },
                total: {
                    show: true,
                    fontSize: '12px',
                    color: 'var(--white)',
                    label: '',
                    formatter: (s: number) => ''
                },
            },
            startAngle: -90,
            endAngle: 90,
            hollow: {
                margin: 2,
                size: '3px',
                position: 'front',
                dropShadow: {
                    enabled: true,
                    top: 3,
                    left: 0,
                    blur: 4,
                    opacity: 0.24
                }
            },
            track: {
                opacity: 0.1,
                strokeWidth: '50%',
                margin: 5, // margin is in pixels
                // dropShadow: {
                //   enabled: true,
                //   top: -3,
                //   left: 0,
                //   blur: 4,
                //   opacity: 0.35,
                //   inset: true,
                // }
            },
        },
    },
    colors: [
        'var(--warning)',
        'var(--danger)',
        'var(--info)',
        'var(--primary)',
        'var(--purple)',
    ],
    fill: {
        size: '50%',
        type: 'gradient',
        gradient: {
            shade: 'diagonal1',
            shadeIntensity: 5,
            inverseColors: false,
            opacityFrom: 1,
            opacityTo: 1,
            gradientToColors: [
                'var(--warning-dark-20)',
                'var(--danger-dark-20)',
                'var(--info-dark-20)',
                'var(--primary-dark-20)',
                'var(--purple-dark-10)',
            ],
            stops: [0, 70]
        },
    },
    stroke: {
        lineCap: 'round'
    },
    // labels: [],
}
export let miniRadialChart = {
    chart: {
        type: 'radialBar',
        toolbar: {
            show: false,
        },
        sparkline: {
            enabled: true,
        }
    },
    grid: {
        show: false,
        padding: {
            bottom: 50,
        }
    },

    // labels: ["60\<x\<128", "128\<x\<256", "256\<x\<512", "512\<x\<1024", "1024<"],
    legend: {
        show: true,
        floating: true,
        // fontSize: '13px',
        position: 'bottom',
        offsetY: -5,
        labels: {
            useSeriesColors: true,
        },
        markers: {
            width: 7,
            height: 7,
        },
    },
    plotOptions: {
        radialBar: {
            dataLabels: {
                name: {
                    show: true,
                    offsetY: 0,
                },
                value: {
                    show: true,
                    offsetY: -10,
                    fontSize: '12px',
                },
                total: {
                    show: true,
                    fontSize: '12px',
                    color: 'var(--white)',
                    label: '',
                    formatter: (s: any) => `${s.globals.series[0]}%`
                },
            },
            hollow: {
                margin: 2,
                size: '40%',
                position: 'front',
                dropShadow: {
                    enabled: true,
                    top: 3,
                    left: 0,
                    blur: 4,
                    opacity: 0.24
                }
            },
            track: {
                opacity: 0.8,
                strokeWidth: '30%',
                margin: 5, // margin is in pixels
                // dropShadow: {
                //   enabled: true,
                //   top: -3,
                //   left: 0,
                //   blur: 4,
                //   opacity: 0.35,
                //   inset: true,
                // }
            },
        },
    },
    colors: [
        'var(--danger)',
        'var(--info)',
        'var(--warning)',
        'var(--primary)',
        'var(--purple)',
        'var(--info)',
    ],
    fill: {
        size: '30%',
        type: 'gradient',
        gradient: {
            shade: 'diagonal1',
            shadeIntensity: 5,
            inverseColors: false,
            opacityFrom: 1,
            opacityTo: 1,
            gradientToColors: [
                'var(--danger-dark-20)',
                'var(--info-dark-20)',
                'var(--warning-dark-20)',
                'var(--primary-dark-20)',
                'var(--purple-dark-10)',
                'var(--secondary-dark-30)',
            ],
            stops: [0, 900]
        },
    },
    stroke: {
        lineCap: 'round'
    },
    // labels: [],
}

export let radarChart = {
    series: [],
    chart: {
        height: 800,
        type: 'polarArea'
    },
    labels: [],
    stroke: {
        width: 0,
        colors: undefined
    },
    yaxis: {
        show: false
    },
    tooltip: { enabled: false },
    legend: {
        position: 'bottom',
        formatter: (seriesName: string, opts: any) => {
            return `${seriesName.toUpperCase()}<br>${(opts.w.globals.series[opts.seriesIndex] > 100 ? 100 : opts.w.globals.series[opts.seriesIndex] - 20).toFixed(2)} % `
        }
    },
    plotOptions: {
        polarArea: {
            rings: {
                strokeWidth: 0
            },
            spokes: {
                strokeWidth: 0
            },
        }
    },
    colors: ['var(--success)', 'var(--info)', 'var(--purple)'],
    fill: {
        opacity: 1,
    },
    // theme: {
    //     monochrome: {
    //         enabled: true,
    //         shadeTo: 'dark',
    //         shadeIntensity: 0.6
    //     }
    // }
};
export let donotocharte = {
    chart: {
        height: 400,
        grid: {
            padding: {
                bottom: -20,
                left: 0,
            }
        }
    },
    stroke: {
        show: false,
    },
    series: [],
    plotOptions: {
        pie: {
            expandOnClick: false,
        }
    },
    dataLabels: {
        enabled: true,
        formatter: function (val: number, opt: any, i: any) {
            return `${(opt.w.globals.series[opt.seriesIndex] - 10.0).toFixed(2)}%`
        },
        style: {
            colors: ['#2468fd', '#ff257b', '#fe6b01'],
            fontSize: '13px',
            fontFamily: 'IranYekan',
        },
        background: {
            enabled: true,
            padding: 10,
            borderRadius: 15,
            borderWidth: 1,
            borderColor: 'transparent',
            opacity: 0.7,
            dropShadow: {
                enabled: true,
                bottom: 1,
                right: 1,
                blur: 5,
                color: '#000',
                opacity: 0.4
            }
        },
    },
    tooltip: {
        enabled: false,
    },
    legend: {
        position: 'bottom',
        show: true,
        floating: false,
        formatter: (r: any) => r.toUpperCase(),
        markers: {
            width: 15,
            height: 2,
            radius: 12,
        },
    },
    colors: ['#2468fd', '#ff257b', '#fe6b01'],
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
        }
    }]
}