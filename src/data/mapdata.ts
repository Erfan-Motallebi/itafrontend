
mapboxgl.accessToken = 'pk.eyJ1IjoibmliYWRub3RlIiwiYSI6ImNqbzEyb29pMTA2b3YzcG11emZieXI5dTcifQ.3k3SpWODbHFC4nqLNsex4A';
const map = new mapboxgl.Map({
    container: 'map',
    // Choose from Mapbox's core styles, or make your own style with Mapbox Studio,
    style: 'mapbox://styles/mapbox/dark-v11',
    center: [-96, 37.8],
    zoom: 3,
    pitch: 40
});

// San Francisco
const origin = [-122.414, 37.776];

// Washington DC
const destination = [-77.032, 38.913];

// A simple line from origin to destination.
const route = {
    'type': 'FeatureCollection',
    'features': [
        {
            'type': 'Feature',
            'geometry': {
                'type': 'LineString',
                'coordinates': [origin, destination]
            }
        }
    ]
};

// A single point that animates along the route.
// Coordinates are initially set to origin.
const point = {
    'type': 'FeatureCollection',
    'features': [
        {
            'type': 'Feature',
            'properties': {},
            'geometry': {
                'type': 'Point',
                'coordinates': origin
            }
        }
    ]
};

// Calculate the distance in kilometers between route start/end point.
const lineDistance = turf.length(route.features[0]);

const arc = [];

// Number of steps to use in the arc and animation, more steps means
// a smoother arc and animation, but too many steps will result in a
// low frame rate
const steps = 100;

// Draw an arc between the `origin` & `destination` of the two points
for (let i = 0; i < lineDistance; i += lineDistance / steps) {
    const segment = turf.along(route.features[0], i);
    arc.push(segment.geometry.coordinates);
}

// Update the route with calculated arc coordinates
route.features[0].geometry.coordinates = arc;

// Used to increment the value of the point measurement against the route.
let counter = 0;

map.on('load', () => {
    // Add a source and layer displaying a point which will be animated in a circle.
    map.addSource('route', {
        'type': 'geojson',
        'data': route
    });

    map.addSource('point', {
        'type': 'geojson',
        'data': point
    });

    map.addLayer({
        'id': 'line-animation',
        'type': 'line',
        'source': {
            'type': 'geojson',
            'data': route
        },
        'layout': {
            'line-cap': 'round',
            'line-join': 'round'
        },
        'paint': {
            'line-color': '#ffffff',
            'line-width': 2
        }
    });


    function animate() {
        if (animationCounter < lineCoordinates.length) {
            geojson.features[0].geometry.coordinates.push(lineCoordinates[animationCounter]);
            map.getSource('line-animation').setData(geojson);

            requestAnimationFrame(animateLine);
            animationCounter++;
        } else {
            var coord = geojson.features[0].geometry.coordinates;
            coord.shift();
            console.log(coord);

            if (coord.length > 0) {
                geojson.features[0].geometry.coordinates = coord;
                map.getSource('route').setData(geojson);

          //-------------- Point2 Animation End ---------------
          /* requestAnimationFrame(animateLine) */;
            }
        }
    }

    document.getElementById('replay').addEventListener('click', () => {
        // Set the coordinates of the original point back to origin
        point.features[0].geometry.coordinates = origin;

        // Update the source layer
        map.getSource('point').setData(point);

        // Reset the counter
        counter = 0;

        // Restart the animation
        animate(counter);
    });

    // Start the animation
    animate(counter);
});
