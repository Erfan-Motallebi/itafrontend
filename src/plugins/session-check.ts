import axios from 'axios'
import { definePlugin } from '/@src/app'
import { useUserSession } from '/@src/stores/userSession'
import { AccessRoles } from '/@src/models/users'

/**
 * Here we are setting up three router navigation guards
 * (note that we can have multiple guards in multiple plugins)
 *
 * We can add meta to pages either by declaring them manualy in the
 * routes declaration (see /@src/router.ts)
 * or by adding a <route> tag into .vue files (see /@src/pages/sidebar/dashboards.ts)
 *
 * <route lang="yaml">
 * meta:
 *   requiresAuth: true
 *   neededAccess: AccessRole.Dashboard
 * </route>
 *
 * <script setup lang="ts">
 *  // TS script
 * </script>
 *
 * <template>
 *  // HTML content
 * </template>
 */
export default definePlugin(async ({ router, api, pinia }) => {
  const userSession = useUserSession(pinia)

  // 1. Check token validity at app startup
  if (userSession.isLoggedIn) {
    try {
      // Do api request call to retreive user profile.
      const { data: user } = await axios.get(`${import.meta.env.VITE_API_BASE_URL}/auth/`, { headers: { Authorization: `bearer ${userSession.token}` } })
      userSession.setUser(user)
    } catch (err) {
      // delete stored token if it fails
      userSession.logoutUser()
    }
  }

  router.beforeEach(async (to) => {
    if (to.meta.requiresAuth && !userSession.isLoggedIn) {
      // 2. If the page requires auth, check if user is logged in
      // if not, redirect to login page.
      return {
        name: '/auth/login',
        // save the location we were at to come back later
        query: { redirect: to.fullPath },
      }
    }
    // if (to.meta.requiresAuth && userSession.user?.changePass)
    //   return {
    //     name: '/auth/setPassword',
    //     // save the location we were at to come back later
    //     query: { redirect: to.fullPath },
    //   }

    // @ts-ignore
    let neededAccess: AccessRoles[] = to.meta.neededAccess?.toString()?.split(',')
    if (neededAccess) {
      const { data: user } = await axios.get(`${import.meta.env.VITE_API_BASE_URL}/auth/`, { headers: { Authorization: `bearer ${userSession.token}` } })
      const userRoles: AccessRoles[] = user.roles
      // 3. If the page requires some special access, and user doesn't have that access
      // if not, redirect to login page.
      if (!neededAccess.find(r => userRoles.includes(r) && !userSession.user?.roles?.includes('all')))
        return {
          path: '/404'
        }
    }
  })
})
