export class Filters {
  constructor(f?: Filters) {
    this.ases = f?.ases || []
    this.isps = f?.isps || []
    this.ips = f?.ips || []
  }

  ases?: Number[];
  isps?: String[];
  ips?: String[];
}
export class User {
  constructor(params: User) {
    this.username = params.username;
    this._id = params._id || params.id;
    this.image = params.image && !params.image.includes(import.meta.env.VITE_API_BASE_URL) ? `${import.meta.env.VITE_API_BASE_URL}/${params.image}` : params.image;
    this.first_name = params.first_name;
    this.last_name = params.last_name;
    this.name = `${this.first_name} ${this.last_name}`;
    this.position = params.position;
    this.lastLogin = params.lastLogin;
    this.roles = params.roles || ["gen"];
    this.excludedWidgets = params.excludedWidgets || [];
    this.filters = new Filters(params.filters) || new Filters();
    this.createdAt = params.createdAt;
  }
  selected?: boolean;
  password?: string | undefined;
  username?: string;
  _id?: string;
  id?: string;
  first_name?: string;
  last_name?: string;
  image?: string | null;
  name?: string;
  position?: string;
  roles?: string[];
  excludedWidgets?: excludedWidgetsEnum[];
  filters: Filters;
  createdAt?: string;
  lastLogin?: string;
  last_activity?: string;
  changePass?: boolean
}
export interface errorState {
  username?: string;
  first_name?: string;
  last_name?: string;
  password?: string;
  mobile?: string;
}

export enum AccessRoles {
  All = "all",
  NetworkVisibility = "nv",
  ApplicationVisibility = "av",
  UserControl = "uc",
  General = "gen",
}

export enum excludedWidgetsEnum {
  // Panes
  Countries_Pane,
  // Filters buttons and panel
  Filters_Panel,
  // Filters fields
  Zone_Filter,
  Country_Filter,
  Source_AS_Filter,
  Destination_AS_Filter,
  Source_ISP_Filter,
  Destination_ISP_Filter,
  Protocol_Filter,
  Packet_Size_Filter,
  TCP_Flags_Filter,
  Application_Layer_Filter,
  // Widgets
  Top_Protocols,
  Top_Zones,
  Top_Source_ISP,
  Top_Destination_ISP,
  Top_Source_AS,
  Top_Destination_AS,
  // TCP Widgets
  TCP_Flags,
  TCP_Packet_Size,
  TCP_Source_IP,
  TCP_Destination_IP,
  TCP_Source_Port,
  TCP_Destination_Port,
  // UDP Widgets
  UDP_Packet_Size,
  UDP_Source_IP,
  UDP_Destination_IP,
  UDP_Source_Port,
  UDP_Destination_Port,
  // ICMP Widgets
  ICMP_Packet_Size,
  ICMP_Source_IP,
  ICMP_Destination_IP,
  // Tabs
  TCP_Tab,
  UDP_Tab,
  ICMP_Tab,

  // Application Layer
  Top_Games,
  Top_Services,
  Top_Websites,
}