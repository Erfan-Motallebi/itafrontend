import axios, { AxiosInstance } from "axios"
import { type App } from "vue"
import { definePlugin } from "./app";

interface IAxiosOptions {
    baseUrl: string,
    token: string
}

declare module '@vue/runtime-core' {
    interface ComponentCustomProperties {
        $axios: AxiosInstance;
    }
}

export default {
    install: (app: App, options: Partial<IAxiosOptions>) => {
        app.config.globalProperties.$axios = axios.create({
            baseURL: options.baseUrl,
            headers: {
                "Authorization": options.token ? `Bearer ${options.token}` : ''
            }
        })
    }
}
