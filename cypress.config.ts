import { defineConfig } from 'cypress'

export default defineConfig({
  projectId: 'ita',
  e2e: {
    baseUrl: 'http://localhost:80',
    video: false,
    chromeWebSecurity: false,
    retries: {
      runMode: 2,
      openMode: 0,
    },
    viewportWidth: 1274,
    viewportHeight: 714,
    execTimeout: 5000,
    taskTimeout: 5000,
    responseTimeout: 10000,
    numTestsKeptInMemory: 0,
    excludeSpecPattern: ['*/**/support'],
  },

  component: {
    devServer: {
      framework: 'vue',
      bundler: 'vite',
    },
  },
})
